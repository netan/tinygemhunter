﻿using UnityEngine;
using System.Collections;
using ArabicSupport;

public class CollectionItemSet : MonoBehaviour {

	public int index;

	public int descriptionIndex;
	public string reward = "";

	public int itemCount;
	public GameObject Odd;
	public GameObject[] OddItem;
	public UILabel[] OddItemCount;
	public GameObject Even;
	public GameObject[] EvenItem;
	public UILabel[] EvenItemCount;
	public UILabel title;
	public int titleIndex;
	public string[] name = new string[6];

	public UILabel descriptionLabel;
	public UILabel countLabel;

	public LoadCSV data;

	private Font[] font = new Font[4];

	private bool tashkeel = true;
	private bool hinduNumbers = true;

	// Use this for initialization
	void Start () {
		font[0] = Resources.Load("Font/JF Flat regular", typeof(Font)) as Font;
		font[1] = Resources.Load("Font/ZN", typeof(Font)) as Font;
		font[2] = Resources.Load("Font/JP", typeof(Font)) as Font;
		font[3] = Resources.Load("Font/CN", typeof(Font)) as Font;
	}
	public void SetPreload() {
		data = GameObject.Find ("EventControoler").GetComponent<LoadCSV> ();
		itemCount = data.countSeriesNumber (index.ToString ());   //先算出有幾個item table_series number..

		SetTitle();
		SetDescription();

		int j = 0;
		for (int i = 0; i < data.array_ItemTable_SeriesNumber.Count; i++) {
			if (data.array_ItemTable_SeriesNumber[i] == index.ToString()) {
				name[j] = "item0" + data.array_ItemTable_Name[i].Substring(3);
				j++;
			}
		}
		SetItem (name);

		//set sprite color
		for (int i = 0; i < OddItemCount.Length; i++) {
			if (OddItemCount[i].text == "0") {
				OddItem[i].GetComponent<UISprite>().color = new Color(50f / 255f, 50f / 255f, 50f / 255f, 1);
			}
		}
		for (int i = 0; i < EvenItemCount.Length; i++) {
			if (EvenItemCount[i].text == "0") {
				EvenItem[i].GetComponent<UISprite>().color = new Color(50f / 255f, 50f / 255f, 50f / 255f, 1);
			}
		}
	}

	public void SetTitle() {
		string titleID = "";
		for (int i = 0; i < data.array_ItemTable_SeriesNumber.Count - 1; i++) {
			if (data.array_ItemTable_SeriesNumber[i] == index.ToString()) {
				titleID = data.array_ItemTable_SeriesName[i];
			}
		}
		titleIndex = data.getIndexByID(titleID);
	}

	public void SetDescription() {
		int first = FindFirstIndex ();
		int last = FindLastIndex ();

		//Debug.Log (data.array_ItemTable_RewardItem01 [first]);
		if (CheckItemCount (3, first, last)) {
			if (data.array_ItemTable_RewardItem01[first] != "解鎖") {
				descriptionLabel.text = "";
				countLabel.text = "3/3";
			} else {
				countLabel.text = "1/1";
			}
		} else if (CheckItemCount (2, first, last)) {
			if (data.array_ItemTable_RewardItem01[first] != "解鎖") {
				GetReward(first, 3);
				countLabel.text = "2/3";
			} else {
				countLabel.text = "1/1";
			}
		} else if (CheckItemCount (1, first, last)) {
			if (data.array_ItemTable_RewardItem01[first] != "解鎖") {
				GetReward(first, 2);
				countLabel.text = "1/3";
			} else {
				countLabel.text = "1/1";
			}
		} else {
			if (data.array_ItemTable_RewardItem01[first] != "解鎖") {
				GetReward(first, 1);
				countLabel.text = "0/3";
			} else {
				countLabel.text = "0/1";
				reward = data.array_ItemTable_RewardContent01[first];
				descriptionIndex = data.getIndexByID("10090");

			}

		}
	}

	public bool CheckItemCount(int i, int first, int last) {
		bool check = true;
		for (int j = first; j <= last; j++) {
			string tmp = "item0" + string.Format("{0:00}", j + 1);
			//PlayerPrefs.SetInt(tmp, Random.Range(0,5));
			if (PlayerPrefs.GetInt(tmp) < i) {
				check = false;
				break;
			}
		}
		return check;
	}

	public void GetReward(int idx, int level) {
		string tmp = "";

		switch (level) {
		case 1:
			tmp = data.array_ItemTable_RewardItem01[idx];
			reward = data.array_ItemTable_RewardContent01[idx];
			break;
		case 2:
			tmp = data.array_ItemTable_RewardItem02[idx];
			reward = data.array_ItemTable_RewardContent02[idx];
			break;
		case 3:
			tmp = data.array_ItemTable_RewardItem03[idx];
			reward = data.array_ItemTable_RewardContent03[idx];
			break;
		}
		if (tmp == "挑戰關卡秒數") {
			descriptionIndex = data.getIndexByID("10062");
		} else if (tmp == "紅寶石分數") {
			descriptionIndex = data.getIndexByID("10063");
		} else if (tmp == "藍寶石分數") {
			descriptionIndex = data.getIndexByID("10064");
		} else if (tmp == "黃寶石分數") {
			descriptionIndex = data.getIndexByID("10065");
		} else if (tmp == "白寶石分數") {
			descriptionIndex = data.getIndexByID("10066");
		} else if (tmp == "綠寶石分數") {
			descriptionIndex = data.getIndexByID("10067");
		} else if (tmp == "土塊分數") {
			descriptionIndex = data.getIndexByID("10068");
		} else if (tmp == "石塊分數") {
			descriptionIndex = data.getIndexByID("10069");
		} else if (tmp == "磚塊分數") {
			descriptionIndex = data.getIndexByID("10070");
		} else if (tmp == "黑塊分數") {
			descriptionIndex = data.getIndexByID("10071");
		} else if (tmp == "泡泡分數") {
			descriptionIndex = data.getIndexByID("10072");
		} else if (tmp == "蛛網分數") {
			descriptionIndex = data.getIndexByID("10073");
		} else if (tmp == "寶箱分數") {
			descriptionIndex = data.getIndexByID("10074");
		} else if (tmp == "黃星星分數") {
			descriptionIndex = data.getIndexByID("10075");
		} else if (tmp == "紫星星分數") {
			descriptionIndex = data.getIndexByID("10076");
		} else if (tmp == "綠星星分數") {
			descriptionIndex = data.getIndexByID("10077");
		} else if (tmp == "沙塊分數") {
			descriptionIndex = data.getIndexByID("10082");
		} else if (tmp == "火山岩分數") {
			descriptionIndex = data.getIndexByID("10083");
		} else if (tmp == "結冰塊分數") {
			descriptionIndex = data.getIndexByID("10084");
		} else if (tmp == "綠塊分數") {
			descriptionIndex = data.getIndexByID("10085");
		} else if (tmp == "果凍球分數") {
			descriptionIndex = data.getIndexByID("10086");
		} else if (tmp == "閃電球分數") {
			descriptionIndex = data.getIndexByID("10087");
		} else if (tmp == "炸彈分數") {
			descriptionIndex = data.getIndexByID("10088");
		} else if (tmp == "Combo分數") {
			descriptionIndex = data.getIndexByID("10089");
		}
	}

	public int FindFirstIndex() {
		int tmp = 0;
		for (int i = 0; i < data.array_ItemTable_SeriesNumber.Count; i++) {
			if (data.array_ItemTable_SeriesNumber[i] == index.ToString()) {
				tmp = i;
				break;
			}
		}
		return tmp;
	}

	public int FindLastIndex() {
		int tmp = 0;
		for (int i = data.array_ItemTable_SeriesNumber.Count - 1; i >= 0; i--) {
			if (data.array_ItemTable_SeriesNumber[i] == index.ToString()) {
				tmp = i;
				break;
			}
		}
		return tmp;
	}

	public void SetItem(string[] name) {
		switch (itemCount) {
		case 1:
			Odd.SetActive(true);
			Even.SetActive(false);
			OddItem[0].SetActive(false);
			OddItem[1].SetActive(false);
			OddItem[2].SetActive(true);
			OddItem[2].GetComponent<UISprite>().spriteName = name[0];
			OddItemCount[2].text = PlayerPrefs.GetInt(name[0]).ToString();
			OddItem[3].SetActive(false);
			OddItem[4].SetActive(false);
			break;
		case 2:
			Odd.SetActive(false);
			Even.SetActive(true);
			EvenItem[0].SetActive(false);
			EvenItem[1].SetActive(false);

			EvenItem[2].SetActive(true);
			EvenItem[2].GetComponent<UISprite>().spriteName = name[0];
			EvenItemCount[2].text = PlayerPrefs.GetInt(name[0]).ToString();

			EvenItem[3].SetActive(true);
			EvenItem[3].GetComponent<UISprite>().spriteName = name[1];
			EvenItemCount[3].text = PlayerPrefs.GetInt(name[1]).ToString();

			EvenItem[4].SetActive(false);
			EvenItem[5].SetActive(false);
			break;
		case 3:
			Odd.SetActive(true);
			Even.SetActive(false);
			OddItem[0].SetActive(false);

			OddItem[1].SetActive(true);
			OddItem[1].GetComponent<UISprite>().spriteName = name[0];
			OddItemCount[1].text = PlayerPrefs.GetInt(name[0]).ToString();

			OddItem[2].SetActive(true);
			OddItem[2].GetComponent<UISprite>().spriteName = name[1];
			OddItemCount[2].text = PlayerPrefs.GetInt(name[1]).ToString();

			OddItem[3].SetActive(true);
			OddItem[3].GetComponent<UISprite>().spriteName = name[2];
			OddItemCount[3].text = PlayerPrefs.GetInt(name[2]).ToString();

			OddItem[4].SetActive(false);
			break;
		case 4:
			Odd.SetActive(false);
			Even.SetActive(true);
			EvenItem[0].SetActive(false);

			EvenItem[1].SetActive(true);
			EvenItem[1].GetComponent<UISprite>().spriteName = name[0];
			EvenItemCount[1].text = PlayerPrefs.GetInt(name[0]).ToString();

			EvenItem[2].SetActive(true);
			EvenItem[2].GetComponent<UISprite>().spriteName = name[1];
			EvenItemCount[2].text = PlayerPrefs.GetInt(name[1]).ToString();

			EvenItem[3].SetActive(true);
			EvenItem[3].GetComponent<UISprite>().spriteName = name[2];
			EvenItemCount[3].text = PlayerPrefs.GetInt(name[2]).ToString();

			EvenItem[4].SetActive(true);
			EvenItem[4].GetComponent<UISprite>().spriteName = name[3];
			EvenItemCount[4].text = PlayerPrefs.GetInt(name[3]).ToString();

			EvenItem[5].SetActive(false);
			break;
		case 5:
			Odd.SetActive(true);
			Even.SetActive(false);
			OddItem[0].SetActive(true);
			OddItem[0].GetComponent<UISprite>().spriteName = name[0];
			OddItemCount[0].text = PlayerPrefs.GetInt(name[0]).ToString();

			OddItem[1].SetActive(true);
			OddItem[1].GetComponent<UISprite>().spriteName = name[1];
			OddItemCount[1].text = PlayerPrefs.GetInt(name[1]).ToString();

			OddItem[2].SetActive(true);
			OddItem[2].GetComponent<UISprite>().spriteName = name[2];
			OddItemCount[2].text = PlayerPrefs.GetInt(name[2]).ToString();

			OddItem[3].SetActive(true);
			OddItem[3].GetComponent<UISprite>().spriteName = name[3];
			OddItemCount[3].text = PlayerPrefs.GetInt(name[3]).ToString();

			OddItem[4].SetActive(true);
			OddItem[4].GetComponent<UISprite>().spriteName = name[4];
			OddItemCount[4].text = PlayerPrefs.GetInt(name[4]).ToString();
			break;
		case 6:
			Odd.SetActive(false);
			Even.SetActive(true);
			EvenItem[0].SetActive(true);
			EvenItem[0].GetComponent<UISprite>().spriteName = name[0];
			EvenItemCount[0].text = PlayerPrefs.GetInt(name[0]).ToString();

			EvenItem[1].SetActive(true);
			EvenItem[1].GetComponent<UISprite>().spriteName = name[1];
			EvenItemCount[1].text = PlayerPrefs.GetInt(name[1]).ToString();

			EvenItem[2].SetActive(true);
			EvenItem[2].GetComponent<UISprite>().spriteName = name[2];
			EvenItemCount[2].text = PlayerPrefs.GetInt(name[2]).ToString();

			EvenItem[3].SetActive(true);
			EvenItem[3].GetComponent<UISprite>().spriteName = name[3];
			EvenItemCount[3].text = PlayerPrefs.GetInt(name[3]).ToString();

			EvenItem[4].SetActive(true);
			EvenItem[4].GetComponent<UISprite>().spriteName = name[4];
			EvenItemCount[4].text = PlayerPrefs.GetInt(name[4]).ToString();

			EvenItem[5].SetActive(true);
			EvenItem[5].GetComponent<UISprite>().spriteName = name[5];
			EvenItemCount[5].text = PlayerPrefs.GetInt(name[5]).ToString();

			break;
		}
	}
	
	// Update is called once per frame
	void Update () {
		title.trueTypeFont = font[PlayerPrefs.GetInt ("Language")];
		descriptionLabel.trueTypeFont = font[PlayerPrefs.GetInt ("Language")];
		if (title.text != data.array_TextTable [titleIndex] [PlayerPrefs.GetInt ("Language")]) {
			title.text = data.array_TextTable [titleIndex] [PlayerPrefs.GetInt ("Language")];
			title.text = ArabicFixer.Fix(title.text, tashkeel, hinduNumbers);

		}
		if (descriptionIndex != 0) {
			if (descriptionLabel.text != string.Format(data.array_TextTable [descriptionIndex] [PlayerPrefs.GetInt ("Language")], reward)) {
				string tmp = string.Format(data.array_TextTable [descriptionIndex] [PlayerPrefs.GetInt ("Language")], reward);
				descriptionLabel.text = tmp.Replace("\\n", "\n");
			}
		} else {

		}
	}
}
