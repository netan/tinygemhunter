using UnityEngine;
using System.Collections;
using ChartboostSDK;

public class AD_Chartboost : IAD
{
	private bool isLoad = false;
	private bool isShouldDisplay = false;

	void IAD.Cache ()
	{
		Chartboost.cacheInterstitial (CBLocation.Default);
	}

	void IAD.Display ()
	{
		isShouldDisplay = true;

		if (!isLoad)
		{
			Chartboost.cacheInterstitial (CBLocation.Default);
		}
		else
		{
			Debug.Log ("Display Chartboost");
			Chartboost.showInterstitial (CBLocation.Default);
		}
	}

	void IAD.Dispose ()
	{
		Chartboost.didCloseInterstitial -= didCloseInterstitial;
		Chartboost.didClickInterstitial -= didClickInterstitial;
		Chartboost.didCacheInterstitial -= didCacheInterstitial;
		Chartboost.shouldDisplayInterstitial -= shouldDisplayInterstitial;
		Chartboost.didDisplayInterstitial -= didDisplayInterstitial;
	}

	public AD_Chartboost ()
	{
		Chartboost.didCloseInterstitial += didCloseInterstitial;
		Chartboost.didClickInterstitial += didClickInterstitial;
		Chartboost.didCacheInterstitial += didCacheInterstitial;
		Chartboost.shouldDisplayInterstitial += shouldDisplayInterstitial;
		Chartboost.didDisplayInterstitial += didDisplayInterstitial;
	}

	void didCloseInterstitial (CBLocation location)
	{
		Debug.Log ("didCloseInterstitial: " + location);
	}
	
	void didClickInterstitial (CBLocation location)
	{
		Debug.Log ("didClickInterstitial: " + location);
	}
	
	void didCacheInterstitial (CBLocation location)
	{
		Debug.Log ("didCacheInterstitial: " + location);
		isLoad = true;

		if (isShouldDisplay)
		{
			Chartboost.showInterstitial (CBLocation.Default);
		}
	}
	
	bool shouldDisplayInterstitial (CBLocation location)
	{
		Debug.Log ("shouldDisplayInterstitial: " + location);
		return true;
	}
	
	void didDisplayInterstitial (CBLocation location)
	{
		isShouldDisplay = false;
		Debug.Log ("didDisplayInterstitial: " + location);
	}

}

