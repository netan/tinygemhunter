﻿#pragma strict

import System.Collections.Generic;

var sound_clear:GameObject;
var sound_fail:GameObject;


public var dealWithEND:boolean=false;

public var  isGameOver:boolean=false;


public var LoseWin_NGUI:GameObject;
public var Win_Panel:GameObject;
public var LOSE_Panel:GameObject;

public var bell01:GameObject;
public var bell02:GameObject;
public var bell03:GameObject;

public var clearText_Congratuation:GameObject;
public var clearText_GoodJob:GameObject;
public var clearText_Excellent:GameObject;

//Gem
var  blueGem_Number:int=0;
var  greenGem_Number:int=0;
var  redGem_Number:int=0;
var  silverGem_Number:int=0;
var  goldGem_Number:int=0;
//Love
var  love_Number:int=0;
var  hourGlass_Number:int=0;
var  star1_Number:int=0;
var  star2_Number:int=0;
var  star3_Number:int=0;







public var listCondition1 = new System.Collections.Generic.List.<boolean>();
public var listCondition2 = new System.Collections.Generic.List.<boolean>();

public var listLimit1 = new System.Collections.Generic.List.<int>();
public var listLimit2 = new System.Collections.Generic.List.<int>();


//Brick Number=====
var  Brick1_Nnumber:int=0;
var  Brick2_Nnumber:int=0;
var  Brick3_Nnumber:int=0;
var  Brick4_Nnumber:int=0;
var  Brick5_Nnumber:int=0;
var  Brick6_Nnumber:int=0;
var  Brick7_Nnumber:int=0;
var  Brick8_Nnumber:int=0;
var  Brick9_Nnumber:int=0;
var  Brick10_Nnumber:int=0;
var  Brick11_Nnumber:int=0;
var  Brick12_Nnumber:int=0;
var  Brick13_Nnumber:int=0;
var  Brick14_Nnumber:int=0;
var  Brick15_Nnumber:int=0;
var  Brick16_Nnumber:int=0;
var  Brick17_Nnumber:int=0;
var  Brick18_Nnumber:int=0;
var  Brick19_Nnumber:int=0;
var  Treasure20_Nnumber:int=0;
var  Bob21_Nnumber:int=0;
var  SpiderSilk22_Nnumber:int=0;
var  FanR23_Nnumber:int=0;
var  FanL24_Nnumber:int=0;
var  Bell25_Nnumber:int=0;

var isGotData:boolean=false; 

public var musicScript:MyMusic;
public var playerScript:PlayerSC;
public var levelScript:MyStage;

public var listName = new System.Collections.Generic.List.<String>();
public var listNumber = new System.Collections.Generic.List.<String>();

public var con1_pic:GameObject;
public var con2_pic:GameObject;
public var con1_text:GameObject;
public var con2_text:GameObject;

public var TimeMode:boolean=false;
public var LifeMode:boolean=false;

//public var timerStatus:int;


//Show Text
var showTextTimer:float=0;
var isShowText:boolean=false;
var text_outOfTime:GameObject;
var text_outOfMove:GameObject;
var Text_NGUI:GameObject;
var CoverPanel_NGUI:GameObject;

var isVoicePlay:boolean=false;











function ShowTextTimerIns(){
showTextTimer+=Time.deltaTime*1.2;
}

function LevelClear(){

levelScript.isWin=true;
levelScript.bellNumber=Bell25_Nnumber;

	if(!dealWithEND)
	{
	
		if(!isVoicePlay)
		{
		var sound_clear:GameObject=Instantiate (sound_clear, this.gameObject.transform.position, Quaternion.identity);
		sound_clear.transform.parent=GameObject.FindGameObjectWithTag("SoundBox").transform;
		isVoicePlay=true;	
		}
	levelScript.isGameOver=true;

	LoseWin_NGUI.SetActive(true);
	Win_Panel.SetActive(true);
	dealWithEND=true;
	Time.timeScale=0;
	}
}

function LevelFail(){

CoverPanel_NGUI.SetActive(true);

ShowTextTimerIns();

levelScript.isWin=false;
	if(!dealWithEND)
	{
		if(!isVoicePlay)
		{
			var sound_fail:GameObject=Instantiate (sound_fail, this.gameObject.transform.position, Quaternion.identity);
			sound_fail.transform.parent=GameObject.FindGameObjectWithTag("SoundBox").transform;
			isVoicePlay=true;	
		}
		
		//遊戲結束
		levelScript.isGameOver=true;

		if(TimeMode)
		{
			Text_NGUI.SetActive(true);
			if(!isOutOfLimit)//非破壞條件
				text_outOfTime.SetActive(true);
		}
		else
		{
			Text_NGUI.SetActive(true);
			if(!isOutOfLimit)//非破壞條件
				text_outOfMove.SetActive(true);
		}


		if(showTextTimer>2)
		{
			if(TimeMode)
			{
				Text_NGUI.SetActive(false);
//				if(!isOutOfLimit)//非破壞條件
					text_outOfTime.SetActive(false);
			}
			else
			{
				Text_NGUI.SetActive(false);
//				if(!isOutOfLimit)//非破壞條件
					text_outOfMove.SetActive(false);
			}
	
		LoseWin_NGUI.SetActive(true);
		LOSE_Panel.SetActive(true);
	
		Time.timeScale=0;
		dealWithEND=true;
		}
		
		
		
	
	}

}


function getAllGemNumber():int {

var gemNumber:int=0;

gemNumber=
blueGem_Number
+greenGem_Number
+redGem_Number
+silverGem_Number
+goldGem_Number;

return gemNumber;
}







var isOutOfLimit:boolean=false;

//偵測是否達成條件
function DetectLevelClear(){

var number:int=0;
//print(number);
//條件1
for(var a:int=0;a<listCondition1.Count;a++){
if(listCondition1[a]==true){
number++;
}
}
//條件2
for(var b:int=0;b<listCondition2.Count;b++){
if(listCondition2[b]==true){
number++;
}
}
//print(number);


//如果 Player已經開始遊戲 即可進行 GAMEOVER偵測
if(playerScript)
var gotX:boolean=playerScript.got;
if(gotX){
	if(TimeMode){
	var timeX:int=playerScript.Timer;
	
		//若有需要到最後殘能分出勝負的
		if(listLimit2[19]!=-1 || listLimit2[20]!=-1 || listLimit2[21]!=-1 ||
		   listLimit1[19]!=-1 || listLimit1[20]!=-1 || listLimit1[21]!=-1)
		{	
			if(timeX+1 <= 0){//時間到
				if(number==listName.Count && number>0)//符合條件
				{
					isGameOver=true;
					GetBellAndPinat();
					musicScript.musicStatus=false;
					yield WaitForSeconds(0.5);
					LevelClear();
					
				}
			}
			else if(number<listName.Count )//破壞條件 過關失敗
			{
			isOutOfLimit=true;
			isGameOver=true;
			musicScript.musicStatus=false;
			yield WaitForSeconds(0.5);
			LevelFail();
			}
			else{
			}
		}
		else{//可直接決定勝負
			if(number==listName.Count && number>0)
			{
				isGameOver=true;
				GetBellAndPinat();
				musicScript.musicStatus=false;
				yield WaitForSeconds(0.5);
				LevelClear();
			}
			else if(timeX+1 <= 0){
				isGameOver=true;
				musicScript.musicStatus=false;
				yield WaitForSeconds(0.5);
				LevelFail();
			}
			else{
//			print("nothing");
			}

		}

		
	}	
	else{//限命模式
	
		if(playerScript)
		var lifeX:int=playerScript.life;

		if(lifeX<=0)//沒命
		{
			isGameOver=true;
			musicScript.musicStatus=false;
			yield WaitForSeconds(0.5);
			LevelFail();
		}
		//破壞條件
		else if(listLimit2[19]!=-1 || listLimit2[20]!=-1 || listLimit2[21]!=-1 ||
		   listLimit1[19]!=-1 || listLimit1[20]!=-1 || listLimit1[21]!=-1)
		{
		 	if(number<listName.Count )//破壞條件
			{
				isOutOfLimit=true;
				isGameOver=true;
				musicScript.musicStatus=false;
				yield WaitForSeconds(0.5);
				LevelFail();
			}
		}
		//達成條件
		else if(number==listName.Count && number>0)
		{
		isGameOver=true;
		GetBellAndPinat();
		musicScript.musicStatus=false;
		yield WaitForSeconds(0.5);
		LevelClear();
		}
	}//else
	
	}//Got=true


}




var isBellSaved:boolean=false;

function GetBellAndPinat(){


//print("test bell");

if(Bell25_Nnumber==0)
{
bell01.SetActive(false);
bell02.SetActive(false);
bell03.SetActive(false);

clearText_Congratuation.SetActive(false);
clearText_GoodJob.SetActive(false);
clearText_Excellent.SetActive(false);
}
else if(Bell25_Nnumber==1)
{
bell01.SetActive(true);
bell02.SetActive(false);
bell03.SetActive(false);

clearText_Congratuation.SetActive(true);
clearText_GoodJob.SetActive(false);
clearText_Excellent.SetActive(false);

}else if(Bell25_Nnumber==2)
{
bell01.SetActive(true);
bell02.SetActive(true);
bell03.SetActive(false);

clearText_Congratuation.SetActive(false);
clearText_GoodJob.SetActive(true);
clearText_Excellent.SetActive(false);
}
else if(Bell25_Nnumber==3)
{
bell01.SetActive(true);
bell02.SetActive(true);
bell03.SetActive(true);

clearText_Congratuation.SetActive(false);
clearText_GoodJob.SetActive(false);
clearText_Excellent.SetActive(true);
}


//Save Star
if(!isBellSaved){
//print(LevelSelcet.stageInt+" "+LevelSelcet.levelInt);

var bellArray:int[]=saveScript.GetStage_Star(LevelSelcet.stageInt);//全部BELL陣列
var oldBellNumber:int=bellArray[LevelSelcet.levelInt-1];//舊的BELL數

//BELL 儲存
if(Bell25_Nnumber>oldBellNumber)
	saveScript.SetStage_Star(LevelSelcet.stageInt,LevelSelcet.levelInt,Bell25_Nnumber);
//過關 儲存
saveScript.SetStage_Clear(LevelSelcet.stageInt,LevelSelcet.levelInt);

isBellSaved=true;
//print("SAVED");
}


}










function ClearCondition1(){
	//擊破土塊
//	 print(listLimit1[0]);
	if(listLimit1[0]!=-1)
	{
		if(Brick1_Nnumber>=listLimit1[0])
		{
			listCondition1[0]=true;
		}
		if(Brick1_Nnumber<listLimit1[0])
		{
			listCondition1[0]=false;
		}
	}
	//擊破石塊
	if(listLimit1[1]!=-1)
	{
		if(Brick2_Nnumber>=listLimit1[1])
		{
			listCondition1[1]=true;
		}
		if(Brick2_Nnumber<listLimit1[1])
		{
			listCondition1[1]=false;
		}
	}
	//擊破紅塊
	if(listLimit1[2]!=-1)
	{
		if(Brick3_Nnumber>=listLimit1[2])
		{
			listCondition1[2]=true;
		}
		if(Brick3_Nnumber<listLimit1[2])
		{
			listCondition1[2]=false;
		}
	}	
	//擊破黑塊
	if(listLimit1[3]!=-1)
	{
		if(Brick4_Nnumber>=listLimit1[3])
		{
			listCondition1[3]=true;
		}
		if(Brick4_Nnumber<listLimit1[3])
		{
			listCondition1[3]=false;
		}
	}
	//清除所有方塊(不含黑塊)
	if(listLimit1[4]!=-1)
	{
		if(levelScript.getAllBlockWithoutBlack()==0)
			listCondition1[4]=true;
		else if(levelScript.getAllBlockWithoutBlack()>0)
			listCondition1[4]=false;
	}	
	//收集寶石
	if(listLimit1[5]!=-1)
	{
		if(getAllGemNumber()>=listLimit1[5])
			listCondition1[5]=true;
		else if(getAllGemNumber()<listLimit1[5])
			listCondition1[5]=false;			
	}		
		
	//收集藍寶石
	if(listLimit1[6]!=-1)
	{
		if(blueGem_Number>=listLimit1[6])
			listCondition1[6]=true;
		else if(blueGem_Number<listLimit1[6])
			listCondition1[6]=false;			
	}			
	
	//收集綠寶石
	if(listLimit1[7]!=-1)
	{
		if(greenGem_Number>=listLimit1[7])
			listCondition1[7]=true;
		else if(greenGem_Number<listLimit1[7])
			listCondition1[7]=false;			
	}		
	//收集紅寶石
	if(listLimit1[8]!=-1)
	{
		if(redGem_Number>=listLimit1[8])
			listCondition1[8]=true;
		else if(redGem_Number<listLimit1[8])
			listCondition1[8]=false;			
	}	
	//收集白寶石
	if(listLimit1[9]!=-1)
	{
		if(silverGem_Number>=listLimit1[9])
			listCondition1[9]=true;
		else if(silverGem_Number<listLimit1[9])
			listCondition1[9]=false;
	}		
	//收集黃寶石
	if(listLimit1[10]!=-1)
	{
		if(goldGem_Number>=listLimit1[10])
			listCondition1[10]=true;
		else if(goldGem_Number<listLimit1[10])
			listCondition1[10]=false;			
	}	
	//泡泡
	if(listLimit1[11]!=-1)
	{
		if(Bob21_Nnumber>=listLimit1[11])
			listCondition1[11]=true;
		else if(Bob21_Nnumber<listLimit1[11])
			listCondition1[11]=false;			
	}			
	//SpiderSilk
	if(listLimit1[12]!=-1)
	{
		if(SpiderSilk22_Nnumber>=listLimit1[12])
			listCondition1[12]=true;
		else if(SpiderSilk22_Nnumber<listLimit1[12])
			listCondition1[12]=false;			
			
	}				
	//Fan
	if(listLimit1[13]!=-1)
	{
		if((FanR23_Nnumber+FanL24_Nnumber)>=listLimit1[13])
			listCondition1[13]=true;
		else if((FanR23_Nnumber+FanL24_Nnumber)<listLimit1[13])
			listCondition1[13]=false;			
			
	}		
	//Love OR Hourglass
	if(listLimit1[14]!=-1)
	{
		if(TimeMode){
			if(hourGlass_Number>=listLimit1[14] )
				listCondition1[14]=true;
			else if(hourGlass_Number<listLimit1[14] )
				listCondition1[14]=false;			
		}else{
			if(love_Number>=listLimit1[14] )
				listCondition1[14]=true;
			else if(love_Number<listLimit1[14] )
				listCondition1[14]=false;		
		}	
	}				
	//Star1
	if(listLimit1[15]!=-1)
	{
		if(star1_Number>=listLimit1[15]  )
			listCondition1[15]=true;
		else if(star1_Number<listLimit1[15]  )
			listCondition1[15]=false;		
	}							
	//Star2
	if(listLimit1[16]!=-1)
	{
		if(star2_Number>=listLimit1[16]  )
			listCondition1[16]=true;
		else if(star2_Number<listLimit1[16]  )
			listCondition1[16]=false;			
	}	
	//Star3
	if(listLimit1[17]!=-1)
	{
		if(star3_Number>=listLimit1[17]  )
			listCondition1[17]=true;
		else if(star3_Number<listLimit1[17]  )
			listCondition1[17]=false;			
	}		
	//Treasure
	if(listLimit1[18]!=-1)
	{
		if(Treasure20_Nnumber>=listLimit1[18]  )
			listCondition1[18]=true;
		else if(Treasure20_Nnumber<listLimit1[18]  )
			listCondition1[18]=false;		
	}	
	//No Bob
	if(listLimit1[19]!=-1)
	{
		if(Bob21_Nnumber<listLimit1[19] )
			listCondition1[19]=true;
		else if(Bob21_Nnumber >=listLimit1[19] )
			listCondition1[19]=false;
	}	
	//No Spidersilk
	if(listLimit1[20]!=-1)
	{
//	print(SpiderSilk22_Nnumber+" "+listLimit1[20]);
		if(SpiderSilk22_Nnumber <listLimit1[20] )
			listCondition1[20]=true;
		else if(SpiderSilk22_Nnumber >=listLimit1[20] )
			listCondition1[20]=false;
	}							
	//No Fan
	if(listLimit1[21]!=-1)
	{
		if((FanR23_Nnumber+FanL24_Nnumber)<listLimit1[21])
		listCondition1[21]=true;
		else if((FanR23_Nnumber+FanL24_Nnumber)>=listLimit1[21])
			listCondition1[21]=false;	
	}													
																									
											
																					
}


function ClearCondition2(){
		//擊破土塊
//	 print(listLimit2[0]);
	if(listLimit2[0]!=-1)
	{
		if(Brick1_Nnumber>=listLimit2[0])
		{
			listCondition2[0]=true;
		}
		if(Brick1_Nnumber<listLimit2[0])
		{
			listCondition2[0]=false;
		}
	}
	//擊破石塊
	if(listLimit2[1]!=-1)
	{
		if(Brick2_Nnumber>=listLimit2[1])
		{
			listCondition2[1]=true;
		}
		if(Brick2_Nnumber<listLimit2[1])
		{
			listCondition2[1]=false;
		}
	}
	//擊破紅塊
	if(listLimit2[2]!=-1)
	{
		if(Brick3_Nnumber>=listLimit2[2])
		{
			listCondition2[2]=true;
		}
		if(Brick3_Nnumber<listLimit2[2])
		{
			listCondition2[2]=false;
		}
	}	
	//擊破黑塊
	if(listLimit2[3]!=-1)
	{
		if(Brick4_Nnumber>=listLimit2[3])
		{
			listCondition2[3]=true;
		}
		if(Brick4_Nnumber<listLimit2[3])
		{
			listCondition2[3]=false;
		}
	}
	//清除所有方塊(不含黑塊)
	if(listLimit2[4]!=-1)
	{
		if(levelScript.getAllBlockWithoutBlack()==0)
			listCondition2[4]=true;
		else if(levelScript.getAllBlockWithoutBlack()>0)
			listCondition2[4]=false;
	}	
	//收集寶石
	if(listLimit2[5]!=-1)
	{
		if(getAllGemNumber()>=listLimit2[5])
			listCondition2[5]=true;
		else if(getAllGemNumber()<listLimit2[5])
			listCondition2[5]=false;			
	}		
		
	//收集藍寶石
	if(listLimit2[6]!=-1)
	{
		if(blueGem_Number>=listLimit2[6])
			listCondition2[6]=true;
		else if(blueGem_Number<listLimit2[6])
			listCondition2[6]=false;			
	}			
	
	//收集綠寶石
	if(listLimit2[7]!=-1)
	{
		if(greenGem_Number>=listLimit2[7])
			listCondition2[7]=true;
		else if(greenGem_Number<listLimit2[7])
			listCondition2[7]=false;			
	}		
	//收集紅寶石
	if(listLimit2[8]!=-1)
	{
		if(redGem_Number>=listLimit2[8])
			listCondition2[8]=true;
		else if(redGem_Number<listLimit2[8])
			listCondition2[8]=false;			
	}	
	//收集白寶石
	if(listLimit2[9]!=-1)
	{
		if(silverGem_Number>=listLimit2[9])
			listCondition2[9]=true;
		else if(silverGem_Number<listLimit2[9])
			listCondition2[9]=false;
	}		
	//收集黃寶石
	if(listLimit2[10]!=-1)
	{
		if(goldGem_Number>=listLimit2[10])
			listCondition2[10]=true;
		else if(goldGem_Number<listLimit2[10])
			listCondition2[10]=false;			
	}	
	//泡泡
	if(listLimit2[11]!=-1)
	{
		if(Bob21_Nnumber>=listLimit2[11])
			listCondition2[11]=true;
		else if(Bob21_Nnumber<listLimit2[11])
			listCondition2[11]=false;			
	}			
	//SpiderSilk
	if(listLimit2[12]!=-1)
	{
		if(SpiderSilk22_Nnumber>=listLimit2[12])
			listCondition2[12]=true;
		else if(SpiderSilk22_Nnumber<listLimit2[12])
			listCondition2[12]=false;			
			
	}				
	//Fan
	if(listLimit2[13]!=-1)
	{
		if((FanR23_Nnumber+FanL24_Nnumber)>=listLimit2[13])
			listCondition2[13]=true;
		else if((FanR23_Nnumber+FanL24_Nnumber)<listLimit2[13])
			listCondition2[13]=false;			
			
	}		
	//Love OR Hourglass
	if(listLimit2[14]!=-1)
	{
		if(TimeMode){
			if(hourGlass_Number>=listLimit2[14] )
				listCondition2[14]=true;
			else if(hourGlass_Number<listLimit2[14] )
				listCondition2[14]=false;			
		}else{
			if(love_Number>=listLimit2[14] )
				listCondition2[14]=true;
			else if(love_Number<listLimit2[14] )
				listCondition2[14]=false;		
		}	
	}				
	//Star1
	if(listLimit2[15]!=-1)
	{
		if(star1_Number>=listLimit2[15]  )
			listCondition2[15]=true;
		else if(star1_Number<listLimit2[15]  )
			listCondition2[15]=false;		
	}							
	//Star2
	if(listLimit2[16]!=-1)
	{
		if(star2_Number>=listLimit2[16]  )
			listCondition2[16]=true;
		else if(star2_Number<listLimit2[16]  )
			listCondition2[16]=false;			
	}	
	//Star3
	if(listLimit2[17]!=-1)
	{
		if(star3_Number>=listLimit2[17]  )
			listCondition2[17]=true;
		else if(star3_Number<listLimit2[17]  )
			listCondition2[17]=false;			
	}		
	//Treasure
	if(listLimit2[18]!=-1)
	{
		if(Treasure20_Nnumber>=listLimit2[18]  )
			listCondition2[18]=true;
		else if(Treasure20_Nnumber<listLimit2[18]  )
			listCondition2[18]=false;		
	}	
	//No Bob
	if(listLimit2[19]!=-1)
	{
//	print(Bob21_Nnumber+" "+listLimit2[19]+" "+listCondition2[19]);
		if(Bob21_Nnumber<listLimit2[19] )
			listCondition2[19]=true;
		else if(Bob21_Nnumber >= listLimit2[19] )
			listCondition2[19]=false;
	}	
	//No Spidersilk
	if(listLimit2[20]!=-1)
	{
		if(SpiderSilk22_Nnumber <listLimit2[20] )
			listCondition2[20]=true;
		else if(SpiderSilk22_Nnumber >=listLimit2[20] )
			listCondition2[20]=false;
	}							
	//No Fan
	if(listLimit2[21]!=-1)
	{
		if((FanR23_Nnumber+FanL24_Nnumber)<listLimit2[21])
		listCondition2[21]=true;
		else if((FanR23_Nnumber+FanL24_Nnumber)>=listLimit2[21])
			listCondition2[21]=false;	
	}	
}





var saveScript:SaveMyData;


function Start () {

//this.GetComponent(ScoreSC);


//過關 或 失敗 音樂 是否播放
isVoicePlay=false;	

musicScript=GameObject.FindGameObjectWithTag("MusicBox").GetComponent(MyMusic);
	musicScript.musicStatus=true;

levelScript=GameObject.FindGameObjectWithTag("LevelManager").GetComponent(MyStage);

playerScript=GameObject.FindGameObjectWithTag("Player").GetComponent(PlayerSC);
if(GameObject.FindGameObjectWithTag ("EventControoler"))
	saveScript = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent (SaveMyData);



isGameOver=false;
Time.timeScale = 1;

for(var i:int=0;i<22;i++){
listCondition1.Add(false);
listCondition2.Add(false);
listLimit1.Add(-1);
listLimit2.Add(-1);
}

}



function Update () {


if(levelScript.isRead==true && isGotData==false)
{
listName=levelScript.arrayOfLimitedRule;
listNumber=levelScript.arrayOfLimitedRuleNumber;

TimeMode=levelScript.timeLimited;
LifeMode=levelScript.lifeLimited;

	if(listName.Count==0){//無條件 
	return;
	}
	else if(listName.Count==1)//條件1種
	{
		SetCondition(listName[0],listNumber[0],"","");
	}
	else if(listName.Count==2)//條件2種
	{
		SetCondition(listName[0],listNumber[0],listName[1],listNumber[1]);
	}

	isGotData=true;//取得條件資料
}


//勝利條件 +數量
for(var a:int=0;a<listName.Count;a++){
//print(listName[a]+" "+listNumber[a]);
ShowConditionText(a,listName[a],listNumber[a]);
}

ClearCondition1();//將條件1數值存入
ClearCondition2();//將條件2數值存入


DetectLevelClear();//偵測 是否達成勝利條件

}



//顯示文字
function ShowConditionText(conditionNumber:int,conditionName:String,conditionLimit:String){
//print(conditionName);

if(conditionName=="清光方塊(不包括黑塊)")
{

}
//	return;
else{
var numberInt=int.Parse(conditionLimit);
}
	
	


switch(conditionName)
{
case "擊破土塊":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("brick1",typeof(Sprite))as Sprite;
		if(Brick1_Nnumber <= numberInt)
			con1_text.guiText.text= Brick1_Nnumber +"/"+conditionLimit;
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("brick1",typeof(Sprite))as Sprite;
		if(Brick1_Nnumber<=numberInt)
			con2_text.guiText.text= Brick1_Nnumber +"/"+conditionLimit;
	}
break;



case "擊破土塊":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("brick1",typeof(Sprite))as Sprite;
		if(Brick1_Nnumber <= numberInt)
			con1_text.guiText.text= Brick1_Nnumber +"/"+conditionLimit;
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("brick1",typeof(Sprite))as Sprite;
		if(Brick1_Nnumber<=numberInt)
			con2_text.guiText.text= Brick1_Nnumber +"/"+conditionLimit;
	}
break;

case "擊破石塊":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("brick2",typeof(Sprite))as Sprite;
		if(Brick2_Nnumber <= numberInt)
			con1_text.guiText.text= Brick2_Nnumber +"/"+conditionLimit;	
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("brick2",typeof(Sprite))as Sprite;
		if(Brick2_Nnumber <= numberInt)
			con2_text.guiText.text= Brick2_Nnumber +"/"+conditionLimit;		
	}
break;

case "擊破紅塊":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("brick3",typeof(Sprite))as Sprite;
		if(Brick3_Nnumber <= numberInt)
			con1_text.guiText.text= Brick3_Nnumber +"/"+conditionLimit;	
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("brick3",typeof(Sprite))as Sprite;
		if(Brick3_Nnumber <= numberInt)
			con2_text.guiText.text= Brick3_Nnumber +"/"+conditionLimit;		
	}
break;

case "擊破黑塊(黑曜石)":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("brick4",typeof(Sprite))as Sprite;
		if(Brick4_Nnumber <= numberInt)
			con1_text.guiText.text= Brick4_Nnumber +"/"+conditionLimit;	
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("brick4",typeof(Sprite))as Sprite;
		if(Brick4_Nnumber <= numberInt)
			con2_text.guiText.text= Brick4_Nnumber +"/"+conditionLimit;		
	}	
break;

case "清光方塊(不包括黑塊)":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("allBricks2",typeof(Sprite))as Sprite;
		if(levelScript.getAllBlockWithoutBlack() >=0)
			con1_text.guiText.text= levelScript.getAllBlockWithoutBlack() +"/0";	
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("allBricks2",typeof(Sprite))as Sprite;
		if(levelScript.getAllBlockWithoutBlack() >=0)
			con2_text.guiText.text= levelScript.getAllBlockWithoutBlack() +"/0";		
	}		
break;

case "收集寶石(不論顏色)":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("allGem",typeof(Sprite))as Sprite;
		if(getAllGemNumber()  <= numberInt)
			con1_text.guiText.text= getAllGemNumber()  +"/"+conditionLimit;
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("allGem",typeof(Sprite))as Sprite;
		if(getAllGemNumber()  <= numberInt)
			con2_text.guiText.text= getAllGemNumber()  +"/"+conditionLimit;	
	}	
break;

case "收集藍寶石":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_b",typeof(Sprite))as Sprite;
		if(blueGem_Number <= numberInt)
			con1_text.guiText.text= blueGem_Number +"/"+conditionLimit;	
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_b",typeof(Sprite))as Sprite;
		if(blueGem_Number <= numberInt)
			con2_text.guiText.text= blueGem_Number +"/"+conditionLimit;		
	}	
break;

case "收集綠寶石":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_g",typeof(Sprite))as Sprite;
		if(greenGem_Number <= numberInt)
			con1_text.guiText.text= greenGem_Number +"/"+conditionLimit;	
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_g",typeof(Sprite))as Sprite;
		if(greenGem_Number <= numberInt)
			con2_text.guiText.text= greenGem_Number +"/"+conditionLimit;	
	}	
break;

case "收集紅寶石":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_r",typeof(Sprite))as Sprite;
		if(redGem_Number <= numberInt)
			con1_text.guiText.text=redGem_Number +"/"+conditionLimit;	
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_r",typeof(Sprite))as Sprite;
		if(redGem_Number <= numberInt)
			con2_text.guiText.text= redGem_Number +"/"+conditionLimit;
	}
break;
						
case "收集白寶石":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_w",typeof(Sprite))as Sprite;
		if(silverGem_Number <= numberInt)
			con1_text.guiText.text= silverGem_Number +"/"+conditionLimit;	
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_w",typeof(Sprite))as Sprite;
		if(silverGem_Number <= numberInt)
			con2_text.guiText.text= silverGem_Number +"/"+conditionLimit;
	}
break;

case "收集黃寶石":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_y",typeof(Sprite))as Sprite;
		if(goldGem_Number <= numberInt)
			con1_text.guiText.text= goldGem_Number +"/"+conditionLimit;	
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_y",typeof(Sprite))as Sprite;
		if(goldGem_Number <= numberInt)
			con2_text.guiText.text= goldGem_Number +"/"+conditionLimit;
	}
break;

case "擊破泡泡":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("bubble",typeof(Sprite))as Sprite;
		if(Bob21_Nnumber <= numberInt)
			con1_text.guiText.text= Bob21_Nnumber +"/"+conditionLimit;	
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("bubble",typeof(Sprite))as Sprite;
		if(Bob21_Nnumber <= numberInt)
			con2_text.guiText.text= Bob21_Nnumber +"/"+conditionLimit;
	}
break;

case "擊破蛛網":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("web1",typeof(Sprite))as Sprite;
		if(SpiderSilk22_Nnumber <= numberInt)
			con1_text.guiText.text= SpiderSilk22_Nnumber +"/"+conditionLimit;	
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("web1",typeof(Sprite))as Sprite;
		if(SpiderSilk22_Nnumber <= numberInt)
			con2_text.guiText.text= SpiderSilk22_Nnumber +"/"+conditionLimit;
	}
break;

case "停住風扇": 
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("fan",typeof(Sprite))as Sprite;
		if((FanR23_Nnumber+FanL24_Nnumber) <= numberInt)
			con1_text.guiText.text= (FanR23_Nnumber+FanL24_Nnumber)  +"/"+conditionLimit;	
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("fan",typeof(Sprite))as Sprite;
		if((FanR23_Nnumber+FanL24_Nnumber)  <= numberInt)
			con2_text.guiText.text= (FanR23_Nnumber+FanL24_Nnumber)  +"/"+conditionLimit;
	}	
break;

case "吃到愛心(沙漏)":
if(TimeMode){
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("hourglass",typeof(Sprite))as Sprite;
		if(hourGlass_Number <= numberInt)
			con1_text.guiText.text= hourGlass_Number +"/"+conditionLimit;	
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("hourglass",typeof(Sprite))as Sprite;
		if(hourGlass_Number  <= numberInt)
			con2_text.guiText.text= hourGlass_Number  +"/"+conditionLimit;
	}	
}
else{
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("heart",typeof(Sprite))as Sprite;
		if(love_Number <= numberInt)
			con1_text.guiText.text= love_Number +"/"+conditionLimit;	
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("heart",typeof(Sprite))as Sprite;
		if(love_Number  <= numberInt)
			con2_text.guiText.text= love_Number  +"/"+conditionLimit;
	}

}			
break;

case "吃到星星1":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("star1",typeof(Sprite))as Sprite;
		if(star1_Number <= numberInt)
			con1_text.guiText.text= star1_Number +"/"+conditionLimit;	
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("star1",typeof(Sprite))as Sprite;
		if(star1_Number  <= numberInt)
			con2_text.guiText.text= star1_Number  +"/"+conditionLimit;
	}
break;

case "吃到星星2":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("star2",typeof(Sprite))as Sprite;
		if(star2_Number <= numberInt)
			con1_text.guiText.text= star2_Number +"/"+conditionLimit;	
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("star2",typeof(Sprite))as Sprite;
		if(star2_Number  <= numberInt)
			con2_text.guiText.text= star2_Number  +"/"+conditionLimit;
	}
break;

case "吃到星星3":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("star3",typeof(Sprite))as Sprite;
		if(star3_Number <= numberInt)
			con1_text.guiText.text= star3_Number +"/"+conditionLimit;	
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("star3",typeof(Sprite))as Sprite;
		if(star3_Number  <= numberInt)
			con2_text.guiText.text= star3_Number  +"/"+conditionLimit;
	}
break;

case "吃到寶箱":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("treasure",typeof(Sprite))as Sprite;
		if(Treasure20_Nnumber <= numberInt)
			con1_text.guiText.text= Treasure20_Nnumber +"/"+conditionLimit;	
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("treasure",typeof(Sprite))as Sprite;
		if(Treasure20_Nnumber  <= numberInt)
			con2_text.guiText.text= Treasure20_Nnumber  +"/"+conditionLimit;
	}
break;

case "不可擊破泡泡":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("bubble_BAN",typeof(Sprite))as Sprite;
		if(Bob21_Nnumber <= numberInt)
			con1_text.guiText.text= Bob21_Nnumber +"/"+numberInt;
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("bubble_BAN",typeof(Sprite))as Sprite;
		if(Bob21_Nnumber <= numberInt)
			con2_text.guiText.text= Bob21_Nnumber +"/"+numberInt;
	}
break;

case "不可擊破蛛網":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("web1_BAN",typeof(Sprite))as Sprite;
		if(SpiderSilk22_Nnumber <= numberInt)
			con1_text.guiText.text= SpiderSilk22_Nnumber +"/"+numberInt;
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("web1_BAN",typeof(Sprite))as Sprite;
		if(SpiderSilk22_Nnumber <= numberInt)
			con2_text.guiText.text= SpiderSilk22_Nnumber +"/"+numberInt;
	}
break;

case "不可停住風扇":
	if(conditionNumber==0){
		con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("fan_BAN",typeof(Sprite))as Sprite;
		if((FanR23_Nnumber+FanL24_Nnumber) <= numberInt)
			con1_text.guiText.text= (FanR23_Nnumber+FanL24_Nnumber)  +"/"+numberInt;
	}
	else{
		con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("fan_BAN",typeof(Sprite))as Sprite;
		if((FanR23_Nnumber+FanL24_Nnumber)  <= numberInt)
			con2_text.guiText.text= (FanR23_Nnumber+FanL24_Nnumber)  +"/"+numberInt;
	}
break;
}






}







//設定過關條件
function SetCondition(condition1:String,number1:String,condition2:String,number2:String){

switch(condition1)
{
case "擊破土塊":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("brick1",typeof(Sprite))as Sprite;
	listLimit1[0]=int.Parse(number1);
break;

case "擊破石塊":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("brick2",typeof(Sprite))as Sprite;
	listLimit1[1]=int.Parse(number1);
break;

case "擊破紅塊":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("brick3",typeof(Sprite))as Sprite;
	listLimit1[2]=int.Parse(number1);
break;

case "擊破黑塊(黑曜石)":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("brick4",typeof(Sprite))as Sprite;
	listLimit1[3]=int.Parse(number1);		
break;

case "清光方塊(不包括黑塊)":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("allBricks2",typeof(Sprite))as Sprite;
	listLimit1[4]=100;
break;

case "收集寶石(不論顏色)":
	listLimit1[5]=int.Parse(number1);
break;

case "收集藍寶石":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_b",typeof(Sprite))as Sprite;
	listLimit1[6]=int.Parse(number1);	
break;

case "收集綠寶石":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_g",typeof(Sprite))as Sprite;
	listLimit1[7]=int.Parse(number1);	
break;

case "收集紅寶石":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_r",typeof(Sprite))as Sprite;
	listLimit1[8]=int.Parse(number1);
break;
						
case "收集白寶石":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_w",typeof(Sprite))as Sprite;
	listLimit1[9]=int.Parse(number1);
break;

case "收集黃寶石":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_y",typeof(Sprite))as Sprite;
	listLimit1[10]=int.Parse(number1);
break;

case "擊破泡泡":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("bubble",typeof(Sprite))as Sprite;
	listLimit1[11]=int.Parse(number1);
break;
case "擊破蛛網":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("web1",typeof(Sprite))as Sprite;
	listLimit1[12]=int.Parse(number1);
break;
case "停住風扇":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("fan",typeof(Sprite))as Sprite;
	listLimit1[13]=int.Parse(number1);
break;
case "吃到愛心(沙漏)":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("heart",typeof(Sprite))as Sprite;
	listLimit1[14]=int.Parse(number1);
break;
case "吃到星星1":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("star",typeof(Sprite))as Sprite;
	listLimit1[15]=int.Parse(number1);
break;
case "吃到星星2":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("star",typeof(Sprite))as Sprite;
	listLimit1[16]=int.Parse(number1);
break;
case "吃到星星3":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("star",typeof(Sprite))as Sprite;
	listLimit1[17]=int.Parse(number1);
break;
case "吃到寶箱":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("treasure",typeof(Sprite))as Sprite;
	listLimit1[18]=int.Parse(number1);
break;
case "不可擊破泡泡":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("bubble_BAN",typeof(Sprite))as Sprite;
	listLimit1[19]=int.Parse(number1);
break;
case "不可擊破蛛網":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("web1_BAN",typeof(Sprite))as Sprite;
	listLimit1[20]=int.Parse(number1);
break;
case "不可停住風扇":
	con1_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("fan_BAN",typeof(Sprite))as Sprite;
	listLimit1[21]=int.Parse(number1);
break;
}


switch(condition2)
{
case "擊破土塊":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("brick1",typeof(Sprite))as Sprite;
	listLimit2[0]=int.Parse(number2);
break;

case "擊破石塊":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("brick2",typeof(Sprite))as Sprite;
	listLimit2[1]=int.Parse(number2);
break;

case "擊破紅塊":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("brick3",typeof(Sprite))as Sprite;
	listLimit2[2]=int.Parse(number2);
break;

case "擊破黑塊(黑曜石)":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("brick4",typeof(Sprite))as Sprite;
	listLimit2[3]=int.Parse(number2);		
break;


case "清光方塊(不包括黑塊)":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("allBricks2",typeof(Sprite))as Sprite;
break;

case "收集寶石(不論顏色)":
	listLimit2[5]=int.Parse(number2);
break;

case "收集藍寶石":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_b",typeof(Sprite))as Sprite;
	listLimit2[6]=int.Parse(number2);	
break;

case "收集綠寶石":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_g",typeof(Sprite))as Sprite;
	listLimit2[7]=int.Parse(number2);	
break;

case "收集紅寶石":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_r",typeof(Sprite))as Sprite;
	listLimit2[8]=int.Parse(number2);
break;
						
case "收集白寶石":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_w",typeof(Sprite))as Sprite;
	listLimit2[9]=int.Parse(number2);
break;

case "收集黃寶石":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("gem_y",typeof(Sprite))as Sprite;
	listLimit2[10]=int.Parse(number2);
break;

case "擊破泡泡":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("bubble",typeof(Sprite))as Sprite;
	listLimit2[11]=int.Parse(number2);
break;

case "擊破蛛網":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("web1",typeof(Sprite))as Sprite;
	listLimit2[12]=int.Parse(number2);
break;
case "停住風扇":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("fan",typeof(Sprite))as Sprite;
	listLimit2[13]=int.Parse(number2);
break;
case "吃到愛心(沙漏)":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("heart",typeof(Sprite))as Sprite;
	listLimit2[14]=int.Parse(number2);
break;
case "吃到星星1":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("star",typeof(Sprite))as Sprite;
	listLimit2[15]=int.Parse(number2);
break;
case "吃到星星2":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("star",typeof(Sprite))as Sprite;
	listLimit2[16]=int.Parse(number2);
break;
case "吃到星星3":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("star",typeof(Sprite))as Sprite;
	listLimit2[17]=int.Parse(number2);
break;
case "吃到寶箱":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("treasure",typeof(Sprite))as Sprite;
	listLimit2[18]=int.Parse(number2);
break;
case "不可擊破泡泡":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("bubble_BAN",typeof(Sprite))as Sprite;
	listLimit2[19]=int.Parse(number2);
break;
case "不可擊破蛛網":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("web1_BAN",typeof(Sprite))as Sprite;
	listLimit2[20]=int.Parse(number2);
break;
case "不可停住風扇":
	con2_pic.GetComponent(SpriteRenderer).sprite=Resources.Load("fan_BAN",typeof(Sprite))as Sprite;
	listLimit2[21]=int.Parse(number2);
break;
}


}
















