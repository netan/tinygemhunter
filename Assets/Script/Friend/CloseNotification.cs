﻿using UnityEngine;
using System.Collections;

public class CloseNotification : MonoBehaviour {

	public GameObject UI;

	public void OnClick() {
		UI.SetActive (false);
	}
}
