﻿using UnityEngine;
using System.Collections;

public class RandomCandy : MonoBehaviour {

	private string[][] scoreData;

	public int ran;
	public int[] itemGet;

	public int Spacing;
	public int Candy_number;
	public int Bubble;
	public int Gold;
	
	// Use this for initialization
	void Start () {
		TextAsset binAsset = Resources.Load ("random", typeof(TextAsset)) as TextAsset;         
		
		//读取每一行的内容  
		string [] lineArray = binAsset.text.Split ("\n"[0]);  
		
		//创建二维数组  
		scoreData = new string [lineArray.Length][];  
		
		//把csv中的数据储存在二位数组中  
		for(int i = 0; i < lineArray.Length; i++)  
		{  
			scoreData[i] = lineArray[i].Split (',');  
		}

		itemGet = new int[scoreData.Length];

		Spacing = int.Parse (scoreData [0] [1]);
		Candy_number = int.Parse (scoreData [1] [1]);
		Bubble = int.Parse (scoreData [2] [1]);
		Gold = int.Parse (scoreData [3] [1]);

	}
	
	// Update is called once per frame
	void Update () {

	}

	public void randCandy () {//隨機掉落物件
		ran = UnityEngine.Random.Range (0, 100);

		if (ran <= Spacing) {

		}

		else if (Spacing < ran && ran <= Spacing + Candy_number) {
			GameObject Candy  = Instantiate((Resources.Load("Bronze")), this.gameObject.transform.position + new Vector3 (0, 0, 1), Quaternion.identity) as GameObject;
		}

		else if (Spacing + Candy_number < ran && ran <= Spacing + Candy_number + Bubble) {

			GameObject Candy  = Instantiate((Resources.Load("Silver")), this.gameObject.transform.position + new Vector3 (0, 0, 1), Quaternion.identity) as GameObject;
		}

		else if (Spacing + Candy_number + Bubble < ran && ran <= Spacing + Candy_number + Bubble + Gold) {
			GameObject Candy  = Instantiate((Resources.Load("Gold")), this.gameObject.transform.position + new Vector3 (0, 0, 1), Quaternion.identity) as GameObject;
			
		}


	}

}
