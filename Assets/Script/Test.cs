﻿using UnityEngine;
using System.Collections;

public class Test : MonoBehaviour
{

	public GameObject item;
	public GameObject FakeStage;

	//Get Save
	public SaveMyData saveSc;
	//Scroll View===
	public UIGrid scoll;
	public LoadCSV CSVsc;
	public int stageNumber = 0;
	public bool isGotStage = false;
	public int stageNow = 1;
	public float gridValue = 0;
	private float islandWidth = 410;
	public GameObject StageLabel;

	//2014.07.17 場景換圖 Jason
	public GameObject uisp;
	public GameObject uisp_foreGround;
	//2014.7.25 島嶼換圖 Jason
	public Sprite[] bg5;

	void Start ()
	{

		//產生 Level_Panel & Stage_Panel
		ProduceLevelPlayPanel ();
		
		scoll = GameObject.FindGameObjectWithTag ("Island").GetComponent<UIGrid> ();
		CSVsc = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent <LoadCSV> ();
		saveSc = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent <SaveMyData> ();
		StageLabel = GameObject.FindGameObjectWithTag ("StageLabel");

		//2014.07.17 場景換圖 Jason
		uisp = GameObject.FindGameObjectWithTag ("World_Panel");
		uisp_foreGround = GameObject.FindGameObjectWithTag ("ForeGround");



	}

	void Update ()
	{

		//產生 小島
		ProduceIsland ();
		//取得 Stage
		GetStageInfo ();
		//佈置Label Brick Name
		SetLevelBrickName ();

	}





	//產生 Level_Panel & Stage_Panel(如果是Play Next)==============
	private void ProduceLevelPlayPanel ()
	{

		LevelSelcet selectSc = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent <LevelSelcet> ();

		if (LevelSelcet.openLevelPanel)
		{
			selectSc.OpenLevelNGUI ();
			//LevelSelcet.openLevelPanel=false;
		}
		if (LevelSelcet.openPlayPanel)
		{
			selectSc.ProducePlayPanel ();
			//LevelSelcet.openPlayPanel=false;
		}
	}
	//產生 小島===================================
	public void ProduceIsland ()
	{
		if (CSVsc)
		{
			stageNumber = CSVsc.GetAllStageNumber ();
		}
		
		if (stageNumber > 0 && !isGotStage)
		{

			//設定解鎖陣列大小
//			InitClearinfo();


			for (int a=1; a<=stageNumber; a++)
			{
				GameObject o = (GameObject)Instantiate (item);
				o.transform.name = "stage0" + a;
				o.transform.parent = scoll.transform;
				o.transform.localPosition = new Vector3 (0, 0, 0);
				o.transform.localScale = new Vector3 (37, 35, 1);
				//o.transform.GetComponent<UISprite>().spriteName="stage0"+a;
				o.transform.GetComponent<SpriteRenderer> ().sprite = bg5 [stageNow + a - 1];

				o.GetComponentInChildren<StageLock> ().index = a;

				scoll.repositionNow = true;
			}

			//fake stage
			/*GameObject t =(GameObject) Instantiate(FakeStage);
			t.transform.parent = scoll.transform;
			t.transform.localPosition = new Vector3(0,0,0);
			t.transform.localScale = new Vector3(1,1,1);*/

			isGotStage = true;
		}
	}
	//取得 Stage==================================
	public void GetStageInfo ()
	{
		if (this.transform.GetComponent<SpringPanel> ())
		{
			gridValue = this.transform.GetComponent<SpringPanel> ().target.x;
		}

		for (int a=1; a<=stageNumber+1; a++)
		{
			if (gridValue < -islandWidth * (a - 1) && gridValue > -islandWidth * a)
			{
				stageNow = a;
			} 
		}


		//StageLabel.GetComponent<UILabel>().text="STAGE "+stageNow; 


		//2014.07.17 場景換圖
		//uisp.GetComponent<UISprite> ().spriteName = "bg03_1";  
		uisp.GetComponent<UISprite> ().spriteName = "bg0" + stageNow + "_1";
		//uisp_foreGround.GetComponent<UISprite> ().spriteName = "bg03_2";
		uisp_foreGround.GetComponent<UISprite> ().spriteName = "bg0" + stageNow + "_2";
		//------------------

	}
	//佈置Label Brick Name========================
	public void SetLevelBrickName ()
	{
//		print (stageNow);

		int levelNumber = CSVsc.GetLevelNumberOfThisStage (stageNow);
		int[] clearArr = saveSc.GetStage_Clear (stageNow);
		int[] bellNumberTemp = saveSc.GetStage_Star (stageNow);

		for (int i=1; i<=20; i++)
		{
			string tempName = "lv" + i;

			if (GameObject.FindGameObjectWithTag (tempName))
			{	
				Transform[] allChildren1 = GameObject.FindGameObjectWithTag (tempName).GetComponentsInChildren<Transform> ();
				foreach (Transform child in allChildren1)
				{
					if (i <= levelNumber)
					{	//顯示 Stage-Level 文字=======================================
						if (child.name == "Label")
						{
							child.GetComponent<UILabel> ().text = i.ToString ();
						}
						//顯示 BELL==================================================
						int LevelBell = bellNumberTemp [i - 1];
						switch (LevelBell)
						{
							case 0:
								if (child.name == "bell1")
								{
									child.transform.GetComponent<UISprite> ().enabled = false;
								}
								if (child.name == "bell2")
								{
									child.transform.GetComponent<UISprite> ().enabled = false;
								}
								if (child.name == "bell3")
								{
									child.transform.GetComponent<UISprite> ().enabled = false;
								}
								break;
							
							case 1:
								if (child.name == "bell1")
								{
									child.transform.GetComponent<UISprite> ().enabled = true;
								}
								if (child.name == "bell2")
								{
									child.transform.GetComponent<UISprite> ().enabled = false;
								}
								if (child.name == "bell3")
								{
									child.transform.GetComponent<UISprite> ().enabled = false;
								}
								break;
							
							case 2:
								if (child.name == "bell1")
								{
									child.transform.GetComponent<UISprite> ().enabled = true;
								}
								if (child.name == "bell2")
								{
									child.transform.GetComponent<UISprite> ().enabled = true;
								}
								if (child.name == "bell3")
								{
									child.transform.GetComponent<UISprite> ().enabled = false;
								}
								break;
							
							case 3:
								if (child.name == "bell1")
								{
									child.transform.GetComponent<UISprite> ().enabled = true;
								}
								if (child.name == "bell2")
								{
									child.transform.GetComponent<UISprite> ().enabled = true;
								}
								if (child.name == "bell3")
								{
									child.transform.GetComponent<UISprite> ().enabled = true;
								}
								break;
						}
					}

					//鎖住 關卡===================================================
					if (child.name == "Lock_Background")
					{
						if (i > levelNumber)
						{	//鎖住，直接空白
							child.GetComponent<UISprite> ().enabled = true;
							child.GetComponent<BoxCollider> ().enabled = true;
						}
						else if (i <= levelNumber)
						{
							//Stage1 Level 1
							if (stageNow == 1 && i == 1)
							{
								child.GetComponent<UISprite> ().enabled = false;
								child.GetComponent<BoxCollider> ().enabled = false;
							}
							else
							{	
								//Stage2以上
								if (i > 1)
								{//第二關以上
									if (clearArr [i - 2] == 1)
									{
										child.GetComponent<UISprite> ().enabled = false;
										child.GetComponent<BoxCollider> ().enabled = false;
									}
									else
									{
										child.GetComponent<UISprite> ().enabled = true;
										child.GetComponent<BoxCollider> ().enabled = true;
									}
								}
								else if (i == 1)
								{	
									//X-1
//									int[] beforeArr = saveSc.GetStage_Clear (stageNow - 1);
//									int x = beforeArr [CSVsc.GetLevelNumberOfThisStage (stageNow - 1) - 1];
//									if (x == 1) {
									child.GetComponent<UISprite> ().enabled = false;
									child.GetComponent<BoxCollider> ().enabled = false;
//									} else {
//										child.GetComponent<UISprite> ().enabled = true;
//										child.GetComponent<BoxCollider> ().enabled = true;
//									}
								}
							}
						}
					}
				}
			}


		}
	}
	//設定關卡名
	public void SetLevelName (GameObject ob)
	{

		string a = ob.name;
		a = a.Replace ("lv", "");

		int st = GameObject.FindGameObjectWithTag ("Island").GetComponent<Test> ().stageNow;
//		print (st);

		LevelSelcet.pressNow_LevelName = st + "-" + a;
		LevelSelcet.stageInt = st;
		LevelSelcet.levelInt = int.Parse (a);
	}
}
