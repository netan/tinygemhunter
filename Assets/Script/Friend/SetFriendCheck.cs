﻿using UnityEngine;
using System.Collections;

public class SetFriendCheck : MonoBehaviour {

	public MyParse myParse;
	public TestNetwork testNetwork;

	public GameObject scrollView;
	public GameObject FriendCheckItem;
	public float localPosition;

	// Use this for initialization
	void Start () {
		myParse = GameObject.Find("EventControoler").GetComponent<MyParse>();
		testNetwork = GameObject.Find("EventControoler").GetComponent<TestNetwork>();

	}
	
	// Update is called once per frame
	void Update () {
		if (testNetwork.isConnect && !testNetwork.isReConnect) {
			if (myParse.isGetFriendCheck) {
				myParse.isGetFriendCheck = false;
				GameObject[] destoryFriendCheck = GameObject.FindGameObjectsWithTag("FriendCheck");
				foreach (GameObject d in destoryFriendCheck) {
					Destroy(d);
				}
				localPosition = 127.0f;
				for (int i = 0; i < myParse.friendCheck.Count; i++) {
					GameObject clone = NGUITools.AddChild (scrollView, FriendCheckItem);
					clone.transform.localPosition = new Vector3(0, localPosition, 0);
					clone.GetComponent<SetFriendCheckData>().SetData(myParse.friendCheckNickname[i].ToString(), myParse.friendCheck[i].ToString());
					localPosition -= 62.0f;
				}
				myParse.isGetFriendCheckRef = true;
			}
		}
	}
}
