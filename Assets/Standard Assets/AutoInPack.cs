﻿using UnityEngine;
using System.Collections;

public class AutoInPack : MonoBehaviour {
	
	public GameObject Pack;
	public GameObject Pack_parent;
	public float speed;
	public string SpriteName="";
	public string path="";


	void Start() {
		speed = 2;
		this.transform.GetComponent<SpriteRenderer>().sprite=Resources.Load(path+SpriteName, typeof(Sprite)) as Sprite;
	}
	

	void Update() {

		Pack = GameObject.FindGameObjectWithTag ("Pack");
		Pack_parent = GameObject.FindGameObjectWithTag ("Pack_Parent");

		if(Pack) {

			this.transform.position = Vector3.Lerp (this.transform.position ,Pack.transform.position,speed * Time.deltaTime);

			float dist = Vector3.Distance(this.transform.position, Pack.transform.position);

			if(dist<3) {
				if(!Pack_parent.animation.isPlaying) {
					Pack_parent.transform.animation.Play();
				}
			}
			if(dist<2) {
				if(this.SpriteName == "B")
				{
					GameObject ItemGot = Instantiate (Resources.Load("Game_Score/Particle_Score"), new Vector3(-6.5f, -11.6f, 0), Quaternion.identity) as GameObject;
					ItemGot.GetComponent<ParticleSystem>().renderer.material.mainTexture = Resources.Load("Game_Score/1") as Texture2D;
					/*ScorePopOut itemSc = ItemGot.GetComponent<ScorePopOut>();	
					itemSc.path="Game_Score/";
					itemSc.SpriteName="1";*/
						
				}
				else if(this.SpriteName == "S")
				{
					GameObject ItemGot = Instantiate (Resources.Load("Game_Score/Particle_Score"), new Vector3(-6.5f, -11.6f, 0), Quaternion.identity) as GameObject;
					ItemGot.GetComponent<ParticleSystem>().renderer.material.mainTexture = Resources.Load("Game_Score/2") as Texture2D;
					/*ScorePopOut itemSc = ItemGot.GetComponent<ScorePopOut>();	
					itemSc.path="Game_Score/";
					itemSc.SpriteName="1";*/
					
				}
				else if(this.SpriteName == "G")
				{
					GameObject ItemGot = Instantiate (Resources.Load("Game_Score/Particle_Score"), new Vector3(-6.5f, -11.6f, 0), Quaternion.identity) as GameObject;	
					ItemGot.GetComponent<ParticleSystem>().renderer.material.mainTexture = Resources.Load("Game_Score/5") as Texture2D;
					/*ScorePopOut itemSc = ItemGot.GetComponent<ScorePopOut>();	
					itemSc.path="Game_Score/";
					itemSc.SpriteName="1";*/
					
				}
				else{}
				Destroy(this.gameObject);
			}
		}
	}
}
