﻿using UnityEngine;
using System.Collections;

public class CreateLevelUI : MonoBehaviour {

	public StageMove stageMove;

	public void Start() {
		stageMove = GameObject.Find ("Island").GetComponent<StageMove> ();
		stageMove.ChangeCount(true);
		Destroy(this);
	}

}
