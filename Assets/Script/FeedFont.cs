﻿using UnityEngine;
using System.Collections;

public class FeedFont : MonoBehaviour {

	private UILabel label;
	// Use this for initialization
	void Start () {
		label = GetComponent<UILabel> ();
		label.trueTypeFont = Resources.Load("Font/JF Flat regular", typeof(Font)) as Font;
	}

}
