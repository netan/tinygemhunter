﻿using UnityEngine;
using System.Collections;

public class OnTouchDown : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		RaycastHit hit = new RaycastHit();
		for (int i = 0; i < Input.touchCount; ++i) {
			if (Input.GetTouch(i).phase.Equals(TouchPhase.Began)) {
				// Construct a ray from the current touch coordinates
				Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
				if (Physics.Raycast(ray, out hit , Mathf.Infinity ,1 << 12 | 1 << 14)){
				//Debug.Log(" You just hit " + hit.collider.gameObject.name);
					if(hit.transform.gameObject.name == "dragon")
					{
					}
					else
					{
					hit.transform.gameObject.SendMessage("OnMouseDown");
					}
				}
			}
		}
	}
}
