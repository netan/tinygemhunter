﻿using UnityEngine;
using System.Collections;

public class PlayBellAnimation : MonoBehaviour {

	public GameObject[] bell;
	public int index;
	public int max;
	public GameObject[] shiny;
	public int scaleState;
	public float delayTime;

	// Use this for initialization
	void Start () {
		index = -1;
		scaleState = 2;
		max = GameObject.Find ("Conversation_NGUI").GetComponent<CharacterScript> ().Bell_Number;
	
		//print (max);
		for (int i = 0; i < max; i++) {
			bell[i].transform.localScale = new Vector3(0,0,0);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (scaleState == 0) {
			bell[index].transform.localScale -= new Vector3(1,1,1) * Time.deltaTime * 15;
			if (bell[index].transform.localScale.x <= 1) {
				bell[index].transform.localScale = new Vector3(1,1,1);
				scaleState = 1;
			}
		} else if (scaleState == 1) {
			if (index < max) {
				shiny[index].GetComponent<Animator>().enabled = true;
				shiny[index].GetComponent<Animator>().Play("GameShiny");
				bell[index].GetComponent<UIPlaySound>().Play();
				delayTime = 0.25f;
				scaleState = 2;
			} else {
				this.enabled = false;
			}
		} else if (scaleState == 2) {
			if (delayTime <= 0 && index < max) {
				index++;
				if (index == max) {
					this.enabled = false;
				} else {
					bell[index].transform.localScale = new Vector3(5,5,5);

				}
				scaleState = 0;
			}
		}
		delayTime -= Time.deltaTime;
	}
}
