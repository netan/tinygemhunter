using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using System;

public class AD_ADMob : IAD
{
	private InterstitialAd interstitial;

	void IAD.Cache ()
	{
		Debug.Log ("AD_ADMob Init");
		
		#if UNITY_ANDROID
		string adUnitId = "INSERT_ANDROID_INTERSTITIAL_AD_UNIT_ID_HERE";
		#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-5883184458438348/1589362718";
		#else
		string adUnitId = "unexpected_platform";
		#endif
		
		// Initialize an InterstitialAd.
		interstitial = new InterstitialAd (adUnitId);
		interstitial.AdLoaded += (object sender, System.EventArgs e) => HandleAdLoaded ();
		interstitial.AdFailedToLoad += (object sender, AdFailedToLoadEventArgs e) => HandleAdFailedToLoad (e);
		interstitial.AdClosing += (object sender, System.EventArgs e) => HandleAdClosing ();

		// Create an empty ad request.
		// AdRequest request = new AdRequest.Builder ().Build ();
		AdRequest request = new AdRequest.Builder ()
			.AddTestDevice (AdRequest.TestDeviceSimulator)       // Simulator.
			.AddTestDevice ("6ba94f0e60e48f9a78a49b068a094130972486f6")  // My test IPad
			.Build ();

		interstitial.LoadAd (request);
	}

	void IAD.Display ()
	{
		if (interstitial.IsLoaded ()) {
//			Debug.Log ("Display AdMob => Show Admob ");
			interstitial.Show ();
		} else {
			Debug.LogWarning ("Need Cache AD");
		}
	}

	void IAD.Dispose ()
	{

	}

	private void HandleAdLoaded ()
	{
		Debug.Log ("enter HandleAdLoaded");
	}

	private void HandleAdFailedToLoad (AdFailedToLoadEventArgs args)
	{
		Debug.Log ("enter HandleAdFailedToLoad " + args.Message);
	}

	private void HandleAdClosing ()
	{
		Debug.Log ("enter HandleAdClosing");
		interstitial.Destroy ();
	}
}

