﻿using UnityEngine;
using System.Collections;
using Prime31;

public class AddMallMoney : MonoBehaviour
{
	public string purchaseProduct;
#if UNITY_IOS
	private StoreKitProduct product;
#endif

	void OnClick ()
	{
		#if UNITY_ANDROID
		GoogleIAB.purchaseProduct( purchaseProduct, "payload that gets stored and returned" );
		Debug.Log ("im here");
		#endif

#if UNITY_IOS
		if (product != null )
		{
			StoreKitBinding.purchaseProduct( product.productIdentifier, 1 );
		}
#endif
	}
#if UNITY_IOS
	public void ApplyStoreKitProduct (StoreKitProduct _product)
	{
		this.product = _product;
	}
#endif
}
	
