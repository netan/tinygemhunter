﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;



public class MyStage : MonoBehaviour {
	
	//Combo
	public int dragonCombo = 0;
	public int comboLv1;//Fucking Table
	
	
	//Health & Life(Player Call This)
	public int dragonHealth=0;
	public int dragonLife = 0;
	//攝影機
	public GameObject NGUI_Camera;
	//遊戲狀態
	public int gameStatus=0;
	/*
	0:Loading
	1:Story
	2:Show Condition & Walk
	3:Teach
	4:Go
	5:Play
	6:Clear
	7:Story
	 */
	
	//NGUI Go Text 
	public GameObject Text_GO;
	
	
	//Prefabs=======================
	public GameObject Player;
	
	public GameObject Prefabs;
	public GameObject PrefabsRandom;
	//ADD
	public GameObject Sand;//1
	public GameObject Soil;//2
	public GameObject Rock;//3
	public GameObject Red;//4
	public GameObject Valcon;//5
	public GameObject Black;//6
	
	public GameObject Sand_Blue;//7
	public GameObject Sand_Green;//8
	public GameObject Sand_Red;//9
	public GameObject Sand_Silver;//10
	public GameObject Sand_Gold;//11
	
	public GameObject Soil_Blue;//12
	public GameObject Soil_Green;//13
	public GameObject Soil_Red;//14
	public GameObject Soil_Silver;//15
	public GameObject Soil_Gold;//16
	
	public GameObject Rock_Blue;//17
	public GameObject Rock_Green;//18
	public GameObject Rock_Red;//19
	public GameObject Rock_Silver;//20
	public GameObject Rock_Gold;//21
	
	public GameObject Red_Blue;//22
	public GameObject Red_Green;//23
	public GameObject Red_Red;//24
	public GameObject Red_Silver;//25
	public GameObject Red_Gold;//26
	
	public GameObject Valcon_Blue;//27
	public GameObject Valcon_Green;//28
	public GameObject Valcon_Red;//29
	public GameObject Valcon_Silver;//30
	public GameObject Valcon_Gold;//31
	
	public GameObject Star1;//32
	public GameObject Star2;//33
	public GameObject Star3;//34
	public GameObject Treasure;//35
	public GameObject ThunderBall;//36
	public GameObject ToxicBall;//37
	public GameObject Bumb;//38
	public GameObject Bell;//39
	public GameObject Sand_Bell;//40
	public GameObject Soil_Bell;//41
	public GameObject Rock_Bell;//42
	public GameObject Red_Bell;//43
	public GameObject Volcan_Bell;//44
	public GameObject Fan_R;//45
	public GameObject Fan_L;//46
	public GameObject Ice;//47
	public GameObject GunPowder;//48
	//support 
	public GameObject Love;//49
	public GameObject Hourglass;//50
	//=======================Prefabsr
	
	//Number========================
	public int Sand_Number;//1
	public int Soil_Number;//2
	public int Rock_Number;//3
	public int Red_Number;//4
	public int Valcon_Number;//5
	public int Black_Number;//6
	
	public int BlueGem_Number;
	public int GreenGem_Number;
	public int RedGem_Number;
	public int SilverGem_Number;
	public int GoldGem_Number;
	
	public int Bell_Number;
	public int Love_Number;
	public int Hourglass_Number;
	
	public int FanR_Number;
	public int FanL_Number;
	
	public int Treasure_Number;
	
	public int Star1_Number;
	public int Star2_Number;
	public int Star3_Number;
	
	public int ThunderBall_Number;
	public int ToxicBall_Number;
	public int Bumb_Number;
	
	public int Ice_Number;
	public int IceCover_Number;
	
	public int GunPowder_Number;
	
	public int Green_Number;
	
	
	//========================Number
	
	
	public int bellNumber=0;
	
	public string stage;
	public string level; 
	
	//前景
	public GameObject foreGround;
	public GameObject teachPic;
	
	public System.Collections.Generic.List<int> listMyItem;
	public bool isGameOver=false;
	public bool isWin=false;
	
	
	//Is .txt File Read ?
	public bool isRead=false;
	
	public float startPointX ;
	public float startPointY=0.0f; 
	public float blockWidth=1.875f;
	
	
	public bool Xmode = true;
	public bool Ymode = false;
	public int[][] jaggedArray = new int[10][];
	
	
	public string txtLv_1 = "";
	public string txtLv_2= "";
	public string txtPicture= "";
	public bool reborn = true;
	
	public System.Collections.Generic.List<string> arrayOfLimitedRule;
	public System.Collections.Generic.List<string> arrayOfLimitedRuleNumber;
	
	public float timer;
	public bool openTxt=false;
	
	public int mouseStatus=-1;
	
	//數量
	public int loveMax=0;
	public int star1Max =0;
	public int star2Max=0;
	public int star3Max =0;
	public int treasureMax =0;
	public int thunderBallMax=0;
	public int toxinBallMax =0;
	public int bumbMax=0;
	public int emptyMax =0;
	//出現機率
	public int lovePercent=0;
	public int star1Percent =0;
	public int star2Percent=0;
	public int star3Percent =0;
	public int treasurePercent =0;
	public int thunderBallPercent=0;
	public int toxinBallPercent =0;
	public int bumbPercent=0;
	public int emptyPercent =0;
	
	
	
	public int txtSupportMaxNumber;
	public string txtSupportRandomTime="";
	
	
	public string txtTimeLimited = "";
	public string txtLifeLimited= "";
	public bool timeLimited=false;//該關卡限制是否是看時間
	public bool lifeLimited=false;//該關卡限制是否是看次數
	
	public Texture2D[][] pictureArray=new Texture2D[10][];
	
	
	//	public float blockTimer = 0.0f;
	public float supportTimer = 0.0f;
	
	
	
	public string levelNameNow;
	
	public GameObject sound_Go;
	
	
	public LoadCSV loadSc;

	public bool isHourglass;
	public bool isMall_Hourglass;

	public int treasure_AllMax =0;

	public ArrayList gainItem = new ArrayList();

	public LoadTeach LT;

	public FrameGroundStar FG;

	public int brickX = 0;
	public int brickY = 0;

	public GameObject TrueLove;


	
	void Start () {	
		startPointY = 12.45f;
		isHourglass = false;
		isMall_Hourglass = false;
		Time.timeScale = 1;
		
		NGUI_Camera.SetActive (true);
		
		
		levelNameNow = LevelSelcet.pressNow_LevelName;
		string[] tempX = levelNameNow.Split ('-');
		stage = tempX [0];
		level = tempX [1];
		
		
		Init ();
		StartCoroutine(ReadFile ());//Read txt. FIle
		
		loadSc = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent<LoadCSV>();

		LT = GameObject.Find ("TeachData").GetComponent<LoadTeach> ();

		FG = GameObject.Find("FrameGround").GetComponent<FrameGroundStar> ();

	}
	
	
	
	
	
	
	
	
	
	//Blocks=====
	public int getBobNumber(){
		int BobNumber=GameObject.FindGameObjectsWithTag ("Bob").Length ;
		return BobNumber;
	}
	public int getSpidersilkNumber(){
		int SplidersilkNumber=GameObject.FindGameObjectsWithTag ("SpiderSilk").Length ;
		return SplidersilkNumber;
	}
	public int getBlockNumber(){
		int BobNumber=GameObject.FindGameObjectsWithTag ("Bob").Length ;
		int SplidersilkNumber=GameObject.FindGameObjectsWithTag ("SpiderSilk").Length ;
		int blockNumber = BobNumber + SplidersilkNumber;
		return blockNumber;
	}
	//Bricks=====
	public int getAllBlockWithoutBlack(){
		int brick1Number=GameObject.FindGameObjectsWithTag ("Brick1").Length ;
		int brick2Number=GameObject.FindGameObjectsWithTag ("Brick3").Length ;
		int brick3Number=GameObject.FindGameObjectsWithTag ("Brick3").Length ;
		return brick1Number+brick2Number+brick3Number;
	}
	
	
	//Supports=====
	public int getLoveNumber(){
		int loveNumber=GameObject.FindGameObjectsWithTag ("Love").Length ;
		return loveNumber;
	}
	public int getHourGlassNumber(){
		int HourGlassNumber=GameObject.FindGameObjectsWithTag ("HourGlass").Length ;
		return HourGlassNumber;
	}
	
	public int getStar1Number(){
		int star1Number=GameObject.FindGameObjectsWithTag ("Star1").Length ;
		return star1Number;
	}
	public int getStar2Number(){
		int star2Number=GameObject.FindGameObjectsWithTag ("Star2").Length ;
		return star2Number;
	}
	public int getStar3Number(){
		int star3Number=GameObject.FindGameObjectsWithTag ("Star3").Length ;
		return star3Number;
	}
	public int getTreasureNumber(){
		int teasureNumber=GameObject.FindGameObjectsWithTag ("Treasure").Length ;
		return teasureNumber;
	}
	public int getThunderBallNumber(){
		int thunderBallNumber=GameObject.FindGameObjectsWithTag ("ThunderBall").Length ;
		return thunderBallNumber;
	}
	public int getToxicBallNumber(){
		int toxicBallNumber=GameObject.FindGameObjectsWithTag ("ToxicBall").Length ;
		return toxicBallNumber;
	}	
	public int getBumbNumber(){
		int bumbNumber=GameObject.FindGameObjectsWithTag ("Bumb").Length ;
		return bumbNumber;
	}
	
	
	public int getAllSupportNumber(){
		int loveNumber=GameObject.FindGameObjectsWithTag ("Love").Length ;
		int hourglassNumber=GameObject.FindGameObjectsWithTag ("HourGlass").Length ;
		int star1Number=GameObject.FindGameObjectsWithTag ("Star1").Length ;
		int star2Number=GameObject.FindGameObjectsWithTag ("Star2").Length ;
		int star3Number=GameObject.FindGameObjectsWithTag ("Star3").Length ;
		int teasureNumber=GameObject.FindGameObjectsWithTag ("Treasure").Length ;
		int ThunderBallNumber=GameObject.FindGameObjectsWithTag ("ThunderBall").Length ;
		int ToxicBallNumber=GameObject.FindGameObjectsWithTag ("ToxicBall").Length ;
		int BumbNumber=GameObject.FindGameObjectsWithTag ("Bumb").Length ;
		
		
		return loveNumber + hourglassNumber+ star1Number + star2Number + star3Number + teasureNumber+
			ThunderBallNumber+ToxicBallNumber+BumbNumber;
	}
	
	
	public float goTimer=0;
	public bool isGone=false;
	public Sprite pic;
	
	
	
	void insGoTime(){
		goTimer+=Time.deltaTime*1;
	}
	
	public float teachTimer=0;
	
	void insTeachTime(){
		teachTimer+=Time.deltaTime*1;
	}
	
	
	
	void Update () {
		
		brickNumber = getAllBrickNumber ();
		
		//	print("遊戲狀態:"+gameStatus);
		//	print (Time.timeScale);
		/*
	0:Loading
	1:Story
	2:Show Condition & Walk
	3:Teach
	4:Go
	5:Play
	6:GameOver Condition & Clear
	7:Story
	 */
		switch(gameStatus)
		{
			
			//讀取檔案
		case 0:
			if(isRead)
				gameStatus=1;
			break;
			
			//播放故事
		case 1:
			break;
			
			//過關條件 & 小恐龍走路
		case 2:
			break;
			
			//教學圖片
		case 3:
			insTeachTime();
			float timeX=loadSc.GetTeachPicDisapppearTime();
			
			pic=Resources.Load("Teach/"+LevelSelcet.pressNow_LevelName+"-1", typeof(Sprite)) as Sprite;
			teachPic.GetComponent<SpriteRenderer>().sprite=pic;
			
			if(teachTimer>=timeX){
				teachPic.GetComponent<FadeOut>().enabled=true;
				teachTimer=0F;
				gameStatus=4;
			}
			break;
			
			//Go
		case 4:
			insGoTime();
			
			if(!isGone)
			{
				isGone=true;
				
				Text_GO.SetActive(true);
				(Instantiate (sound_Go, this.gameObject.transform.position, Quaternion.identity) as GameObject).transform.parent=GameObject.FindGameObjectWithTag("SoundBox").transform;//GO 聲音
			}
			if(goTimer>=2.0f)
			{
				Text_GO.SetActive(false);
				goTimer=0;
				gameStatus=5;
			}
			break;
			
			//Play Game
		case 5:
			
			//隨機增加 補給品
			//RandomAddSupport();		
			break;
			
			//GameOver Condition & Clear
		case 6:
			//frameground play
			FG.isPlay = false;
			break;
			
			//Story
		case 7:
				//Time.timeScale=0;

			break;	
			
		case 8:
			
			break;			
			
		case 9:


			if (LT.index == LT.length) {

				gameStatus=10;
			}
			break;
			
		}
		
		
		
		
		
	}
	
	
	public int Love_weight=0;
	public int Star1_weight=0;
	public int Star2_weight=0;
	public int Star3_weight=0;
	public int Treasure_weight=0;
	public int ThunderBall_weight=0;
	public int ToxicBall_weight=0;
	public int Bumb_weight=0;
	public int Empty_weight=0;
	
	public int brickNumber=0;
	
	
	//Reset Brick Position
	public void getEmptyMessage(int x,int y){
		
		
		/*int posA =(int)(x - startPointX) / (int)blockWidth;
		int posB =(int)(y - startPointY) / (int)-blockWidth;
	
		//Reset 0
		
		if(Xmode)
		{
			if(posB%2==0)
				jaggedArray[posB][posA]=0;
			else
				jaggedArray[posB][posA]=0;
		}
		else{
			jaggedArray[posB][posA]=0;
		}*/
		
		//		print (posB+","+posA+"為空!");


		jaggedArray[x][y] = 0;
	}


	
	
	
	public bool isCalled=false;
	public bool isPercent = false;
	
	
	
	public void RandomAddSupport(){
		
		
		////supportTimer+= Time.deltaTime * 0.5f;
		
		
		////int ranTime = 0;
		
		if(!isPercent){
			
			
			Love_weight=lovePercent;
			Star1_weight=star1Percent+ Love_weight;
			Star2_weight=star2Percent+ Star1_weight;
			Star3_weight=star3Percent+ Star2_weight;
			Treasure_weight=treasurePercent+ Star3_weight;
			ThunderBall_weight=thunderBallPercent+ Treasure_weight;
			ToxicBall_weight=toxinBallPercent+ ThunderBall_weight;
			Bumb_weight=bumbPercent+ ToxicBall_weight;
			Empty_weight=emptyPercent+ Bumb_weight;
			
			isPercent=true;
		}
		
		
		
		//		print (txtSupportRandomTime+" "+txtSupportRandomTime);
		////if (txtSupportRandomTime != "") 
		////{
			////ranTime=int.Parse(txtSupportRandomTime);
			
			if(/*supportTimer>=ranTime &&*/  getAllSupportNumber() < txtSupportMaxNumber  )
			{
				int ran=UnityEngine.Random.Range (0, 101);
				
				if(ran<Love_weight)
				{
					if(getLoveNumber() < loveMax  &&
					   getHourGlassNumber()< loveMax)
					{
						if(lifeLimited)
						{
							//							if(supportTimer>0)
							RandomAdd(49);//愛心
							////supportTimer=0;
						}	
						else
						{
							//							if(supportTimer>0)
							RandomAdd(50);//沙漏
							////supportTimer=0;
						}
						
					}
				}
				else if(ran>=Love_weight && ran < Star1_weight)
				{
					if(getStar1Number() < star1Max )
					{
						//						if(supportTimer>0)
						RandomAdd(32);
						////supportTimer=0;
					}
					
				}
				else if(ran>=Star1_weight && ran < Star2_weight)
				{
					if(getStar2Number() < star2Max )
					{
						//						if(supportTimer>0)
						RandomAdd(33);
						////supportTimer=0;
					}
				}
				else if(ran>=Star2_weight && ran < Star3_weight)
				{
					if(getStar3Number() < star3Max )
					{
						//						if(supportTimer>0)
						RandomAdd(34);
						////supportTimer=0;
					}					
				}
				else if(ran>=Star3_weight && ran < Treasure_weight && treasure_AllMax <= 3)
				{
					if(getTreasureNumber() < treasureMax )
					{
						//						if(supportTimer>0)
						RandomAdd(35);
					treasure_AllMax++;
						////supportTimer=0;
					}						
				}
				else if(ran>=Treasure_weight && ran < ThunderBall_weight )
				{
					if(getThunderBallNumber() < treasureMax )
					{
						//						if(supportTimer>0)
						RandomAdd(36);
						////supportTimer=0;
					}	
				}
				else if(ran>=ThunderBall_weight && ran < ToxicBall_weight)
				{
					if(getToxicBallNumber() < treasureMax )
					{
						//						if(supportTimer>0)
						RandomAdd(37);
						////supportTimer=0;
					}	
				}			
				else if(ran>=ToxicBall_weight && ran < Bumb_weight)
				{
					if(getBumbNumber() < treasureMax )
					{
						//						if(supportTimer>0)
						RandomAdd(38);
						////supportTimer=0;
					}	
				}	
				else if(ran>=Bumb_weight && ran < Empty_weight)//Empty
				{
					////supportTimer=0;
				}	
			}			
		//}
	}
	
	
	IEnumerator MyMethod() {
		//		Debug.Log("Before Waiting 2 seconds");
		yield return new WaitForSeconds(0.2f);
		//		Debug.Log("After Waiting 2 Seconds");
	}
	
	
	
	public void RandomAdd(int valueBlock){
		//		print ("Random");
		
		//		bool ok = false;
		
		int ranY=0;
		int ranX=0;
		
		if (Xmode) {//Xmode 最大75
			//>=A && <B 
			ranX=UnityEngine.Random.Range (0, 10);//Y
			
			if(ranX%2==0)
				ranY=UnityEngine.Random.Range (0, 8);//X
			else
				ranY=UnityEngine.Random.Range (0, 7);//X
		} else {
			ranY=UnityEngine.Random.Range (0, 8);//X
			ranX=UnityEngine.Random.Range (0, 10);//Y
		}
		
		
		
		
		if(Xmode){//Xmode 最大75
			if(brickNumber<75 ){
				
				if(jaggedArray[ranX][ranY]==0)
				{
					jaggedArray[ranX][ranY]=valueBlock;
					addBlocksRandom (valueBlock, ranX, ranY);
					
				}
				else
				{
					StartCoroutine(MyMethod());
					RandomAdd(valueBlock);	
				}
				
			}
		}
		else//Ymode 最大80
		{
			if(brickNumber<80){
				
				if(jaggedArray[ranX][ranY]==0)
				{
					jaggedArray[ranX][ranY]=valueBlock;
					addBlocksRandom (valueBlock, ranX, ranY);
				}
				else
				{
					StartCoroutine(MyMethod());
					RandomAdd(valueBlock);
					
				}
			}
		}
		
		
		
		
		
	}
	
	
	int getAllBrickNumber(){
		int allNumber = 0;
		for(int a=0;a<jaggedArray.Length;a++){
			string tmp = "";
			for(int b=0;b<jaggedArray[a].Length;b++){
				tmp += jaggedArray[a][b] + " ";
				if(jaggedArray[a][b]!=0) {
					allNumber++;
				}
			}
			//Debug.Log(tmp);
			tmp = "";
		}
		//Debug.Log("-----------------");
		return allNumber;
	}
	
	
	
	
	
	
	
	//	void CreateFile(string path,string name,string info) 
	//	{ 
	//		//文件流信息 
	//		StreamWriter sw; 
	//		FileInfo t = new FileInfo(path+"//"+ name); 
	//
	//			if(!t.Exists) 
	//			{ 
	//				sw = t.CreateText(); 
	//			tex.guiText.text="CREATE";
	//			} 
	//			else 
	//			{ 
	//			sw = t.CreateText();
	////			sw = t.AppendText(); 
	//			tex.guiText.text="EXSIT";
	//			} 
	//				sw.WriteLine(info); 
	//				sw.Close(); 
	//				sw.Dispose(); 
	//	
	//	}   
	
	
	
	
	
	//	ArrayList LoadFile(string path,string name) 
	//	{ 
	//
	//		//使用流的形式读取 
	//		StreamReader sr =null; 
	//		try{  
	//			sr = File.OpenText(path+"/"+ name); 
	//		}catch(Exception e) 
	//		{ 
	//			print ("ERROR");
	//			return null; 
	//		} 
	//		string line; 
	//		ArrayList arrlist = new ArrayList(); 
	//		while ((line = sr.ReadLine()) != null) 
	//		{ 
	//			arrlist.Add(line); 
	//		} 
	//		sr.Close(); 
	//		sr.Dispose(); 
	//
	//		
	//		return arrlist; 
	//		
	//	}  
	
	
	
	//	void biuldFile(){
	//
	//		string[] allLines = System.IO.File.ReadAllLines(levelNameNow+".txt");
	//
	//		string path = Application.persistentDataPath + "/";
	//
	//		CreateFile (path, "1-1.txt", allLines[0]);
	//		tex.guiText.text = "File Created!";
	////		print ("File Created!");
	//	}
	
	
	
	IEnumerator ReadFile() {
		
		string path;
		//在Android中实例化WWW不能在路径前面加"file://"  
		if (Application.platform == RuntimePlatform.Android) {
			path =  Application.streamingAssetsPath + "/"+levelNameNow+".txt";
		} 
		else 
		{
			path="file://" +Application.streamingAssetsPath+ "/"+levelNameNow+".txt";
			//			path=Application.streamingAssetsPath+ "/"+levelNameNow+".txt";
		}
		//          FileInfo fileinfo=new FileInfo(path);  不能在Android中使用  
		//          StreamReader r=fileinfo.OpenText();   
		WWW www=new WWW(path);///WWW读取在各个平台上都可使用  
		yield return www;   
		
		//		tex.guiText.text=www.text;
		loadLevelData (www.text);
		
		
	}
	
	void Init(){
		//		blockWidth = Sand.gameObject.transform.renderer.bounds.size.x;
		
		jaggedArray [0] = new int[] {0,0,0,0,0,0,0,0};
		jaggedArray [1] = new int[] {0,0,0,0,0,0,0,0};
		jaggedArray [2] = new int[] {0,0,0,0,0,0,0,0};
		jaggedArray [3] = new int[] {0,0,0,0,0,0,0,0};
		jaggedArray [4] = new int[] {0,0,0,0,0,0,0,0};
		jaggedArray [5] = new int[] {0,0,0,0,0,0,0,0};
		jaggedArray [6] = new int[] {0,0,0,0,0,0,0,0};
		jaggedArray [7] = new int[] {0,0,0,0,0,0,0,0};
		jaggedArray [8] = new int[] {0,0,0,0,0,0,0,0};
		jaggedArray [9] = new int[] {0,0,0,0,0,0,0,0};
		
		
		
		
		pictureArray [0] = new Texture2D[8];
		pictureArray [1] = new Texture2D[8];
		pictureArray [2] = new Texture2D[8];
		pictureArray [3] = new Texture2D[8];
		pictureArray [4] = new Texture2D[8];
		pictureArray [5] = new Texture2D[8];
		pictureArray [6] = new Texture2D[8];
		pictureArray [7] = new Texture2D[8];
		pictureArray [8] = new Texture2D[8];
		pictureArray [9] = new Texture2D[8];
		
		Xmode = true;
		Ymode = false;
	}
	
	
	void addPlayer(){
		Vector3 playerPos = new Vector3(0,0, 0);
		Instantiate(Player, playerPos, Quaternion.identity);
	}
	
	
	
	void addBlocksRandom(int valueBlock,int aBlock,int bBlock){
		//		print (aBlock+" "+bBlock);
		
		
		if(valueBlock==0)
		{
			return;
		}
		else{
			
			if(Xmode)
			{
				if(aBlock%2==0)//可能會出錯1?
					startPointX = -4*blockWidth+blockWidth/2;
				else
					startPointX = -4*blockWidth+blockWidth;
				
				
				Vector3 dropPos = new Vector3(startPointX+blockWidth*bBlock, startPointY-aBlock*blockWidth, 0);
				
				CreatePrefab(valueBlock,dropPos, aBlock, bBlock);
				
			}
			else//Ymode
			{
				startPointX = -4*blockWidth+blockWidth/2;
				
				Vector3 dropPos = new Vector3(startPointX+blockWidth*bBlock, startPointY-aBlock*blockWidth, 0);
				
				CreatePrefab(valueBlock,dropPos, aBlock, bBlock);
				
			}
		}
	}
	
	void addBlocks(int valueBlock,int aBlock,int bBlock){
		
		blockWidth = Sand.gameObject.transform.renderer.bounds.size.x;
		
		if(valueBlock==0)
		{
			return;
		}
		else{
			
			if(Xmode)
			{
				if(aBlock%2==0)
					startPointX = -4*blockWidth+blockWidth/2;
				else
					startPointX = -4*blockWidth+blockWidth;
				
				Vector3 dropPos = new Vector3(startPointX+blockWidth*bBlock, startPointY-aBlock*blockWidth, 0);
				
				CreatePrefab(valueBlock,dropPos, aBlock, bBlock);
				
			}
			else//Ymode
			{
				startPointX = -4*blockWidth+blockWidth/2;
				
				Vector3 dropPos = new Vector3(startPointX+blockWidth*bBlock, startPointY-aBlock*blockWidth, 0);
				
				CreatePrefab(valueBlock,dropPos, aBlock, bBlock);
			}
		}
	}
	
	
	
	
	void CreatePrefab(int valueBlock,Vector3 dropPos, int X, int Y){
		switch(valueBlock)
		{
		case 1:
			GameObject ob1=(GameObject)Instantiate(Sand, dropPos, Quaternion.identity);
			ob1.gameObject.transform.parent=Prefabs.transform;
			ob1.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob1.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 2:
			GameObject ob2=(GameObject)Instantiate(Soil, dropPos, Quaternion.identity);
			ob2.gameObject.transform.parent=Prefabs.transform;
			ob2.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob2.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 3:
			GameObject ob3=(GameObject)Instantiate(Rock, dropPos, Quaternion.identity);
			ob3.gameObject.transform.parent=Prefabs.transform;
			ob3.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob3.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 4:
			GameObject ob4=(GameObject)Instantiate(Red, dropPos, Quaternion.identity);
			ob4.gameObject.transform.parent=Prefabs.transform;
			ob4.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob4.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 5:
			GameObject ob5=(GameObject)Instantiate(Valcon, dropPos, Quaternion.identity);
			ob5.gameObject.transform.parent=Prefabs.transform;
			ob5.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob5.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 6:
			GameObject ob6=(GameObject)Instantiate(Black, dropPos, Quaternion.identity);
			ob6.gameObject.transform.parent=Prefabs.transform;
			ob6.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob6.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
			//Sand==================================
		case 7:
			GameObject ob7=(GameObject)Instantiate(Sand_Blue, dropPos, Quaternion.identity);
			ob7.gameObject.transform.parent=Prefabs.transform;
			ob7.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob7.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 8:
			GameObject ob8=(GameObject)Instantiate(Sand_Green, dropPos, Quaternion.identity);
			ob8.gameObject.transform.parent=Prefabs.transform;
			ob8.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob8.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 9:
			GameObject ob9=(GameObject)Instantiate(Sand_Red, dropPos, Quaternion.identity);
			ob9.gameObject.transform.parent=Prefabs.transform;
			ob9.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob9.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 10:
			GameObject ob10=(GameObject)Instantiate(Sand_Silver, dropPos, Quaternion.identity);
			ob10.gameObject.transform.parent=Prefabs.transform;
			ob10.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob10.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 11:
			GameObject ob11=(GameObject)Instantiate(Sand_Gold, dropPos, Quaternion.identity);
			ob11.gameObject.transform.parent=Prefabs.transform;
			ob11.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob11.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
			//Sand==================================
		case 12:
			GameObject ob12=(GameObject)Instantiate(Soil_Blue, dropPos, Quaternion.identity);
			ob12.gameObject.transform.parent=Prefabs.transform;
			ob12.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob12.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 13:
			GameObject ob13=(GameObject)Instantiate(Soil_Green, dropPos, Quaternion.identity);
			ob13.gameObject.transform.parent=Prefabs.transform;
			ob13.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob13.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 14:
			GameObject ob14=(GameObject)Instantiate(Soil_Red, dropPos, Quaternion.identity);
			ob14.gameObject.transform.parent=Prefabs.transform;
			ob14.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob14.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 15:
			GameObject ob15=(GameObject)Instantiate(Soil_Silver, dropPos, Quaternion.identity);
			ob15.gameObject.transform.parent=Prefabs.transform;
			ob15.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob15.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 16:
			GameObject ob16=(GameObject)Instantiate(Soil_Gold, dropPos, Quaternion.identity);
			ob16.gameObject.transform.parent=Prefabs.transform;
			ob16.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob16.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
			//Rock==================================
		case 17:
			GameObject ob17=(GameObject)Instantiate(Rock_Blue, dropPos, Quaternion.identity);
			ob17.gameObject.transform.parent=Prefabs.transform;
			ob17.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob17.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 18:
			GameObject ob18=(GameObject)Instantiate(Rock_Green, dropPos, Quaternion.identity);
			ob18.gameObject.transform.parent=Prefabs.transform;
			ob18.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob18.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 19:
			GameObject ob19=(GameObject)Instantiate(Rock_Red, dropPos, Quaternion.identity);
			ob19.gameObject.transform.parent=Prefabs.transform;
			ob19.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob19.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;			
		case 20:
			GameObject ob20=(GameObject)Instantiate(Rock_Silver, dropPos, Quaternion.identity);
			ob20.gameObject.transform.parent=Prefabs.transform;
			ob20.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob20.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 21:
			GameObject ob21=(GameObject)Instantiate(Rock_Gold, dropPos, Quaternion.identity);
			ob21.gameObject.transform.parent=Prefabs.transform;
			ob21.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob21.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
			//Red==================================
		case 22:
			GameObject ob22=(GameObject)Instantiate(Red_Blue, dropPos, Quaternion.identity);
			ob22.gameObject.transform.parent=Prefabs.transform;
			ob22.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob22.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 23:
			GameObject ob23=(GameObject)Instantiate(Red_Green, dropPos, Quaternion.identity);
			ob23.gameObject.transform.parent=Prefabs.transform;
			ob23.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob23.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 24:
			GameObject ob24=(GameObject)Instantiate(Red_Red, dropPos, Quaternion.identity);
			ob24.gameObject.transform.parent=Prefabs.transform;
			ob24.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob24.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 25:
			GameObject ob25=(GameObject)Instantiate(Red_Silver, dropPos, Quaternion.identity);
			ob25.gameObject.transform.parent=Prefabs.transform;
			ob25.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob25.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 26:
			GameObject ob26=(GameObject)Instantiate(Red_Gold, dropPos, Quaternion.identity);
			ob26.gameObject.transform.parent=PrefabsRandom.transform;
			ob26.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob26.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
			//Volcan==================================
		case 27:
			GameObject ob27=(GameObject)Instantiate(Valcon_Blue, dropPos, Quaternion.identity);
			ob27.gameObject.transform.parent=PrefabsRandom.transform;
			ob27.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob27.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 28:
			GameObject ob28=(GameObject)Instantiate(Valcon_Green, dropPos, Quaternion.identity);
			ob28.gameObject.transform.parent=PrefabsRandom.transform;
			ob28.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob28.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 29:
			GameObject ob29=(GameObject)Instantiate(Valcon_Red, dropPos, Quaternion.identity);
			ob29.gameObject.transform.parent=PrefabsRandom.transform;
			ob29.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob29.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 30:
			GameObject ob30=(GameObject)Instantiate(Valcon_Silver, dropPos, Quaternion.identity);
			ob30.gameObject.transform.parent=PrefabsRandom.transform;
			ob30.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob30.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 31:
			GameObject ob31=(GameObject)Instantiate(Valcon_Gold, dropPos, Quaternion.identity);
			ob31.gameObject.transform.parent=PrefabsRandom.transform;
			ob31.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob31.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
			//Star========================
		case 32:
			GameObject ob32=(GameObject)Instantiate(Star1, dropPos, Quaternion.identity);
			ob32.gameObject.transform.parent=PrefabsRandom.transform;
			ob32.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob32.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 33:
			GameObject ob33=(GameObject)Instantiate(Star2, dropPos, Quaternion.identity);
			ob33.gameObject.transform.parent=PrefabsRandom.transform;
			ob33.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob33.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 34:
			GameObject ob34=(GameObject)Instantiate(Star3, dropPos, Quaternion.identity);
			ob34.gameObject.transform.parent=PrefabsRandom.transform;
			ob34.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob34.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
			//Treasure==================
		case 35:
			GameObject ob35=(GameObject)Instantiate(Treasure, dropPos, Quaternion.identity);
			ob35.gameObject.transform.parent=PrefabsRandom.transform;
			ob35.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob35.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
			//New Ball==================36~38
		case 36:
			GameObject ob36=(GameObject)Instantiate(ThunderBall, dropPos, Quaternion.identity);
			ob36.gameObject.transform.parent=PrefabsRandom.transform;
			ob36.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob36.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 37:
			GameObject ob37=(GameObject)Instantiate(ToxicBall, dropPos, Quaternion.identity);
			ob37.gameObject.transform.parent=PrefabsRandom.transform;
			ob37.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob37.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 38:
			GameObject ob38=(GameObject)Instantiate(Bumb, dropPos, Quaternion.identity);
			ob38.gameObject.transform.parent=PrefabsRandom.transform;
			ob38.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob38.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
			//Bell=================================
		case 39:
			GameObject ob39=(GameObject)Instantiate(Bell, dropPos, Quaternion.identity);
			ob39.gameObject.transform.parent=PrefabsRandom.transform;
			ob39.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob39.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 40:
			GameObject ob40=(GameObject)Instantiate(Sand_Bell, dropPos, Quaternion.identity);
			ob40.gameObject.transform.parent=PrefabsRandom.transform;
			ob40.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob40.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 41:
			GameObject ob41=(GameObject)Instantiate(Soil_Bell, dropPos, Quaternion.identity);
			ob41.gameObject.transform.parent=PrefabsRandom.transform;
			ob41.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob41.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 42:
			GameObject ob42=(GameObject)Instantiate(Red_Bell, dropPos, Quaternion.identity);
			ob42.gameObject.transform.parent=PrefabsRandom.transform;
			ob42.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob42.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 43:
			GameObject ob43=(GameObject)Instantiate(Rock_Bell, dropPos, Quaternion.identity);
			ob43.gameObject.transform.parent=PrefabsRandom.transform;
			ob43.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob43.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 44:
			GameObject ob44=(GameObject)Instantiate(Volcan_Bell, dropPos, Quaternion.identity);
			ob44.gameObject.transform.parent=PrefabsRandom.transform;
			ob44.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob44.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
			//Fan=================================
		case 45:
			GameObject ob45=(GameObject)Instantiate(Fan_R, dropPos, Quaternion.identity);
			ob45.gameObject.transform.parent=PrefabsRandom.transform;
			ob45.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob45.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 46:
			GameObject ob46=(GameObject)Instantiate(Fan_L, dropPos, Quaternion.identity);
			ob46.gameObject.transform.parent=PrefabsRandom.transform;
			ob46.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob46.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
			//New Cube=================================
		case 47:
			GameObject ob47=(GameObject)Instantiate(Ice, dropPos, Quaternion.identity);
			ob47.gameObject.transform.parent=PrefabsRandom.transform;
			ob47.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob47.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 48:
			GameObject ob48=(GameObject)Instantiate(GunPowder, dropPos, Quaternion.identity);
			ob48.gameObject.transform.parent=PrefabsRandom.transform;
			ob48.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob48.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 49:
			GameObject ob49=(GameObject)Instantiate(Love, dropPos, Quaternion.identity);
			ob49.gameObject.transform.parent=PrefabsRandom.transform;
			Transform[] children = GetComponentsInChildren <Transform>();
			foreach (Transform child   in children) {
				
				if(child.name=="Love(Clone)")
				{
					TrueLove = child.gameObject;
				}
			}
				

			TrueLove.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			TrueLove.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
		case 50:
			GameObject ob50=(GameObject)Instantiate(Hourglass, dropPos, Quaternion.identity);
			ob50.gameObject.transform.parent=PrefabsRandom.transform;
			ob50.gameObject.GetComponent<SetBrickPosition>().XBrick = X;
			ob50.gameObject.GetComponent<SetBrickPosition>().YBrick = Y;
			break;
			
		}
		
	}
	
	
	
	
	
	void loadLevelData(string data){
		
		string[] myDataMain=data.Split ('#');
		string[] myDataX = myDataMain[0].Split (';');
		//		myData[0] //Empty
		string[] myData0=myDataX[1].Split (',');
		string[] myData1=myDataX[2].Split (',');
		string[] myData2=myDataX[3].Split (',');
		string[] myData3=myDataX[4].Split (',');
		string[] myData4=myDataX[5].Split (',');
		string[] myData5=myDataX[6].Split (',');
		string[] myData6=myDataX[7].Split (',');
		string[] myData7=myDataX[8].Split (',');
		string[] myData8=myDataX[9].Split (',');
		string[] myData9=myDataX[10].Split (',');
		
		if(myDataX[0]=="True")//Xmode
		{
			Xmode=true;
			Ymode=false;
			
			for (int a=0; a<jaggedArray.Length; a++) 
			{
				if(a%2==0)
				{
					for (int b=0; b<jaggedArray[a].Length; b++) 
					{
						if(a==0)
						{
							jaggedArray[a][b]=int.Parse(myData0[b]);  
							addBlocks(jaggedArray[a][b],a,b);
						}
						if(a==1)
						{
							jaggedArray[a][b]=int.Parse(myData1[b]);
							addBlocks(jaggedArray[a][b],a,b);
						}
						if(a==2)
						{
							jaggedArray[a][b]=int.Parse(myData2[b]);
							addBlocks(jaggedArray[a][b],a,b);
						}
						if(a==3)
						{
							jaggedArray[a][b]=int.Parse(myData3[b]);
							addBlocks(jaggedArray[a][b],a,b);
						}
						if(a==4)
						{
							jaggedArray[a][b]=int.Parse(myData4[b]);
							addBlocks(jaggedArray[a][b],a,b);
						}
						if(a==5)
						{
							jaggedArray[a][b]=int.Parse(myData5[b]);
							addBlocks(jaggedArray[a][b],a,b);
						}
						if(a==6)
						{
							jaggedArray[a][b]=int.Parse(myData6[b]);
							addBlocks(jaggedArray[a][b],a,b);
						}
						if(a==7)
						{
							jaggedArray[a][b]=int.Parse(myData7[b]);
							addBlocks(jaggedArray[a][b],a,b);
						}
						if(a==8)
						{
							jaggedArray[a][b]=int.Parse(myData8[b]);
							addBlocks(jaggedArray[a][b],a,b);
						}
						if(a==9)
						{
							jaggedArray[a][b]=int.Parse(myData9[b]);
							addBlocks(jaggedArray[a][b],a,b);
						}
					}
				}
				else
				{
					for (int b=0; b<jaggedArray[a].Length-1; b++) 
					{
						if(a==0)
						{
							jaggedArray[a][b]=int.Parse(myData0[b]);
							addBlocks(jaggedArray[a][b],a,b);
						}
						if(a==1)
						{
							jaggedArray[a][b]=int.Parse(myData1[b]);
							addBlocks(jaggedArray[a][b],a,b);
						}
						if(a==2)
						{
							jaggedArray[a][b]=int.Parse(myData2[b]);
							addBlocks(jaggedArray[a][b],a,b);
						}
						if(a==3)
						{
							jaggedArray[a][b]=int.Parse(myData3[b]);
							addBlocks(jaggedArray[a][b],a,b);
						}
						if(a==4)
						{
							jaggedArray[a][b]=int.Parse(myData4[b]);
							addBlocks(jaggedArray[a][b],a,b);
						}
						if(a==5)
						{
							jaggedArray[a][b]=int.Parse(myData5[b]);
							addBlocks(jaggedArray[a][b],a,b);
						}
						if(a==6)
						{
							jaggedArray[a][b]=int.Parse(myData6[b]);
							addBlocks(jaggedArray[a][b],a,b);
						}
						if(a==7)
						{
							jaggedArray[a][b]=int.Parse(myData7[b]);
							addBlocks(jaggedArray[a][b],a,b);
						}
						if(a==8)
						{
							jaggedArray[a][b]=int.Parse(myData8[b]);
							addBlocks(jaggedArray[a][b],a,b);
						}
						if(a==9)
						{
							jaggedArray[a][b]=int.Parse(myData9[b]);
							addBlocks(jaggedArray[a][b],a,b);
						}
					}//for END
				}
			}
		}
		else//Ymode
		{
			Ymode=true;
			Xmode=false;
			
			for (int a=0; a<jaggedArray.Length; a++) 
			{
				for (int b=0; b<jaggedArray[a].Length; b++) 
				{
					if(a==0)
					{
						jaggedArray[a][b]=int.Parse(myData0[b]);
						addBlocks(jaggedArray[a][b],a,b);
					}
					if(a==1)
					{
						jaggedArray[a][b]=int.Parse(myData1[b]);
						addBlocks(jaggedArray[a][b],a,b);
					}
					if(a==2)
					{
						jaggedArray[a][b]=int.Parse(myData2[b]);addBlocks(jaggedArray[a][b],a,b);
					}
					if(a==3)
					{
						jaggedArray[a][b]=int.Parse(myData3[b]);addBlocks(jaggedArray[a][b],a,b);}
					if(a==4)
					{
						jaggedArray[a][b]=int.Parse(myData4[b]);
						addBlocks(jaggedArray[a][b],a,b);
					}
					if(a==5)
					{
						jaggedArray[a][b]=int.Parse(myData5[b]);
						addBlocks(jaggedArray[a][b],a,b);
					}
					if(a==6)
					{
						jaggedArray[a][b]=int.Parse(myData6[b]);
						addBlocks(jaggedArray[a][b],a,b);
					}
					if(a==7)
					{
						jaggedArray[a][b]=int.Parse(myData7[b]);
						addBlocks(jaggedArray[a][b],a,b);
					}
					if(a==8)
					{
						jaggedArray[a][b]=int.Parse(myData8[b]);
						addBlocks(jaggedArray[a][b],a,b);
					}
					if(a==9)
					{
						jaggedArray[a][b]=int.Parse(myData9[b]);
						addBlocks(jaggedArray[a][b],a,b);
					}
				}
			}
		}
		
		//		print (myDataMain [1]);
		
		string[] myDataSecondPart=myDataMain[1].Split(',');
		string[] myDataLevel = myDataSecondPart [0].Split ('-');
		
		//關卡資訊===========
		//Level
		txtLv_1 = myDataLevel [0];
		txtLv_2 = myDataLevel [1];
		//		print ("bg0"+txtLv_1+"_1");
		
		
		//		this.GetComponent<SpriteRenderer>().sprite=Resources.Load("bg0"+stage+"_1",typeof(Sprite))as Sprite;
		//		foreGround.GetComponent<SpriteRenderer>().sprite=Resources.Load("bg0"+stage+"_2",typeof(Sprite))as Sprite;
		
		
		//方塊重生
		reborn=Convert.ToBoolean(myDataSecondPart[1]);
		
		//關卡限制===========
		//1.次數限制
		if(myDataSecondPart[4]!="")
		{
			lifeLimited =Convert.ToBoolean(myDataSecondPart[4]);//是否 次數 限制
			txtLifeLimited=myDataSecondPart[5];//次數
			if (myDataSecondPart[5] != "") { 
				dragonLife = int.Parse(myDataSecondPart[5]);
			}
		}
		//			print("次數限制"+txtLifeLimited+"次");
		
		//2.時間限制
		if(myDataSecondPart[2]!="")
		{
			timeLimited =Convert.ToBoolean(myDataSecondPart[2]);//是否 時間限制
			txtTimeLimited=myDataSecondPart[3];//秒數
			if (myDataSecondPart[3] != "") { 
				
				
				
			}
		}
		//			print("時間限制"+txtTimeLimited+"SECs");
		
		
		//補給品===========
		//==隨機時間==
		txtSupportRandomTime =myDataSecondPart[6];
		//==最大數量==
		if (myDataSecondPart [7] == "") {//無限大
			txtSupportMaxNumber=int.MaxValue;
		} 
		else 
		{
			txtSupportMaxNumber =int.Parse(myDataSecondPart[7]);
		}
		
		//				print ("補給品 時間:"+myDataSecondPart[6]);
		//				print ("補給品 數量:"+myDataSecondPart[7]);
		//				print ("愛心或沙漏 機率:"+myDataSecondPart[8]);
		//				print ("愛心或沙漏 數量:"+myDataSecondPart[9]);
		//				print ("無敵星星 機率:"+myDataSecondPart[10]);
		//				print ("無敵星星 數量:"+myDataSecondPart[11]);
		//				print ("能量星星 機率:"+myDataSecondPart[12]);
		//				print ("能量星星 數量:"+myDataSecondPart[13]);
		//				print ("分身星星 機率:"+myDataSecondPart[14]);
		//				print ("分身星星 數量:"+myDataSecondPart[15]);
		//				print ("寶箱 機率:"+myDataSecondPart[16]);
		//				print ("寶箱 數量:"+myDataSecondPart[17]);
		//				print ("閃電球 機率:"+myDataSecondPart[18]);
		//				print ("閃電球 數量:"+myDataSecondPart[19]);
		//				print ("毒液球 機率:"+myDataSecondPart[20]);
		//				print ("毒液球 數量:"+myDataSecondPart[21]);
		//				print ("炸彈 機率:"+myDataSecondPart[22]);
		//				print ("炸彈 數量:"+myDataSecondPart[23]);
		//				print ("空白 機率:"+myDataSecondPart[24]);
		//				print ("空白 數量:"+myDataSecondPart[25]);
		
		
		
		lovePercent=int.Parse(myDataSecondPart[8]);
		if(myDataSecondPart[9]=="")
		{
			loveMax=int.MaxValue;
		}else
		{
			loveMax =int.Parse(myDataSecondPart[9]);
		}
		
		star1Percent=int.Parse(myDataSecondPart[10]);
		if(myDataSecondPart[11]=="")
		{
			star1Max=int.MaxValue;
		}else
		{
			star1Max =int.Parse(myDataSecondPart[11]);
		}
		
		star2Percent=int.Parse(myDataSecondPart[12]);
		if(myDataSecondPart[13]=="")
		{
			star2Max=int.MaxValue;
		}else
		{
			star2Max =int.Parse(myDataSecondPart[13]);
		}
		
		star3Percent=int.Parse(myDataSecondPart[14]);
		if(myDataSecondPart[15]=="")
		{
			star3Max=int.MaxValue;
		}else
		{
			star3Max =int.Parse(myDataSecondPart[15]);
		}
		
		treasurePercent=int.Parse(myDataSecondPart[16]);
		if(myDataSecondPart[17]=="")
		{
			treasureMax=int.MaxValue;
		}else
		{
			treasureMax =int.Parse(myDataSecondPart[17]);
		}
		
		
		thunderBallPercent = int.Parse (myDataSecondPart [18]);
		if(myDataSecondPart[19]=="")
		{
			thunderBallMax=int.MaxValue;
		}else
		{
			thunderBallMax =int.Parse(myDataSecondPart[19]);
		}
		
		toxinBallPercent=int.Parse(myDataSecondPart[20]);
		if(myDataSecondPart[21]=="")
		{
			toxinBallMax=int.MaxValue;
		}else
		{
			toxinBallMax =int.Parse(myDataSecondPart[21]);
		}
		
		bumbPercent=int.Parse(myDataSecondPart[22]);
		if(myDataSecondPart[23]=="")
		{
			bumbMax=int.MaxValue;
		}else
		{
			bumbMax =int.Parse(myDataSecondPart[23]);
		}
		
		emptyPercent=int.Parse(myDataSecondPart[24]);
		if(myDataSecondPart[25]=="")
		{
			emptyMax=int.MaxValue;
		}else
		{
			emptyMax =int.Parse(myDataSecondPart[25]);
		}
		
		
		//過關條件=================
		//		print (myDataMain [2]);
		
		string[] arrayWinTemp = myDataMain [2].Split (';');
		//		print (arrayWinTemp.Length);
		string[] arrayWinRule = arrayWinTemp [0].Split (',');
		string[] arrayWinNumber = arrayWinTemp [1].Split (',');
		//					print (arrayWinRule.Length);
		//					print (arrayWinNumber.Length);
		
		//		print (arrayWinRule.Length );
		//		print (arrayWinNumber.Length);
		//		print (arrayWinRule [0]);
		//		print (arrayWinNumber [0]);
		
		//Empty
		if (arrayWinRule.Length == 1 
		    && arrayWinNumber.Length == 1 
		    && arrayWinRule [0]=="") 
		{
			//			print ("空");
			//					print (arrayWinRule.Length );
			//					print (arrayWinNumber.Length);
			//					print (arrayWinRule [0]);
			//					print (arrayWinNumber [0]);
			//				return;
			isRead=true;
		}
		else//Have Clear Condition
		{
			for(int a=0;a<arrayWinRule.Length;a++)
			{
				arrayOfLimitedRule.Add(arrayWinRule[a]);
				//				print(arrayWinRule[a]);
			}
			for(int b=0;b<arrayWinNumber.Length;b++)
			{
				arrayOfLimitedRuleNumber.Add(arrayWinNumber[b]);
				//				print(arrayWinNumber[b]);
			}
			isRead=true;
		}
		
	}
	
	
	public void GainItem() {
		for (int i = 0; i < gainItem.Count; i++) {
			Debug.Log("get " + gainItem[i].ToString());
			PlayerPrefs.SetInt(gainItem[i].ToString(), (PlayerPrefs.GetInt(gainItem[i].ToString()) + 1));
		}
	}
	
	
	
	
	
	
}





