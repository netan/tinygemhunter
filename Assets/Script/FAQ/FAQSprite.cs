﻿using UnityEngine;
using System.Collections;

public class FAQSprite : MonoBehaviour {

	public string NAME;

	// Use this for initialization
	void Start () {
		GetComponent<UI2DSprite>().sprite2D = Resources.Load("Teach/" + NAME.Trim(), typeof(Sprite)) as Sprite;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
