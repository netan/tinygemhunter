﻿using UnityEngine;
using System.Collections;

public class OpenLock : MonoBehaviour {
	
	public StageMove stageMove;
	public StageLock stage;
	public GameObject lockPanel;
	public AudioSource audio;

	public StageLock SL;
	
	// Use this for initialization
	void Start () {
		stageMove = GameObject.Find ("Island").GetComponent<StageMove> ();
		stage.StateSet("End");
		stage.stringUpSpeed = 150;
	}
	
	public void OnClick() {
		//if (stage.CheckItemCount(1, stage.FindFirstIndex(), stage.FindLastIndex())) {
		if (isOpen()) {
			audio.enabled = true;
			stage.StateSet("StringUP");
		} else {
			if (stage.isWork() && lockPanel.transform.localScale.x == 0) {
				stageMove.UICount++;
				lockPanel.GetComponent<TweenScale>().PlayForward();
			}
		}
	}
	
	public bool isOpen() {
		bool tmp = false;
		if (SL.index == stage.stage.stageNow) {
			int sum = ShowBellCount.BellCount();
			switch (stage.stage.stageNow) {
			case 2:
				if (sum >= 50) {
					tmp = true;
				}
				break;
			case 3:
				if (sum >= 110) {
					tmp = true;
				}
				break;
			case 4:
				if (sum >= 165) {
					tmp = true;
				}
				break;
			case 5:
				if (sum >= 220) {
					tmp = true;
				}
				break;
			case 6:
				if (sum >= 280) {
					tmp = true;
				}
				break;
			case 7:
				if (sum >= 340) {
					tmp = true;
				}
				break;
			}
		}
		return tmp;
	}
}