﻿using UnityEngine;
using System.Collections;
using ArabicSupport;

public class GetItemTeachData : MonoBehaviour {

	public LoadItemTeach LIT;
	public UI2DSprite sprite;
	public UILabel label;

	public bool tashkeel = true;
	public bool hinduNumbers = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (LIT.length > 0) {
			sprite.sprite2D = LIT.getSprite ();
			label.text = LIT.getText ();
			label.text = ArabicFixer.Fix(label.text, tashkeel, hinduNumbers);
		}
	}
}
