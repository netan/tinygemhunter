﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;

public class LoadGameEdit : MonoBehaviour
{

	public int loadNumber = 0;
	public int[] isLoad ;//是否 已讀取
	public float lodingPercent = 0;
	public int StageTotalNumber = 0;
	public LoadCSV CSVsc;
	public static bool isReadFinish = false;
	public System.Collections.Generic.List<string> array_txtDataName;//關卡名

	public System.Collections.Generic.List<string> array_Limited;//限制
	public System.Collections.Generic.List<string> array_LimitedNumber;//限制 數目
	
	public System.Collections.Generic.List<string> array_Rule_1;//條件1
	public System.Collections.Generic.List<string> array_Rule_1_Number;//條件1 數目
	public System.Collections.Generic.List<string> array_Rule_2;//條件2
	public System.Collections.Generic.List<string> array_Rule_2_Number;//條件2 數目

	public string[] myDataSecondPart;//過關限制
	
	void Start ()
	{
		CSVsc = this.GetComponent<LoadCSV> ();
	}

	void Update ()
	{
		if (LoadCSV.isReadFinish && CSVsc) {
			StageTotalNumber = CSVsc.GetAllStageNumber ();
			lodingPercent = loadNumber / CSVsc.GetAllLevelCount () * 100;
		}
			
		if (StageTotalNumber > 0 && !isReadFinish) {
			isLoad = new int[StageTotalNumber];

			for (int a=1; a<=StageTotalNumber; a++) {

				int levelNumber = CSVsc.GetLevelNumberOfThisStage (a);

				for (int b=1; b<=levelNumber; b++) {

					string tempS = a.ToString () + "-" + b.ToString ();
					StartCoroutine (ReadFile (tempS));
				}
			}

			isReadFinish = true;
		}
			

	}
	
	public string[] GetConditions (string levelName)
	{

//		print (levelName);
		string[] tempCondition = new string[6];

		for (int m=0; m<array_txtDataName.Count; m++) {
			if (array_txtDataName [m] == levelName) {
//				print (levelName);
				tempCondition [0] = array_Limited [m];
				tempCondition [1] = array_LimitedNumber [m];
				tempCondition [2] = array_Rule_1 [m];
				tempCondition [3] = array_Rule_1_Number [m];
				tempCondition [4] = array_Rule_2 [m];
				tempCondition [5] = array_Rule_2_Number [m];

//				for(int f=0;f<tempCondition.Length;f++){
//					print(tempCondition[f]);
//				}

			}
		}
	
		return tempCondition;
	}

	//取得資料夾內所有檔案
	void GetAlltxtFileInFolder ()
	{
//		String[] FileCollection;
		String FilePath = "";

		if (Application.platform == RuntimePlatform.Android) {
			
			FilePath = Application.streamingAssetsPath + "/";
		}
		else {
			FilePath = "file://" + Application.streamingAssetsPath + "/";
//						path=Application.streamingAssetsPath+ "/"+levelNameNow+".txt";
		}

//		String FilePath =  Application.streamingAssetsPath + "/";
//		FileInfo theFileInfo;
//		FileCollection = Directory.GetFiles(FilePath, "*.txt");


//		string[] directories = Directory.GetDirectories(FilePath);

//		print (directories[0]);

//		for(int i = 0 ; i < FileCollection.Length ; i++)
//		{
//			theFileInfo = new FileInfo(FileCollection[i]);
//			string[] levelNumber=theFileInfo.Name.Split('.');
//			array_txtDataName.Add(levelNumber[0]);


//			StartCoroutine(ReadFile (levelNumber[0]));//Read txt. FIle


//		}

//		isReadFinish = true;
	}

	public IEnumerator ReadFile (string levelNameNow)
	{

		string path;
		if (Application.platform == RuntimePlatform.Android) {
			path = Application.streamingAssetsPath + "/" + levelNameNow + ".txt";
		}
		else {
			path = "file://" + Application.streamingAssetsPath + "/" + levelNameNow + ".txt";
		}   
		WWW www = new WWW (path);///WWW读取在各个平台上都可使用  
		yield return www;   

//		loadLevelData (www.text);
		yield return StartCoroutine (loadLevelData (www.text));

		array_txtDataName.Add (levelNameNow);
	}

	IEnumerator loadLevelData (string data)
	{
		string[] myDataMain = data.Split ('#');
		string[] myDataX = myDataMain [0].Split (';');
		//		myData[0] //Empty
		string[] myData0 = myDataX [1].Split (',');
		string[] myData1 = myDataX [2].Split (',');
		string[] myData2 = myDataX [3].Split (',');
		string[] myData3 = myDataX [4].Split (',');
		string[] myData4 = myDataX [5].Split (',');
		string[] myData5 = myDataX [6].Split (',');
		string[] myData6 = myDataX [7].Split (',');
		string[] myData7 = myDataX [8].Split (',');
		string[] myData8 = myDataX [9].Split (',');
		string[] myData9 = myDataX [10].Split (',');


		myDataSecondPart = myDataMain [1].Split (',');
		string[] myDataLevel = myDataSecondPart [0].Split ('-');

		array_Limited.Add (myDataSecondPart [2]);//時間
		
		if (Convert.ToBoolean (myDataSecondPart [2])) {//時間 限制
			array_LimitedNumber.Add (myDataSecondPart [3]);

		}
		else if (Convert.ToBoolean (myDataSecondPart [4])) {//次數 限制
			array_LimitedNumber.Add (myDataSecondPart [5]);

		}

		//過關條件=================
		//		print (myDataMain [2]);
			
		string[] arrayWinTemp = myDataMain [2].Split (';');
		//		print (arrayWinTemp.Length);
		string[] arrayWinRule = arrayWinTemp [0].Split (',');
		string[] arrayWinNumber = arrayWinTemp [1].Split (',');
		//					print (arrayWinRule.Length);
		//					print (arrayWinNumber.Length);
		
//				print (arrayWinRule.Length );
//				print (arrayWinNumber.Length);
//				print (arrayWinRule [0]);
//				print (arrayWinNumber [0]);
		
		//Empty
		if (arrayWinRule.Length == 1 
			&& arrayWinNumber.Length == 1 
			&& arrayWinRule [0] == "") {
			array_Rule_1.Add ("empty");
			array_Rule_1_Number.Add ("empty");
			array_Rule_2.Add ("empty");
			array_Rule_2_Number.Add ("empty");
//			print ("空");

			//					print (arrayWinRule.Length );
			//					print (arrayWinNumber.Length);
			//					print (arrayWinRule [0]);
			//					print (arrayWinNumber [0]);
			//				return;
		}
		else {//Have Clear Condition
			for (int a=0; a<arrayWinRule.Length; a++) {//條件名
				if (a == 0) {//條件1
					array_Rule_1.Add (arrayWinRule [a]);
				}
				else if (a == 1) {//條件2
					array_Rule_2.Add (arrayWinRule [a]);
				}
//				print(arrayWinRule[a]);
			}

			for (int b=0; b<arrayWinNumber.Length; b++) {//條件 數量
				if (b == 0) {//條件1
					array_Rule_1_Number.Add (arrayWinNumber [b]);
				}
				else if (b == 1) {//條件2
					array_Rule_2_Number.Add (arrayWinNumber [b]);
				}

//				print(arrayWinNumber[b]);

			}

//			print ("條件數:"+arrayWinRule.Length);
			if (arrayWinRule.Length == 1) {
				array_Rule_2.Add ("empty");
				array_Rule_2_Number.Add ("empty");
			}

		}
		
//		yield return 0;
		yield return new WaitForSeconds (0.05f);
		loadNumber++;
	}


}
