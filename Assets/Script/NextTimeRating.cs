﻿using UnityEngine;
using System.Collections;

public class NextTimeRating : MonoBehaviour {

	public GameObject UI;

	void OnClick() {
		RateManager.NextTimeRating ();
		UI.SetActive (false);
	}
}
