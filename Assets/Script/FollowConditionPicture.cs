﻿using UnityEngine;
using System.Collections;

public class FollowConditionPicture : MonoBehaviour {

	public UISprite a;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<UISprite> ().atlas = a.atlas;
		GetComponent<UISprite> ().spriteName = a.spriteName;
	}
}
