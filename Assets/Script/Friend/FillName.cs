﻿using UnityEngine;
using System.Collections;
using Parse;

public class FillName : MonoBehaviour {

	public bool isName;

	// Use this for initialization
	void Start () {
		if (isName) {
			if (ParseUser.CurrentUser != null) {
				GetComponent<UILabel>().text = string.Format("{0:0,0}", int.Parse(ParseUser.CurrentUser.Username));
			}
		} else {
			GetComponent<UIInput>().value = WWW.UnEscapeURL(PlayerPrefs.GetString("nickName"));
		}
	}
}
