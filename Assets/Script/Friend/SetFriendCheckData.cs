﻿using UnityEngine;
using System.Collections;

public class SetFriendCheckData : MonoBehaviour {

	public UILabel label;
	public string realID;
	
	public void SetData(string nickname, string ID) {
		label.text = nickname;
		realID = ID;
	}
}
