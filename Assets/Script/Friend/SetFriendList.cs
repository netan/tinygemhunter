﻿using UnityEngine;
using System.Collections;

public class SetFriendList : MonoBehaviour {

	public MyParse myParse;
	public TestNetwork testNetwork;
	
	public GameObject scrollView;
	public GameObject FriendListItem;
	public float localPosition;

	public ChangeLanguage loading;

	// Use this for initialization
	void Start () {
		myParse = GameObject.Find("EventControoler").GetComponent<MyParse>();
		testNetwork = GameObject.Find("EventControoler").GetComponent<TestNetwork>();
	}
	
	// Update is called once per frame
	void Update () {
		if (testNetwork.isConnect && !testNetwork.isReConnect) {
			if (myParse.isGetFriendList) {
				myParse.isGetFriendList = false;
				GameObject[] destoryFriendList = GameObject.FindGameObjectsWithTag("FriendList");
				foreach (GameObject d in destoryFriendList) {
					Destroy(d);
				}
				localPosition = 127.0f;
				for (int i = 0; i < myParse.friend.Count; i++) {
					GameObject clone = NGUITools.AddChild (scrollView, FriendListItem);
					clone.transform.localPosition = new Vector3(0, localPosition, 0);
					clone.GetComponent<SetFriendListData>().SetData(myParse.friend[i].ToString(), myParse.nickName[i].ToString(), myParse.bell[i].ToString());
					localPosition -= 62.0f;
				}
				myParse.isGetFriendListRef = true;
				loading.gameObject.SetActive(false);
			}
		} else {
			loading.ReLoadID("10117");
		}
	}
}
