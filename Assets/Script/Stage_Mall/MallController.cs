﻿using UnityEngine;
using System.Collections;

public class MallController : MonoBehaviour
{

	public TestNetwork testNetwork;

	//商城幣和各個商城道具的數量
	//public int MallMoney = 3;
	public int LoveOrTime_Number = 0;
	public int Star_Number = 0;
	public int Bomb_Number = 0;
	public int RandomItem_Number = 0;

	//商城道具所消耗的商城幣量
	public int LoveOrTime_Cost = 3;
	public int Star_Cost = 3;
	public int Bomb_Cost = 3;
	public int RandomItem_Cost = 3;
	public int TotalCost = 0;
	public int NowMallMoney = 0;

	//是否使用商城道具
	public bool Used_LoveOrTime = false;
	public bool Used_Star = false;
	public bool Used_Bomb = false;
	public bool Used_RandomItem = false;

	//是否消耗商城幣
	public bool isPayMoney = false;

	//是否消耗商城道具數量
	public bool isPayLoveOrTime = false;
	public bool isPayStar = false;
	public bool isPayBomb = false;
	public bool isPayRandomItem = false;

	//隨機到哪樣道具
	public bool ranLoveOrTime = false;
	public bool ranStar1 = false;
	public bool ranBomb = false;

	//進入的關卡的限制條件是愛心還沙漏
	public bool isLove = true;

	//抓其他物件的方法
	public LevelSelcet LS;
	public bool isNeedWarning;
	public GameObject hi;
	// Use this for initialization
	void Start ()
	{

		testNetwork = GameObject.Find ("EventControoler").GetComponent<TestNetwork> ();

		LS = GameObject.Find ("EventControoler").GetComponent<LevelSelcet> ();

		MyParse.QueryCount = 1;
		MyParse.QueryMallMoney ();

		LoveOrTime_Number = PlayerPrefs.GetInt ("LoveOrTime_Number");
		Star_Number = PlayerPrefs.GetInt ("Star_Number");
		Bomb_Number = PlayerPrefs.GetInt ("Bomb_Number");
		RandomItem_Cost = PlayerPrefs.GetInt ("RandomItem_Cost");
	}
	
	// Update is called once per frame
	void Update ()
	{
//		if (testNetwork.isConnect)
//		{
//			if (!testNetwork.isReConnect)
//			{
//				if (MyParse.isGetMallMoney)
//				{
//					MyParse.isGetMallMoney = false;
//					NowMallMoney = MyParse.mallMoney - TotalCost;
//					PlayerPrefs.SetInt ("mallMoney", MyParse.mallMoney);
//					if (PlayerPrefs.GetInt ("costed") > 0)
//					{
//						MyParse.MinusMallMoney (PlayerPrefs.GetInt ("costed"));
//					}
//					PlayerPrefs.SetInt ("costed", 0);
//					MyParse.QueryCount = 2;
//					MyParse.QueryMallMoney ();
//				}
//			}
//		}
//		else
//		{
		MyParse.mallMoney = PlayerPrefs.GetInt ("mallMoney") - PlayerPrefs.GetInt ("costed");
//		}
//		Debug.Log (Used_Bomb);
	}

	public bool CheckUsedItem (int index)
	{
		isNeedWarning = Used_LoveOrTime || Used_Star || Used_Bomb || Used_RandomItem;
		if (index == 1)
		{
			Debug.Log ("im here");

			if (LoveOrTime_Number > 0)
			{
				isPayLoveOrTime = true;
				isNeedWarning = true;
				return true;
			}
			else
			{
				if (NowMallMoney - LoveOrTime_Cost >= 0)
				{
					isPayMoney = true;
					NowMallMoney -= LoveOrTime_Cost;
					TotalCost += LoveOrTime_Cost;
					return true;
				}
				else
				{
					GameObject check = Instantiate (Resources.Load ("CheckMoney")) as GameObject;
					check.transform.parent = GameObject.FindGameObjectWithTag ("SecondPanel").transform;
					;
					check.transform.localPosition = new Vector3 (-2.5f, -22, 0);
					check.transform.localScale = new Vector3 (1, 1, 1);
				}

			}
		}
		else if (index == 2)
		{
			//Debug.Log("im here");
			if (Star_Number > 0)
			{
				isPayStar = true;
				isNeedWarning = true;
				return true;
			}
			else
			{
				if (NowMallMoney - Star_Cost >= 0)
				{
					isPayMoney = true;
					NowMallMoney -= Star_Cost;
					TotalCost += Star_Cost;
					return true;
				}
				else
				{
					GameObject check = Instantiate (Resources.Load ("CheckMoney")) as GameObject;
					check.transform.parent = GameObject.FindGameObjectWithTag ("SecondPanel").transform;
					;
					check.transform.localPosition = new Vector3 (-2.5f, -22, 0);
					check.transform.localScale = new Vector3 (1, 1, 1);
				}
				
			}
		}
		else if (index == 3)
		{
			//Debug.Log("im here");
			if (Bomb_Number > 0)
			{
				isPayBomb = true;
				isNeedWarning = true;
				return true;
			}
			else
			{
				if (NowMallMoney - Bomb_Cost >= 0)
				{
					isPayMoney = true;
					NowMallMoney -= Bomb_Cost;
					TotalCost += Bomb_Cost;
					return true;
				}
				else
				{
					GameObject check = Instantiate (Resources.Load ("CheckMoney")) as GameObject;
					check.transform.parent = GameObject.FindGameObjectWithTag ("SecondPanel").transform;
					;
					check.transform.localPosition = new Vector3 (-2.5f, -22, 0);
					check.transform.localScale = new Vector3 (1, 1, 1);
				}
				
			}
		}
		else if (index == 4)
		{
			//Debug.Log("im here");
			if (RandomItem_Number > 0)
			{
				isPayRandomItem = true;
				isNeedWarning = true;
				return true;
			}
			else
			{
				if (NowMallMoney - RandomItem_Cost >= 0)
				{
					isPayMoney = true;
					NowMallMoney -= RandomItem_Cost;
					TotalCost += RandomItem_Cost;
					return true;
				}
				else
				{
					GameObject check = Instantiate (Resources.Load ("CheckMoney")) as GameObject;
					check.transform.parent = GameObject.FindGameObjectWithTag ("SecondPanel").transform;
					;
					check.transform.localPosition = new Vector3 (-2.5f, -22, 0);
					check.transform.localScale = new Vector3 (1, 1, 1);
				}
				
			}
		}
		return false;
	}

	public void CloseUsedItem (int index)
	{   //cancel item and reset NowMallMoney and TotalCost
		isNeedWarning = Used_LoveOrTime || Used_Star || Used_Bomb || Used_RandomItem;
		if (index == 1)
		{
			if (LoveOrTime_Number == 0)
			{
				NowMallMoney += LoveOrTime_Cost;
				TotalCost -= LoveOrTime_Cost;
			}
		}
		else if (index == 2)
		{
			if (Star_Number == 0)
			{
				NowMallMoney += Star_Cost;
				TotalCost -= Star_Cost;
			}
		}
		else if (index == 3)
		{
			if (Bomb_Number == 0)
			{
				NowMallMoney += Bomb_Cost;
				TotalCost -= Bomb_Cost;
			}
		}
		else if (index == 4)
		{
			if (RandomItem_Number == 0)
			{
				NowMallMoney -= RandomItem_Cost;
				TotalCost += RandomItem_Cost;
			}
		}
	}

	public void MallAndLoadingGame ()
	{
		if (isPayMoney || isPayLoveOrTime || isPayStar || isPayBomb)
		{
			GameObject check = Instantiate (Resources.Load ("CheckUseMallItem")) as GameObject;
			check.transform.parent = GameObject.FindGameObjectWithTag ("SecondPanel").transform;
			check.transform.localPosition = new Vector3 (-2.5f, -22, 0);
			check.transform.localScale = new Vector3 (1, 1, 1);
			StageMove stageMove = GameObject.Find ("Island").GetComponent<StageMove> ();
			stageMove.ChangeCount (true);
		}
		else
		{
			if (GameObject.Find ("EventControoler").GetComponent<LevelSelcet> ().s == "1" && GameObject.Find ("EventControoler").GetComponent<LevelSelcet> ().l == "1")
			{
				Global.GetInstance ().loadName = "Story";
				Application.LoadLevel ("Loading");
			}
			else
			{
				Global.GetInstance ().loadName = "Game";
				Application.LoadLevel ("Loading");
			}	
		}
	}

	public void CostItemOrMoneyEnterGame ()
	{
//		MyParse.Instance.CoseMallMoney (TotalCost);
//		TotalCost = 0;

		if (GameObject.Find ("EventControoler").GetComponent<LevelSelcet> ().s == "1" && GameObject.Find ("EventControoler").GetComponent<LevelSelcet> ().l == "1")
		{
			Global.GetInstance ().loadName = "Story";
			Application.LoadLevel ("Loading");
		}
		else
		{
			Global.GetInstance ().loadName = "Game";
			Application.LoadLevel ("Loading");
		}	
	}

	public void GiveReward (int loveOrTime, int star, int bomb, int rand)
	{

		PlayerPrefs.SetInt ("LoveOrTime_Number", PlayerPrefs.GetInt ("LoveOrTime_Number") + loveOrTime);
		LoveOrTime_Number = PlayerPrefs.GetInt ("LoveOrTime_Number");

		PlayerPrefs.SetInt ("Star_Number", PlayerPrefs.GetInt ("Star_Number") + star);
		Star_Number = PlayerPrefs.GetInt ("Star_Number");

		PlayerPrefs.SetInt ("Bomb_Number", PlayerPrefs.GetInt ("Bomb_Number") + bomb);
		Bomb_Number = PlayerPrefs.GetInt ("Bomb_Number");

		PlayerPrefs.SetInt ("RandomItem_Cost", PlayerPrefs.GetInt ("RandomItem_Cost") + rand);
		RandomItem_Cost = PlayerPrefs.GetInt ("RandomItem_Cost");

	}

	public void ResetData ()
	{
		TotalCost = 0;
		NowMallMoney = MyParse.mallMoney;
		Used_LoveOrTime = false;
		Used_Star = false;
		Used_Bomb = false;
		Used_RandomItem = false;
		isPayMoney = false;
		isPayLoveOrTime = false;
		isPayStar = false;
		isPayBomb = false;
		isPayRandomItem = false;
		isNeedWarning = false;
	}
}
