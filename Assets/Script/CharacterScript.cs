﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;
using ArabicSupport;

public class CharacterScript : MonoBehaviour
{

	//NGUI 
	public GameObject Text_OutOfMove;
	public GameObject Text_OutOfTime;
	public GameObject Pic_Win_NGUI;
	public GameObject Win_Panel;
	public GameObject Lose_Panel;
	public GameObject Treasure_Panel;
	public GameObject Cover_Panel;
	public GameObject Level_Panel;
	public GameObject Level_Panel_levelName;
	public bool isSetLevelName;
	public GameObject Pic_Pack;
	public GameObject Text_OutOf;
	public string outofText = "";
	public ChangeMultiLanguage fail;
	public GameObject Bell1;
	public GameObject Bell2;
	public GameObject Bell3;
	public GameObject Text_Con;
	public GameObject Text_Good;
	public GameObject Text_Exce;
	public GameObject Text_Record;
	public GameObject Win_clear;
	public bool isMall_HPTime = false;
	public GameObject Mall_HPTime;
	public bool isMall_Star = false;
	public GameObject Mall_Star;
	public bool isMall_Bomb = false;
	public GameObject Mall_Bomb;
	public float time;
	
	//Fade Out 
	private float alphaSpeed = 1.1f;

	//Other Tiimer
	public float otherTimer = 0;
	public Boolean isWin = false;
	public bool isWinSet = false;
	
	
	//計時(全部時間)
	public float Timer = 0;
	public float timeSpeed = 1;
	
	//New====================
	//Combo
	public GameObject ComboLabel;
	public int ComboMax = 0;
	public int dragonCombo = 0;
	public int comboLv1 = 0;//Fucking Table
	//Health & Life
	public int dragonHealth = 0;
	public int dragonLife = 0;
	
	//Number========================
	public int Sand_Number;//1
	public int Soil_Number;//2
	public int Rock_Number;//3
	public int Red_Number;//4
	public int Valcon_Number;//5
	public int Black_Number;//6
	
	public int BlueGem_Number;
	public int GreenGem_Number;
	public int RedGem_Number;
	public int SilverGem_Number;
	public int GoldGem_Number;
	public int Bell_Number;
	public int Love_Number;
	public int Hourglass_Number;
	public int FanR_Number;
	public int FanL_Number;
	public int Treasure_Number;
	public int Star1_Number;
	public int Star2_Number;
	public int Star3_Number;
	public int ThunderBall_Number;
	public int ToxicBall_Number;
	public int Bumb_Number;
	public int Ice_Number;
	public int IceCover_Number;
	public int GunPowder_Number;
	public int Green_Number;
	//========================Number
	
	
	//	"擊破沙塊",
	//	"擊破土塊",
	//	"擊破石塊",
	//	"擊破紅塊",
	//	"擊破火山岩",
	//	"擊破黑塊(黑曜石)",
	//	"清光方塊(不包括黑塊)",
	//	"收集寶石(不論顏色)",
	//	"收集藍寶石",
	//	"收集綠寶石",
	//	"收集紅寶石",
	//	"收集白寶石",
	//	"收集黃寶石",
	//	"獲得愛心(沙漏)",
	//	"獲得無敵星星",
	//	"獲得分身星星",
	//	"獲得能量星星",
	//	"獲得寶箱",
	//	"獲得閃電球",
	//	"獲得毒液球",
	//	"獲得炸彈",
	//	"擊破結冰塊",
	//	"擊破火藥岩"
	
	//角色==
	public GameObject Char_Left;
	public GameObject Char_Right;
	public GameObject Con_Content;
	public GameObject Con_Background;
	public GameObject Con_Plus_Left;
	public GameObject Con_Plus_Right;
	public GameObject Con_Black;
	public GameObject Skip_Button;
	
	//過關條件===(2:Show Condition)
	public GameObject BlackBackgrund;
	public GameObject Condition_Text;
	public GameObject Condition1;
	public GameObject Condition2;
	public GameObject Limited;
	public GameObject LimitedPicture;
	//過關條件 顯示===(5.Play)
	public GameObject Limited_Pic_Love;
	public GameObject Limited_Number_Love;
	public GameObject Limited_Pic_Hour;
	public GameObject Limited_Number_Hour;
	public GameObject Condition1_Pic;
	public GameObject Condition1_Number;
	public GameObject Condition2_Pic;
	public GameObject Condition2_Number;
	public UIAtlas Condition2Atlas;
	//Pause NGUI
	public GameObject pause_NGUI;
	public GameObject pause_Button;
	//Camera
	public GameObject camera;
	public int conNumber = 0;
	public int conversation_order = 0;
	public System.Collections.Generic.List<string> array_Conversation;
	public System.Collections.Generic.List<string> array_Conversation_Pic;
	public MyStage stageSc;
	LoadCSV saveSc;
	LoadGameEdit editSc;
	LevelSelcet levelSelect;
	CharacterToBeCall conSc;
	public bool isLoad = false;
	public AudioSource BGM;
	public AudioSource soundVD;
	public AudioClip victory;
	public AudioClip defeat;
	public bool isPlayed;
	public bool isSetMusicOff;
	public bool isFirstFade;
	public bool isFirstMove;
	public string typeID;
	public string typeID2;
	public int typeIndex;
	public int typeIndex2;
	public ConditionLanguage CL;
	public string limit = "";
	public string limit_Number = "";
	public string rule1 = "";
	public string rule1_Number = "";
	public string rule2 = "";
	public string rule2_Number = "";
	public int rule1Index;
	public int rule2Index;
	public int CharTextIndex;
	public CharBehavior CB;
	public bool ismoved;
	public float MouseUPTime = 0;
	public LoadScore LS;
	//黑畫面
	public GameObject AllBlack;
	public bool Darkness;
	public Animator AllBlack_Animator;

	//teach
	public LoadTeach LT;
	public GameObject teachUI;
	public GameObject teachSprite;
	public GameObject teachText;
	public UILabel teachButton;
	private Font[] font = new Font[4];

	//對話
	public bool isTexted = false;
	public bool isSpaceTexted = false;
	public string changedText;

	//鈴鐺破紀錄
	public bool isBellUP;
	public MyParse myParse;
	public TestNetwork testNetwork;

	//商城道具是否使用過了
	MallController MC;
	public bool isMallItemUp = false;
	public bool isMallUp_Star = false;
	public bool isMallUp_Bomb = false;
	public bool isMallUp_RandomItem = false;

	//商城隨機道具表演
	public GameObject RandomItemBG;
	public GameObject RandomItem;
	public RateManager RM;
	private static int Number_Game = 0;
	private static int Number_Win = 0;
	private static int Number_Lose = 0;
	private int Number_Max_Game = 2;
	private int Number_Max_Game_Video = 2;
	private int whichAD = 3;
	private bool isAddNumber = false;
	private bool isPlayedAd = false;


	//Arabic
	private bool tashkeel = true;
	private bool hinduNumbers = true;
	private bool isChangeConditionText = false;
	
	void Start ()
	{
//		Debug.Log ("enter character script isplayad");
		AdvertiseManager.Request ();

		myParse = GameObject.Find ("EventControoler").GetComponent<MyParse> ();
		CB = this.GetComponent<CharBehavior> ();

		isSetMusicOff = false;
		isFirstFade = false;
		for (int i=0; i < 23; i++)
		{
			listCondition1.Add (false);
			listCondition2.Add (false);
			listLimit1.Add (-1);
			listLimit2.Add (-1);
		}
		
		stageSc = GameObject.FindGameObjectWithTag ("LevelManager").GetComponent<MyStage> ();
		saveSc = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent<LoadCSV> ();
		editSc = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent<LoadGameEdit> ();
		levelSelect = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent<LevelSelcet> ();
		saveScript = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent<SaveMyData> ();
		MC = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent<MallController> ();
		conSc = this.GetComponent<CharacterToBeCall> ();
		
		BGM = GameObject.FindGameObjectWithTag ("MusicBox").GetComponent<AudioSource> ();
		soundVD = GameObject.Find ("soundVD").GetComponent<AudioSource> ();
		//		print (stageSc.txtTimeLimited);

		CL = GetComponent<ConditionLanguage> ();

		LS = GameObject.Find ("ScoreData").GetComponent<LoadScore> ();

		LT = GameObject.Find ("TeachData").GetComponent<LoadTeach> ();

		font [0] = Resources.Load ("Font/JF Flat regular", typeof(Font)) as Font;
		font [1] = Resources.Load ("Font/ZN", typeof(Font)) as Font;
		font [2] = Resources.Load ("Font/JP", typeof(Font)) as Font;
		font [3] = Resources.Load ("Font/CN", typeof(Font)) as Font;

		myParse = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent<MyParse> ();
		testNetwork = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent<TestNetwork> ();

		RM = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent<RateManager> ();

		MyParse.MinusMallMoney (MC.TotalCost);
	}
	
	void MainTimerDes ()
	{
		Timer -= Time.deltaTime * timeSpeed;
	}
	
	void OtherTimerIns ()
	{
		otherTimer += Time.deltaTime * timeSpeed;
	}

	//NGUI 使用
	void Skip ()
	{
		Over ();
	}

	public void Over ()
	{
		Char_Left.SetActive (false);
		Char_Right.SetActive (false);
		Con_Content.SetActive (false);
		Con_Background.SetActive (false);
		Con_Plus_Left.SetActive (false);
		Con_Plus_Right.SetActive (false);
		Con_Black.SetActive (false);
		Skip_Button.SetActive (false);

		array_Conversation.Clear ();
		array_Conversation_Pic.Clear ();
		
		if (stageSc.gameStatus == 1)
		{
			//			print("Over When status1");
			stageSc.gameStatus = 9;	
			isFirstFade = false;
		}
		else if (stageSc.gameStatus == 7)
		{
			stageSc.gameStatus = 6;
		}
	}

	public bool getTimer = false;
	
	int GetFinishCondition ()
	{
		int winNumber = 0;
		//條件1
		for (int a = 0; a < listCondition1.Count; a++)
		{
			if (listCondition1 [a] == true)
			{
				winNumber++;
			}
		}
		//條件2
		for (int b = 0; b < listCondition2.Count; b++)
		{
			if (listCondition2 [b] == true)
			{
				winNumber++;
			}
		}
		//		print (winNumber);
		return winNumber;
	}

	public void SetText ()
	{
		changedText = array_Conversation [conNumber].Replace ("\\n", "\n");
		changedText = changedText.Replace ("/comma", ", ");
		/*Con_Content.GetComponent<UILabel> ().text = array_Conversation[conNumber].Replace("\\n","\n");
		Con_Content.GetComponent<UILabel> ().text = Con_Content.GetComponent<UILabel> ().text.Replace("%d", ", ");*/
		Con_Content.GetComponent<UILabel> ().text = changedText;
		isTexted = true;
	}

	public void SpaceText ()
	{
		Con_Content.GetComponent<UILabel> ().text = "";
		isSpaceTexted = true;
	}

	void Update ()
	{
		//Debug.Log (stageSc.gameStatus);
		for (int a = 0; a < editSc.array_txtDataName.Count; a++)
		{
			if (LevelSelcet.pressNow_LevelName == editSc.array_txtDataName [a])
			{
				limit = editSc.array_Limited [a];
				limit_Number = editSc.array_LimitedNumber [a];
				rule1 = editSc.array_Rule_1 [a];
				rule1_Number = editSc.array_Rule_1_Number [a];
				rule2 = editSc.array_Rule_2 [a];
				rule2_Number = editSc.array_Rule_2_Number [a];
			}
		}

		if (stageSc.isHourglass)
		{
			stageSc.isHourglass = false;
			Timer += 15;
		}

		if (stageSc.isMall_Hourglass)
		{
			stageSc.isMall_Hourglass = false;
			Timer += 30;
		}
		/*
	0:Loading
	1:Story
	2:Show Condition & Walk
	3:Teach
	4:Go
	5:Play
	6:Clear
	7:Story
	 */
		switch (stageSc.gameStatus)
		{
			
		//Story
			case 1:
				Condition1_Pic.GetComponent<UISprite> ().spriteName = "";
				Condition2_Pic.GetComponent<UISprite> ().spriteName = "";
			
				Con_Content.GetComponent<UILabel> ().trueTypeFont = font [PlayerPrefs.GetInt ("Language")];

				if (LoadCSV.isReadFinish && !isLoad)
				{
					//取得圖片 內容 陣列
					array_Conversation = saveSc.GetConversationContent (LevelSelcet.pressNow_LevelName, "關卡開始", "EN");
					//取得圖片 名稱 陣列
					array_Conversation_Pic = saveSc.GetConversationPicture (LevelSelcet.pressNow_LevelName, "關卡開始");
				
					isLoad = true;
				}

				if (saveSc && isLoad == true && array_Conversation.Count == 0)
				{//沒有對話
					Over ();
				}
				else if (saveSc && array_Conversation.Count > 0)
				{//有對話
				
					Con_Content.SetActive (true);
					Con_Background.SetActive (true);
					Con_Black.SetActive (true);
					Skip_Button.SetActive (true);
				
					Con_Plus_Left.SetActive (true);
					Char_Left.SetActive (true);
					Con_Plus_Right.SetActive (true);
					Char_Right.SetActive (true);
				
					//對話未結束
					if (conNumber < array_Conversation.Count)
					{
						for (int i = 0; i < saveSc.array_SceneTable_Name.Count; i++)
						{
							if (array_Conversation_Pic [conNumber] == saveSc.array_SceneTable_Name [i])
							{
								//Debug.Log("I'M here1" + array_Conversation_Pic[conNumber]);
								array_Conversation = saveSc.GetConversationContentJoin (array_Conversation_Pic [conNumber]);
								array_Conversation_Pic = saveSc.GetConversationPictureJoin (array_Conversation_Pic [conNumber]);
							
								conNumber = 0;
							
								//Debug.Log("I'M here2" + array_Conversation_Pic[conNumber]);
								//取得圖片 名稱 陣列
								//array_Conversation_Pic = saveSc.GetConversationPicture (LevelSelcet.pressNow_LevelName,"關卡開始");
								break;
							}
						}

						SpriteRenderer char_right_renderer;
						char_right_renderer = Char_Right.GetComponent<SpriteRenderer> ();
					
						CharPosition CP;
						CP = this.GetComponent<CharPosition> ();
					
						if (Darkness)
						{
							if (AllBlack.GetComponent<UI2DSprite> ().alpha == 0)
							{
								conNumber++;
							}
						}
						if (array_Conversation_Pic [conNumber] == "Black")
						{
							AllBlack.SetActive (true);
							Darkness = true;
						}
						else
						{
							AllBlack.SetActive (false);
							Darkness = false;
						}

						if (!isFirstMove)
						{
							isFirstMove = true;
							CP.MovePosition (array_Conversation_Pic [conNumber].Substring (1, 1), "", array_Conversation_Pic [conNumber].Substring (0, 1));
						}
						if (!Darkness)
						{
							if (array_Conversation_Pic [conNumber].Substring (0, 1) == "R")
							{
								Con_Plus_Left.GetComponent<UISprite> ().enabled = false;
								Con_Plus_Right.GetComponent<UISprite> ().enabled = true;
								char_right_renderer.sprite = Resources.Load<Sprite> ("Altas/Char/" + array_Conversation_Pic [conNumber].Substring (3));
							}
							else
							{
								Con_Plus_Left.GetComponent<UISprite> ().enabled = true;
								Con_Plus_Right.GetComponent<UISprite> ().enabled = false;
								Char_Left.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Altas/Char/" + array_Conversation_Pic [conNumber].Substring (3));
							}
						}

						if (array_Conversation_Pic [conNumber].Substring (1, 1) == "I" && !ismoved)
						{
							CP.check = 0;
							CP.MovePosition (array_Conversation_Pic [conNumber].Substring (1, 1), "", array_Conversation_Pic [conNumber].Substring (0, 1));
							ismoved = true;
						}	

						if (Input.GetMouseButtonUp (0))
						{//左鍵OR一指觸碰
						
						
							if (array_Conversation_Pic [conNumber].Substring (1, 1) != "I")
							{
								SetText ();
							}
						
							if (array_Conversation_Pic [conNumber].Substring (0, 1) == "R")
							{
								if (Char_Right.transform.localPosition.x <= 121)
								{
								
									if (Con_Content.GetComponent<UILabel> ().text == changedText)
									{//左鍵，字出現後接下一句
		
										isSpaceTexted = false;
										ismoved = false;

										conNumber++;
										if (conNumber == array_Conversation_Pic.Count)
										{
											return;
										}
										if (array_Conversation_Pic [conNumber].Substring (1, 1) != "I")
										{
											isTexted = false;
										}
									
									}
								}

								if (!CP.Bool_L_Move && !Darkness && !isSpaceTexted)
								{
									SpaceText ();
								}
							}
							else
							{
								if (Char_Left.transform.localPosition.x >= -101)
								{
								
									if (Con_Content.GetComponent<UILabel> ().text == changedText)
									{//左鍵，字出現後接下一句
									
										isSpaceTexted = false;
										ismoved = false;

										conNumber++;
										if (conNumber == array_Conversation_Pic.Count)
										{
											return;
										}
									
										if (array_Conversation_Pic [conNumber].Substring (1, 1) != "I")
										{
											isTexted = false;
										}

									}
								}
								if (!CP.Bool_L_Move && !Darkness && !isSpaceTexted)
								{
									SpaceText ();
								}
							}

							if (!Darkness)
							{
								if (Char_Right.transform.localPosition.x >= 122)
								{//左鍵，右角色圖如未到位則直接到位
									Char_Right.transform.localPosition = new Vector3 (120, 148, 0);
								}
							
								if (Char_Left.transform.localPosition.x <= -101)
								{//左鍵，右角色圖如未到位則直接到位
									Char_Left.transform.localPosition = new Vector3 (-101, 152, 0);
								} 
							}
						
							if (Darkness)
							{
								AllBlack_Animator.SetBool ("UnDark_Bool", true);
								char_right_renderer.sprite = null;
								Char_Left.GetComponent<SpriteRenderer> ().sprite = null;
							}
						}
						if (array_Conversation_Pic [conNumber].Substring (0, 1) == "R")
						{
							if (!CP.Bool_R_Move && !Darkness && !isTexted)
							{
								SetText ();
							} 
						}
						else
						{
							if (!CP.Bool_L_Move && !Darkness && !isTexted)
							{
								SetText ();
							}
						}
					
					}
					else if (conNumber >= array_Conversation.Count)
					{
						Over ();
					}
				}
				break;

			
		//過關條件 & 小恐龍走路
			case 2:
				SetAllBrickNumber ();
				teachUI.SetActive (false);
			
			//重設 故Story參數 重覆使用
				isLoad = false;
				conNumber = 0;
			
				BlackBackgrund.SetActive (true);
				Condition_Text.SetActive (true);
				Condition1.SetActive (true);
				Condition2.SetActive (true);
				Limited.SetActive (true);
				LimitedPicture.SetActive (true);
			
			//預先讀取 MODE
				TimeMode = Convert.ToBoolean (stageSc.timeLimited);
				LifeMode = Convert.ToBoolean (stageSc.lifeLimited);
				if (TimeMode)
				{
					//				print ("TimMode");
					Limited_Pic_Hour.SetActive (true);
					Limited_Number_Hour.SetActive (true);
					Limited_Pic_Love.SetActive (false);
					Limited_Number_Love.SetActive (false);
				}
				else
				{
					//				print ("LifeMode");
					Limited_Pic_Hour.SetActive (false);
					Limited_Number_Hour.SetActive (false);
					Limited_Pic_Love.SetActive (true);
					Limited_Number_Love.SetActive (true);
				}
				if (listName.Count == 1)
				{
					Condition1_Pic.SetActive (true);
					Condition1_Number.SetActive (true);
				}
				if (listName.Count == 2)
				{
					Condition1_Pic.SetActive (true);
					Condition1_Number.SetActive (true);
					Condition2_Pic.SetActive (true);
					Condition2_Number.SetActive (true);
				}
			//暫停 按鈕
				pause_Button.SetActive (true);
			//背包
				Pic_Pack.SetActive (true);
			//顯示 限制 NGUI
				ShowLimited ();
			//取得時間
				if (!getTimer && TimeMode)
				{
					Timer = float.Parse (stageSc.txtTimeLimited) + 0.5f;
					getTimer = true;
				}
			//顯示 條件 NGUI
				ShowCondition ();
			
				string[] tempCondition = editSc.GetConditions (LevelSelcet.pressNow_LevelName);
			
				string sMain = "";
				string s1 = "";
				string s2 = "";
			
			//限制條件
			//if(Convert.ToBoolean(tempCondition[0]))
				if (TimeMode)
				{
					LimitedPicture.GetComponent<UISprite> ().spriteName = "hourglass";
				}
				else
				{
					LimitedPicture.GetComponent<UISprite> ().spriteName = "heart";
				}
			//中文
/*			if(LevelSelcet.language=="CH")
			{
				if(Convert.ToBoolean(tempCondition[0]))//Time
					sMain="限時 "+tempCondition[1] +"秒";
				else
					sMain="彈射 "+tempCondition[1] +"次";
				if(tempCondition[2]!="empty")
					s1=tempCondition[2]+" "+ tempCondition[3]  +"個";
				if(tempCondition[4]!="empty")
					s2=tempCondition[4]+" "+ tempCondition[5]  +"個";
			}
			else{//英文
				
				if(Convert.ToBoolean(tempCondition[0]))//Time
					sMain="Limit "+tempCondition[1] +"Secs";
				else
					sMain="Limit "+tempCondition[1] +"Times";

				if(tempCondition[2]!="empty")
					s1=tempCondition[2]+" x"+ tempCondition[3] ;
				if(tempCondition[4]!="empty")
					s2=tempCondition[4]+" x"+ tempCondition[5] ;
			}

*/

				if (Convert.ToBoolean (tempCondition [0]))
				{   //Time
					typeID = "10058";
					typeID2 = "10060";
				}
				else
				{
					typeID = "10059";
					typeID2 = "10061";
				}
			
				for (int i = 0; i < saveSc.array_TextTable_Number.Count - 1; i++)
				{
					if (saveSc.array_TextTable_Number [i] == typeID)
					{
						typeIndex = i;
						break;
					}
				}
			
				for (int i = 0; i < saveSc.array_TextTable_Number.Count - 1; i++)
				{
					if (saveSc.array_TextTable_Number [i] == typeID2)
					{
						typeIndex2 = i;
						break;
					}
				}
				sMain = saveSc.array_TextTable [typeIndex] [PlayerPrefs.GetInt ("Language")] + tempCondition [1] + saveSc.array_TextTable [typeIndex2] [PlayerPrefs.GetInt ("Language")];
			
				for (int i = 0; i < 23; i++)
				{
					//Debug.Log(CL.languageString[i]);
					if (CL.languageString [i] == rule1)
					{
						rule1Index = i + 14;
					}
					if (CL.languageString [i] == rule2)
					{
						rule2Index = i + 14;
					}
				}
				if (tempCondition [2] != "empty")
				{
					s1 = String.Format (saveSc.array_TextTable [rule1Index] [PlayerPrefs.GetInt ("Language")], tempCondition [3]);
				}
				if (tempCondition [4] != "empty")
				{
					s2 = String.Format (saveSc.array_TextTable [rule2Index] [PlayerPrefs.GetInt ("Language")], tempCondition [5]);
				}
		

				s1 = s1.Replace ("\n", "");
				s2 = s2.Replace ("\n", "");
				s1 = s1.Replace ("/comma", ", ");
				s2 = s2.Replace ("/comma", ", ");
				Limited.GetComponent<UILabel> ().text = sMain;
				Condition1.GetComponent<UILabel> ().text = s1;
				Condition2.GetComponent<UILabel> ().text = s2;


	

			//Limited.GetComponent<UILabel>().trueTypeFont = font[PlayerPrefs.GetInt ("Language")];
			//Condition1.GetComponent<UILabel>().trueTypeFont = font[PlayerPrefs.GetInt ("Language")];
			//Condition2.GetComponent<UILabel>().trueTypeFont = font[PlayerPrefs.GetInt ("Language")];
			
			
			//			Condition1=tempCondition[2];
			//			string Condition1Number=tempCondition[3];
			//
			//			Condition2=tempCondition[4];
			//			string Condition2Number=tempCondition[5];
			
			//			print(tempCondition[0]);
			//			print(tempCondition[1]);
			//			print(tempCondition[2]);
			//			print(tempCondition[3]);
			//			print(tempCondition[4]);
			//			print(tempCondition[5]);
				break;
			
		//教學圖片
			case 3:
			
				ShowLimited ();
			
			//過關條件 & 限制
				Condition_Text.GetComponent<UILabel> ().alpha -= alphaSpeed * Time.deltaTime;
				Condition1.GetComponent<UILabel> ().alpha -= alphaSpeed * Time.deltaTime;
				Condition2.GetComponent<UILabel> ().alpha -= alphaSpeed * Time.deltaTime;
				Limited.GetComponent<UILabel> ().alpha -= alphaSpeed * Time.deltaTime;
				LimitedPicture.GetComponent<UISprite> ().alpha -= alphaSpeed * Time.deltaTime;
	
				break;
		//Go
			case 4:

			//預先讀取 MODE
			/*TimeMode=Convert.ToBoolean(stageSc.timeLimited);
			LifeMode=Convert.ToBoolean(stageSc.lifeLimited);
			if(TimeMode){
				//				print ("TimMode");
				Limited_Pic_Hour.SetActive(true);
				Limited_Number_Hour.SetActive(true);
				Limited_Pic_Love.SetActive(false);
				Limited_Number_Love.SetActive(false);
			}else
			{
				//				print ("LifeMode");
				Limited_Pic_Hour.SetActive(false);
				Limited_Number_Hour.SetActive(false);
				Limited_Pic_Love.SetActive(true);
				Limited_Number_Love.SetActive(true);
			}

			//顯示 限制 NGUI
			ShowLimited();
			//顯示 條件 NGUI
			ShowCondition();
			
			
			if(listName.Count==1)
			{
				Condition1_Pic.SetActive(true);
				Condition1_Number.SetActive(true);
			}
			if(listName.Count==2)
			{
				Condition1_Pic.SetActive(true);
				Condition1_Number.SetActive(true);
				Condition2_Pic.SetActive(true);
				Condition2_Number.SetActive(true);
			}*/
			
			
			//暫停 按鈕
			//pause_Button.SetActive(true);
			//背包
			//Pic_Pack.SetActive(true);
			//顯示 限制 NGUI
			//ShowLimited();
			//顯示 條件 NGUI
			//ShowCondition();
			
				break;
			
		//Playing Game
			case 5:

				if (PlayerPrefs.GetInt ("docontinue") == 1)
				{
					PlayerPrefs.SetInt ("docontinue", 0);
					if (TimeMode)
					{
						Timer = float.Parse (stageSc.txtTimeLimited) + 0.5f;
					}
					else
					{
						stageSc.dragonLife = int.Parse (stageSc.txtLifeLimited);
					}
				}
			
				Condition_Text.SetActive (false);
				Condition1.SetActive (false);
				Condition2.SetActive (false);
				Limited.SetActive (false);
				LimitedPicture.SetActive (false);

			//顯示 限制 NGUI
				ShowLimited ();
			//顯示 條件 NGUI
				ShowCondition ();
			//Combo
				ShowComboLabel ();
			
			//取得時間
			/*if(!getTimer && TimeMode)
			{
				Timer = float.Parse (stageSc.txtTimeLimited)+0.5f;
				getTimer=true;
			}*/
			//時間倒數
				MainTimerDes ();
			
			//取得方塊數
				SetAllBrickNumber ();
			
			
			//取得 達成條件 數目
				int winNumber = GetFinishCondition ();
			
				if (TimeMode)
				{   //時間模式
					BlackBackgrund.SetActive (false);
					if (Timer <= 0)
					{   //時間到
						Timer = 0;
						fail.preSet ();
						outofText = fail.getLanguage ("10049");
						CallWaitFunction (0.05f);
						LevelFail ();
						CoinsClear ();
					}
					else
					{   //時間內(WIN)
						//Win
						if (winNumber >= listName.Count)
						{
							CallWaitFunction (0.05f);
							LS.AddTimeScore (Timer);
							LS.AddComboScore (ComboMax);
							LevelClear ();
							CoinsClear ();
						}
					}
				}
				else
				{   //次數模式
					BlackBackgrund.SetActive (false);
				
					if (dragonLife > 0)
					{   //活著(WIN)
						//Win
						if (winNumber >= listName.Count)
						{
							CallWaitFunction (0.05f);
							LS.AddLifeScore (dragonLife);
							LS.AddComboScore (ComboMax);
							LevelClear ();
							CoinsClear ();
						}
					}
					else
					{   //死亡
						fail.preSet ();
						outofText = fail.getLanguage ("10050");
						CallWaitFunction (0.05f);
						LevelFail ();
						CoinsClear ();
					}
				}
				break;
			
		//Clear
			case 6:

//				if (!isPlayedAd)
//				{
//					isPlayedAd = true;
//
//					if (Number_Win >= 2)
//					{
//						Number_Win = 0;
//						AdvertiseManager.Display ();
//					}
//
//					if (Number_Lose >= 2)
//					{
//						Number_Lose = 0;
//						AdvertiseManager.Display ();
//					}
//				}
				
				if (!isPlayedAd)
				{
					isPlayedAd = true;
					AdvertiseManager.Display ();
				}

				isMallItemUp = false;
				isMallUp_Star = false;
				MC.isPayMoney = false;
				MC.isPayLoveOrTime = false;
				MC.isPayStar = false;

			
			/*if (otherTimer<5) {
				isPlayed = false;
			}
			
			OtherTimerIns();*/
			
				Text_OutOf.GetComponent<UILabel> ().text = outofText;
			
			/*if(otherTimer>5 && otherTimer<8)//顯示CLEAR畫面
			{	
				BlackBackgrund.SetActive(false);
				Text_OutOfMove.SetActive(false);
				Text_OutOfTime.SetActive(false);
				Pic_Win_NGUI.SetActive(false);	
				Pic_Pack.SetActive(false);
				
				Limited_Pic_Love.SetActive(false);
				Limited_Number_Love.SetActive(false);
				Limited_Pic_Hour.SetActive(false);
				Limited_Number_Hour.SetActive(false);
				Condition1_Pic.SetActive(false);
				Condition1_Number.SetActive(false);
				Condition2_Pic.SetActive(false);
				Condition2_Number.SetActive(false);
				
				pause_Button.SetActive(false);
				ComboLabel.SetActive(false);*/
				
				
				
				Level_Panel.SetActive (true);
				if (!isSetLevelName)
				{
					isSetLevelName = true;
					Level_Panel_levelName.GetComponent<UILabel> ().text = LevelSelcet.pressNow_LevelName;
				}			
			/*if (!isSetMusicOff) {
				isSetMusicOff = true;
				BGM.volume = 0.0f;
			}*/

				if (isWin)
				{//Win
					Win_Panel.SetActive (true);

					if (!isBellUP)
					{
						//Text_Record.SetActive(false);
					}
					if (Treasure_Number > 0)
					{
						Treasure_Panel.SetActive (true);
					}

					if (!isWinSet)
					{
						isWinSet = true;
						stageSc.GainItem ();
					}
					//Cover_Panel.SetActive(false);
					//soundVD.clip = victory;
				
				}
				else
				{//Lose
					Lose_Panel.SetActive (true);
					Level_Panel.transform.localPosition = new Vector3 (0, -110f, 0);
					//Cover_Panel.SetActive(true);
					//soundVD.clip = defeat;
				}
				if (!isPlayed)
				{
					isPlayed = true;
					soundVD.volume = PlayerPrefs.GetFloat ("musicVolume");
					soundVD.Play ();
				}
				else if (otherTimer > 8)
				{
					BGM.volume = PlayerPrefs.GetFloat ("musicVolume");
					BGM.pitch = 1.0f;
					//stageSc.gameStatus=7;
				}
				break;
		//Stroy 過關
			case 7:
				Condition1_Pic.GetComponent<UISprite> ().spriteName = "";
				Condition2_Pic.GetComponent<UISprite> ().spriteName = "";

				if (LoadCSV.isReadFinish && !isLoad)
				{
					if (isWin)
					{//關卡成功
						//取得圖片 內容 陣列
						array_Conversation = saveSc.GetConversationContent (LevelSelcet.pressNow_LevelName, "關卡成功", "EN");
						//取得圖片 名稱 陣列
						array_Conversation_Pic = saveSc.GetConversationPicture (LevelSelcet.pressNow_LevelName, "關卡成功");
					}
					else
					{   //關卡失敗
						//取得圖片 內容 陣列
						array_Conversation = saveSc.GetConversationContent (LevelSelcet.pressNow_LevelName, "關卡失敗", "EN");
						//取得圖片 名稱 陣列
						array_Conversation_Pic = saveSc.GetConversationPicture (LevelSelcet.pressNow_LevelName, "關卡失敗");
					}
					isLoad = true;
				}

				if (saveSc && isLoad == true && array_Conversation.Count == 0)
				{//沒有對話
					Over ();
				}
				else if (saveSc && array_Conversation.Count > 0)
				{//有對話
				
					Con_Content.SetActive (true);
					Con_Background.SetActive (true);
					Con_Black.SetActive (true);
					Skip_Button.SetActive (true);
				
					Con_Plus_Left.SetActive (true);
					Char_Left.SetActive (true);
					Con_Plus_Right.SetActive (true);
					Char_Right.SetActive (true);

					//對話未結束
					if (conNumber < array_Conversation.Count)
					{
						for (int i = 0; i < saveSc.array_SceneTable_Name.Count; i++)
						{
							if (array_Conversation_Pic [conNumber] == saveSc.array_SceneTable_Name [i])
							{
								//Debug.Log("I'M here1" + array_Conversation_Pic[conNumber]);
								array_Conversation = saveSc.GetConversationContentJoin (array_Conversation_Pic [conNumber]);
								array_Conversation_Pic = saveSc.GetConversationPictureJoin (array_Conversation_Pic [conNumber]);
							
								conNumber = 0;
							
								//Debug.Log("I'M here2" + array_Conversation_Pic[conNumber]);
								//取得圖片 名稱 陣列
								//array_Conversation_Pic = saveSc.GetConversationPicture (LevelSelcet.pressNow_LevelName,"關卡開始");
								break;
							}
						}

						SpriteRenderer char_right_renderer;
						char_right_renderer = Char_Right.GetComponent<SpriteRenderer> ();
					
						CharPosition CP;
						CP = this.GetComponent<CharPosition> ();
					
						if (Darkness)
						{
							if (AllBlack.GetComponent<UI2DSprite> ().alpha == 0)
							{
								conNumber++;
							}
						}
						if (array_Conversation_Pic [conNumber] == "Black")
						{
							AllBlack.SetActive (true);
							Darkness = true;
						}
						else
						{
							AllBlack.SetActive (false);
							Darkness = false;
						}

						if (!isFirstMove)
						{
							isFirstMove = true;
							CP.MovePosition (array_Conversation_Pic [conNumber].Substring (1, 1), "", array_Conversation_Pic [conNumber].Substring (0, 1));
						}
						if (!Darkness)
						{
							if (array_Conversation_Pic [conNumber].Substring (0, 1) == "R")
							{
								Con_Plus_Left.GetComponent<UISprite> ().enabled = false;
								Con_Plus_Right.GetComponent<UISprite> ().enabled = true;
								char_right_renderer.sprite = Resources.Load<Sprite> ("Altas/Char/" + array_Conversation_Pic [conNumber].Substring (3));
							}
							else
							{
								Con_Plus_Left.GetComponent<UISprite> ().enabled = true;
								Con_Plus_Right.GetComponent<UISprite> ().enabled = false;
								Char_Left.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Altas/Char/" + array_Conversation_Pic [conNumber].Substring (3));
							}
						}

						if (array_Conversation_Pic [conNumber].Substring (1, 1) == "I" && !ismoved)
						{
							CP.check = 0;
							CP.MovePosition (array_Conversation_Pic [conNumber].Substring (1, 1), "", array_Conversation_Pic [conNumber].Substring (0, 1));
							ismoved = true;
						}	

						if (Input.GetMouseButtonUp (0))
						{//左鍵OR一指觸碰

							if (array_Conversation_Pic [conNumber].Substring (1, 1) != "I")
							{
								SetText ();
							}
						
							if (array_Conversation_Pic [conNumber].Substring (0, 1) == "R")
							{
								if (Char_Right.transform.localPosition.x <= 121)
								{
								
									if (Con_Content.GetComponent<UILabel> ().text == changedText)
									{//左鍵，字出現後接下一句

										isSpaceTexted = false;
										ismoved = false;

										conNumber++;
										if (conNumber == array_Conversation_Pic.Count)
										{
											return;
										}
										if (array_Conversation_Pic [conNumber].Substring (1, 1) != "I")
										{
											isTexted = false;
										}
									}
								}

								if (!CP.Bool_L_Move && !Darkness && !isSpaceTexted)
								{
									SpaceText ();
								}
							}
							else
							{
								if (Char_Left.transform.localPosition.x >= -101)
								{
								
									if (Con_Content.GetComponent<UILabel> ().text == changedText)
									{//左鍵，字出現後接下一句
									
										isSpaceTexted = false;
										ismoved = false;

										conNumber++;
										if (conNumber == array_Conversation_Pic.Count)
										{
											return;
										}
									
										if (array_Conversation_Pic [conNumber].Substring (1, 1) != "I")
										{
											isTexted = false;
										}

									}
								}
								if (!CP.Bool_L_Move && !Darkness && !isSpaceTexted)
								{
									SpaceText ();
								}
							}

							if (!Darkness)
							{
								if (Char_Right.transform.localPosition.x >= 122)
								{//左鍵，右角色圖如未到位則直接到位
									Char_Right.transform.localPosition = new Vector3 (120, 148, 0);
								}
							
								if (Char_Left.transform.localPosition.x <= -101)
								{//左鍵，右角色圖如未到位則直接到位
									Char_Left.transform.localPosition = new Vector3 (-101, 152, 0);
								} 
							}
						
							if (Darkness)
							{
								AllBlack_Animator.SetBool ("UnDark_Bool", true);
								char_right_renderer.sprite = null;
								Char_Left.GetComponent<SpriteRenderer> ().sprite = null;
							}
						}
						if (array_Conversation_Pic [conNumber].Substring (0, 1) == "R")
						{
							if (!CP.Bool_R_Move && !Darkness && !isTexted)
							{
								SetText ();
							} 
						}
						else
						{
							if (!CP.Bool_L_Move && !Darkness && !isTexted)
							{
								SetText ();
							}
						}
					
					}
					else if (conNumber >= array_Conversation.Count)
					{
						Over ();
					}
				}
				break;

			case 8:
				CoinsClear ();

				if (otherTimer < 1)
				{
					isPlayed = false;
				}
			
				OtherTimerIns ();

				if (!isWin)
				{
					soundVD.clip = defeat;
				}
				else if (!isWin && otherTimer > 2)
				{
					/*Lose_Panel.SetActive(true);
				Cover_Panel.SetActive(true);*/
					BlackBackgrund.SetActive (false);
					Text_OutOfMove.SetActive (false);
					Text_OutOfTime.SetActive (false);
					Pic_Win_NGUI.SetActive (false);

					Win_clear.SetActive (false);//2014.08.26_Win_Clear_Jason
					Pic_Pack.SetActive (false);
				
					Limited_Pic_Love.SetActive (false);
					Limited_Number_Love.SetActive (false);
					Limited_Pic_Hour.SetActive (false);
					Limited_Number_Hour.SetActive (false);
					Condition1_Pic.SetActive (false);
					Condition1_Number.SetActive (false);
					Condition2_Pic.SetActive (false);
					Condition2_Number.SetActive (false);
				
					pause_Button.SetActive (false);
					ComboLabel.SetActive (false);
				
					BGM.volume = PlayerPrefs.GetFloat ("musicVolume");
					BGM.pitch = 1.0f;
					stageSc.gameStatus = 7;
				}

				if (otherTimer > 1 && otherTimer < 3)
				{   //顯示CLEAR畫面
					if (!isSetMusicOff)
					{
						isSetMusicOff = true;
						BGM.volume = 0.0f;
					}
				
					if (isWin)
					{   //Win
						//Win_Panel.SetActive(true);
						Cover_Panel.SetActive (false);
						soundVD.clip = victory;
					}
					else
					{   //Lose
						/*Lose_Panel.SetActive(true);
					Cover_Panel.SetActive(true);
					soundVD.clip = defeat;*/
					}
				
					if (!isPlayed)
					{
						isPlayed = true;
						soundVD.volume = PlayerPrefs.GetFloat ("musicVolume");
						soundVD.Play ();
					}
				}
				else if (otherTimer > 3)
				{
					BlackBackgrund.SetActive (false);
					Text_OutOfMove.SetActive (false);
					Text_OutOfTime.SetActive (false);
					Pic_Win_NGUI.SetActive (false);	

					Win_clear.SetActive (false);//2014.08.26_Win_Clear_Jason
					Pic_Pack.SetActive (false);
				
					Limited_Pic_Love.SetActive (false);
					Limited_Number_Love.SetActive (false);
					Limited_Pic_Hour.SetActive (false);
					Limited_Number_Hour.SetActive (false);
					Condition1_Pic.SetActive (false);
					Condition1_Number.SetActive (false);
					Condition2_Pic.SetActive (false);
					Condition2_Number.SetActive (false);
				
					pause_Button.SetActive (false);
					ComboLabel.SetActive (false);

					BGM.volume = PlayerPrefs.GetFloat ("musicVolume");
					BGM.pitch = 1.0f;
					stageSc.gameStatus = 7;
				}
				break;
			
			case 9: //教學
				if (LT.index < LT.length)
				{
					teachUI.SetActive (true);
					teachSprite.GetComponent<UI2DSprite> ().sprite2D = LT.getSprite ();
					teachText.GetComponent<UILabel> ().text = LT.getText ();
					teachText.GetComponent<UILabel> ().text = ArabicFixer.Fix (teachText.GetComponent<UILabel> ().text, tashkeel, hinduNumbers);
					teachText.GetComponent<UILabel> ().trueTypeFont = font [PlayerPrefs.GetInt ("Language")];

					if (LT.index < LT.length - 1)
					{
						teachButton.text = saveSc.getTextByID ("10091");
					}
					else
					{
						teachButton.text = saveSc.getTextByID ("10092");
					}
					teachButton.trueTypeFont = font [PlayerPrefs.GetInt ("Language")];
				}
				break;

			case 10: //商城道具_愛心和沙漏
				if (MC.Used_LoveOrTime /*&& (MC.isPayLoveOrTime || MC.isPayMoney)*/ && !isMallItemUp)
				{
					isMallItemUp = true;
					if (MC.isPayLoveOrTime)
					{
						MC.LoveOrTime_Number--;
						PlayerPrefs.SetInt ("LoveOrTime_Number", PlayerPrefs.GetInt ("LoveOrTime_Number") - 1);
						//MC.isPayLoveOrTime = false;
					}
					else if (MC.isPayMoney)
					{
//						if (testNetwork.isConnect && !testNetwork.isReConnect)
//						{
//
//						}
//						else
//						{
							PlayerPrefs.SetInt ("costed", PlayerPrefs.GetInt ("costed") + MC.LoveOrTime_Cost);
//							Debug.LogWarning ("enter cost " + PlayerPrefs.GetInt ("costed"));
//						}
						MC.TotalCost = 0;
						//MC.isPayMoney = false;
					}
					MC.Used_LoveOrTime = false;
					MC.isPayMoney = MC.Used_LoveOrTime || MC.Used_Star || MC.Used_Bomb || MC.Used_RandomItem;
					teachUI.SetActive (false);
					if (MC.isLove)
					{
						GameObject Mall_Love = Instantiate (Resources.Load ("Game_Mall_Items/Mall_Love", typeof(GameObject)), new Vector3 (0, -9.44f, 0), Quaternion.identity) as GameObject;
					}
					else
					{
						GameObject Mall_Hourglass = Instantiate (Resources.Load ("Game_Mall_Items/Mall_Hourglass", typeof(GameObject)), new Vector3 (0, -9.44f, 0), Quaternion.identity) as GameObject;
					}
				}
				else
				{
					if (isMallItemUp)
					{
						return;
					}
					else
					{
						stageSc.gameStatus = 11;
					}
				}
				break;
			case 11: //商城道具_超級星星

				if (MC.Used_Star /*&& (MC.isPayLoveOrTime || MC.isPayMoney)*/ && !isMallUp_Star)
				{
					//Debug.Log("im here     " +"MC.Used_LoveOrTim     "+MC.Used_LoveOrTime+"     MC.isPayLoveOrTime     "+MC.isPayLoveOrTime+"  MC.isPayMoney    "+ MC.isPayMoney   +  "  isMallItemUp "+ isMallItemUp);
					isMallUp_Star = true;
					if (MC.isPayStar)
					{
						MC.Star_Number--;
						PlayerPrefs.SetInt ("Star_Number", PlayerPrefs.GetInt ("Star_Number") - 1);
						//MC.isPayStar = false;
					}
					else if (MC.isPayMoney)
					{
//						if (testNetwork.isConnect && !testNetwork.isReConnect)
//						{
//
//						}
//						else
//						{
							PlayerPrefs.SetInt ("costed", PlayerPrefs.GetInt ("costed") + MC.Star_Cost);
//						}
						MC.TotalCost = 0;
						//MC.isPayMoney = false;
					}
					MC.Used_Star = false;
					MC.isPayMoney = MC.Used_LoveOrTime || MC.Used_Star || MC.Used_Bomb || MC.Used_RandomItem;
					teachUI.SetActive (false);
					GameObject Mall_Star1 = Instantiate (Resources.Load ("Game_Mall_Items/Mall_Star1", typeof(GameObject)), new Vector3 (0, -9.44f, 0), Quaternion.identity) as GameObject;
				}
				else
				{
					if (isMallUp_Star)
					{
						return;
					}
					else
					{
						stageSc.gameStatus = 12;
					}
				}
			
				break;

			case 12: //商城道具_鞭炮

				if (listName.Count == 1)
				{
					Condition1_Pic.SetActive (true);
					Condition1_Number.SetActive (true);
				}
				if (listName.Count == 2)
				{
					Condition1_Pic.SetActive (true);
					Condition1_Number.SetActive (true);
					Condition2_Pic.SetActive (true);
					Condition2_Number.SetActive (true);
				}
			//顯示 條件 NGUI
				ShowCondition ();

				if (MC.Used_Bomb /*&& (MC.isPayLoveOrTime || MC.isPayMoney)*/ && !isMallUp_Bomb)
				{
					//Debug.Log("im here     " +"MC.Used_LoveOrTim     "+MC.Used_LoveOrTime+"     MC.isPayLoveOrTime     "+MC.isPayLoveOrTime+"  MC.isPayMoney    "+ MC.isPayMoney   +  "  isMallItemUp "+ isMallItemUp);
					isMallUp_Bomb = true;
					if (MC.isPayBomb)
					{
						MC.Bomb_Number--;
						PlayerPrefs.SetInt ("Bomb_Number", PlayerPrefs.GetInt ("Bomb_Number") - 1);
						Debug.Log ("im hrer");
						//MC.isPayStar = false;
					}
					else if (MC.isPayMoney)
					{
//						if (testNetwork.isConnect && !testNetwork.isReConnect)
//						{
//
//						}
//						else
//						{
							PlayerPrefs.SetInt ("costed", PlayerPrefs.GetInt ("costed") + MC.Bomb_Cost);
//						}
						MC.TotalCost = 0;
						//MC.isPayMoney = false;
					}

					MC.Used_Bomb = false;
					MC.isPayMoney = MC.Used_LoveOrTime || MC.Used_Star || MC.Used_Bomb || MC.Used_RandomItem;
					teachUI.SetActive (false);
					GameObject Mall_Bomb = Instantiate (Resources.Load ("Game_Mall_Items/Mall_Bomb", typeof(GameObject)), new Vector3 (0, 2.18f, 0), Quaternion.identity) as GameObject;	
				}
				else
				{
					if (isMallUp_Bomb)
					{
						return;
					}
					else
					{
						stageSc.gameStatus = 2;
					}
				}
				break;
			case 13: //商城道具_隨機道具
				if (MC.Used_RandomItem /*&& (MC.isPayLoveOrTime || MC.isPayMoney)*/ && !isMallUp_RandomItem)
				{
					//Debug.Log("im here     " +"MC.Used_LoveOrTim     "+MC.Used_LoveOrTime+"     MC.isPayLoveOrTime     "+MC.isPayLoveOrTime+"  MC.isPayMoney    "+ MC.isPayMoney   +  "  isMallItemUp "+ isMallItemUp);
					isMallUp_RandomItem = true;
					if (MC.isPayRandomItem)
					{
						MC.RandomItem_Number--;
						PlayerPrefs.SetInt ("RandomItem_Cost", PlayerPrefs.GetInt ("RandomItem_Cost") - 1);
						//MC.isPayStar = false;
					}
					else if (MC.isPayMoney)
					{
//						if (testNetwork.isConnect && !testNetwork.isReConnect)
//						{
//		
//						}
//						else
//						{
							PlayerPrefs.SetInt ("costed", PlayerPrefs.GetInt ("costed") + MC.RandomItem_Cost);
//						}
						MC.TotalCost = 0;
						//MC.isPayMoney = false;
					}
			
					MC.Used_RandomItem = false;
					MC.isPayMoney = MC.Used_LoveOrTime || MC.Used_Star || MC.Used_Bomb || MC.Used_RandomItem;
					teachUI.SetActive (false);
					//RandomItemBG.SetActive(true);
					Game_RandomItem GR = this.GetComponent<Game_RandomItem> ();
					GR.RandomMallItem ();
					//GameObject Mall_Bomb = Instantiate(Resources.Load("Game_Mall_Items/Mall_Bomb" , typeof(GameObject) ), new Vector3(0, 2.18f, 0), Quaternion.identity) as GameObject;	
				}
				else
				{
					if (isMallUp_RandomItem)
					{
						return;
					}
					else
					{
						stageSc.gameStatus = 2;

					}
				}
				break;
			case 14: //get mall item
				if (isMall_HPTime)
				{
					isMall_HPTime = false;
					time = 3f;
					Mall_HPTime.SetActive (true);
				}
				if (isMall_Star)
				{
					isMall_Star = false;
					time = 3f;
					Mall_Star.SetActive (true);
				}
				if (isMall_Bomb)
				{
					isMall_Bomb = false;
					time = 3f;
					Mall_Bomb.SetActive (true);
				}

				time -= Time.deltaTime;
				if (time <= 0.0f)
				{
					Mall_HPTime.SetActive (false);
					Mall_Star.SetActive (false);
					Mall_Bomb.SetActive (false);

				}

				break;
			case 15:
				Text_OutOfMove.SetActive (false);
				Text_OutOfTime.SetActive (false);

				break;
		}//Case END
	}//UPDATE END
	
	//等待時間=======================================
	private void CallWaitFunction (float fSecond)
	{
		StartCoroutine (WaitSomeSecond (fSecond));
	}
	
	IEnumerator WaitSomeSecond (float fSecond)
	{    
		yield return new WaitForSeconds (fSecond);
	}
	//=======================================等待時間
	
	
	
	
	void LevelClear ()
	{
		Number_Win += 1;
		if (!isAddNumber)
		{
			isAddNumber = true;
			Number_Game += 1;
		}

		if (LS.getScore () > PlayerPrefs.GetInt ("score" + levelSelect.s + "-" + levelSelect.l))
		{
			PlayerPrefs.SetInt ("score" + levelSelect.s + "-" + levelSelect.l, LS.getScore ());
//			if (testNetwork.isConnect)
//			{
//				myParse.InsertOrUpdateStageScore (levelSelect.s + "-" + levelSelect.l, LS.getScore ());
//			}
//			else
//			{
				PlayerPrefs.SetInt ("notUpload" + levelSelect.s + "-" + levelSelect.l, 1);
//			}
		}

		string levelTmp = LevelSelcet.pressNow_LevelName;

		//open mall can buy item
		if (levelTmp == "1-15" && PlayerPrefs.GetInt ("openLoveOrTime") != 1)
		{
			PlayerPrefs.SetInt ("openLoveOrTime", 1);
			Debug.Log (PlayerPrefs.GetInt ("openLoveOrTime"));
		}
		else if (levelTmp == "2-10" && PlayerPrefs.GetInt ("openStar") != 1)
		{
			PlayerPrefs.SetInt ("openStar", 1);
			Debug.Log (PlayerPrefs.GetInt ("openStar"));
		}
		else if (levelTmp == "3-10" && PlayerPrefs.GetInt ("openBomb") != 1)
		{
			PlayerPrefs.SetInt ("openBomb", 1);
			Debug.Log (PlayerPrefs.GetInt ("openBomb"));
		}
		else if (levelTmp == "7-20" && PlayerPrefs.GetInt ("openRandomItem") != 1)
		{
			PlayerPrefs.SetInt ("openRandomItem", 1);
			Debug.Log (PlayerPrefs.GetInt ("openRandomItem"));
		}

		//PlayerPrefs.SetInt ("reward" + levelTmp, 0);
		if (levelTmp == "1-15" && PlayerPrefs.GetInt ("reward" + levelTmp) != 1)
		{
			isMall_HPTime = true;
			levelSelect.isTeachMallItem = true;
			levelSelect.itemTeachIndex = 1;
			MC.GiveReward (1, 0, 0, 0);
			PlayerPrefs.SetInt ("reward" + LevelSelcet.pressNow_LevelName, 1);
		}
		else if (levelTmp == "1-20" && PlayerPrefs.GetInt ("reward" + levelTmp) != 1)
		{
			isMall_HPTime = true;
			MC.GiveReward (1, 0, 0, 0);
			PlayerPrefs.SetInt ("reward" + LevelSelcet.pressNow_LevelName, 1);
		}
		else if (levelTmp == "2-10" && PlayerPrefs.GetInt ("reward" + levelTmp) != 1)
		{
			isMall_Star = true;
			levelSelect.isTeachMallItem = true;
			//levelSelect.itemTeachIndex = 2;
			MC.GiveReward (0, 1, 0, 0);
			PlayerPrefs.SetInt ("reward" + LevelSelcet.pressNow_LevelName, 1);
		}
		else if (levelTmp == "2-20" && PlayerPrefs.GetInt ("reward" + levelTmp) != 1)
		{
			isMall_Star = true;
			MC.GiveReward (0, 1, 0, 0);
			PlayerPrefs.SetInt ("reward" + LevelSelcet.pressNow_LevelName, 1);
		}
		else if (levelTmp == "3-10" && PlayerPrefs.GetInt ("reward" + levelTmp) != 1)
		{
			isMall_Bomb = true;
			levelSelect.isTeachMallItem = true;
			//levelSelect.itemTeachIndex = 3;
			MC.GiveReward (0, 0, 1, 0);
			PlayerPrefs.SetInt ("reward" + LevelSelcet.pressNow_LevelName, 1);
		}
		else if (levelTmp == "3-20" && PlayerPrefs.GetInt ("reward" + levelTmp) != 1)
		{
			isMall_Bomb = true;
			MC.GiveReward (0, 0, 1, 0);
			PlayerPrefs.SetInt ("reward" + LevelSelcet.pressNow_LevelName, 1);
		}
		else if (levelTmp == "4-10" && PlayerPrefs.GetInt ("reward" + levelTmp) != 1)
		{
			isMall_HPTime = true;
			MC.GiveReward (1, 0, 0, 0);
			PlayerPrefs.SetInt ("reward" + LevelSelcet.pressNow_LevelName, 1);
		}
		else if (levelTmp == "4-20" && PlayerPrefs.GetInt ("reward" + levelTmp) != 1)
		{
			isMall_Star = true;
			MC.GiveReward (0, 1, 0, 0);
			PlayerPrefs.SetInt ("reward" + LevelSelcet.pressNow_LevelName, 1);
		}
		else if (levelTmp == "5-10" && PlayerPrefs.GetInt ("reward" + levelTmp) != 1)
		{
			isMall_HPTime = true;
			MC.GiveReward (1, 0, 0, 0);
			PlayerPrefs.SetInt ("reward" + LevelSelcet.pressNow_LevelName, 1);
		}
		else if (levelTmp == "5-20" && PlayerPrefs.GetInt ("reward" + levelTmp) != 1)
		{
			isMall_Bomb = true;
			MC.GiveReward (0, 0, 1, 0);
			PlayerPrefs.SetInt ("reward" + LevelSelcet.pressNow_LevelName, 1);
		}
		else if (levelTmp == "6-10" && PlayerPrefs.GetInt ("reward" + levelTmp) != 1)
		{
			isMall_HPTime = true;
			MC.GiveReward (1, 0, 0, 0);
			PlayerPrefs.SetInt ("reward" + LevelSelcet.pressNow_LevelName, 1);
		}

		RM.AddCount ();

		//BlackBackgrund.SetActive(true);
		//Pic_Win_NGUI.SetActive(true);
		Win_clear.SetActive (true);
		isWin = true;
		stageSc.gameStatus = 8;
		//儲存Bell & 儲存關卡
		GetBellAndPinat ();
	}
	
	void LevelFail ()
	{
		Number_Lose += 1;
		if (!isAddNumber)
		{
			isAddNumber = true;
			Number_Game += 1;
		}

		if (TimeMode)
		{
			Text_OutOfTime.SetActive (true);
		}
		else
		{
			Text_OutOfMove.SetActive (true);
		}
		isWin = false;
		//stageSc.gameStatus = 8;
	}

	void CoinsClear ()
	{
		GameObject[] AllCoins;
		AllCoins = GameObject.FindGameObjectsWithTag ("Coin");
		for (int a = 0; a < AllCoins.Length; a++)
		{
			Destroy (AllCoins [a]);
		}
	}

	public bool isBellSaved = false;
	public SaveMyData saveScript;

	//顯示Win_panel Bell & 儲存Bell & 儲存關卡
	void GetBellAndPinat ()
	{
		
		switch (Bell_Number)
		{
			case 0:
				Bell1.SetActive (false);
				Bell2.SetActive (false);
				Bell3.SetActive (false);
			
				Text_Con.SetActive (false);
				Text_Good.SetActive (false);
				Text_Exce.SetActive (false);
				break;
			case 1:
				Bell1.SetActive (true);
				Bell2.SetActive (false);
				Bell3.SetActive (false);
			
				Text_Con.SetActive (true);
				Text_Good.SetActive (false);
				Text_Exce.SetActive (false);
				break;
			case 2:
				Bell1.SetActive (true);
				Bell2.SetActive (true);
				Bell3.SetActive (false);
			
				Text_Con.SetActive (false);
				Text_Good.SetActive (true);
				Text_Exce.SetActive (false);
				break;
			case 3:
				Bell1.SetActive (true);
				Bell2.SetActive (true);
				Bell3.SetActive (true);
			
				Text_Con.SetActive (false);
				Text_Good.SetActive (false);
				Text_Exce.SetActive (true);
				break;
		}
		//Save Star
		if (!isBellSaved)
		{
			int[] bellArray = saveScript.GetStage_Star (LevelSelcet.stageInt);//全部BELL陣列
			
			int oldBellNumber = bellArray [LevelSelcet.levelInt - 1];//舊的BELL數
			
			//BELL 儲存
			if (Bell_Number > oldBellNumber)
			{
				saveScript.SetStage_Star (LevelSelcet.stageInt, LevelSelcet.levelInt, Bell_Number);
				PlayerPrefs.SetInt ("Bell" + LevelSelcet.stageInt.ToString () + "-" + LevelSelcet.levelInt.ToString (), Bell_Number);
//				if (testNetwork.isConnect && !testNetwork.isReConnect)
//				{
//					myParse.UpdateBellCount (ShowBellCount.BellCount ());
//				}
				isBellUP = true;
			}
			//過關 儲存
			saveScript.SetStage_Clear (LevelSelcet.stageInt, LevelSelcet.levelInt);
			isBellSaved = true;
		}
	}

	void ShowComboLabel ()
	{
		//Combo
		this.dragonCombo = stageSc.dragonCombo;
		if (dragonCombo > ComboMax)
		{
			ComboMax = dragonCombo;
		}
		this.comboLv1 = stageSc.comboLv1;
		
		if (dragonCombo >= comboLv1)
		{
			ComboLabel.SetActive (true);
			ComboLabel.GetComponent<UILabel> ().text = dragonCombo + "x";
		}
		else
		{
			ComboLabel.SetActive (false);
			ComboLabel.GetComponent<UILabel> ().text = "";			
		}
	}
	
	void SetAllBrickNumber ()
	{
		this.Sand_Number = stageSc.Sand_Number;
		this.Soil_Number = stageSc.Soil_Number;
		this.Rock_Number = stageSc.Rock_Number;
		this.Red_Number = stageSc.Red_Number;
		this.Valcon_Number = stageSc.Valcon_Number;
		this.Black_Number = stageSc.Black_Number;
		
		this.BlueGem_Number = stageSc.BlueGem_Number;
		this.GreenGem_Number = stageSc.GreenGem_Number;
		this.RedGem_Number = stageSc.RedGem_Number;
		this.SilverGem_Number = stageSc.SilverGem_Number;
		this.GoldGem_Number = stageSc.GoldGem_Number;
		
		this.Bell_Number = stageSc.Bell_Number;
		this.Love_Number = stageSc.Love_Number;
		this.Hourglass_Number = stageSc.Hourglass_Number;
		this.FanR_Number = stageSc.FanR_Number;
		this.FanL_Number = stageSc.FanL_Number;
		
		this.Treasure_Number = stageSc.Treasure_Number;
		this.Star1_Number = stageSc.Star1_Number;
		this.Star2_Number = stageSc.Star2_Number;
		this.Star3_Number = stageSc.Star3_Number;
		
		this.ThunderBall_Number = stageSc.ThunderBall_Number;
		this.ToxicBall_Number = stageSc.ToxicBall_Number;
		this.Bumb_Number = stageSc.Bumb_Number;
		this.Ice_Number = stageSc.Ice_Number;
		this.IceCover_Number = stageSc.IceCover_Number;
		this.GunPowder_Number = stageSc.GunPowder_Number;
		this.Green_Number = stageSc.Green_Number;
	}
	
	void ShowLimited ()
	{
		//Health & Life
		this.dragonHealth = stageSc.dragonHealth;
		this.dragonLife = stageSc.dragonLife;
		//		print (this.dragonLife);
		if (TimeMode)
		{
			Limited_Number_Hour.GetComponent<UILabel> ().text = ((int)Timer).ToString ();
		}
		else
		{
			//取得生命
			Limited_Number_Love.GetComponent<UILabel> ().text = dragonLife.ToString ();
		}
	}
	
	void ShowCondition ()
	{
		//取得 條件限制 資料=====
		if (stageSc.isRead && !isGotLimited)
		{
			listName = stageSc.arrayOfLimitedRule;
			listNumber = stageSc.arrayOfLimitedRuleNumber;
			
			if (listName.Count == 0)
			{   //無條件 
				return;
			}
			else if (listName.Count == 1)
			{   //條件1種
				SetCondition (listName [0], listNumber [0], "", "");
			}
			else if (listName.Count == 2)
			{   //條件2種
				SetCondition (listName [0], listNumber [0], listName [1], listNumber [1]);
			}
			isGotLimited = true;
		}

		//勝利條件 +數量
		for (int a = 0; a < listName.Count; a++)
		{
			ShowConditionText (a, listName [a], listNumber [a]);
		}
		CheckCondition1 ();//將條件1數值存入
		CheckCondition2 ();//將條件2數值存入
	}

	//限制
	public System.Collections.Generic.List<string> listName;
	public System.Collections.Generic.List<string> listNumber;
	public bool isGotLimited = false;
	public bool TimeMode = false;
	public bool LifeMode = false;
	public System.Collections.Generic.List<int> listLimit1;
	public System.Collections.Generic.List<int> listLimit2;
	public  System.Collections.Generic.List<bool> listCondition1;
	public  System.Collections.Generic.List<bool> listCondition2;
	
	
	
	//1~5
	//	"擊破沙塊",
	//	"擊破土塊",
	//	"擊破石塊",
	//	"擊破紅塊",
	//	"擊破火山岩",
	//6~8
	//	"擊破黑塊(黑曜石)",
	//	"清光方塊(不包括黑塊)",
	//	"收集寶石(不論顏色)",
	//9~13
	//	"收集藍寶石",
	//	"收集綠寶石",
	//	"收集紅寶石",
	//	"收集白寶石",
	//	"收集黃寶石",
	//14~17
	//	"獲得愛心(沙漏)",
	//	"獲得無敵星星",
	//	"獲得分身星星",
	//	"獲得能量星星",
	//18~23
	//	"獲得寶箱",
	//	"獲得閃電球",
	//	"獲得毒液球",
	//	"獲得炸彈",
	//	"擊破結冰塊",
	//	"擊破火藥岩"
	
	public void SetCondition (string condition1, string number1, string condition2, string number2)
	{
		//		print (condition1+" "+number1+","+condition2+" "+number2);
		switch (condition1)
		{
			case "擊破沙塊":
				listLimit1 [0] = int.Parse (number1);
				break;
			case "擊破土塊":
				listLimit1 [1] = int.Parse (number1);
				break;
			case "擊破石塊":
				listLimit1 [2] = int.Parse (number1);
				break;
			case "擊破紅塊":
				listLimit1 [3] = int.Parse (number1);
				break;
			case "擊破火山岩":
				listLimit1 [4] = int.Parse (number1);
				break;
			case "擊破黑塊(黑曜石)":
				listLimit1 [5] = int.Parse (number1);
				break;
			case "清光方塊(不包括黑塊)":
				listLimit1 [6] = int.Parse (number1);
				break;
			case "收集寶石(不論顏色)":
				listLimit1 [7] = int.Parse (number1);
				break;
			case "收集藍寶石":
				listLimit1 [8] = int.Parse (number1);
				break;
			case "收集綠寶石":
				listLimit1 [9] = int.Parse (number1);
				break;
			case "收集紅寶石":
				listLimit1 [10] = int.Parse (number1);
				break;
			case "收集白寶石":
				listLimit1 [11] = int.Parse (number1);
				break;
			case "收集黃寶石":
				listLimit1 [12] = int.Parse (number1);
				break;
			case "獲得愛心(沙漏)":
				listLimit1 [13] = int.Parse (number1);
				break;
			case "獲得無敵星星":
				listLimit1 [14] = int.Parse (number1);
				break;
			case "獲得分身星星":
				listLimit1 [15] = int.Parse (number1);
				break;
			case "獲得能量星星":
				listLimit1 [16] = int.Parse (number1);
				break;
			case "獲得寶箱":
				listLimit1 [17] = int.Parse (number1);
				break;
			case "獲得閃電球":
				listLimit1 [18] = int.Parse (number1);
				break;
			case "獲得毒液球":
				listLimit1 [19] = int.Parse (number1);
				break;
			case "獲得炸彈":
				listLimit1 [20] = int.Parse (number1);
				break;
			case "擊破結冰塊":
				listLimit1 [21] = int.Parse (number1);
				break;
			case "擊破火藥岩":
				listLimit1 [22] = int.Parse (number1);
				break;
		}

		switch (condition2)
		{
			case "擊破沙塊":
				listLimit2 [0] = int.Parse (number2);
				break;
			case "擊破土塊":
				listLimit2 [1] = int.Parse (number2);
				break;
			case "擊破石塊":
				listLimit2 [2] = int.Parse (number2);
				break;
			case "擊破紅塊":
				listLimit2 [3] = int.Parse (number2);
				break;
			case "擊破火山岩":
				listLimit2 [4] = int.Parse (number2);
				break;
			case "擊破黑塊(黑曜石)":
				listLimit2 [5] = int.Parse (number2);
				break;
			case "清光方塊(不包括黑塊)":
				listLimit2 [6] = int.Parse (number2);
				break;
			case "收集寶石(不論顏色)":
				listLimit2 [7] = int.Parse (number2);
				break;
			case "收集藍寶石":
				listLimit2 [8] = int.Parse (number2);
				break;
			case "收集綠寶石":
				listLimit2 [9] = int.Parse (number2);
				break;
			case "收集紅寶石":
				listLimit2 [10] = int.Parse (number2);
				break;
			case "收集白寶石":
				listLimit2 [11] = int.Parse (number2);
				break;
			case "收集黃寶石":
				listLimit2 [12] = int.Parse (number2);
				break;
			case "獲得愛心(沙漏)":
				listLimit2 [13] = int.Parse (number2);
				break;
			case "獲得無敵星星":
				listLimit2 [14] = int.Parse (number2);
				break;
			case "獲得分身星星":
				listLimit2 [15] = int.Parse (number2);
				break;
			case "獲得能量星星":
				listLimit2 [16] = int.Parse (number2);
				break;
			case "獲得寶箱":
				listLimit2 [17] = int.Parse (number2);
				break;
			case "獲得閃電球":
				listLimit2 [18] = int.Parse (number2);
				break;
			case "獲得毒液球":
				listLimit2 [19] = int.Parse (number2);
				break;
			case "獲得炸彈":
				listLimit2 [20] = int.Parse (number2);
				break;
			case "擊破結冰塊":
				listLimit2 [21] = int.Parse (number2);
				break;
			case "擊破火藥岩":
				listLimit2 [22] = int.Parse (number2);
				break;
		}
	}
	
	//顯示文字
	void ShowConditionText (int conditionNumber, string conditionName, string conditionLimit)
	{
		int conditionLimitINT = int.Parse (conditionLimit);
		switch (conditionName)
		{
			case "擊破沙塊":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
				
					if (Sand_Number < conditionLimitINT)
					{//小於
						text = Sand_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
				
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "1";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (Sand_Number < conditionLimitINT)
					{//小於
						text = Sand_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "1";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			case "擊破土塊":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (Soil_Number < conditionLimitINT)
					{//小於
						text = Soil_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().atlas = Condition2Atlas;
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "brick02";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (Soil_Number < conditionLimitINT)
					{//小於
						text = Soil_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().atlas = Condition2Atlas;
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "brick02";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			case "擊破石塊":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (Rock_Number < conditionLimitINT)
					{//小於
						text = Rock_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "gem2";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (Rock_Number < conditionLimitINT)
					{//小於
						text = Rock_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().atlas = Condition2Atlas;
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "gem2";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			case "擊破紅塊":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (Red_Number < conditionLimitINT)
					{//小於
						text = Red_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "brick04";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (Red_Number < conditionLimitINT)
					{//小於
						text = Red_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "brick04";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			case "擊破火山岩":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (Valcon_Number < conditionLimitINT)
					{//小於
						text = Valcon_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "5";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (Valcon_Number < conditionLimitINT)
					{//小於
						text = Valcon_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "5";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			case "擊破黑塊(黑曜石)":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (Black_Number < conditionLimitINT)
					{//小於
						text = Black_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "gem4";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (Black_Number < conditionLimitINT)
					{//小於
						text = Black_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "gem4";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			case "清光方塊(不包括黑塊)":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (GetAllBrickNumberWithOutBlack () > 0)
					{
						text = GetAllBrickNumberWithOutBlack () + "/" + "0";
					}
					else
					{//等於
						text = "0" + "/" + "0";
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "gem1";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (GetAllBrickNumberWithOutBlack () > 0)
					{
						text = GetAllBrickNumberWithOutBlack () + "/" + "0";
					}
					else
					{//等於
						text = "0" + "/" + "0";
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "gem1";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			case "收集寶石(不論顏色)":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (getAllGemNumber () < conditionLimitINT)
					{//小於
						text = getAllGemNumber () + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().atlas = Condition2Atlas;
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "AllGem";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (getAllGemNumber () < conditionLimitINT)
					{//小於
						text = getAllGemNumber () + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "Allgem";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			
			
			case "收集藍寶石":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (BlueGem_Number < conditionLimitINT)
					{//小於
						text = BlueGem_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "gem_b";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (BlueGem_Number < conditionLimitINT)
					{//小於
						text = BlueGem_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "gem_b";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			case "收集綠寶石":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (GreenGem_Number < conditionLimitINT)
					{//小於
						text = GreenGem_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "gem_g";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (GreenGem_Number < conditionLimitINT)
					{//小於
						text = GreenGem_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "gem_g";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			case "收集紅寶石":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (RedGem_Number < conditionLimitINT)
					{//小於
						text = RedGem_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "gem_r";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (RedGem_Number < conditionLimitINT)
					{//小於
						text = RedGem_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "gem_r";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			case "收集白寶石":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (SilverGem_Number < conditionLimitINT)
					{//小於
						text = SilverGem_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "gem_w";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (SilverGem_Number < conditionLimitINT)
					{//小於
						text = SilverGem_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "gem_w";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			case "收集黃寶石":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (GoldGem_Number < conditionLimitINT)
					{//小於
						text = GoldGem_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "gem_y";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (GoldGem_Number < conditionLimitINT)
					{//小於
						text = GoldGem_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "gem_y";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			case "獲得愛心(沙漏)":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (TimeMode)
					{
						if (Hourglass_Number < conditionLimitINT)
						{//小於
							text = Hourglass_Number + "/" + conditionLimit;
						}
						else
						{//等於
							text = conditionLimit + "/" + conditionLimit;
						}
						Condition1_Pic.GetComponent<UISprite> ().spriteName = "hourglass";
					}
					else
					{
						if (Love_Number < conditionLimitINT)
						{//小於
							text = Love_Number + "/" + conditionLimit;
						}
						else
						{//等於
							text = conditionLimit + "/" + conditionLimit;
						}
						Condition1_Pic.GetComponent<UISprite> ().spriteName = "heart";
					}
				
					text = text.Replace ("\n", "");
				
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				
				}
				else
				{//條件2
					string text = "";
					if (TimeMode)
					{
						if (Hourglass_Number < conditionLimitINT)
						{//小於
							text = Hourglass_Number + "/" + conditionLimit;
						}
						else
						{//等於
							text = conditionLimit + "/" + conditionLimit;
						}
						Condition2_Pic.GetComponent<UISprite> ().spriteName = "hourglass";
					}
					else
					{
						if (Love_Number < conditionLimitINT)
						{//小於
							text = Love_Number + "/" + conditionLimit;
						}
						else
						{//等於
							text = conditionLimit + "/" + conditionLimit;
						}
						Condition2_Pic.GetComponent<UISprite> ().spriteName = "heart";
					}
				
					text = text.Replace ("\n", "");
				
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			case "獲得無敵星星":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (Star1_Number < conditionLimitINT)
					{//小於
						text = Star1_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "star1";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (Star1_Number < conditionLimitINT)
					{//小於
						text = Star1_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "star1";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			case "獲得分身星星":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (Star3_Number < conditionLimitINT)
					{//小於
						text = Star3_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "star3";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (Star3_Number < conditionLimitINT)
					{//小於
						text = Star3_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "star3";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			case "獲得能量星星":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (Star2_Number < conditionLimitINT)
					{//小於
						text = Star2_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "star2";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (Star2_Number < conditionLimitINT)
					{//小於
						text = Star2_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "star2";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			case "獲得寶箱":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (Treasure_Number < conditionLimitINT)
					{//小於
						text = Treasure_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "treasure";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (Treasure_Number < conditionLimitINT)
					{//小於
						text = Treasure_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "treasure";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			case "獲得閃電球":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (ThunderBall_Number < conditionLimitINT)
					{//小於
						text = ThunderBall_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "36";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (ThunderBall_Number < conditionLimitINT)
					{//小於
						text = ThunderBall_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "36";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			case "獲得毒液球":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (ToxicBall_Number < conditionLimitINT)
					{//小於
						text = ToxicBall_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "37";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (ToxicBall_Number < conditionLimitINT)
					{//小於
						text = ToxicBall_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "37";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			case "獲得炸彈":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (Bumb_Number < conditionLimitINT)
					{//小於
						text = Bumb_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "38";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (Bumb_Number < conditionLimitINT)
					{//小於
						text = Bumb_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "38";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			case "擊破結冰塊":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (Ice_Number < conditionLimitINT)
					{//小於
						text = Ice_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "47";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (Ice_Number < conditionLimitINT)
					{//小於
						text = Ice_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "47";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			case "擊破火藥岩":
				if (conditionNumber == 0)
				{//條件1
					string text = "";
					if (GunPowder_Number < conditionLimitINT)
					{//小於
						text = GunPowder_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition1_Pic.GetComponent<UISprite> ().spriteName = "48";
					Condition1_Number.GetComponent<UILabel> ().text = text;	
				}
				else
				{//條件2
					string text = "";
					if (GunPowder_Number < conditionLimitINT)
					{//小於
						text = GunPowder_Number + "/" + conditionLimit;
					}
					else
					{//等於
						text = conditionLimit + "/" + conditionLimit;
					}
					text = text.Replace ("\n", "");
					Condition2_Pic.GetComponent<UISprite> ().spriteName = "48";
					Condition2_Number.GetComponent<UILabel> ().text = text;
				}
				break;
			
			
			
			
			
			
			
			
		}//switch END
		
		
		
		
		
		
		
		
		
		
		
		
	}
	
	int getAllGemNumber ()
	{
		int gemNumber = BlueGem_Number + GreenGem_Number + RedGem_Number + SilverGem_Number + GoldGem_Number;
		return gemNumber;
	}
	
	int GetAllBrickNumberWithOutBlack ()
	{
		int brick1Number = GameObject.FindGameObjectsWithTag ("Sand").Length;
		int brick2Number = GameObject.FindGameObjectsWithTag ("Soil").Length;
		int brick3Number = GameObject.FindGameObjectsWithTag ("Rock").Length;
		int brick4Number = GameObject.FindGameObjectsWithTag ("Red").Length;
		int brick5Number = GameObject.FindGameObjectsWithTag ("Valcon").Length;
		int brick6Number = GameObject.FindGameObjectsWithTag ("Green").Length;
		return brick1Number + brick2Number + brick3Number + brick4Number + brick5Number + brick6Number;
		;
	}
	
	void CheckCondition1 ()
	{
		
		
		//	"擊破沙塊",
		//	"擊破土塊",
		//	"擊破石塊",
		//	"擊破紅塊",
		//	"擊破火山岩",
		//	"擊破黑塊(黑曜石)",
		
		//	"清光方塊(不包括黑塊)",
		
		//	"收集寶石(不論顏色)",
		//	"收集藍寶石",
		//	"收集綠寶石",
		//	"收集紅寶石",
		//	"收集白寶石",
		//	"收集黃寶石",
		
		//	"獲得愛心(沙漏)",
		//	"獲得無敵星星",
		//	"獲得分身星星",
		//	"獲得能量星星",
		//	"獲得寶箱",
		//	"獲得閃電球",
		//	"獲得毒液球",
		//	"獲得炸彈",
		//	"擊破結冰塊",
		//	"擊破火藥岩"
		
		//擊破沙塊
		if (listLimit1 [0] != -1)
		{
			if (Sand_Number >= listLimit1 [0])
			{
				listCondition1 [0] = true;
			}
			if (Sand_Number < listLimit1 [0])
			{
				listCondition1 [0] = false;
			}
		}
		//擊破土塊
		if (listLimit1 [1] != -1)
		{
			if (Soil_Number >= listLimit1 [1])
			{
				listCondition1 [1] = true;
			}
			if (Soil_Number < listLimit1 [1])
			{
				listCondition1 [1] = false;
			}
		}
		//擊破石塊
		if (listLimit1 [2] != -1)
		{
			if (Rock_Number >= listLimit1 [2])
			{
				listCondition1 [2] = true;
			}
			if (Rock_Number < listLimit1 [2])
			{
				listCondition1 [2] = false;
			}
		}
		//擊破紅塊
		if (listLimit1 [3] != -1)
		{
			if (Red_Number >= listLimit1 [3])
			{
				listCondition1 [3] = true;
			}
			if (Red_Number < listLimit1 [3])
			{
				listCondition1 [3] = false;
			}
		}	
		//擊破火山岩
		if (listLimit1 [4] != -1)
		{
			if (Valcon_Number >= listLimit1 [4])
			{
				listCondition1 [4] = true;
			}
			if (Valcon_Number < listLimit1 [4])
			{
				listCondition1 [4] = false;
			}
		}
		//擊破黑塊(黑曜石)
		if (listLimit1 [5] != -1)
		{
			if (Black_Number >= listLimit1 [5])
			{
				listCondition1 [5] = true;
			}
			if (Black_Number < listLimit1 [5])
			{
				listCondition1 [5] = false;
			}
		}
		//清光方塊(不包括黑塊)
		if (listLimit1 [6] != -1)
		{
			if (GetAllBrickNumberWithOutBlack () == 0)
			{
				listCondition1 [6] = true;
			}
			else if (GetAllBrickNumberWithOutBlack () > 0)
			{
				listCondition1 [6] = false;
			}
		}
		//收集寶石(不論顏色)
		if (listLimit1 [7] != -1)
		{
			if (getAllGemNumber () >= listLimit1 [7])
			{
				listCondition1 [7] = true;
			}
			else if (getAllGemNumber () < listLimit1 [7])
			{
				listCondition1 [7] = false;
			}			
		}		
		//	"收集藍寶石",
		if (listLimit1 [8] != -1)
		{
			if (BlueGem_Number >= listLimit1 [8])
			{
				listCondition1 [8] = true;
			}
			else if (BlueGem_Number < listLimit1 [8])
			{
				listCondition1 [8] = false;
			}			
		}	
		//	"收集綠寶石",
		if (listLimit1 [9] != -1)
		{
			if (GreenGem_Number >= listLimit1 [9])
			{
				listCondition1 [9] = true;
			}
			else if (GreenGem_Number < listLimit1 [9])
			{
				listCondition1 [9] = false;
			}			
		}	
		//	"收集紅寶石",
		if (listLimit1 [10] != -1)
		{
			if (RedGem_Number >= listLimit1 [10])
			{
				listCondition1 [10] = true;
			}
			else if (RedGem_Number < listLimit1 [10])
			{
				listCondition1 [10] = false;
			}			
		}	
		//	"收集白寶石",
		if (listLimit1 [11] != -1)
		{
			if (SilverGem_Number >= listLimit1 [11])
			{
				listCondition1 [11] = true;
			}
			else if (SilverGem_Number < listLimit1 [11])
			{
				listCondition1 [11] = false;
			}			
		}	
		//	"收集黃寶石",
		if (listLimit1 [12] != -1)
		{
			if (GoldGem_Number >= listLimit1 [12])
			{
				listCondition1 [12] = true;
			}
			else if (GoldGem_Number < listLimit1 [12])
			{
				listCondition1 [12] = false;
			}			
		}	
		//	"獲得愛心(沙漏)",
		if (listLimit1 [13] != -1)
		{
			if (GreenGem_Number >= listLimit1 [13])
			{
				listCondition1 [13] = true;
			}
			else if (GreenGem_Number < listLimit1 [13])
			{
				listCondition1 [13] = false;
			}			
		}	
		//	"獲得無敵星星",
		if (listLimit1 [14] != -1)
		{
			if (Star1_Number >= listLimit1 [14])
			{
				listCondition1 [14] = true;
			}
			else if (Star1_Number < listLimit1 [14])
			{
				listCondition1 [14] = false;
			}			
		}
		//	"獲得分身星星",
		if (listLimit1 [15] != -1)
		{
			if (Star3_Number >= listLimit1 [15])
			{
				listCondition1 [15] = true;
			}
			else if (Star3_Number < listLimit1 [15])
			{
				listCondition1 [15] = false;
			}			
		}
		//	"獲得能量星星",
		if (listLimit1 [16] != -1)
		{
			if (Star2_Number >= listLimit1 [16])
			{
				listCondition1 [16] = true;
			}
			else if (Star2_Number < listLimit1 [16])
			{
				listCondition1 [16] = false;
			}			
		}
		//	"獲得寶箱",
		if (listLimit1 [17] != -1)
		{
			if (Treasure_Number >= listLimit1 [17])
			{
				listCondition1 [17] = true;
			}
			else if (Treasure_Number < listLimit1 [17])
			{
				listCondition1 [17] = false;
			}			
		}
		//	"獲得閃電球",
		if (listLimit1 [18] != -1)
		{
			if (ThunderBall_Number >= listLimit1 [18])
			{
				listCondition1 [18] = true;
			}
			else if (ThunderBall_Number < listLimit1 [18])
			{
				listCondition1 [18] = false;
			}			
		}
		//	"獲得毒液球",
		if (listLimit1 [19] != -1)
		{
			if (ToxicBall_Number >= listLimit1 [19])
			{
				listCondition1 [19] = true;
			}
			else if (ToxicBall_Number < listLimit1 [19])
			{
				listCondition1 [19] = false;
			}			
		}
		//	"獲得炸彈",
		if (listLimit1 [20] != -1)
		{
			if (Bumb_Number >= listLimit1 [20])
			{
				listCondition1 [20] = true;
			}
			else if (Bumb_Number < listLimit1 [20])
			{
				listCondition1 [20] = false;
			}			
		}
		//	"擊破結冰塊",
		if (listLimit1 [21] != -1)
		{
			if (Ice_Number >= listLimit1 [21])
			{
				listCondition1 [21] = true;
			}
			else if (Ice_Number < listLimit1 [21])
			{
				listCondition1 [21] = false;
			}			
		}
		//	"擊破火藥岩"
		if (listLimit1 [22] != -1)
		{
			if (GunPowder_Number >= listLimit1 [22])
			{
				listCondition1 [22] = true;
			}
			else if (GunPowder_Number < listLimit1 [22])
			{
				listCondition1 [22] = false;
			}			
		}
		
	}
	
	void CheckCondition2 ()
	{
		//擊破沙塊
		if (listLimit2 [0] != -1)
		{
			if (Sand_Number >= listLimit2 [0])
			{
				listCondition2 [0] = true;
			}
			if (Sand_Number < listLimit2 [0])
			{
				listCondition2 [0] = false;
			}
		}
		//擊破土塊
		if (listLimit2 [1] != -1)
		{
			if (Soil_Number >= listLimit2 [1])
			{
				listCondition2 [1] = true;
			}
			if (Soil_Number < listLimit2 [1])
			{
				listCondition2 [1] = false;
			}
		}
		//擊破石塊
		if (listLimit2 [2] != -1)
		{
			if (Rock_Number >= listLimit2 [2])
			{
				listCondition2 [2] = true;
			}
			if (Rock_Number < listLimit2 [2])
			{
				listCondition2 [2] = false;
			}
		}
		//擊破紅塊
		if (listLimit2 [3] != -1)
		{
			if (Red_Number >= listLimit2 [3])
			{
				listCondition2 [3] = true;
			}
			if (Red_Number < listLimit2 [3])
			{
				listCondition2 [3] = false;
			}
		}	
		//擊破火山岩
		if (listLimit2 [4] != -1)
		{
			if (Valcon_Number >= listLimit2 [4])
			{
				listCondition2 [4] = true;
			}
			if (Valcon_Number < listLimit2 [4])
			{
				listCondition2 [4] = false;
			}
		}
		//擊破黑塊(黑曜石)
		if (listLimit2 [5] != -1)
		{
			if (Black_Number >= listLimit2 [5])
			{
				listCondition2 [5] = true;
			}
			if (Black_Number < listLimit2 [5])
			{
				listCondition2 [5] = false;
			}
		}
		//清光方塊(不包括黑塊)
		if (listLimit2 [6] != -1)
		{
			if (GetAllBrickNumberWithOutBlack () == 0)
			{
				listCondition2 [6] = true;
			}
			else if (GetAllBrickNumberWithOutBlack () > 0)
			{
				listCondition2 [6] = false;
			}
		}
		//收集寶石(不論顏色)
		if (listLimit2 [7] != -1)
		{
			if (getAllGemNumber () >= listLimit2 [7])
			{
				listCondition2 [7] = true;
			}
			else if (getAllGemNumber () < listLimit2 [7])
			{
				listCondition2 [7] = false;
			}			
		}		
		//	"收集藍寶石",
		if (listLimit2 [8] != -1)
		{
			if (BlueGem_Number >= listLimit2 [8])
			{
				listCondition2 [8] = true;
			}
			else if (BlueGem_Number < listLimit2 [8])
			{
				listCondition2 [8] = false;
			}			
		}	
		//	"收集綠寶石",
		if (listLimit2 [9] != -1)
		{
			if (GreenGem_Number >= listLimit2 [9])
			{
				listCondition2 [9] = true;
			}
			else if (GreenGem_Number < listLimit2 [9])
			{
				listCondition2 [9] = false;
			}			
		}	
		//	"收集紅寶石",
		if (listLimit2 [10] != -1)
		{
			if (RedGem_Number >= listLimit2 [10])
			{
				listCondition2 [10] = true;
			}
			else if (RedGem_Number < listLimit2 [10])
			{
				listCondition2 [10] = false;
			}			
		}	
		//	"收集白寶石",
		if (listLimit2 [11] != -1)
		{
			if (SilverGem_Number >= listLimit2 [11])
			{
				listCondition2 [11] = true;
			}
			else if (SilverGem_Number < listLimit2 [11])
			{
				listCondition2 [11] = false;
			}			
		}	
		//	"收集黃寶石",
		if (listLimit2 [12] != -1)
		{
			if (GoldGem_Number >= listLimit2 [12])
			{
				listCondition2 [12] = true;
			}
			else if (GoldGem_Number < listLimit2 [12])
			{
				listCondition2 [12] = false;
			}			
		}	
		//	"獲得愛心(沙漏)",
		if (listLimit2 [13] != -1)
		{
			if (GreenGem_Number >= listLimit2 [13])
			{
				listCondition2 [13] = true;
			}
			else if (GreenGem_Number < listLimit2 [13])
			{
				listCondition2 [13] = false;
			}			
		}	
		//	"獲得無敵星星",
		if (listLimit2 [14] != -1)
		{
			if (Star1_Number >= listLimit2 [14])
			{
				listCondition2 [14] = true;
			}
			else if (Star1_Number < listLimit2 [14])
			{
				listCondition2 [14] = false;
			}			
		}
		//	"獲得分身星星",
		if (listLimit2 [15] != -1)
		{
			if (Star3_Number >= listLimit2 [15])
			{
				listCondition2 [15] = true;
			}
			else if (Star3_Number < listLimit2 [15])
			{
				listCondition2 [15] = false;
			}			
		}
		//	"獲得能量星星",
		if (listLimit2 [16] != -1)
		{
			if (Star2_Number >= listLimit2 [16])
			{
				listCondition2 [16] = true;
			}
			else if (Star2_Number < listLimit2 [16])
			{
				listCondition2 [16] = false;
			}			
		}
		//	"獲得寶箱",
		if (listLimit2 [17] != -1)
		{
			if (Treasure_Number >= listLimit2 [17])
			{
				listCondition2 [17] = true;
			}
			else if (Treasure_Number < listLimit2 [17])
			{
				listCondition2 [17] = false;
			}			
		}
		//	"獲得閃電球",
		if (listLimit2 [18] != -1)
		{
			if (ThunderBall_Number >= listLimit2 [18])
			{
				listCondition2 [18] = true;
			}
			else if (ThunderBall_Number < listLimit2 [18])
			{
				listCondition2 [18] = false;
			}			
		}
		//	"獲得毒液球",
		if (listLimit2 [19] != -1)
		{
			if (ToxicBall_Number >= listLimit2 [19])
			{
				listCondition2 [19] = true;
			}
			else if (ToxicBall_Number < listLimit2 [19])
			{
				listCondition2 [19] = false;
			}			
		}
		//	"獲得炸彈",
		if (listLimit2 [20] != -1)
		{
			if (Bumb_Number >= listLimit2 [20])
			{
				listCondition2 [20] = true;
			}
			else if (Bumb_Number < listLimit2 [20])
			{
				listCondition2 [20] = false;
			}			
		}
		//	"擊破結冰塊",
		if (listLimit2 [21] != -1)
		{
			if (Ice_Number >= listLimit2 [21])
			{
				listCondition2 [21] = true;
			}
			else if (Ice_Number < listLimit2 [21])
			{
				listCondition2 [21] = false;
			}			
		}
		//	"擊破火藥岩"
		if (listLimit2 [22] != -1)
		{
			if (GunPowder_Number >= listLimit2 [22])
			{
				listCondition2 [22] = true;
			}
			else if (GunPowder_Number < listLimit2 [22])
			{
				listCondition2 [22] = false;
			}			
		}
	}

	
	
}
