﻿using UnityEngine;
using System.Collections;

public class ChangeStageInfo : MonoBehaviour {

	public LevelSelcet LS;
	public Transform island;
	public SpringPanel SP;

	public bool isMove = false;
	public float moveNextTime;
	public float nextIndex;

	// Use this for initialization
	void Start () {
		//Debug.Log("1");
		LS = GameObject.Find ("EventControoler").GetComponent<LevelSelcet> ();
		island = GameObject.Find ("Island").GetComponent<Transform> ();
		//Debug.Log(LevelSelcet.openPlayPanel);
		if ((LevelSelcet.openPlayPanel || LevelSelcet.isQuit || LevelSelcet.isMoveNext)) {
			//Debug.Log("2");
			GameObject.Find("Island").AddComponent<SpringPanel>();
			SP = GameObject.Find ("Island").GetComponent<SpringPanel>();

			LevelSelcet.openLevelPanel = false;
			LevelSelcet.openPlayPanel = false;
			LevelSelcet.isQuit = false;
			Vector2 cr = GetComponent<UIPanel>().clipOffset;

			int stageIndex = int.Parse(LS.s);
			if (LevelSelcet.isMoveNext) {
				stageIndex--;
			}

			switch (stageIndex) {
			case 1:
				island.transform.localPosition = new Vector3(-3, -26, 0);
				SP.enabled = false;
				cr.x = -3;
				cr.y = -26;
				GetComponent<UIPanel>().clipOffset = cr;
				SP.target.x = -3;
				break;
			case 2:
				island.transform.localPosition = new Vector3(-413, -26, 0);
				SP.enabled = false;
				cr.x = -3+410;
				cr.y = -26;
				GetComponent<UIPanel>().clipOffset = cr;
				SP.target.x = -413;
				break;
			case 3:
				island.transform.localPosition = new Vector3(-823, -26, 0);
				SP.enabled = false;
				cr.x = -3+820;
				cr.y = -26;
				GetComponent<UIPanel>().clipOffset = cr;
				SP.target.x = -823;
				break;
			case 4:
				island.transform.localPosition = new Vector3(-1233, -26, 0);
				SP.enabled = false;
				cr.x = -3+1230;
				cr.y = -26;
				GetComponent<UIPanel>().clipOffset = cr;
				SP.target.x = -1233;
				break;
			case 5:
				island.transform.localPosition = new Vector3(-1643, -26, 0);
				SP.enabled = false;
				cr.x = -3+1640;
				cr.y = -26;
				GetComponent<UIPanel>().clipOffset = cr;
				SP.target.x = -1643;
				break;
			case 6:
				island.transform.localPosition = new Vector3(-2053, -26, 0);
				SP.enabled = false;
				cr.x = -3+2050;
				cr.y = -26;
				GetComponent<UIPanel>().clipOffset = cr;
				SP.target.x = -2053;
				break;
			case 7:
				island.transform.localPosition = new Vector3(-2463, -26, 0);
				SP.enabled = false;
				cr.x = -3+2460;
				cr.y = -26;
				GetComponent<UIPanel>().clipOffset = cr;
				SP.target.x = -2463;
				break;
			case 8:
				island.transform.localPosition = new Vector3(-2873, -26, 0);
				SP.enabled = false;
				cr.x = -3+2870;
				cr.y = -26;
				GetComponent<UIPanel>().clipOffset = cr;
				SP.target.x = -2873;
				break;
			}
			SP.target.y = -26;
			if (LevelSelcet.isMoveNext) {
				LevelSelcet.isMoveNext = false;
				nextIndex = -3 - (stageIndex * 410);
				isMove = true;
				moveNextTime = 0.5f;
			}

		}
	}

	public void HandMove(int stageIndex, int next) {
		if (next >= 1 && next <= 7) {
			if (GameObject.Find ("Island").GetComponent<SpringPanel>() == null) {
				GameObject.Find("Island").AddComponent<SpringPanel>();
				SP = GameObject.Find ("Island").GetComponent<SpringPanel>();
			}
			Vector2 cr = GetComponent<UIPanel>().clipOffset;
			switch (stageIndex) {
			case 1:
				island.transform.localPosition = new Vector3(-3, -26, 0);
				SP.enabled = false;
				cr.x = -3;
				cr.y = -26;
				GetComponent<UIPanel>().clipOffset = cr;
				SP.target.x = -3;
				break;
			case 2:
				island.transform.localPosition = new Vector3(-413, -26, 0);
				SP.enabled = false;
				cr.x = -3+410;
				cr.y = -26;
				GetComponent<UIPanel>().clipOffset = cr;
				SP.target.x = -413;
				break;
			case 3:
				island.transform.localPosition = new Vector3(-823, -26, 0);
				SP.enabled = false;
				cr.x = -3+820;
				cr.y = -26;
				GetComponent<UIPanel>().clipOffset = cr;
				SP.target.x = -823;
				break;
			case 4:
				island.transform.localPosition = new Vector3(-1233, -26, 0);
				SP.enabled = false;
				cr.x = -3+1230;
				cr.y = -26;
				GetComponent<UIPanel>().clipOffset = cr;
				SP.target.x = -1233;
				break;
			case 5:
				island.transform.localPosition = new Vector3(-1643, -26, 0);
				SP.enabled = false;
				cr.x = -3+1640;
				cr.y = -26;
				GetComponent<UIPanel>().clipOffset = cr;
				SP.target.x = -1643;
				break;
			case 6:
				island.transform.localPosition = new Vector3(-2053, -26, 0);
				SP.enabled = false;
				cr.x = -3+2050;
				cr.y = -26;
				GetComponent<UIPanel>().clipOffset = cr;
				SP.target.x = -2053;
				break;
			case 7:
				island.transform.localPosition = new Vector3(-2463, -26, 0);
				SP.enabled = false;
				cr.x = -3+2460;
				cr.y = -26;
				GetComponent<UIPanel>().clipOffset = cr;
				SP.target.x = -2463;
				break;
			case 8:
				island.transform.localPosition = new Vector3(-2873, -26, 0);
				SP.enabled = false;
				cr.x = -3+2870;
				cr.y = -26;
				GetComponent<UIPanel>().clipOffset = cr;
				SP.target.x = -2873;
				break;
			}
			SP.target.y = -26;
			nextIndex = -3 - ((next - 1) * 410);
			isMove = true;
			moveNextTime = 0.0f;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (isMove) {
			if (moveNextTime <= 0) {
				SpringPanel.Begin(gameObject, new Vector3(nextIndex, -26, 0), 5);
				isMove = false;
			} else {
				moveNextTime -= Time.deltaTime;
			}
		}
	}
}
