﻿using UnityEngine;
using System.Collections;

public class ShowRank : MonoBehaviour {

	public MyParse myParse;
	public TestNetwork testNetwork;
	public LevelSelcet LS;

	public GameObject top1;
	public UILabel top1Name;
	public UILabel top1Score;
	public GameObject top1Loading;
	
	public GameObject self;
	public UILabel selfName;
	public UILabel selfScore;
	public GameObject selfLoading;

	public GameObject top1Sprite;
	public GameObject needNet;

	private bool done;

	// Use this for initialization
	void Start () {
		myParse = GameObject.Find ("EventControoler").GetComponent<MyParse> ();
		testNetwork = GameObject.Find ("EventControoler").GetComponent<TestNetwork> ();
		LS = GameObject.Find ("EventControoler").GetComponent<LevelSelcet> ();
		if (testNetwork.isConnect && !testNetwork.isReConnect) {
			myParse.GetStageScore(LS.s + "-" + LS.l);
		}
		if (PlayerPrefs.GetInt ("hasFriend") == 0) {
			top1.SetActive(false);
			self.SetActive(false);
		}
		if (PlayerPrefs.GetInt ("top1score" + LS.s + "-" + LS.l) > 0) {
			if (WWW.UnEscapeURL(PlayerPrefs.GetString ("top1name" + LS.s + "-" + LS.l)) == WWW.UnEscapeURL(PlayerPrefs.GetString("nickName")).ToString()) {
				top1Name.text = WWW.UnEscapeURL(PlayerPrefs.GetString ("top1name" + LS.s + "-" + LS.l));
				top1Score.text = PlayerPrefs.GetInt ("top1score" + LS.s + "-" + LS.l).ToString();
				top1.SetActive(true);
			} else {
				top1Name.text = WWW.UnEscapeURL(PlayerPrefs.GetString ("top1name" + LS.s + "-" + LS.l));
				top1Score.text = PlayerPrefs.GetInt ("top1score" + LS.s + "-" + LS.l).ToString();
				top1.SetActive(true);
				if (PlayerPrefs.GetInt ("selfscore" + LS.s + "-" + LS.l) > 0) {
					selfName.text = WWW.UnEscapeURL(PlayerPrefs.GetString ("selfname" + LS.s + "-" + LS.l));
					selfScore.text = PlayerPrefs.GetInt ("selfscore" + LS.s + "-" + LS.l).ToString();
					self.SetActive(true);
				}
			}
		}
		done = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (testNetwork.isConnect && !testNetwork.isReConnect) {
			if (myParse.isGetScore) {
				myParse.isGetScore = false;
				if (myParse.scoreName.Count == 0) {
					top1.SetActive(false);
					self.SetActive(false);
				} else if (myParse.scoreName.Count == 1) {
					if (myParse.stageScore[0].ToString() != "0") {
						top1Name.text = myParse.scoreName[0].ToString();
						top1Name.color = Color.yellow;
						top1Score.text = myParse.stageScore[0].ToString();
						top1.SetActive(true);
						self.SetActive(false);
						PlayerPrefs.SetString("top1name" + LS.s + "-" + LS.l, WWW.EscapeURL(myParse.scoreName[0].ToString()));
						PlayerPrefs.SetInt("top1score" + LS.s + "-" + LS.l, int.Parse(myParse.stageScore[0].ToString()));
					} else {
						top1.SetActive(false);
						self.SetActive(false);
					}
				} else {
					top1Name.text = myParse.scoreName[0].ToString();
					top1Score.text = myParse.stageScore[0].ToString();
					selfName.text = myParse.scoreName[myParse.scoreName.Count-1].ToString();
					selfName.color = Color.yellow;
					selfScore.text = myParse.stageScore[myParse.scoreName.Count-1].ToString();
					top1.SetActive(true);
					self.SetActive(true);

					PlayerPrefs.SetString("top1name" + LS.s + "-" + LS.l, WWW.EscapeURL(myParse.scoreName[0].ToString()));
					PlayerPrefs.SetInt("top1score" + LS.s + "-" + LS.l, int.Parse(myParse.stageScore[0].ToString()));
					PlayerPrefs.SetString("selfname" + LS.s + "-" + LS.l, WWW.EscapeURL(myParse.scoreName[myParse.scoreName.Count-1].ToString()));
					PlayerPrefs.SetInt("selfscore" + LS.s + "-" + LS.l, int.Parse(myParse.stageScore[myParse.scoreName.Count-1].ToString()));
				}
				top1Loading.SetActive(false);
				selfLoading.SetActive(false);
			}
		} else {
			needNet.SetActive(true);
			top1Name.gameObject.SetActive(false);
			top1Score.gameObject.SetActive(false);
			top1Sprite.SetActive(false);
			self.SetActive(false);

		}
	}
}
