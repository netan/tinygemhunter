﻿using UnityEngine;
using System.Collections;

public class DeleteFriendQuest : MonoBehaviour {

	public MyParse myParse;

	public GameObject UI;
	public GameObject deleteFriendItem;

	// Use this for initialization
	void Start () {
		UI = GameObject.Find ("FriendList");
	}
	
	public void OnClick() {
		myParse = GameObject.Find ("EventControoler").GetComponent<MyParse> ();
		if (myParse.isGetFriendListRef) {
			GameObject clone = NGUITools.AddChild (UI, deleteFriendItem);
			clone.GetComponentInChildren<DeleteFriend> ().FriendUI = gameObject.GetComponent<UISprite> ();
			clone.GetComponentInChildren<DeleteFriend> ().FriendButton = gameObject.GetComponent<UIButton> ();
			clone.GetComponentInChildren<DeleteFriend> ().ID = GetComponent<SetFriendListData> ().realID;
		}
	}
}
