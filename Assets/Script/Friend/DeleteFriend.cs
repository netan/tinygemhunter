﻿using UnityEngine;
using System.Collections;

public class DeleteFriend : MonoBehaviour {

	public MyParse myParse;
	public GameObject UI;
	public UISprite FriendUI;
	public UIButton FriendButton;
	public string ID;

	public bool isCancel;

	void Start() {
		myParse = GameObject.Find("EventControoler").GetComponent<MyParse>();
	}

	public void OnClick(){
		if (!isCancel) {
			FriendUI.color = Color.gray;
			FriendButton.isEnabled = false;
			myParse.DeleteFriend (ID);
		}
		Destroy (UI);
	}
}
