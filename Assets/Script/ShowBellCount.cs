﻿using UnityEngine;
using System.Collections;

public class ShowBellCount : MonoBehaviour {

	public UILabel label;
	public LoadGameEdit LGE;

	void Start () {
		LGE = GameObject.Find("EventControoler").GetComponent<LoadGameEdit>();
		label.text = BellCount().ToString() + "/" + (LGE.loadNumber * 3).ToString();
	}

	public static int BellCount() {
		int sum = 0;
		for (int i = 1; i < 8; i++) {
			for (int j = 1; j <= 20;j++) {
				sum += PlayerPrefs.GetInt("Bell" + i.ToString() + "-" + j.ToString());
			}
		}
		return sum;
	}
}
