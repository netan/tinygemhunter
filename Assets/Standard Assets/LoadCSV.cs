﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;

public class LoadCSV : MonoBehaviour
{

	//主背景圖
	public static string background_main;

	//是否讀取完成
	public static  bool isReadFinish = false;
	
	//亂數用
	public System.Collections.Generic.List<int> array_MyItems;

	//文字表 x3===============================================
	public System.Collections.Generic.List<string> array_TextTable_Number;//Number
	public System.Collections.Generic.List<string> array_TextTable_CH;//中文
	public System.Collections.Generic.List<string> array_TextTable_EN;//英文

	public List<string[]> array_TextTable = new List<string[]> ();   //all

	//劇情表 x21===============================================
	public System.Collections.Generic.List<string> array_SceneTable_Number;//Number
	public System.Collections.Generic.List<string> array_SceneTable_Name;//明稱
	public System.Collections.Generic.List<string> array_SceneTable_ApperaCondition;//出現條件
	public System.Collections.Generic.List<string> array_SceneTable_ConditionContent;//條件內容01
	public System.Collections.Generic.List<string> array_SceneTable_AppearOnce;//只出現一次

	public System.Collections.Generic.List<string> array_SceneTable_Head01;
	public System.Collections.Generic.List<string> array_SceneTable_Conversation01;
	public System.Collections.Generic.List<string> array_SceneTable_Head02;
	public System.Collections.Generic.List<string> array_SceneTable_Conversation02;
	public System.Collections.Generic.List<string> array_SceneTable_Head03;
	public System.Collections.Generic.List<string> array_SceneTable_Conversation03;
	public System.Collections.Generic.List<string> array_SceneTable_Head04;
	public System.Collections.Generic.List<string> array_SceneTable_Conversation04;
	public System.Collections.Generic.List<string> array_SceneTable_Head05;
	public System.Collections.Generic.List<string> array_SceneTable_Conversation05;
	public System.Collections.Generic.List<string> array_SceneTable_Head06;
	public System.Collections.Generic.List<string> array_SceneTable_Conversation06;
	public System.Collections.Generic.List<string> array_SceneTable_Head07;
	public System.Collections.Generic.List<string> array_SceneTable_Conversation07;
	public System.Collections.Generic.List<string> array_SceneTable_Head08;
	public System.Collections.Generic.List<string> array_SceneTable_Conversation08;
	private string[] SceneTable_wordlist = new string[6]{
		"關卡成功",
		"關卡失敗",
		"關卡開始",
		"選擇關卡",
		"關卡解鎖",
		"獲得道具"};

	//道具表===============================================
	public System.Collections.Generic.List<string> array_ItemTable_Number;
	public System.Collections.Generic.List<string> array_ItemTable_Name;//名稱
	public System.Collections.Generic.List<string> array_ItemTable_SeriesNumber;//系列編號
	public System.Collections.Generic.List<string> array_ItemTable_SeriesName;//系列名稱
	public System.Collections.Generic.List<string> array_ItemTable_Class;//類別
	public System.Collections.Generic.List<string> array_ItemTable_Scene;//場景
	public System.Collections.Generic.List<string> array_ItemTable_Possibility;//機率
	private string[] ItemTable_wordlist = new string[25]{
		"解鎖",
		"挑戰關卡秒數",
		"紅寶石分數",
		"藍寶石分數",
		"黃寶石分數",
		"白寶石分數",
		"綠寶石分數",
		"土塊分數",
		"石塊分數",
		"磚塊分數",
		"黑塊分數",
		"泡泡分數",
		"蛛網分數",
		"寶箱分數",
		"黃星星分數",
		"紫星星分數",
		"綠星星分數",
		"沙塊分數",
		"火山岩分數",
		"結冰塊分數",
		"綠塊分數",
		"果凍球分數",
		"閃電球分數",
		"炸彈分數",
		"Combo分數"
	};
	public System.Collections.Generic.List<string> array_ItemTable_RewardItem01;//獎勵項目01
	public System.Collections.Generic.List<string> array_ItemTable_RewardContent01;//獎勵內容01
	public System.Collections.Generic.List<string> array_ItemTable_RewardItem02;//獎勵項目02
	public System.Collections.Generic.List<string> array_ItemTable_RewardContent02;//獎勵內容02
	public System.Collections.Generic.List<string> array_ItemTable_RewardItem03;//獎勵項目03
	public System.Collections.Generic.List<string> array_ItemTable_RewardContent03;//獎勵內容03

	//參數表===============================================
	public System.Collections.Generic.List<string> array_ParameterTable_Number;
	public System.Collections.Generic.List<string> array_ParameterTable_Parameter;//參數
	

	//關卡 相關========================
	public int GetAllLevelCount ()
	{
		int total = 0;
		int stages = GetAllStageNumber ();
		for (int a=1; a<=stages; a++) {
			total += GetLevelNumberOfThisStage (a);
		}
		return total;
	}



	//取得 Level數
	public int GetLevelNumberOfThisStage (int stage)
	{
		int levelNumber = 0;
		string temp = "";

		if (stage < 10)
			temp = "4020" + stage.ToString ();
		else 
			temp = "402" + stage.ToString ();

		for (int a=0; a<array_ParameterTable_Number.Count; a++) {
			if (array_ParameterTable_Number [a] == temp) {
				levelNumber = int.Parse (array_ParameterTable_Parameter [a]);
			}
		}
		return levelNumber;
	}
	
	//取得 總Stage數
	public int GetAllStageNumber ()
	{
		int levelTotal = 0;
		for (int a=0; a<array_ParameterTable_Number.Count; a++) {
			if (array_ParameterTable_Number [a] == "40200") {
				levelTotal = int.Parse (array_ParameterTable_Parameter [a]);
			}
		}
		return levelTotal;
	}
	//========================關卡 相關
	//取得Combo 次數 1,2,3
	public int[] GetComboNumber123 ()
	{

		int[] abc123 = new int[3];

		for (int a=0; a<array_ParameterTable_Number.Count; a++) {

			if (array_ParameterTable_Number [a] == "40104") {
				abc123 [0] = int.Parse (array_ParameterTable_Parameter [a]);
			}
			if (array_ParameterTable_Number [a] == "40105") {
				abc123 [1] = int.Parse (array_ParameterTable_Parameter [a]);
			}
			if (array_ParameterTable_Number [a] == "40106") {
				abc123 [2] = int.Parse (array_ParameterTable_Parameter [a]);
			}
		}
		return abc123;
	}


	//轉 文字表 數字為文字
	public string ChangeNumberToText (string textNumber, string language)
	{

		string text = "";

		for (int a=0; a<array_TextTable_Number.Count; a++) {
			if (textNumber == array_TextTable_Number [a]) {
				/*switch(language)
				{
				case "ZH":
					text=array_TextTable_CH[a];
				break;
				case "EN":
					text=array_TextTable_EN[a];
					break;
				}*/
				text = array_TextTable [a] [PlayerPrefs.GetInt ("Language")];
			}
		}
		return text;
	}



	//談話 圖像
	public  System.Collections.Generic.List<string> array_Conversation_Pic;//參數

	//取得劇情表 圖像編號
	public System.Collections.Generic.List<string> GetConversationPicture (string levelName, string condition)
	{
		
		array_Conversation_Pic.Clear ();

		for (int a=0; a<array_SceneTable_ConditionContent.Count; a++) {

			if (levelName == array_SceneTable_ConditionContent [a] && condition == array_SceneTable_ApperaCondition [a]) {//1-1  ==  條件內容1

				if (array_SceneTable_Head01 [a] != "")
					array_Conversation_Pic.Add (array_SceneTable_Head01 [a]);
				if (array_SceneTable_Head02 [a] != "")
					array_Conversation_Pic.Add (array_SceneTable_Head02 [a]);
				if (array_SceneTable_Head03 [a] != "")
					array_Conversation_Pic.Add (array_SceneTable_Head03 [a]);
				if (array_SceneTable_Head04 [a] != "")
					array_Conversation_Pic.Add (array_SceneTable_Head04 [a]);
				if (array_SceneTable_Head05 [a] != "")
					array_Conversation_Pic.Add (array_SceneTable_Head05 [a]);
				if (array_SceneTable_Head06 [a] != "")
					array_Conversation_Pic.Add (array_SceneTable_Head06 [a]);
				if (array_SceneTable_Head07 [a] != "")
					array_Conversation_Pic.Add (array_SceneTable_Head07 [a]);
				if (array_SceneTable_Head08 [a] != "")
					array_Conversation_Pic.Add (array_SceneTable_Head08 [a]);

			}
		}
		
		return array_Conversation_Pic;
	}





	//談話 內容
	public  System.Collections.Generic.List<string> array_Conversation;
	//取得劇情表 文字內容
	public System.Collections.Generic.List<string> GetConversationContent (string levelName, string condition, string language)
	{

		array_Conversation.Clear ();

		for (int a=0; a<array_SceneTable_ConditionContent.Count; a++) {   //tempString[3]);//條件內容01

			if (levelName == array_SceneTable_ConditionContent [a] && condition == array_SceneTable_ApperaCondition [a]) {  //(tempString[2])] );//出現條件

				if (array_SceneTable_Conversation01 [a] != "")
					array_Conversation.Add (ChangeNumberToText (array_SceneTable_Conversation01 [a], language));
				if (array_SceneTable_Conversation02 [a] != "")
					array_Conversation.Add (ChangeNumberToText (array_SceneTable_Conversation02 [a], language));
				if (array_SceneTable_Conversation03 [a] != "")
					array_Conversation.Add (ChangeNumberToText (array_SceneTable_Conversation03 [a], language));
				if (array_SceneTable_Conversation04 [a] != "")
					array_Conversation.Add (ChangeNumberToText (array_SceneTable_Conversation04 [a], language));
				if (array_SceneTable_Conversation05 [a] != "")
					array_Conversation.Add (ChangeNumberToText (array_SceneTable_Conversation05 [a], language));
				if (array_SceneTable_Conversation06 [a] != "")
					array_Conversation.Add (ChangeNumberToText (array_SceneTable_Conversation06 [a], language));
				if (array_SceneTable_Conversation07 [a] != "")
					array_Conversation.Add (ChangeNumberToText (array_SceneTable_Conversation07 [a], language));
				if (array_SceneTable_Conversation08 [a] != "")
					array_Conversation.Add (ChangeNumberToText (array_SceneTable_Conversation08 [a], language));
			}

		}
		return array_Conversation;
	}
	
	//取得劇情表 文字內容
	public System.Collections.Generic.List<string> GetConversationContentJoin (string CharName)
	{

		array_Conversation.Clear ();
		
		for (int a = 0; a < array_SceneTable_ConditionContent.Count; a++) {
			
			if (CharName == array_SceneTable_Name [a]) {



				
				if (array_SceneTable_Conversation01 [a] != "") {
					array_Conversation.Add (ChangeNumberToText (array_SceneTable_Conversation01 [a], ""));
				}

				if (array_SceneTable_Conversation02 [a] != "") {
					array_Conversation.Add (ChangeNumberToText (array_SceneTable_Conversation02 [a], ""));
				}

				if (array_SceneTable_Conversation03 [a] != "") {
					array_Conversation.Add (ChangeNumberToText (array_SceneTable_Conversation03 [a], ""));
				}

				if (array_SceneTable_Conversation04 [a] != "") {
					array_Conversation.Add (ChangeNumberToText (array_SceneTable_Conversation04 [a], ""));
				}
				if (array_SceneTable_Conversation05 [a] != "") {
					array_Conversation.Add (ChangeNumberToText (array_SceneTable_Conversation05 [a], ""));
				}
				if (array_SceneTable_Conversation06 [a] != "") {
					array_Conversation.Add (ChangeNumberToText (array_SceneTable_Conversation06 [a], ""));
				}
				if (array_SceneTable_Conversation07 [a] != "") {
					array_Conversation.Add (ChangeNumberToText (array_SceneTable_Conversation07 [a], ""));
				}
				if (array_SceneTable_Conversation08 [a] != "") {
					array_Conversation.Add (ChangeNumberToText (array_SceneTable_Conversation08 [a], ""));
				}


			}
			
		}
		return array_Conversation;
	
	}

	
	//取得劇情表 圖像編號
	public System.Collections.Generic.List<string> GetConversationPictureJoin (string CharName)
	{
		
		array_Conversation_Pic.Clear ();
		
		for (int a=0; a<array_SceneTable_ConditionContent.Count; a++) {
			
			if (CharName == array_SceneTable_Name [a]) {
				
				if (array_SceneTable_Head01 [a] != "")
					array_Conversation_Pic.Add (array_SceneTable_Head01 [a]);
				if (array_SceneTable_Head02 [a] != "")
					array_Conversation_Pic.Add (array_SceneTable_Head02 [a]);
				if (array_SceneTable_Head03 [a] != "")
					array_Conversation_Pic.Add (array_SceneTable_Head03 [a]);
				if (array_SceneTable_Head04 [a] != "")
					array_Conversation_Pic.Add (array_SceneTable_Head04 [a]);
				if (array_SceneTable_Head05 [a] != "")
					array_Conversation_Pic.Add (array_SceneTable_Head05 [a]);
				if (array_SceneTable_Head06 [a] != "")
					array_Conversation_Pic.Add (array_SceneTable_Head06 [a]);
				if (array_SceneTable_Head07 [a] != "")
					array_Conversation_Pic.Add (array_SceneTable_Head07 [a]);
				if (array_SceneTable_Head08 [a] != "")
					array_Conversation_Pic.Add (array_SceneTable_Head08 [a]);
				
			}
		}
		
		return array_Conversation_Pic;
	}

	//談話 圖像
	
	//取得劇情表 圖像編號
	/*public System.Collections.Generic.List<string> GetConversationPictureJoin(string levelName,string condition){
		
		array_Conversation_Pic.Clear ();
		
		for(int a=0;a<array_SceneTable_ConditionContent.Count;a++){
			
			if(levelName==array_SceneTable_ConditionContent[a] && condition== array_SceneTable_ApperaCondition[a]){//1-1  ==  條件內容1
				
				if(array_SceneTable_Head01[a]!="")
					array_Conversation_Pic.Add(array_SceneTable_Head01[a]);
				if(array_SceneTable_Head02[a]!="")
					array_Conversation_Pic.Add(array_SceneTable_Head02[a]);
				if(array_SceneTable_Head03[a]!="")
					array_Conversation_Pic.Add(array_SceneTable_Head03[a]);
				if(array_SceneTable_Head04[a]!="")
					array_Conversation_Pic.Add(array_SceneTable_Head04[a]);
				if(array_SceneTable_Head05[a]!="")
					array_Conversation_Pic.Add(array_SceneTable_Head05[a]);
				if(array_SceneTable_Head06[a]!="")
					array_Conversation_Pic.Add(array_SceneTable_Head06[a]);
				if(array_SceneTable_Head07[a]!="")
					array_Conversation_Pic.Add(array_SceneTable_Head07[a]);
				if(array_SceneTable_Head08[a]!="")
					array_Conversation_Pic.Add(array_SceneTable_Head08[a]);
				
			}
		}
		
		return array_Conversation_Pic;
	}*/



















	//放所需道具用
	public  System.Collections.Generic.List<int> array_Parameter;//參數

	//解鎖需要道具
	public System.Collections.Generic.List<int>  GetLockItemsNeed (string levelName)
	{

		array_Parameter.Clear ();

		for (int x=0; x<array_ItemTable_RewardItem01.Count; x++) {
			if (array_ItemTable_RewardItem01 [x] == "解鎖" && array_ItemTable_RewardContent01 [x] == levelName) {
				array_Parameter.Add (x);
			}
		}
//		for(int y=0;y<array_Parameter.Count;y++){
//			print ("所需道具: 道具 "+array_Parameter[y]);
//		}

		return array_Parameter;
	}










	//取得道具"總"數量
	public int GetItemNumber ()
	{
		int itemNumber = array_ItemTable_Number.Count;
		return itemNumber;
	}


	//取得被背景圖
	public string getMainpageBackground ()
	{
		string mainBackgound = "";

		for (int a=0; a<array_ParameterTable_Number.Count; a++) {
			if (array_ParameterTable_Number [a] == "40100") {
				mainBackgound = array_ParameterTable_Parameter [a];
			}
		}
		return mainBackgound;
	}




	//無意義讀表格=====================================================
	//取得 教學圖片 消失時間
	public float GetTeachPicDisapppearTime ()
	{
		float time = 0.0f;
		for (int a=0; a<array_ParameterTable_Number.Count; a++) {
			if (array_ParameterTable_Number [a] == "40101") {
				time = float.Parse (array_ParameterTable_Parameter [a]);
			}
		}
//		print ("消失時間:"+time);
		return time;
	}
	//取得 撞擊地版 次數 
	public int GetDefaultEnergy ()
	{
		int time = 0;
		for (int a=0; a<array_ParameterTable_Number.Count; a++) {
			if (array_ParameterTable_Number [a] == "40102") {
				time = int.Parse (array_ParameterTable_Parameter [a]);
			}
		}
		return time;
	}
	//取得 預設  生命
	public int GetDefaultLife ()
	{
		int time = 0;
		for (int a=0; a<array_ParameterTable_Number.Count; a++) {
			if (array_ParameterTable_Number [a] == "40103") {
				time = int.Parse (array_ParameterTable_Parameter [a]);
			}
		}
		return time;
	}


	//=====================================================無意義讀表格






	void Start ()
	{
//		ReadFile ();
		StartCoroutine (ReadFileWWW ());
//		LoadFile ();

	}

	void Update ()
	{


	}

	IEnumerator  ReadFileWWW ()
	{
//		print ("getCSV");

		string levelNameNow = "Table";
		string path;
		if (Application.platform == RuntimePlatform.Android) {
			path = Application.streamingAssetsPath + "/" + levelNameNow + ".csv";//OK!!!
		} else {	
			path = "file://" + Application.streamingAssetsPath + "/" + levelNameNow + ".csv";
		}
		WWW data = new WWW (path);
		yield return data;

		CreateFile (Application.persistentDataPath, "Table.txt", data.text);
		LoadFile (Application.persistentDataPath, "Table.txt"); 

	}

	void CreateFile (string path, string name, string info)
	{   
		StreamWriter sw;   
		FileInfo t = new FileInfo (path + "//" + name);   
//		if(!t.Exists)   
//		{   
		sw = t.CreateText ();   
//		}   
//		else
//		{   
//			sw = t.AppendText();   
//		}    
		sw.WriteLine (info);   
		sw.Close ();    
		sw.Dispose ();   
	}

	void LoadFile (string path, string name)
	{   

		Debug.Log  ( "path ? " + path + "//" + name );
		//使用流的形式读取   
		StreamReader sr = null;   
//		try{   
		sr = File.OpenText (path + "//" + name);   
//		}catch(Exception e)   
//		{   
		//路径与名称未找到文件则直接返回空   
//			return null;   
//		}   
		string line;   
//		ArrayList arrlist = new ArrayList();   
		while ((line = sr.ReadLine()) != null  &&  line!="") {   
//			print (line);


			//一行一行的读取   
			//将每一行的内容存入数组链表容器中   
			string tempSR = line;


			string[] tempString = tempSR.Split (',');
						
						
			int titleNumber = int.Parse (tempString [0]);
//						TestText=titleNumber.ToString();
			//			print (titleNumber);
						
						
			//文字表
			if (titleNumber > 10000 && titleNumber < 20000) {
				string text_CH = tempString [1];
							
				//Debug.Log (text_CH);
				string text_EN = tempString [2];
				//
				array_TextTable_Number.Add (titleNumber.ToString ());
				array_TextTable_CH.Add (text_CH);
				array_TextTable_EN.Add (text_EN);

				string[] temp = new string[4];
				for (int i = 0; i < 4; i++) {
					temp [i] = tempString [i + 1];
					//Debug.Log("array_text : " + tempString[i+1]);
				}

				array_TextTable.Add (temp);
/*				for (int j = 0; j < 2; j++) {
					Debug.Log(array_TextTable[array_TextTable.Count - 1][j]);
				}
*/
			}
						//劇情表
						else if (titleNumber > 20000 && titleNumber < 30000) {
				array_SceneTable_Number.Add (titleNumber.ToString ());//Number
				array_SceneTable_Name.Add (tempString [1]);//明稱
				array_SceneTable_ApperaCondition.Add (SceneTable_wordlist [int.Parse (tempString [2])]);//出現條件

				array_SceneTable_ConditionContent.Add (tempString [3]);//條件內容01
				array_SceneTable_AppearOnce.Add (tempString [4]);//只出現一次
				array_SceneTable_Head01.Add (tempString [5]);
				array_SceneTable_Conversation01.Add (tempString [6]);
				array_SceneTable_Head02.Add (tempString [7]);
				array_SceneTable_Conversation02.Add (tempString [8]);
				array_SceneTable_Head03.Add (tempString [9]);
				array_SceneTable_Conversation03.Add (tempString [10]);
				array_SceneTable_Head04.Add (tempString [11]);
				array_SceneTable_Conversation04.Add (tempString [12]);
				array_SceneTable_Head05.Add (tempString [13]);
				array_SceneTable_Conversation05.Add (tempString [14]);
				array_SceneTable_Head06.Add (tempString [15]);
				array_SceneTable_Conversation06.Add (tempString [16]);
				array_SceneTable_Head07.Add (tempString [17]);
				array_SceneTable_Conversation07.Add (tempString [18]);
				array_SceneTable_Head08.Add (tempString [19]);
				array_SceneTable_Conversation08.Add (tempString [20]);
			}
						//道具表
						else if (titleNumber > 30000 && titleNumber < 40000) {
							
				//				print (titleNumber);
							
				array_ItemTable_Number.Add (titleNumber.ToString ());
							
							
				if (tempSR.Contains ("\"")) {
					string[] tempStringSP = tempSR.Split ('\"');
					//					print (tempStringSP[0]);
					string[] tempStringSP2 = tempStringSP [0].Split (',');
					//					print (tempStringSP2[0]);
					//					print (tempStringSP2[1]);
					//					print (tempStringSP2[2]);
					//					print (tempStringSP2[3]);
					//					print (tempStringSP2[4]);
					array_ItemTable_Name.Add (tempStringSP2 [1]);//名稱
					array_ItemTable_SeriesNumber.Add (tempStringSP2 [2]);//系列編號
					array_ItemTable_SeriesName.Add (tempStringSP2 [3]);//系列名稱
					array_ItemTable_Class.Add (tempStringSP2 [4]);//類別
								
					//複數場景
					//					print (tempStringSP[1]);
					array_ItemTable_Scene.Add (tempStringSP [1]);//場景
					//					string[] tempScene=tempStringSP[1].Split(',');
					//					print (tempScene[0]);
								
					//					print (tempStringSP[2]);
					string[] tempLeft = tempStringSP [2].Split (',');
					//					print (tempLeft[0]);//空
					//					print (tempLeft[1]);//機率
					//					print (tempLeft[2]);//獎勵項目01
								
					array_ItemTable_Possibility.Add (tempLeft [1]);//機率
								
					if (tempLeft [2] != "")
						array_ItemTable_RewardItem01.Add (ItemTable_wordlist [int.Parse (tempLeft [2])]);//獎勵項目01
								else {
						array_ItemTable_RewardItem01.Add ("");//獎勵項目01
					}
					array_ItemTable_RewardContent01.Add (tempLeft [3]);//獎勵內容01
					//					array_ItemTable_RewardItem02.Add(tempLeft[4]);//獎勵項目02
					if (tempLeft [4] != "")
						array_ItemTable_RewardItem02.Add (ItemTable_wordlist [int.Parse (tempLeft [4])]);//獎勵項目02
								else {
						array_ItemTable_RewardItem02.Add ("");//獎勵項目01
					}
					array_ItemTable_RewardContent02.Add (tempLeft [5]);//獎勵內容02
					//					array_ItemTable_RewardItem03.Add(tempLeft[6]);//獎勵項目03
					if (tempLeft [6] != "")
						array_ItemTable_RewardItem03.Add (ItemTable_wordlist [int.Parse (tempLeft [6])]);//獎勵項目03
								else {
						array_ItemTable_RewardItem03.Add ("");//獎勵項目03
					}
					array_ItemTable_RewardContent03.Add (tempLeft [7]);//獎勵內容03
								
								
				} else {
					array_ItemTable_Name.Add (tempString [1]);//名稱
					array_ItemTable_SeriesNumber.Add (tempString [2]);//系列編號
					array_ItemTable_SeriesName.Add (tempString [3]);//系列名稱
					array_ItemTable_Class.Add (tempString [4]);//類別
								
					array_ItemTable_Scene.Add (tempString [5]);//場景
								
					array_ItemTable_Possibility.Add (tempString [6]);//機率
								
								
					if (tempString [7] != "")
						array_ItemTable_RewardItem01.Add (ItemTable_wordlist [int.Parse (tempString [7])]);//獎勵項目01
								else {
						array_ItemTable_RewardItem01.Add ("");//獎勵項目01
					}
					array_ItemTable_RewardContent01.Add (tempString [8]);//獎勵內容01
					if (tempString [9] != "")
						array_ItemTable_RewardItem02.Add (ItemTable_wordlist [int.Parse (tempString [9])]);//獎勵項目02
								else {
						array_ItemTable_RewardItem02.Add ("");//獎勵項目02
					}
					array_ItemTable_RewardContent02.Add (tempString [10]);//獎勵內容02
								
					if (tempString [11] != "")
						array_ItemTable_RewardItem03.Add (ItemTable_wordlist [int.Parse (tempString [11])]);//獎勵項目02
								else {
						array_ItemTable_RewardItem03.Add ("");//獎勵項目02
					}
					array_ItemTable_RewardContent03.Add (tempString [12]);//獎勵內容03
								
				}
							
			}
						//參數表
						else if (titleNumber > 40000 && titleNumber < 50000) {
//						print (titleNumber);
				array_ParameterTable_Number.Add (titleNumber.ToString ());
				array_ParameterTable_Parameter.Add (tempString [1]);//參數(數字)
			}
//			arrlist.Add(line);   
		}   
		//关闭流   
		sr.Close ();   
		//销毁流   
		sr.Dispose ();   
		//将数组链表容器返回   
//		return arrlist;   

		isReadFinish = true;

		background_main = getMainpageBackground ();



//		print (GetItemNumber());
	}

	void ReadFile ()
	{
		string levelNameNow = "Table";
		string path;
		print (Application.platform);
		if (Application.platform == RuntimePlatform.Android) {
//			path = "jar:file://"+Application.streamingAssetsPath + "/"+levelNameNow+".csv";//No
			path = Application.streamingAssetsPath + "/" + levelNameNow + ".csv";//No
//			path = "file://" + Application.streamingAssetsPath + "/"+levelNameNow+".csv";
		} else {	
			path = Application.streamingAssetsPath + "/" + levelNameNow + ".csv";//No
//			path = "file://" + Application.streamingAssetsPath + "/"+levelNameNow+".csv";
		}
		FileStream fs = new FileStream (path, FileMode.Open, FileAccess.Read);
		StreamReader sr = new StreamReader (fs, Encoding.Default); 



		while (sr.Peek() >= 0) {
			string tempSR = sr.ReadLine ();
			string[] tempString = tempSR.Split (',');

			int titleNumber = int.Parse (tempString [0]);
//			TestText=titleNumber.ToString();
//			print (titleNumber);


			//文字表
			if (titleNumber > 10000 && titleNumber < 20000) {
				string text_CH = tempString [1];
				string text_EN = tempString [2];
//
				array_TextTable_Number.Add (titleNumber.ToString ());
				array_TextTable_CH.Add (text_CH);
				array_TextTable_EN.Add (text_EN);
			}
//			//劇情表
			else if (titleNumber > 20000 && titleNumber < 30000) {
				array_SceneTable_Number.Add (titleNumber.ToString ());//Number
				array_SceneTable_Name.Add (tempString [1]);//明稱
				print (tempString [2]);
				array_SceneTable_ApperaCondition.Add (tempString [2]);//出現條件
				array_SceneTable_ConditionContent.Add (tempString [3]);//條件內容01
				array_SceneTable_AppearOnce.Add (tempString [4]);//只出現一次
//
				array_SceneTable_Head01.Add (tempString [5]);
				array_SceneTable_Conversation01.Add (tempString [6]);
				array_SceneTable_Head02.Add (tempString [7]);
				array_SceneTable_Conversation02.Add (tempString [8]);
				array_SceneTable_Head03.Add (tempString [9]);
				array_SceneTable_Conversation03.Add (tempString [10]);
				array_SceneTable_Head04.Add (tempString [11]);
				array_SceneTable_Conversation04.Add (tempString [12]);
				array_SceneTable_Head05.Add (tempString [13]);
				array_SceneTable_Conversation05.Add (tempString [14]);
				array_SceneTable_Head06.Add (tempString [15]);
				array_SceneTable_Conversation06.Add (tempString [16]);
				array_SceneTable_Head07.Add (tempString [17]);
				array_SceneTable_Conversation07.Add (tempString [18]);
				array_SceneTable_Head08.Add (tempString [19]);
				array_SceneTable_Conversation08.Add (tempString [20]);
			}
			//道具表
			else if (titleNumber > 30000 && titleNumber < 40000) {

//				print (titleNumber);

				array_ItemTable_Number.Add (titleNumber.ToString ());


				if (tempSR.Contains ("\"")) {
					string[] tempStringSP = tempSR.Split ('\"');
//					print (tempStringSP[0]);
					string[] tempStringSP2 = tempStringSP [0].Split (',');
//					print (tempStringSP2[0]);
//					print (tempStringSP2[1]);
//					print (tempStringSP2[2]);
//					print (tempStringSP2[3]);
//					print (tempStringSP2[4]);
					array_ItemTable_Name.Add (tempStringSP2 [1]);//名稱
					array_ItemTable_SeriesNumber.Add (tempStringSP2 [2]);//系列編號
					array_ItemTable_SeriesName.Add (tempStringSP2 [3]);//系列名稱
					array_ItemTable_Class.Add (tempStringSP2 [4]);//類別

					//複數場景
//					print (tempStringSP[1]);
					array_ItemTable_Scene.Add (tempStringSP [1]);//場景
//					string[] tempScene=tempStringSP[1].Split(',');
//					print (tempScene[0]);

//					print (tempStringSP[2]);
					string[] tempLeft = tempStringSP [2].Split (',');
//					print (tempLeft[0]);//空
//					print (tempLeft[1]);//機率
//					print (tempLeft[2]);//獎勵項目01

					array_ItemTable_Possibility.Add (tempLeft [1]);//機率

					if (tempLeft [2] != "")
						array_ItemTable_RewardItem01.Add (ItemTable_wordlist [int.Parse (tempLeft [2])]);//獎勵項目01
					else {
						array_ItemTable_RewardItem01.Add ("");//獎勵項目01
					}
					array_ItemTable_RewardContent01.Add (tempLeft [3]);//獎勵內容01
//					array_ItemTable_RewardItem02.Add(tempLeft[4]);//獎勵項目02
					if (tempLeft [4] != "")
						array_ItemTable_RewardItem02.Add (ItemTable_wordlist [int.Parse (tempLeft [4])]);//獎勵項目02
					else {
						array_ItemTable_RewardItem02.Add ("");//獎勵項目01
					}
					array_ItemTable_RewardContent02.Add (tempLeft [5]);//獎勵內容02
//					array_ItemTable_RewardItem03.Add(tempLeft[6]);//獎勵項目03
					if (tempLeft [6] != "")
						array_ItemTable_RewardItem03.Add (ItemTable_wordlist [int.Parse (tempLeft [6])]);//獎勵項目03
					else {
						array_ItemTable_RewardItem03.Add ("");//獎勵項目03
					}
					array_ItemTable_RewardContent03.Add (tempLeft [7]);//獎勵內容03


				} else {
					array_ItemTable_Name.Add (tempString [1]);//名稱
					array_ItemTable_SeriesNumber.Add (tempString [2]);//系列編號
					array_ItemTable_SeriesName.Add (tempString [3]);//系列名稱
					array_ItemTable_Class.Add (tempString [4]);//類別

					array_ItemTable_Scene.Add (tempString [5]);//場景

					array_ItemTable_Possibility.Add (tempString [6]);//機率


					if (tempString [7] != "")
						array_ItemTable_RewardItem01.Add (ItemTable_wordlist [int.Parse (tempString [7])]);//獎勵項目01
					else {
						array_ItemTable_RewardItem01.Add ("");//獎勵項目01
					}
					array_ItemTable_RewardContent01.Add (tempString [8]);//獎勵內容01
					if (tempString [9] != "")
						array_ItemTable_RewardItem02.Add (ItemTable_wordlist [int.Parse (tempString [9])]);//獎勵項目02
					else {
						array_ItemTable_RewardItem02.Add ("");//獎勵項目02
					}
					array_ItemTable_RewardContent02.Add (tempString [10]);//獎勵內容02

					if (tempString [11] != "")
						array_ItemTable_RewardItem03.Add (ItemTable_wordlist [int.Parse (tempString [11])]);//獎勵項目02
					else {
						array_ItemTable_RewardItem03.Add ("");//獎勵項目02
					}
					array_ItemTable_RewardContent03.Add (tempString [12]);//獎勵內容03

				}

			}
			//參數表
			else if (titleNumber > 40000 && titleNumber < 50000) {
				array_ParameterTable_Number.Add (titleNumber.ToString ());
				array_ParameterTable_Parameter.Add (tempString [1]);//參數(數字)
			}
		}
		isReadFinish = true;

//		GetReward ("1");

	}

	public int GetReward (string number)
	{

		int itemGot = -1;

		array_MyItems.Clear ();

		int numberX = 0;

		//0~100 Random
		int ran = UnityEngine.Random.Range (0, 101);

		for (int a=0; a<array_ItemTable_Scene.Count; a++) {
			if (array_ItemTable_Scene [a].Contains (number)) {
//				print ("取得:"+a);
//				print (ran+" "+int.Parse(array_ItemTable_Possibility[a]));
				if (ran < int.Parse (array_ItemTable_Possibility [a])) {

					array_MyItems.Add (a);
					numberX++;
				}
			}
		}

		if (array_MyItems.Count > 0) {
//			print ("亂數:"+ran+" 有機會得到的道具有 "+numberX+" 種");

			//0~numberX Random
			int ran2 = UnityEngine.Random.Range (0, numberX);
//			print ("亂數2:"+ran2+" 取得道具:"+array_ItemTable_Name[array_MyItems[ran2]]);

			for (int b=0; b<array_TextTable_Number.Count; b++) {
//				print (array_ItemTable_Name[ran2]+" "+array_TextTable_Number[b]);
				if (array_ItemTable_Name [array_MyItems [ran2]] == array_TextTable_Number [b]) {
//					print (array_MyItems[ran2]);
					itemGot = int.Parse (array_MyItems [ran2].ToString ()) + 1;
//					print (array_TextTable_CH[b]);
				}

			}


		}

		return itemGot;

	}

	public int countSeriesNumber (string SN)
	{
		int count = 0;
		for (int i = 0; i < array_ItemTable_SeriesNumber.Count; i++) {
			if (array_ItemTable_SeriesNumber [i] == SN) {
				count++;
			}
		}
		return count;
	}

	public string getSeriesNameByID (string ID)
	{
		string tmp = "";
		for (int i = 0; i < array_ItemTable_Number.Count; i++) {
			if (array_ItemTable_Number [i] == ID) {
				tmp = array_ItemTable_SeriesName [i];
				break;
			}
		}
		return tmp;
	}

	public string getTextByID (string ID)
	{
		string tmp = "";
		for (int i = 0; i < array_TextTable_Number.Count; i++) {
			if (array_TextTable_Number [i] == ID) {
				tmp = array_TextTable [i] [PlayerPrefs.GetInt ("Language")];
			}
		}
		tmp = tmp.Replace ("\\n", "\n");
		tmp = tmp.Replace ("/comma", ",");

		return tmp;
	}

	public string getTextByIndex (int index)
	{
		string tmp = array_TextTable [index] [PlayerPrefs.GetInt ("Language")];
		tmp = tmp.Replace ("\\n", "\n");
		tmp = tmp.Replace ("/comma", ",");
		return tmp;
	}

	public int getIndexByID (string ID)
	{
		int tmp = 0;
		for (int i = 0; i < array_TextTable_Number.Count; i++) {
			if (array_TextTable_Number [i] == ID) {
				tmp = i;
			}
		}
		return tmp;
	}



}
