﻿using UnityEngine;
using System.Collections;
using Parse;

public class UpdateScore : MonoBehaviour {

	public MyParse myParse;
	public TestNetwork testNetwork;

	private bool done;

	// Use this for initialization
	void Start () {
		done = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (!done) {
			if (testNetwork.isConnect && !testNetwork.isReConnect) {
				if (ParseUser.CurrentUser != null) {
					done = true;
					myParse.UpdateBellCount(ShowBellCount.BellCount());
					for (int i = 1; i < 8; i++) {
						for (int j = 1; j <= 20;j++) {
							if (PlayerPrefs.GetInt("notUpload" + i.ToString() + "-" + j.ToString()) == 1) {
								myParse.InsertOrUpdateStageScore(i.ToString() + "-" + j.ToString(), PlayerPrefs.GetInt("score" + i.ToString() + "-" + j.ToString()));
							}
						}
					}
				}
			}
		}
	}
}
