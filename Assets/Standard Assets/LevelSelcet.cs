﻿using UnityEngine;
using System.Collections;


public class LevelSelcet : MonoBehaviour {
	public static string loadName;
	//是否打開 Level
	public static bool openLevelPanel = false;
	public static bool openPlayPanel = false;
	public static bool isQuit = false;
	public static bool isMoveNext = false;
	
	public static bool forRepaintLevel=false;
	
	public static string language = "CH"; 
	
	public Animator anim;
	
	public static  string pressNow_LevelName = "";
	
	public string s; 
	public string l;

	public bool openBomb = false;
	public bool openStar = false;
	public bool openLoveOrTime = false;
	public bool openRandomItem = false;


	
	public static string stage;
	public static string level;
	
	public static int stageInt =0;
	public static int levelInt =0;
	
	public bool isPause=false;
	
	public GameObject voiceControll;

	public AudioClip bgmSource;

	public bool isTeachMallItem;
	public int itemTeachIndex;


	
	//Version 2.0 Method======================
	public void GotoScene_Satge(){
		if (Application.loadedLevelName != "Start") {
			AudioSource bgm = GameObject.Find ("MusicBox").GetComponent<AudioSource> ();
			AudioSource sound = GameObject.Find ("soundVD").GetComponent<AudioSource> ();
		
			bgm.clip = bgmSource;
			bgm.volume = PlayerPrefs.GetFloat ("musicVolume");
			bgm.pitch = 1.0f;

			isQuit = true;
		}
		//Application.LoadLevel ("Stage");

		Global.GetInstance().loadName = "Stage";
		Application.LoadLevel ("Loading");
		Time.timeScale = 1;
	}
	public void GotoScene_Game(){
		Application.LoadLevel ("Game");

	}
	public void OpenGUI(GameObject ob){
		ob.SetActive (true);
	}
	public void CloseGUI(GameObject ob){
		ob.SetActive (false);
	}
	public void setLevel(GameObject ob){
		
		pressNow_LevelName = ob.name;
		
		ChangeStageLevelInfo ();
	}

	public void LoadingGame(){
		if (GameObject.Find ("EventControoler").GetComponent<LevelSelcet> ().s == "1" && GameObject.Find ("EventControoler").GetComponent<LevelSelcet> ().l == "1") {
			Global.GetInstance().loadName = "Story";
			Application.LoadLevel ("Loading");
		} else {
			Global.GetInstance().loadName = "Game";
			Application.LoadLevel ("Loading");
		}
	}
	
	/*public void PlayGame(){
		Application.LoadLevel ("Game");
	}*/
	public void RePlayGame(){

		AudioSource bgm = GameObject.Find ("MusicBox").GetComponent<AudioSource> ();
		AudioSource sound = GameObject.Find ("soundVD").GetComponent<AudioSource> ();
		bgm.clip = bgmSource;
		bgm.volume = PlayerPrefs.GetFloat("musicVolume");
		bgm.pitch = 1.0f;

		Global.GetInstance().loadName = "Game";
		Application.LoadLevel ("Loading");
	}
	
	//轉pressNow_LevelName 成 Stage & Level
	public void ChangeStageLevelInfo(){
		string[] tempX = pressNow_LevelName.Split ('-');
		stage=tempX[0];
		level = tempX [1];
		stageInt =int.Parse(tempX[0]);
		levelInt =int.Parse(tempX [1]);
	}
	

	
	
	//玩下一關
	public void PlayNextLevel(){

		ChangeStageLevelInfo ();
		
		LoadCSV GCSVsc=GameObject.FindGameObjectWithTag ("EventControoler").GetComponent <LoadCSV>();
		int lvNumber=GCSVsc.GetLevelNumberOfThisStage (stageInt);
		int stNumber = GCSVsc.GetAllStageNumber ();
		
		//STAGE尾
		if((lvNumber==levelInt && stageInt < stNumber) || (stageInt == 7 && levelInt == 20))
		{
			stageInt+=1;
			levelInt=1;
			stage=stageInt.ToString();
			level=levelInt.ToString();
			pressNow_LevelName=stage+"-"+level;
			print (pressNow_LevelName);

			isMoveNext = true;
		}
		else{//正常Stage Level
			
			levelInt+=1;
			level=levelInt.ToString();
			pressNow_LevelName=stage+"-"+level;

			// 打開 Play & Level NGUI
			openLevelPanel = true;
			openPlayPanel = true;
		}
		
		// 打開 Play & Level NGUI
		//openLevelPanel = true;
		//openPlayPanel = true;

		AudioSource bgm = GameObject.Find ("MusicBox").GetComponent<AudioSource> ();
		
		bgm.clip = bgmSource;
		bgm.volume = PlayerPrefs.GetFloat("musicVolume");
		bgm.pitch = 1.0f;

		//Application.LoadLevel ("Stage");
		Global.GetInstance().loadName = "Stage";
		Application.LoadLevel ("Loading");
	}
	
	public void PauseEventOn(){
		isPause = true;
		Time.timeScale = 0;
	}
	public void PauseEventOff(){
		isPause = false;
		Time.timeScale = 1;
	}
	
	//清除所有資料
	public void clearAll(){
		SaveMyData	saveBoxSC = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent <SaveMyData>();
		saveBoxSC.ClearAllStageRecord ();
	}
	
	
	//Level NGUI=================
	public void OpenLevelNGUI(){
		if (GameObject.FindGameObjectWithTag ("LevelNGUI") == null) {
			GameObject lv  =(GameObject) Instantiate(Resources.Load("Level"));
			
			lv.transform.parent = GameObject.FindGameObjectWithTag ("World_Panel").transform;
			lv.transform.localPosition=new Vector3(0,0,0);
			lv.transform.localScale=new Vector3(1,1,1);
			lv.SetActive (true);
		}
	}
	public void CloseLevelNGUI(){
		GameObject lv = GameObject.FindGameObjectWithTag ("LevelNGUI");
		Destroy (lv, 0.5f);
	}
	//=================Level NGUI
	//產生 Play Panel
	public void ProducePlayPanel(){
		GameObject o  =(GameObject) Instantiate(Resources.Load("Play"));
		//SetItemActive ();
		//	o.transform.name="stage0"+a;
		o.transform.parent = GameObject.FindGameObjectWithTag ("SecondPanel").transform;;
		o.transform.localPosition=new Vector3(-2.5f,90,0);
		o.transform.localScale=new Vector3(1,1,1);
	}
	public void DestroyNGUI(GameObject ob){
		Destroy (ob);
	}
	//======================Version 2.0 Method
	
	
	
	
	void Start () {
		//		levelNGUI = GameObject.FindGameObjectWithTag ("LevelNGUI");

		stage = "1";
		level ="1";
		DontDestroyOnLoad (this);
		
	}
	
	
	
	public GameObject x1;
	public GameObject x2;
	
	public void AutoOpenGUI(){
		if (stageInt == 1) 
		{	
			if(x1)
				x1.SetActive(true);
			if(x2)
				x2.SetActive(false);
		} 
		else if (stageInt == 2) 
		{
			if(x1)
				x1.SetActive(false);
			if(x2)
				x2.SetActive(true);
		}
		
	}
	
	public void changeVoice1(GameObject voice) {
		
	}
	
	public void changeVoice2() {
		
		
		
		//
	}
	
	
	static bool isOpenLevel=true;
	
	void Update () {
		

		s = stageInt.ToString();
		l = levelInt.ToString ();
		
		
		
		if (!isOpenLevel) {
			
			x1 = GameObject.FindGameObjectWithTag ("lv1");
			x2 = GameObject.FindGameObjectWithTag ("lv2");
			AutoOpenGUI();
			isOpenLevel=true;
			
		}


	
	}

/*	public void SetItemActive () {
		if (stageInt == 1 && levelInt == 2 && PlayerPrefs.GetInt("openLoveOrTime") != 1 ) 
		{
			//openBomb = true;
			PlayerPrefs.SetInt ("openLoveOrTime", 1);
			Debug.Log(PlayerPrefs.GetInt("openLoveOrTime"));
		}
		if (stageInt == 1 && levelInt == 3 && PlayerPrefs.GetInt("openStar") != 1) 
		{
			//openStar= true;
			//Debug.Log(openStar);
			PlayerPrefs.SetInt ("openStar", 1);
			Debug.Log(PlayerPrefs.GetInt("openStar"));
		}
		if (stageInt == 1 && levelInt == 8 && PlayerPrefs.GetInt("openBomb") != 1) 
		{
			//openLoveOrTime = true;
			//Debug.Log(openLoveOrTime);
			PlayerPrefs.SetInt ("openBomb", 1);
			Debug.Log(PlayerPrefs.GetInt("openBomb"));
		}
		if (stageInt == 2 && levelInt == 2 && PlayerPrefs.GetInt("openRandomItem") != 1) 
		{
			//openRandomItem = true;
			//Debug.Log(openRandomItem);
			PlayerPrefs.SetInt ("openRandomItem", 1);
			Debug.Log(PlayerPrefs.GetInt("openRandomItem"));
		}
	}
*/	
	
	
	
	
	
	
	
}
