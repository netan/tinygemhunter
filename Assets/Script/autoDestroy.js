﻿#pragma strict

var deadTime : float = 0.5;

function Start () {
	if (audio != null) {
		audio.volume = PlayerPrefs.GetFloat("soundVolume");
	}
	Destroy(this.gameObject,deadTime);
}