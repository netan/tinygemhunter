﻿using UnityEngine;
using System.Collections;
using Parse;

public class Preload : MonoBehaviour {

	public MyParse myParse;
	public TestNetwork testNetwork;
	public float delayTime;

	private bool isCallCreateAccount;
	private bool isGetFriendDetail = false;

	public  bool isLoaded = false;

	public void Reset() {
		isCallCreateAccount = false;
	}

	void Start() {
		if (PlayerPrefs.GetInt ("firstIn") != 1) {
			PlayerPrefs.SetFloat("soundVolume", 1.0f);
			PlayerPrefs.SetInt("firstIn", 1);
		}
		NGUITools.soundVolume = PlayerPrefs.GetFloat("soundVolume");
	}

	void Update () {
		if (LoadCSV.isReadFinish && LoadGameEdit.isReadFinish && !isLoaded) {
			Global.GetInstance().loadName = "Start";
			Application.LoadLevel ("Loading");
			//Application.LoadLevel("Start");
			//Destroy(this);
			isLoaded = true;
		}

		if (testNetwork.isConnect) {
			if (!testNetwork.isReConnect) {
				if (PlayerPrefs.GetString("account") == "") {   //if no account, should create account for user
					if (!isCallCreateAccount) {
						isCallCreateAccount = true;
						if (ParseUser.CurrentUser != null) {
							ParseUser.LogOut();
						}
						myParse.CreateAccount();
					}
				}
				if (!isGetFriendDetail) {   //load friend's file in init
					isGetFriendDetail = true;
					myParse.GetFriendList();
					myParse.GetWhoAddMe();
					//myParse.GetMallList();
				}
			}
		} else {
			if (Time.time >= delayTime) {   //let system retest network in 5 sec later
				delayTime += 5.0f;
				testNetwork.ReTestConnection();
			}
		}
	}
}
