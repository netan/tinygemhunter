﻿using UnityEngine;
using System.Collections;

public class StageLock : MonoBehaviour {
	
	public Test stage;
	public LoadCSV LC;
	
	public GameObject Lock_body;
	public GameObject Lock_string;
	public GameObject Lock_left;
	public GameObject Lock_right;
	public GameObject Lock_light;
	
	public int index;
	
	public SpriteRenderer spriteColor;
	
	public enum LockState {
		StringUP = 0,
		Open = 1,
		FadeOut = 2,
		End = 3
	};
	
	public LockState state;
	
	public void StateSet(string s) {
		switch (s) {
		case "StringUP":
			state = LockState.StringUP;
			lightState = LightState.Start;
			break;
		case "Open":
			state = LockState.Open;
			break;
		case "End":
			state = LockState.End;
			lightState = LightState.End;
			break;
		}
	}
	
	public float stringUpSpeed;
	
	public float fadeOutAlpha;
	
	public enum LightState {
		Start = 0,
		FadeOut = 1,
		End = 3
	};
	
	public LightState lightState;
	
	public float lightAlpha;
	
	// Use this for initialization
	void Start () {
		stage = GameObject.Find ("Island").GetComponent<Test> ();
		LC = GameObject.Find ("EventControoler").GetComponent<LoadCSV> ();
		fadeOutAlpha = 1f;
		lightAlpha = 1f;
		//PlayerPrefs.SetInt ("StageLock2", 0);
	}
	
	// Update is called once per frame
	void Update () {
		if (index == 1 || PlayerPrefs.GetInt("StageLock" + index.ToString()) == 1) {
			GameObject.Find("stage0" + index.ToString()).GetComponent<IslandSelf>().enabled = true;
			Lock_body.SetActive(false);
			Lock_string.SetActive(false);
			Lock_string.transform.localPosition = new Vector3(0,45,0);
			Lock_left.SetActive(false);
			Lock_right.SetActive(false);
		} else {
			GameObject.Find("stage0" + index.ToString()).GetComponent<IslandSelf>().enabled = false;
			Vector3 tmp = GameObject.Find("stage0" + index.ToString()).transform.localPosition;
			GameObject.Find("stage0" + index.ToString()).transform.localPosition = new Vector3(tmp.x, 0, tmp.z);
			Lock_body.SetActive(true);
			Lock_body.GetComponent<UISprite>().spriteName = "Lock" + index.ToString();
			spriteColor.color = Color.gray;
			Lock_string.SetActive(true);
			Lock_left.SetActive(true);
			Lock_right.SetActive(true);
		}
		
		switch(state) {
		case LockState.StringUP:
			if (StringUp(stringUpSpeed * Time.deltaTime)) {
				state = LockState.Open;
			}
			break;
		case LockState.Open:
			Lock_body.SetActive(false);
			Lock_string.SetActive(false);
			Lock_left.SetActive(false);
			Lock_right.SetActive(false);
			state = LockState.FadeOut;
			break;
		case LockState.FadeOut:
			
			if (fadeOutAlpha > 0) {
				fadeOutAlpha -= Time.deltaTime;
				Color tmp = Color.white;
				tmp.a = fadeOutAlpha;
				Lock_body.GetComponent<UISprite>().color = tmp;
				Lock_string.GetComponent<UISprite>().color = tmp;
				Lock_left.GetComponent<UISprite>().color = tmp;
				Lock_right.GetComponent<UISprite>().color = tmp;
			} else {
				PlayerPrefs.SetInt ("StageLock" + index, 1);
				spriteColor.color = Color.white;
				GameObject.Find("stage0" + index.ToString()).GetComponent<IslandSelf>().enabled = true;
				state = LockState.End;
			}
			break;
		}
		
		switch(lightState) {
		case LightState.Start:
			Lock_light.SetActive(true);
			lightState = LightState.FadeOut;
			break;
		case LightState.FadeOut:
			if (lightAlpha > 0) {
				lightAlpha -= Time.deltaTime * 0.8f;
				Color tmp = Color.white;
				tmp.a = lightAlpha;
				Lock_light.GetComponent<UISprite>().color = tmp;
				Lock_light.transform.Rotate(Vector3.forward, Time.deltaTime * 100);
				Lock_light.transform.localScale -= Vector3.one * Time.deltaTime * 0.5f;
			} else {
				lightState = LightState.End;
			}
			break;
		}
	}
	
	/*public int FindFirstIndex() {
		int tmp = 0;
		for (int i = 0; i < LC.array_ItemTable_SeriesNumber.Count; i++) {
			if (LC.array_ItemTable_SeriesNumber[i] == (stage.stageNow - 1).ToString()) {
				tmp = i;
				break;
			}
		}
		return tmp;
	}
	
	public int FindLastIndex() {
		int tmp = 0;
		for (int i = LC.array_ItemTable_SeriesNumber.Count - 1; i >= 0; i--) {
			if (LC.array_ItemTable_SeriesNumber[i] == (stage.stageNow - 1).ToString()) {
				tmp = i;
				break;
			}
		}
		return tmp;
	}

	public bool CheckItemCount(int i, int first, int last) {
		bool check = true;
		for (int j = first; j <= last; j++) {
			string tmp = "item0" + string.Format("{0:00}", j + 1);
			//PlayerPrefs.SetInt(tmp, Random.Range(0,5));
			if (PlayerPrefs.GetInt(tmp) < i) {
				check = false;
				break;
			}
		}
		return check;
	}*/
	
	public bool StringUp(float speed) {
		Lock_string.transform.localPosition += Vector3.up * speed;
		return (Lock_string.transform.localPosition.y >= 80);
	}
	
	public bool isWork() {
		return (index == stage.stageNow);
	}
}
