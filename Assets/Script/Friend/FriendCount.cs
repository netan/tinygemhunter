﻿using UnityEngine;
using System.Collections;
using Parse;

public class FriendCount : MonoBehaviour {

	public UILabel label;
	MyParse myParse;
	TestNetwork testNetwork;

	// Use this for initialization
	void Start () {
		myParse = GameObject.Find ("EventControoler").GetComponent<MyParse> ();
		testNetwork = GameObject.Find ("EventControoler").GetComponent<TestNetwork> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (testNetwork.isConnect && !testNetwork.isReConnect) {
			label.gameObject.SetActive(true);
			label.text = myParse.friendCount.ToString() + "/20";
		} else {
			label.gameObject.SetActive(false);
		}
	}
}
