﻿using UnityEngine;
using System.Collections;

public class OpenLevelNGUI : MonoBehaviour {

	public void OnClick() {
		if (GameObject.FindGameObjectWithTag ("LevelNGUI") == null && gameObject.GetComponentInChildren<StageLock>().index == gameObject.GetComponentInParent<Test>().stageNow) {
			GameObject lv  =(GameObject) Instantiate(Resources.Load("Level"));
			
			lv.transform.parent = GameObject.FindGameObjectWithTag ("World_Panel").transform;
			lv.transform.localPosition=new Vector3(0,0,0);
			lv.transform.localScale=new Vector3(1,1,1);
			lv.SetActive (true);
		}
	}
}
