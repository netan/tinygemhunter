﻿#pragma strict


var anim_child_cover:Animator;
var collider_child_cover:BoxCollider2D;
var iceRange3:GameObject;

var dragonStatus:Dragon;


var index:int=0;
var indexNow:int=0;

var isSpread : boolean;

var powerRange:GameObject;//POWER 範圍

var MS:MyStage;

var PS : PlayerSC;//打冰塊加COMBO

function Start () {

isSpread = false;
index = indexNow;

MS = GameObject.FindGameObjectWithTag("LevelManager").GetComponent(MyStage);

if(GameObject.FindGameObjectWithTag("Player") != null) {
	dragonStatus=GameObject.FindGameObjectWithTag("Player").GetComponent(Dragon);
	PS = GameObject.FindGameObjectWithTag("Player").GetComponent(PlayerSC);
}
anim_child_cover =GetComponent(Animator);
collider_child_cover =GetComponent(BoxCollider2D);


}

function Update () {
//Debug.Log("index:"+index);
if(MS.gameStatus == 5){
	if(collider_child_cover.enabled && indexNow ==2 &&(dragonStatus.status == 3 || dragonStatus.status == 11) && !isSpread){
		IceSpread2();
		isSpread = true;
		//Debug.Log("indexNow:"+indexNow);
}
	else if(dragonStatus.status!=11 &&  dragonStatus.status!=3 && isSpread)
	{
		isSpread = false;
	}
}
}


function OnCollisionEnter2D (coll: Collision2D) {
		
		if(coll.gameObject.name=="dragon")
		{
//		Debug.Log("1");
			if(coll.gameObject.GetComponent(PlayerSC).powerExsit)	
			{
				Instantiate (powerRange, this.gameObject.transform.position, Quaternion.identity);
				coll.gameObject.GetComponent(PlayerSC).powerExsit=false;
				//Debug.Log("2");
			}
		}
		PS.comboNumber++;
	
		anim_child_cover.SetInteger("status", -1);
		collider_child_cover.enabled=false;
		yield WaitForSeconds(0.2);
		
		anim_child_cover.SetInteger("status", 0);
		
		
		
		
	
	

	//child_cover.GetComponent(BoxCollider2D).enabled=true;
	//Destroy(this.gameObject);
	
	/*if(coll.gameObject.tag=="IceRange")//只有冰以外的東西才會結冰
	{
		index++;
		indexNow = index;
	}*/
}


function OnTriggerEnter2D(other: Collider2D) {

	if(other.gameObject.tag=="IceRange")//只有冰以外的東西才會結冰
		{
			if(indexNow < 2)
			{
			index++;
			indexNow = index;
			//Debug.Log("i+1");
			}
		}
	if(other.gameObject.tag=="PowerCrackRange")//冰罩碰到集氣攻擊碰撞區會破掉
		{
		anim_child_cover.SetInteger("status", -1);
		collider_child_cover.enabled=false;
		yield WaitForSeconds(0.2);
		
		anim_child_cover.SetInteger("status", 0);
		}

	
}






function IceSpread2(){
	
		Instantiate (iceRange3, this.gameObject.transform.position, Quaternion.identity);
		
}