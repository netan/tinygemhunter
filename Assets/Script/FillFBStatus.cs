﻿using UnityEngine;
using System.Collections;

public class FillFBStatus : MonoBehaviour {

	public string id;
	public UIInput label;
	
	private Font[] font;
	
	private LoadCSV csv;
	public int index;
	
	// Use this for initialization
	void Start () {
		csv = GameObject.Find ("EventControoler").GetComponent<LoadCSV> ();
		index = csv.getIndexByID(id);
		font = new Font[4];
		font[0] = Resources.Load("Font/JF Flat regular", typeof(Font)) as Font;
		font[1] = Resources.Load("Font/ZN", typeof(Font)) as Font;
		font[2] = Resources.Load("Font/JP", typeof(Font)) as Font;
		font[3] = Resources.Load("Font/CN", typeof(Font)) as Font;
		label.value = string.Format(csv.getTextByIndex(index), PlayerPrefs.GetString("account"));
		//Destroy (this);
	}

}
