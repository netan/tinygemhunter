﻿using UnityEngine;
using System.Collections;

public class SetVoice : MonoBehaviour {
	public AudioSource voice;
	// Use this for initialization
	void Awake () {
		voice = GameObject.Find("MusicBox").GetComponent<AudioSource>();
		if (Application.loadedLevelName == "Preload") {
			if (PlayerPrefs.GetInt("isSetMusic") == 0) {
				voice.volume = 1.0f;
				PlayerPrefs.SetInt("isSetMusic",1);
				PlayerPrefs.SetFloat("musicVolume",1.0f);
				
			} else {
				voice.volume = PlayerPrefs.GetFloat("musicVolume");
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
