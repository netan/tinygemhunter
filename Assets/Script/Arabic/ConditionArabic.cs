﻿using UnityEngine;
using System.Collections;
using ArabicSupport;

public class ConditionArabic : MonoBehaviour {

	private UILabel label;
	private bool tashkeel = true;
	private bool hinduNumbers = true;

	private CharacterScript CS;

	void Awake () {
		label = this.gameObject.GetComponent<UILabel> ();
		CS = GameObject.FindGameObjectWithTag ("Conversation").GetComponent<CharacterScript> ();
	}
	
	void Update () {
		if (CS.stageSc.gameStatus == 2) 
		{
			label.text = ArabicFixer.Fix (label.text, tashkeel, hinduNumbers);
		}
	}
}
