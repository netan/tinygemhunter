﻿using UnityEngine;
using System.Collections;
using ArabicSupport;

public class ChangeIslandLanguage : ChangeLanguage {

	public Test t;
	public int islandIndex;

	private Font[] font;

	private bool tashkeel = true;
	private bool hinduNumbers = true;

	// Use this for initialization
	void Start () {
		csv = GameObject.Find ("EventControoler").GetComponent<LoadCSV> ();
		for (int i = 0; i < csv.array_TextTable_Number.Count - 1; i++) {
			if (csv.array_TextTable_Number[i] == id) {
				index = i;
				break;
			}
		}

		t = GameObject.Find ("Island").GetComponent<Test> ();
		for (int i = 0; i < csv.array_TextTable_Number.Count - 1; i++) {
			if (csv.array_TextTable_Number[i] == id) {
				islandIndex = i;
				break;
			}
		}
		font = new Font[4];
		font[0] = Resources.Load("Font/JF Flat regular", typeof(Font)) as Font;
		font[1] = Resources.Load("Font/ZN", typeof(Font)) as Font;
		font[2] = Resources.Load("Font/JP", typeof(Font)) as Font;
		font[3] = Resources.Load("Font/CN", typeof(Font)) as Font;

	}
	
	// Update is called once per frame
	void Update () {
		index = islandIndex + (t.stageNow - 1);
		if (t.stageNow > t.stageNumber) {
			index = csv.getIndexByID("10139");
		}
		if (csv.array_TextTable [index] [PlayerPrefs.GetInt ("Language")] != label.text) {
			label.text = csv.array_TextTable [index] [PlayerPrefs.GetInt ("Language")];
			label.text = ArabicFixer.Fix(label.text, tashkeel, hinduNumbers);
		}
		label.trueTypeFont = font[PlayerPrefs.GetInt ("Language")];
	}
}
