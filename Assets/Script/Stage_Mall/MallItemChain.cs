﻿using UnityEngine;
using System.Collections;

public class MallItemChain : MonoBehaviour {

	public GameObject hook;
	public GameObject quantity;
	public GameObject item;
	public GameObject BG;
	public GameObject money;
	public GameObject moneynumber;

	public string myConditions;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (PlayerPrefs.GetInt (myConditions) != 1 /*&& quantity.gameObject.activeSelf*/) 
		{
			this.GetComponent<UIButton>().enabled = false;
			hook.gameObject.SetActive(false);
			quantity.gameObject.SetActive(false);
			item.gameObject.SetActive(false);
			money.gameObject.SetActive(false);
			moneynumber.gameObject.SetActive(false);
			BG.GetComponent<UISprite>().spriteName = "Stage_Play_Space";
		}
		else{}

		if (PlayerPrefs.GetInt (myConditions) == 1 && !this.GetComponent<UIButton>().isEnabled) 
		{
			this.GetComponent<UIButton>().enabled = true;	
		}
	
	}
}
