﻿using UnityEngine;
using System.Collections;

public class ChangeVoice : MonoBehaviour {

	UISlider mSlider;
	public AudioSource voice;
	public UIButton sprite;

	void Awake ()
	{
		voice = GameObject.Find("MusicBox").GetComponent<AudioSource>();
		mSlider = GetComponent<UISlider>();
		mSlider.value = voice.volume;
		EventDelegate.Add(mSlider.onChange, OnChange);
	}
	
	void OnChange ()
	{
		if (UISlider.current.value == 0) {
			sprite.normalSprite = "25-2";
		} else {
			sprite.normalSprite = "25-1";
		}
		voice.volume = UISlider.current.value;
		PlayerPrefs.SetFloat ("musicVolume", UISlider.current.value);
	}
}
