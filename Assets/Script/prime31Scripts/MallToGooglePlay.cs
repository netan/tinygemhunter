﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Prime31;

#if UNITY_ANDROID
public class MallToGooglePlay : MonoBehaviour {

	//public MyParse myParse;
	private string MyKey;
	// Use this for initialization
	void Start () {
		//myParse = GetComponent<MyParse> ();
		MyKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkvSmWmjCiudvG3ZzGpDZchaahsvI1qjR2L67mb2wGCbcjiu9YIMVBhB0mb+Bz0W+b0+gGAHxXi225XZSNJfCWsEeHv7me7AxLT0AyXjilRmNPlIKcoJvm2IhQjYjQ+FFgrKEil53HPtT0NiBpFaIm8hlHJQwaMHyn7plS/a/OeegVVVM+GIQ6sNEQwInrMfim70volm13RKBeTVds9tol2YXHqStY5+1/MY61kKkC74+E0uH/Q9E4YHd55Jg7zSXONmd4yVpB0huzHJTh24a2qWHD3ZjT7P0rUKj5uTu/hJWCkwnpP7d4PeEq/8NTLe7gSDo2U0BRQXRJK8XpBhkRQIDAQAB";
		GoogleIAB.init(MyKey);
	}

	public void AskOrders(){
		var skus = new string[] { "com.netantest.money", "android.test.purchased"};
		GoogleIAB.queryInventory( skus );
	}
	
	public void BuyProduct(){
		GoogleIAB.purchaseProduct( "com.netantest.money", "payload that gets stored and returned" );
		//GoogleIAB.purchaseProduct( skus[1], "payload that gets stored and returned" );
	}
	
	public void BuyProductAgain(){
		GoogleIAB.consumeProduct( "com.netantest.money" );
		//GoogleIAB.consumeProduct(  skus[1] );
		
	}


}
	#endif
