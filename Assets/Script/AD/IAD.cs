using UnityEngine;
using System.Collections;

public interface IAD
{
	void Cache ();

	void Display ();

	void Dispose ();
}

