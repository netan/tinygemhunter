﻿using UnityEngine;
using System.Collections;

public class SetBrickPosition : MonoBehaviour {

	public int XBrick = 0;
	public int YBrick = 0;
	public MyStage MS;
	// Use this for initialization
	void Start () {
		MS = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<MyStage>();
	
	}

	public void SendEmptyAndDestroy () {
		MS.getEmptyMessage (XBrick, YBrick);
		Destroy (this.gameObject);
	}
	
}
