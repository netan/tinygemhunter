﻿using UnityEngine;
using System.Collections;

public class ChangeTab : MonoBehaviour {

	public MyParse myParse;

	public GameObject selfTab;
	public GameObject[] otherTab;

	public void OnClick() {
		selfTab.SetActive (true);

		foreach (GameObject other in otherTab) {
			other.SetActive(false);
		}

		if (selfTab.name == "FriendCheck" && !myParse.isGetFriendCheck) {
			if (myParse.isGetFriendCheckRef) {
				myParse.GetWhoAddMe();
			}
		} else if (selfTab.name == "FriendList" && !myParse.isGetFriendList) {
			if (myParse.isGetFriendListRef) {
				myParse.GetFriendList();
			}
		}

	}

	public void Start() {
		myParse = GameObject.Find ("EventControoler").GetComponent<MyParse> ();
	}

	public void Update() {

	}
}
