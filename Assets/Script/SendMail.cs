﻿using UnityEngine;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

public class SendMail : MonoBehaviour {

	public TestNetwork testNetwork;

	//public GameObject UI;
	public UILabel emailLabel;
	public UILabel msgLabel;
	public ChangeLanguage msgResult;

	public void Send(string email, string message, string mailFrom = "tgh.netan@gmail.com", string mailPwd = "LeveL_Up1105", string mailTo = "tgh.netan@gmail.com") {
		MailMessage mail = new MailMessage();
		
		mail.From = new MailAddress(mailFrom);
		mail.To.Add(mailTo);
		mail.Subject = "Tiny Gem Hunter - Bug Report";
		mail.IsBodyHtml = true;

		/*WWW myExtIPWWW = new WWW("http://checkip.dyndns.org");
		if(myExtIPWWW==null) return;
		while (!myExtIPWWW.isDone) {}
		string myExtIP = myExtIPWWW.data;
		myExtIP = myExtIP.Substring(myExtIP.IndexOf(":")+1);
		myExtIP = myExtIP.Substring(0,myExtIP.IndexOf("<"));*/


		mail.Body = "來自" + email + "的訊息:<BR><BR>" + message +
//			"<BR><BR>IP :" + myExtIP +
				"<BR><BR><BR>OS:" + SystemInfo.operatingSystem +
				"<BR>Processor:" + SystemInfo.processorCount.ToString() +
//				"<BR>Memory:" + (SystemInfo.systemMemorySize / 1000).ToString() + "G" +
				"<BR>DeviceModel:" + SystemInfo.deviceModel +
				"<BR>GraphicsDeviceName:" + SystemInfo.graphicsDeviceName;
		
		SmtpClient smtpServer = new SmtpClient("smtp.gmail.com", 587);
		smtpServer.Credentials = new System.Net.NetworkCredential(mailFrom, mailPwd) as ICredentialsByHost;
		smtpServer.EnableSsl = true;
		ServicePointManager.ServerCertificateValidationCallback = 
			delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) 
		{ return true; };
		smtpServer.Send (mail);
	}

	void OnClick() {
		testNetwork = GameObject.Find("EventControoler").GetComponent<TestNetwork>();
		if (testNetwork.isConnect && !testNetwork.isReConnect) {
			Send( (emailLabel.text == "Type your Email address") ? "unkown" : emailLabel.text, msgLabel.text);
		} else {
			msgResult.ReLoadID("10117");
		}
		//UI.SetActive (false);
	}



}
