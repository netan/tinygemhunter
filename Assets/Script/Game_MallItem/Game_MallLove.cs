﻿using UnityEngine;
using System.Collections;

public class Game_MallLove : MonoBehaviour {

	MyStage MS;
	MallController MC;
	// Use this for initialization
	void Start () {
		MS = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<MyStage>();
		MC = GameObject.FindGameObjectWithTag("EventControoler").GetComponent<MallController> ();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void DoneAnimation () {

		if (this.gameObject.name == "Mall_Love") 
		{
			this.GetComponent<Animator> ().Play ("Mall_Love_StandBy_Animation");
			MS.gameStatus = 11;

		} else {}

		if (this.gameObject.name == "Mall_Hourglass") 
		{
			this.GetComponent<Animator> ().Play ("standby");
	 		MS.gameStatus = 11;

		} else {}

		if (this.gameObject.name == "Mall_Star1") 
		{
			this.GetComponent<Animator> ().Play ("Mall_Star1_StandBy_Animation");
			MS.gameStatus = 12;

		} else {}

		if (this.gameObject.name == "Mall_Bomb") 
		{

			Instantiate (Resources.Load("Game_Mall_Items/Mall_BigBomb"), new Vector3(0, 2.18f, 0), Quaternion.identity);
	 		MS.gameStatus = 2;

			Destroy(this.gameObject);
		} else {}
	}
}
