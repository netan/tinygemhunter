﻿using UnityEngine;
using System.Collections;

public class StageMove : MonoBehaviour {

	public ChangeStageInfo cs;
	public Test test;

	public float startPositionX;
	public float startPositionY;

	public int UICount;

	// Use this for initialization
	void Start () {

	}

	public void ChangeCount(bool b) {
//		Debug.LogWarning ( "chage count ? " + b );
		if (b) {
			UICount++;
		} else {
			UICount--;
		}

	}

	// Update is called once per frame
	void Update () {
//		Debug.Log(UICount);
		if (UICount == 0) {
			if (Input.touchCount > 0) {
				if (Input.GetTouch(0).phase == TouchPhase.Began) {
					startPositionX = Input.GetTouch(0).position.x;
					startPositionY = Input.GetTouch(0).position.y;
				} else if (Input.GetTouch(0).phase == TouchPhase.Ended) {
					if (Mathf.Abs(startPositionX - Input.GetTouch(0).position.x) >= 10 && Mathf.Abs(startPositionX - Input.GetTouch(0).position.x) > Mathf.Abs(startPositionY - Input.GetTouch(0).position.y)) {
						cs.HandMove(test.stageNow, (int) (test.stageNow + Mathf.Sign(startPositionX - Input.GetTouch(0).position.x)));
					}
				}
			}
			if (Input.GetMouseButtonDown(0)) {
				startPositionX = Input.mousePosition.x;
				startPositionY = Input.mousePosition.y;
			} else if (Input.GetMouseButtonUp(0)) {
				if (Mathf.Abs(startPositionX - Input.mousePosition.x) >= 10 && Mathf.Abs(startPositionX - Input.mousePosition.x) > Mathf.Abs(startPositionY - Input.mousePosition.y)) {
					cs.HandMove(test.stageNow, (int) (test.stageNow + Mathf.Sign(startPositionX - Input.mousePosition.x)));
				}
			}
		} else {
			startPositionX = Input.mousePosition.x;
			startPositionY = Input.mousePosition.y;
		}

		/* (lastMoveTime <= Time.time && Mathf.Abs(Input.GetAxis ("Mouse X")) > 0.1f) {
			lastMoveTime = Time.time + 0.5f;
			cs.HandMove(test.stageNow, (int) (test.stageNow - Mathf.Sign(Input.GetAxis ("Mouse X"))));

			/*cs.handMove = true;
			cs.handMoveIndex = (int) (test.stageNow + Mathf.Sign(Input.GetAxis ("Mouse X")));
			cs.Start();
		}*/
	}
}
