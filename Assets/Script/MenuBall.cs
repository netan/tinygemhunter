﻿using UnityEngine;
using System.Collections;

public class MenuBall : MonoBehaviour {

	public UI2DSprite[] ball;
	public Sprite light;
	public Sprite dark;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		int index = GameObject.Find ("Island").GetComponent<Test>().stageNow - 1;
		if (index < 7) {
			if (ball [index].sprite2D != light) {
				for (int i = 0; i < 7; i++) {
					ball[i].sprite2D = dark;
				}

				ball [index].sprite2D = light;
			}
		}
	}
}
