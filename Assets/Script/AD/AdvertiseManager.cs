﻿using UnityEngine;
using System.Collections;
using System;

public class AdvertiseManager : MonoBehaviour
{
	private const string AD_RECORD = "ad_Record";
	private static IAD advertise;
	private static AD_Flag flag = AD_Flag.None;

	public static void Request ()
	{
		if (advertise != null)
		{
			advertise.Dispose ();
		}
		advertise = null;

		System.GC.Collect ();

		//test chartboost
//		AssignAdByEnum (AD_Flag.Chartboost);
//		advertise.Cache ();
//		return;

		Array values = Enum.GetValues (typeof(AD_Flag));

		if (flag == AD_Flag.None)
		{
			System.Random random = new System.Random ();
			AD_Flag randomAd = (AD_Flag)values.GetValue (random.Next (values.Length));
			AssignAdByEnum (randomAd);
		}
		else
		{
			foreach (object item in values)
			{
				AD_Flag temp = (AD_Flag)item;
				if ((flag & temp) != temp)
				{
					AssignAdByEnum (temp);
				}
			}
		}

		if (advertise != null)
		{
			advertise.Cache ();
		}
	}

	private static void AssignAdByEnum (AD_Flag type)
	{
		Debug.LogWarning ("AssignAdByEnum  type : " + type);

		switch (type)
		{
			case AD_Flag.None:
			case AD_Flag.Admob:
				advertise = new AD_ADMob ();
				flag = flag | AD_Flag.Admob;
				break;
			case AD_Flag.Chartboost:
				advertise = new AD_Chartboost ();
				flag = flag | AD_Flag.Chartboost;
				break;
			default :
				break;
		}

		if ((flag & AD_Flag.Admob) == AD_Flag.Admob && (flag & AD_Flag.Chartboost) == AD_Flag.Chartboost)
		{
			flag = AD_Flag.None;
		}
	}

	public static void Display ()
	{
		if (advertise == null)
		{
			Request ();
		}
		advertise.Display ();
	}

	[System.Flags]
	enum AD_Flag
	{
		None = 0 ,
		Admob  = 1 << 1, 
		Chartboost = 1 << 2 
	}
}


