﻿using UnityEngine;
using System.Collections;

public class ConditionLanguage : MonoBehaviour {
	
	public string[] languageString = new string[23];
	
	// Use this for initialization
	void Start () {
		languageString[0] = "擊破沙塊";
		languageString[1] = "擊破土塊";
		languageString[2] = "擊破石塊";
		languageString[3] = "擊破紅塊";
		languageString[4] = "擊破火山岩";
		languageString[5] = "擊破黑塊(黑曜石)";
		languageString[6] = "清光方塊(不包括黑塊)";
		languageString[7] = "收集寶石(不論顏色)";
		languageString[8] = "收集藍寶石";
		languageString[9] = "收集綠寶石";
		languageString[10] = "收集紅寶石";
		languageString[11] = "收集白寶石";
		languageString[12] = "收集黃寶石";
		languageString[13] = "獲得愛心(沙漏)";
		languageString[14] = "獲得無敵星星";
		languageString[15] = "獲得分身星星";
		languageString[16] = "獲得能量星星";
		languageString[17] = "獲得寶箱";
		languageString[18] = "獲得閃電球";
		languageString[19] = "獲得毒液球";
		languageString[20] = "獲得炸彈";
		languageString[21] = "擊破結冰塊";
		languageString[22] = "擊破火藥岩";
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
