﻿using UnityEngine;
using System.Collections;

public class IslandSelf : MonoBehaviour {

	public bool MoveDown_bool = false;
	public bool MoveUp_bool = false;
	public float moveSpeed = 15f;
	void Start () {
		this.transform.GetComponent<UIButton> ().normalSprite = this.transform.name;

		MoveDown_bool = true;
	}
	
	void Update () {
		//Debug.Log (transform.localPosition.y);
		if (transform.localPosition.y >= 15) {
			MoveDown_bool = true;
			MoveUp_bool = false;
			moveSpeed = 15f;
		} 

		if(MoveDown_bool)
		{
			StageMoveDown();
		}

		if (transform.localPosition.y <= -5) {
			MoveDown_bool = false;
			MoveUp_bool =true;
			moveSpeed = 15f;
		}

		if (MoveUp_bool) {
			StageMoveUp ();
		}
		
		moveSpeed -= Time.deltaTime;
		if (moveSpeed <= 5f) {
			moveSpeed = 5f;
		}

		
		
		/*else if (transform.position.y <= -25) {
			MoveDown_bool = true;
			StageMoveUp ();
		}*/
		                                      
		}
	public void StageMoveDown (){
		//transform.Translate(Vector3.up * Time.deltaTime * -moveSpeed);
		transform.localPosition = transform.localPosition + new Vector3 (0, Time.deltaTime * -moveSpeed, 0);

	}

	public void StageMoveUp (){
		//transform.Translate(Vector3.up * Time.deltaTime * moveSpeed);
		transform.localPosition = transform.localPosition + new Vector3 (0, Time.deltaTime * moveSpeed, 0);
	}
}
