﻿using UnityEngine;
using System.Collections;

public class PlayScoreAnimation : MonoBehaviour {

	//public LoadCSV LC;
	public LoadScore LS;
	public LevelSelcet LS1;
	public int score;
	public int highScore;
	public float s;
	public float time;
	//public string scoreLabel;

	public UILabel scoreText;
	public GameObject newRecord;
	public UISlider scroeSlider;

	public MyStage MS;

	// Use this for initialization
	void Start () {
		//LC = GameObject.Find ("EventControoler").GetComponent<LoadCSV> ();
		//scoreLabel = LC.getTextByID("10078");
		LS1 = GameObject.Find ("EventControoler").GetComponent<LevelSelcet> ();
		score = LS.getScore();
		highScore = PlayerPrefs.GetInt ("top1score" + LS1.s + "-" + LS1.l);
		if (score > highScore) {
			highScore = score;
			PlayerPrefs.SetInt ("top1score" + LS1.s + "-" + LS1.l, score);
		}
		newRecord.SetActive(false);
		time = (float) GameObject.Find ("Conversation_NGUI").GetComponent<CharacterScript> ().Bell_Number;
		if (time == 0) {
			time = 1;
		}
		s = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (s < score) {
			s += (score * Time.deltaTime / (time / 2));
			scoreText.text = /*scoreLabel + */string.Format("{0:000000}", s); 
			scroeSlider.value = s / highScore;
		} else {
			scoreText.text = /*scoreLabel + */string.Format("{0:000000}", score);
			scroeSlider.value = 1.0f * score / highScore;
			if (score == highScore) {
				newRecord.SetActive(true);
			}
			enabled = false;
			MS.gameStatus = 14;
		}
	}
}
