﻿using UnityEngine;
using System.Collections;

public class CloseLock : MonoBehaviour {

	public StageMove stageMove;
	public GameObject lockPanel;

	// Use this for initialization
	void Start () {
		stageMove = GameObject.Find ("Island").GetComponent<StageMove> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnClick() {
		stageMove.UICount--;
		lockPanel.GetComponent<TweenScale>().PlayReverse();
	}
}
