﻿using UnityEngine;
using System.Collections;

public class MallLoveAndTime : MonoBehaviour {

	//計算商城道具數量
	public UILabel label;
	public UILabel label2;
	public UILabel label3;
	public UILabel label4;

	//商城道具0關數字
	public GameObject StuffNumber;
	public GameObject StuffNumber2;
	public GameObject StuffNumber3;
	public GameObject StuffNumber4;

	//宣告找控制器
	MallController MC;

	public

	// Use this for initialization
	void Start () {
		MC = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent<MallController> ();

		if (MC.LoveOrTime_Number == 0) 
		{
			StuffNumber.SetActive(false);	
		}
		else
		{
			StuffNumber.SetActive(true);		
		}

		if (MC.Star_Number == 0) 
		{
			StuffNumber2.SetActive(false);	
		}
		else
		{
			StuffNumber2.SetActive(true);		
		}

		if (MC.Bomb_Number == 0) 
		{
			StuffNumber3.SetActive(false);	
		}
		else
		{
			StuffNumber3.SetActive(true);		
		}

		/*if (MC.RandomItem_Number == 0) 
		{
			StuffNumber.SetActive(false);	
		}
		else
		{
			StuffNumber.SetActive(true);		
		}*/
		
	}
	
	// Update is called once per frame
	void Update () {
		label.text = MC.LoveOrTime_Number.ToString();
		label2.text = MC.Star_Number.ToString();
		label3.text = MC.Bomb_Number.ToString();
		//label4.text = MC.RandomItem_Number.ToString();
	}
}
