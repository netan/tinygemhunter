﻿using UnityEngine;
using System.Collections;

public class RateManager : MonoBehaviour {
	
	private static bool opened;
	private static bool isNeedRate;
	
	private int firstPlay;
	private int againPlay;
	
	private int firstNumber;
	private int againNumber;

	public GameObject Parent;
	public GameObject UI;
	
	void Start () {
		if (PlayerPrefs.GetInt ("rated") == 1) {
			opened = true;
		}
		firstNumber = 10;
		againNumber = 10;

		//Rate Test
		/*opened = false;
		isNeedRate = true;
		firstNumber = 1;
		againNumber = 1;*/
	}
	
	public void AddCount() {
		firstPlay++;
		if (firstPlay > firstNumber) {
			againPlay = (againPlay % againNumber) + 1;
		}
		if (firstPlay == firstNumber || againPlay == againNumber) {
			isNeedRate = true;
		}

	}
	
	// Update is called once per frame
	void Update () {
		if (!opened) {
			if (Application.loadedLevelName == "Stage" && isNeedRate) {
				isNeedRate = false;
				Parent = GameObject.Find("Second_Panel");
				NGUITools.AddChild(Parent, UI);
			}
		}
	}
	
	public static void NextTimeRating() {
		//opened = true;
	}
	
	public static void GotoPlayStore() {
		PlayerPrefs.SetInt("rated", 1);
		OpenRate();
	}
	
	public static void OpenRate() {
		
		isNeedRate = false;
		opened = true;
		
		#if UNITY_EDITOR
		Application.OpenURL ("http://unity3d.com/");
		#endif
		
		#if UNITY_ANDROID
		Application.OpenURL ("https://play.google.com/store/apps/details?id=co.tamatem.tinygemhunter");
		#endif
		
		#if UNITY_IPHONE
		Application.OpenURL ("https://itunes.apple.com/app/998476988");
		#endif
		
	}
}
