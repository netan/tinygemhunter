﻿using UnityEngine;
using System.Collections;

public class SetVoice1 : MonoBehaviour {

	public UISlider mSlider;
	public UIButton sprite;

	void Start () {
		sprite = GetComponent<UIButton> ();
		mSlider.value = PlayerPrefs.GetFloat("soundVolume");
	}

	public void OnClick() {
		if (sprite.normalSprite == "26-1") {
			PlayerPrefs.SetFloat ("preSoundVolume", mSlider.value);
			NGUITools.soundVolume = 0.0f;
			mSlider.value = 0.0f;
		} else {
			mSlider.value = PlayerPrefs.GetFloat("preSoundVolume");
			NGUITools.soundVolume = PlayerPrefs.GetFloat("preSoundVolume");
		}
		
		if (mSlider.value == 0.0f) {
			sprite.normalSprite = "26-2";
		} else {
			sprite.normalSprite = "26-1";
		}
	}
}
