﻿using UnityEngine;
using System.Collections;

public class FAQLabel : MonoBehaviour {

	public string ID;

	public LoadCSV LC;
	public UILabel label;
	public int index;

	private Font[] font;

	// Use this for initialization
	void Start () {
		LC = GameObject.Find ("EventControoler").GetComponent<LoadCSV> ();
		index = LC.getIndexByID(ID);
		label = GetComponent<UILabel> ();
		font = new Font[4];
		font[0] = Resources.Load("Font/JF Flat regular", typeof(Font)) as Font;
		font[1] = Resources.Load("Font/ZN", typeof(Font)) as Font;
		font[2] = Resources.Load("Font/JP", typeof(Font)) as Font;
		font[3] = Resources.Load("Font/CN", typeof(Font)) as Font;
	}
	
	// Update is called once per frame
	void Update () {
		if (LC.array_TextTable [index] [PlayerPrefs.GetInt ("Language")] != label.text) {
			label.text = LC.array_TextTable [index] [PlayerPrefs.GetInt ("Language")];
		}
		label.trueTypeFont = font[PlayerPrefs.GetInt ("Language")];
	}
}
