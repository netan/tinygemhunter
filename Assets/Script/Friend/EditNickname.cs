﻿using UnityEngine;
using System.Collections;
using Parse;

public class EditNickname : MonoBehaviour {

	public TestNetwork testNetwork;

	public UILabel nickname;

	public GameObject NotificationUI;
	public ChangeLanguage NotificationLabel;
	
	public void OnClick() {
		testNetwork = GameObject.Find ("EventControoler").GetComponent<TestNetwork> ();
		GetComponent<UIButton>().isEnabled = false;
		NotificationUI.SetActive(true);
		NotificationLabel.ReLoadID ("10116");
		if (testNetwork.isConnect && !testNetwork.isReConnect) {
			if (nickname.text.Length > 0 && nickname.text.Length <= 16) {
				var query = ParseUser.Query
					.WhereEqualTo("username", ParseUser.CurrentUser.Username);
				query.FindAsync().ContinueWith(t => {
					foreach (var tmp in t.Result) {
						tmp.SaveAsync().ContinueWith(tt => {
							tmp["nickname"] = nickname.text;
							tmp.SaveAsync();
							PlayerPrefs.SetString("nickName", WWW.EscapeURL(nickname.text));
							NotificationLabel.ReLoadID("10118");
							Debug.Log("nickname updated");
						});
					}
					GetComponent<UIButton>().isEnabled = true;
				});

				var query1 = ParseObject.GetQuery("Bell")
					.WhereEqualTo("username", ParseUser.CurrentUser.Username);
				
				query1.FindAsync().ContinueWith(t => {
					foreach (var tt in t.Result) {
						tt.SaveAsync().ContinueWith(ttt => {
							tt["nickname"] = nickname.text;
							tt.SaveAsync();
							Debug.Log("nickname updated2");
						});
					}
				});
			} else {
				NotificationLabel.ReLoadID ("10125");
				Debug.Log("Please input less than 16 character");
				GetComponent<UIButton>().isEnabled = true;
			}
		} else {
			NotificationLabel.ReLoadID ("10117");
			Debug.Log("please connect internet");
			GetComponent<UIButton>().isEnabled = true;
		}
	}
}
