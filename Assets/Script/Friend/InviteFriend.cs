﻿using UnityEngine;
using System.Collections;
using Parse;
using System.Threading.Tasks;

public class InviteFriend : MonoBehaviour {

	public TestNetwork testNetwork;

	public UIButton button;
	public UILabel ID;

	public GameObject NotificationUI;
	public ChangeLanguage NotificationLabel;

	public void OnClick() {
		testNetwork = GameObject.Find ("EventControoler").GetComponent<TestNetwork> ();
		GetComponent<UIButton>().isEnabled = false;
		NotificationUI.SetActive(true);
		NotificationLabel.ReLoadID ("10116");

		if (testNetwork.isConnect && !testNetwork.isReConnect) {
			if (ID.text == ParseUser.CurrentUser.Username) {
				NotificationLabel.ReLoadID ("10122");
				Debug.Log("Don't invite yourself");
				GetComponent<UIButton>().isEnabled = true;
			} else {
				var query = ParseUser.Query
							.WhereEqualTo("username", ID.text);
				query.CountAsync().ContinueWith(t => {
					if (t.Result > 0) {
						var query2 = ParseObject.GetQuery("FriendList")
									.WhereEqualTo("A", ParseUser.CurrentUser.Username)
									.WhereEqualTo("B", ID.text);
						query2.CountAsync().ContinueWith(tt => {
							if (tt.Result == 0) {
								ParseObject friendList = new ParseObject("FriendList");
								friendList["A"] = ParseUser.CurrentUser.Username;
								friendList["B"] = ID.text;
								friendList["nickname"] = WWW.UnEscapeURL(PlayerPrefs.GetString("nickName"));
								Task saveTask = friendList.SaveAsync();
								NotificationLabel.ReLoadID ("10119");
								Debug.Log(ID.text + " invited success");
							} else {
								NotificationLabel.ReLoadID ("10120");
								Debug.Log("You have invited already" + ID.text);
							}
						});
					} else {
						NotificationLabel.ReLoadID ("10121");
						Debug.Log("not find ID");
					}
					GetComponent<UIButton>().isEnabled = true;
				});
			}
		} else {
			NotificationLabel.ReLoadID ("10117");
			Debug.Log("please connect internet");
			GetComponent<UIButton>().isEnabled = true;
		}
	}
}
