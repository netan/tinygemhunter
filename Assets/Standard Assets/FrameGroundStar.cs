﻿using UnityEngine;
using System.Collections;

public class FrameGroundStar : MonoBehaviour {

	public bool isPlay;

	public Sprite[] SP;
	public Sprite normal;

	public int index;

	public float changeTime;

	public float time;

	public SpriteRenderer SR;
	// Use this for initialization
	void Start () {
		isPlay = false;
		index = 0;
		changeTime = 0.2f;
		time = changeTime;
	}
	
	// Update is called once per frame
	void Update () {
		if (isPlay) {
			if (time <= 0) {
				index = (index + 1) % SP.Length;
				time = changeTime;
			}
			SR.sprite = SP[index];
			time -= Time.deltaTime;
		} else {
			SR.sprite = normal;
		}
	}
}
