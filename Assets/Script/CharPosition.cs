﻿using UnityEngine;
using System.Collections;

public class CharPosition : MonoBehaviour {

	public GameObject Char_Right;
	public GameObject Char_Left;
	public float speed = 0.000001f;
	public string CanMoveIn;
	public string CanMoveOut;
	public string MoveDirection;

	public bool Bool_Move = false;
	public bool Bool_R_Move = false;
	public bool Bool_L_Move = false;
	public bool Bool_SetPosition;

	public bool Bool_MoveOut = false;

	public int check = 0;

	public CharacterScript CS;
	// Use this for initialization
	void Start () {
		CS =this.gameObject.GetComponent<CharacterScript> ();
	}
	
	// Update is called once per frame
	void Update () {

		//Debug.Log (Bool_Move);
		if (MoveDirection == "R") {
			Bool_L_Move = false;
			if (Bool_SetPosition) {

				Char_Right.transform.localPosition = new Vector3 (351, 148, 0);
				Bool_SetPosition = false;
				Bool_R_Move = true;
				check = 1;
				//Debug.Log ("Bool_Move  " +Bool_Move + "    " + "   Char_Right.transform.localPosition.x :  " + Char_Right.transform.localPosition.x + "   Bool_SetPosition:   " + Bool_SetPosition);
				
			}

			if (Bool_R_Move) {

				if( Char_Right.transform.localPosition.x > 121) {
					//Debug.Log ("1Bool_Move  " +Bool_Move + "    " + "   Char_Right.transform.localPosition.x :  " + Char_Right.transform.localPosition.x + "   Bool_SetPosition:   " + Bool_SetPosition);

					Char_Right.transform.Translate (Vector3.right * speed * Time.deltaTime );
				} else {
					CS.isTexted =false;
					//Debug.Log ("2Bool_Move  " +Bool_Move + "    " + "   Char_Right.transform.localPosition.x :  " + Char_Right.transform.localPosition.x + "   Bool_SetPosition:   " + Bool_SetPosition);
					Char_Right.transform.localPosition = new Vector3(120, 148, 0); 
					Bool_Move = false;
					Bool_R_Move = false;

					if(check ==1)
					{
					check = 2;

					}
				}

			} 




		} 
		else {///L
			Bool_R_Move = false;
			if (Bool_SetPosition) {
				Char_Left.transform.localPosition = new Vector3 (-358, 152, 0);
				Bool_SetPosition = false;
				Bool_L_Move = true;
				check = 1;
			}
			if (Bool_L_Move) {



				if (Char_Left.transform.localPosition.x <= -102) {

					//Debug.Log (Bool_Move + "    " + Char_Right.transform.localPosition.x + "    " + Bool_SetPosition);
					Char_Left.transform.Translate (Vector3.right * speed * Time.deltaTime);
					
				} else {
					CS.isTexted =false;
					Char_Left.transform.localPosition = new Vector3(-101, 152, 0); 
					Bool_Move = false;
					Bool_L_Move = false;
					if(check ==1)
					{
						check = 2;
						
					}

				}
			}
			
			

				
		}

		if (Bool_MoveOut) {

			}

	}

	public void MovePosition (string a, string b, string c){
		CanMoveIn = a;
		CanMoveOut = b;
		MoveDirection = c;
		if (CanMoveIn == "I") 
		{
			MoveIn();
		}
		if (CanMoveOut == "O") 
		{
			MoveOut ();
		}

	}

	public void MoveIn(){
		//Debug.Log ("I'm here!!!!");
		Bool_SetPosition = true;
		Bool_Move = true;

	}

	public void MoveOut(){
		Bool_MoveOut = true;
	}




}
