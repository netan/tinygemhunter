﻿using UnityEngine;
using System.Collections;

public class CharBehavior : MonoBehaviour {

	public GameObject Char_Right;
	public GameObject Char_Left;

	public bool fadeOut;
	public float fadeAlpha;
	public bool fadeIn;
	public bool FadeInFirst;

	public string RL;
	public string lastRL;

	public CharacterScript CS;


	// Use this for initialization
	void Start () {
		fadeOut = false;
		fadeIn = false;
		FadeInFirst = false;
	}


	public void FadeOut (string a, string b) {
		fadeOut = true;
		fadeAlpha = 1.0f;
		RL = a;
		lastRL = b;
	}

	public void FirstFadeIn (string a) {
		fadeAlpha = 0.0f;
		FadeInFirst = true;
		RL = a;

	}

	void Update() {

		if (FadeInFirst) 
		{
			fadeAlpha += Time.deltaTime * 7.5F;
			if (RL == "R") {
				Color color = Char_Right.GetComponent<SpriteRenderer> ().color;
				color.a = fadeAlpha;
				Char_Right.GetComponent<SpriteRenderer> ().color = color;
			} else {
				Color color = Char_Left.GetComponent<SpriteRenderer> ().color;
				color.a = fadeAlpha;
				Char_Left.GetComponent<SpriteRenderer> ().color = color;
			}
			if (fadeAlpha >= 1) 
			{
				FadeInFirst = false;
			}
		}

		if (fadeOut) 
		{
			fadeAlpha -= Time.deltaTime * 7.5F;
			if (lastRL == "R") {
				Color color = Char_Right.GetComponent<SpriteRenderer> ().color;
				color.a = fadeAlpha;
				Char_Right.GetComponent<SpriteRenderer> ().color = color;
			} else {
				Color color = Char_Left.GetComponent<SpriteRenderer> ().color;
				color.a = fadeAlpha;
				Char_Left.GetComponent<SpriteRenderer> ().color = color;
			}
			if (fadeAlpha <= 0) 
			{
				fadeOut = false;
				//CS.conNumber++;
				//CS.ismoved = false;

				fadeIn = true;
				fadeAlpha = 0.0f;
			}
		}

		if (fadeIn) 
		{
			fadeAlpha += Time.deltaTime * 7.5F;
			if (RL == "R") {
				Color color = Char_Right.GetComponent<SpriteRenderer> ().color;
				color.a = fadeAlpha;
				Char_Right.GetComponent<SpriteRenderer> ().color = color;
			} else {
				Color color = Char_Left.GetComponent<SpriteRenderer> ().color;
				color.a = fadeAlpha;
				Char_Left.GetComponent<SpriteRenderer> ().color = color;
			}
			if (fadeAlpha >= 1) 
			{
				fadeIn = false;
			}
		}
	}
}
