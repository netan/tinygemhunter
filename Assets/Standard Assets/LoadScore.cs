﻿using UnityEngine;
using System.Collections;

public class LoadScore : MonoBehaviour {

	private string[][] scoreData;

	public int totalScore;
	public int[] itemGet;

	// Use this for initialization
	void Start () {
		TextAsset binAsset = Resources.Load ("score", typeof(TextAsset)) as TextAsset;         
		
		//读取每一行的内容  
		string [] lineArray = binAsset.text.Split ("\n"[0]);  
		
		//创建二维数组  
		scoreData = new string [lineArray.Length][];  
		
		//把csv中的数据储存在二位数组中  
		for(int i = 0; i < lineArray.Length; i++)  
		{  
			scoreData[i] = lineArray[i].Split (',');  
		}
		/*string tmp = "";
		for (int i = 0; i < scoreData.Length; i++) {
			tmp = "";
			for (int j = 0; j < scoreData[i].Length; j++) {
				tmp = tmp + scoreData[i][j] + " ";
			}
			print(tmp);
		}*/
		itemGet = new int[scoreData.Length];
		totalScore = 0;
	}

	public int getScore() {
		return totalScore;
	}

	public void AddScore(int index) {
		itemGet [index]++;
		totalScore += int.Parse (scoreData [index] [1]);
	}

	public void AddLifeScore(int l) {
		itemGet [23]++;
		totalScore += (int.Parse (scoreData [23] [1]) * (l-1));
	}

	public void AddTimeScore(float t) {
		itemGet [24]++;
		totalScore += (int)(float.Parse (scoreData [24] [1]) * t);
	}

	public void AddComboScore(int c) {
		itemGet [22]++;
		totalScore += (int.Parse (scoreData [22] [1]) * ((1+c)*c/2));
	}

}
