﻿using UnityEngine;
using System.Collections;

public class CollectionItem : MonoBehaviour {

	public LoadCSV data;
	public int SNCount;
	public GameObject CI;
	public UIScrollView SV;
	// Use this for initialization
	void Start () {
		data = GameObject.Find ("EventControoler").GetComponent<LoadCSV> ();
		SNCount = int.Parse(data.array_ItemTable_SeriesNumber [data.array_ItemTable_SeriesNumber.Count - 1]);
		for (int i = 0; i < SNCount; i++) {
			GameObject clone = NGUITools.AddChild (gameObject, CI);
			clone.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
			CollectionItemSet CIclone = clone.GetComponent<CollectionItemSet>();
			CIclone.index = i + 1;
			CIclone.SetPreload();
		}
		SV.ResetPosition ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
