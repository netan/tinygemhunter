﻿using UnityEngine;
using System.Collections;

public class TestNetwork : MonoBehaviour {

	public bool isConnect;   //check network is connected
	public bool isReConnect;   //recheck network is connected, if not set isConnect to false

	private void Start() {
		isConnect = false;
		isReConnect = false;
		StartCoroutine(CheckConnectionServer());
	}

	public void ReTestConnection() {
		isReConnect = true;
		StartCoroutine(CheckConnectionServer());
	}

	private IEnumerator CheckConnectionServer() {
		WWW www = new WWW ("https://www.google.com.tw/");
		yield return www;
		if (www.error == null) {
			isConnect = true;
			isReConnect = false;
			//Debug.Log("Connect Internet successful");
		} else {
			isConnect = false;
			isReConnect = false;
			Debug.Log("Connect Internet unsuccessful");
		}
	}
}
