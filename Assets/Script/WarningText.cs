﻿using UnityEngine;
using System.Collections;

public class WarningText : MonoBehaviour {

	private MallController MC;

	// Use this for initialization
	void Start () {
		MC = GameObject.Find ("EventControoler").GetComponent<MallController>();
		if (!MC.isNeedWarning) {
			gameObject.SetActive(false);
		}
		Destroy (this);
	}
}
