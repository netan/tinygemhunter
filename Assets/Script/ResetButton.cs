﻿using UnityEngine;
using System.Collections;

public class ResetButton : MonoBehaviour {

	public float stayTime;
	public GameObject ui;
	public GameObject btn;

	public AudioSource voice;

	public UILabel label;

	private Preload preload;

	void Start() {
		preload = GameObject.Find ("EventControoler").GetComponent<Preload> ();
		voice = GameObject.Find("MusicBox").GetComponent<AudioSource>();
	}

	// Update is called once per frame
	void Update () {
		if (stayTime > -10.0f) {
			stayTime -= Time.deltaTime;
			if (stayTime <= 0.0f) {
				GetComponent<UIButton> ().isEnabled = true;
			} else {
				label.text = ((int)stayTime + 1).ToString();
			}
		}
	}

	void OnClick() {
		if (btn != null) {
			//Time.timeScale = 1.0f;
			ui.SetActive (true);
			btn.GetComponent<ResetButton> ().stayTime = 5.0f;
			btn.GetComponent<UIButton> ().isEnabled = false;
		} else {
			//ui.SetActive (false);
			SaveMyData	saveBoxSC = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent <SaveMyData>();
			saveBoxSC.ClearAllStageRecord ();
			preload.Reset();
			PlayerPrefs.SetInt("isSetLanguage", 0);
			PlayerPrefs.SetInt("Language", 0);
			PlayerPrefs.SetInt("isSetMusic",1);
			PlayerPrefs.SetFloat("musicVolume",1.0f);
			voice.volume = 1.0f;
			PlayerPrefs.SetFloat("soundVolume",1.0f);

			Global.GetInstance().loadName = "Start";
			Application.LoadLevel ("Loading");
		}
	}
}
