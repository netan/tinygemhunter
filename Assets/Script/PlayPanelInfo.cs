﻿using UnityEngine;
using System.Collections;
using System;
using ArabicSupport;


public class PlayPanelInfo : MonoBehaviour {


	public UISprite limit_Sprite;
	public UILabel limit_Label;
	public UILabel condition1_Label;
	public UILabel condition2_Label;
	public bool TimeMode=false;
	public bool LifeMode=false;
	LoadGameEdit stageSc;

	public string limit = "";
	public string limit_Number = "";
	public string rule1="";
	public string rule1_Number="";
	public string rule2="";
	public string rule2_Number="";


	public LoadCSV csv;
	public int index;
	public int rule1Index;
	public int rule2Index;
	public string rule1Thranslate;
	public string rule2Thranslate;

	public ConditionLanguage CL;

	public UISprite sprite1;
	public UISprite sprite2;

	public UIAtlas[] atlas1;

	private Font[] font = new Font[4];

	private MallController MC;

	public LevelSelcet LS;

	private bool tashkeel = true;
	private bool hinduNumbers = true;

	void Start () {
		stageSc=GameObject.FindGameObjectWithTag("EventControoler").GetComponent<LoadGameEdit>();
		csv = GameObject.Find ("EventControoler").GetComponent<LoadCSV> ();
		CL = GetComponent<ConditionLanguage> ();
		LS = GameObject.Find ("EventControoler").GetComponent<LevelSelcet> ();

		if (LS.isTeachMallItem) {
			GameObject.Find("MallItemTeach").GetComponent<UI2DSprite>().color = new Color(1, 1, 1, 1);
			LS.isTeachMallItem = false;
		}

		MC = GameObject.Find ("EventControoler").GetComponent<MallController>();
		MC.ResetData();

		font[0] = Resources.Load("Font/JF Flat regular", typeof(Font)) as Font;
		font[1] = Resources.Load("Font/ZN", typeof(Font)) as Font;
		font[2] = Resources.Load("Font/JP", typeof(Font)) as Font;
		font[3] = Resources.Load("Font/CN", typeof(Font)) as Font;
	}

	void Update () {

		for(int a=0;a<stageSc.array_txtDataName.Count;a++){

			if(LevelSelcet.pressNow_LevelName==stageSc.array_txtDataName[a])
			{
				limit=stageSc.array_Limited[a];
				limit_Number=stageSc.array_LimitedNumber[a];
				rule1=stageSc.array_Rule_1[a];
				rule1_Number=stageSc.array_Rule_1_Number[a];
				rule2=stageSc.array_Rule_2[a];
				rule2_Number=stageSc.array_Rule_2_Number[a];
			}
		}

		//過關條件語言切換..
/*		for (int i = 0; i < csv.array_TextTable_Number.Count - 1; i++) {
			if (csv.array_TextTable[i][1] == rule1) {
				rule1Index = i;
			}
			if (csv.array_TextTable[i][1] == rule2) {
				rule2Index = i;
			}
		}
*/
		for (int i = 0; i < 23; i++) {
			//Debug.Log(CL.languageString[i]);
			if (CL.languageString[i] == rule1) {

				rule1Index = i + 14;
			}
			if (CL.languageString[i] == rule2) {
				rule2Index = i + 14;
			}
		}

		SetConditionPicture(rule1, atlas1,sprite1);
		if (rule2 != "empty") {
			SetConditionPicture (rule2, atlas1, sprite2);
		} else {
			sprite2.gameObject.SetActive(false);
		}


		//限制=====================================

		TimeMode = Convert.ToBoolean (limit);

		string st="";

		/*if (TimeMode)
		{
			limit_Sprite.spriteName = "hourglass";
			st="Time Limit:";

		}
		else{
			limit_Sprite.spriteName = "heart";
			st="Life Limit:";
		}


		limit_Label.text =st+limit_Number;*/
			

			



		//過關條件=====================================
		string st1 = "";
		string st2 = "";

		if (rule1 == "empty") {
			st1 = "";
		} else {
			//st1=rule1+":"+rule1_Number;
			//st1=csv.array_TextTable[rule1Index][PlayerPrefs.GetInt ("Language")] + ":" + rule1_Number;
			st1 = String.Format (csv.array_TextTable[rule1Index][PlayerPrefs.GetInt ("Language")], rule1_Number);
			st1 = st1.Replace("/comma", ", ");
		}
		if (rule2 == "empty") {
			st2 = "";
		} else {
			//st2=rule2+":"+rule2_Number;
			//st2=csv.array_TextTable[rule2Index][PlayerPrefs.GetInt ("Language")] + ":" + rule2_Number;
			st2 = String.Format (csv.array_TextTable[rule2Index][PlayerPrefs.GetInt ("Language")], rule2_Number);
			st2 = st2.Replace("/comma", ", ");
		}

		st1 = st1.Replace("\n", "");
		st2 = st2.Replace("\n", "");
		condition1_Label.text=st1;
		condition1_Label.text = ArabicFixer.Fix(condition1_Label.text, tashkeel, hinduNumbers); 
		condition2_Label.text=st2;
		condition2_Label.text = ArabicFixer.Fix(condition2_Label.text, tashkeel, hinduNumbers); 

		condition1_Label.trueTypeFont = font[PlayerPrefs.GetInt ("Language")];
		condition2_Label.trueTypeFont = font[PlayerPrefs.GetInt ("Language")];


	}

	
	void SetConditionPicture(string conditionName, UIAtlas[] atlas, UISprite sprite){
		

		
		switch(conditionName)
		{
		case "擊破沙塊":
			sprite.atlas = atlas[0];
			sprite.spriteName="1";
			break;
			
		case "擊破土塊":
			sprite.atlas = atlas[1];
			sprite.spriteName="brick02";
			break;
		case "擊破石塊":
			sprite.atlas = atlas[0];
			sprite.spriteName="gem2";
			break;
			
		case "擊破紅塊":
			sprite.atlas = atlas[0];
			sprite.spriteName="gem3";
			break;
			
		case "擊破火山岩":
			sprite.atlas = atlas[0];
			sprite.spriteName="5";
			break;
			
		case "擊破黑塊(黑曜石)":
			sprite.atlas = atlas[0];
			sprite.spriteName="gem4";
			break;
			
		case "清光方塊(不包括黑塊)":
			sprite.atlas = atlas[0];
			sprite.spriteName="gem1";
			break;
			
		case "收集寶石(不論顏色)":
			sprite.atlas = atlas[1];
			sprite.spriteName="AllGem";
			break;

		case "收集藍寶石":
			sprite.atlas = atlas[0];
			sprite.spriteName="gem_b";
			break;
		case "收集綠寶石":
			sprite.atlas = atlas[0];
			sprite.spriteName="gem_g";
			break;
		case "收集紅寶石":
			sprite.atlas = atlas[0];
			sprite.spriteName="gem_r";
			break;
			
		case "收集白寶石":
			sprite.atlas = atlas[0];
			sprite.spriteName="gem_w";
			break;
			
		case "收集黃寶石":
			sprite.atlas = atlas[0];
			sprite.spriteName="gem_y";
			break;
			
		case "獲得愛心(沙漏)":
			sprite.atlas = atlas[0];
			sprite.spriteName="hourglass";
			break;
			
		case "獲得無敵星星":
			sprite.atlas = atlas[0];
			sprite.spriteName="star1";
			break;
			
		case "獲得分身星星":
			sprite.atlas = atlas[0];
			sprite.spriteName="star3";
			break;
			
		case "獲得能量星星":
			sprite.atlas = atlas[0];
			sprite.spriteName="star2";
			break;
			
		case "獲得寶箱":
			sprite.atlas = atlas[0];
			sprite.spriteName="treasure";
			break;
			
		case "獲得閃電球":
			sprite.atlas = atlas[0];
			sprite.spriteName="36";
			break;
			
		case "獲得毒液球":
			sprite.atlas = atlas[0];
			sprite.spriteName="37";
			break;
			
		case "獲得炸彈":
			sprite.atlas = atlas[0];
			sprite.spriteName="38";
			break;
			
		case "擊破結冰塊":
			sprite.atlas = atlas[0];
			sprite.spriteName="47";
			break;
			
		case "擊破火藥岩":
			sprite.atlas = atlas[0];
			sprite.spriteName="48";
			break;

		}

		
	}
}
