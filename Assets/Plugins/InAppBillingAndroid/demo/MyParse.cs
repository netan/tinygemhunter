﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Parse;
using System.Threading.Tasks;

public class MyParse : MonoBehaviour
{
	public static MyParse Instance;
	public bool isLogin;   //use it to check is user logined
	//private bool isCheckLogin;
	
	public IEnumerable<ParseObject> results;   //return for user's query results
	public bool isGetQuest;   //check user's query results is done

	public ArrayList friend = new ArrayList ();
	public int friendCount = 0;
	public ArrayList bell = new ArrayList ();
	public ArrayList nickName = new ArrayList ();
	public bool isGetFriendList;
	public bool isGetFriendListRef;
	public ArrayList friendCheck = new ArrayList ();
	public ArrayList friendCheckNickname = new ArrayList ();
	public bool isGetFriendCheck;
	public bool isGetFriendCheckRef;
	public ArrayList scoreName = new ArrayList ();
	public ArrayList stageScore = new ArrayList ();
	public bool isGetScore;
	public static int mallMoney;
	public static bool isGetMallMoney;
	public static int QueryCount;
	public ArrayList mallList = new ArrayList ();
	public ArrayList mallMoneyList = new ArrayList ();
	public ArrayList mallIndexList = new ArrayList ();
	public ArrayList mallTagList = new ArrayList ();
	public bool isGetMallList;

	void Awake ()
	{
		Instance = this;
	}

	void OnDestroy ()
	{
		Instance = null;
	}

	// Use this for initialization
	void Start ()
	{
//		isLogin = false;
//		if (ParseUser.CurrentUser != null) {
//			isLogin = true;
//		} else {
//			Login(PlayerPrefs.GetString("account"), PlayerPrefs.GetString("account"));
//		}
//		isGetFriendList = true;
//		isGetFriendCheck = true;
	}

	public void AddMallMoney (int addMoney)
	{
		int current = PlayerPrefs.GetInt ("mallMoney", 0); 
		PlayerPrefs.SetInt ("mallMoney", current + addMoney);
//		mallMoney = PlayerPrefs.GetInt ("mallMoney");
	}

	public void CoseMallMoney (int lessMoney)
	{
		int current = PlayerPrefs.GetInt ("mallMoney", 0); 
		PlayerPrefs.SetInt ("mallMoney", current - lessMoney);
	}

	public void CreateAccount ()
	{
		/* 1.count users and give new user next ID(ex:0000001)
		 * 2.login
		 * */
//		MyParse.mallMoney = 0;
//		var query = ParseUser.Query;
//		query.CountAsync().ContinueWith (t => {
//			
//			string tmp = string.Format("{0:0000000}", (t.Result + 303010031));
//			PlayerPrefs.SetString("account", tmp);
//			PlayerPrefs.SetString("nickName", tmp);
//			var user = new ParseUser()
//			{
//				Username = tmp,
//				Password = tmp
//			};
//			user["nickname"] = tmp;
//			user["mallMoney"] = 0;
//			Task signUpTask = user.SignUpAsync();
//			Debug.Log("Create account");
//
//			ParseObject bell = new ParseObject("Bell");
//			bell["username"] = tmp;
//			bell["nickname"] = tmp;
//			bell["bell"] = 0;
//			Task saveTask = bell.SaveAsync();
//			Login(tmp, tmp);
//		});
	}
	
	public void Login (string username, string password)
	{
		/* 1.use checkLogin to check is it want to login
		 * 2.use Parse API to login
		 * 3.if login successful, then set isLogin to true
		 * 4.return checkLogin done
		 * */
//		Debug.Log("Loginning");
//		isLogin = false;
//		//isCheckLogin = false;
//		ParseUser.LogInAsync(username, password).ContinueWith(t => {
//			if (t.IsFaulted || t.IsCanceled) {
//				Debug.Log("Not Logined");
//				Login(PlayerPrefs.GetString("account"), PlayerPrefs.GetString("account"));
//			} else {
//				isLogin = true;
//				UpdateBellCount(0);
//				GetFriendList();
//				Debug.Log("Logined");
//			}
//			//isCheckLogin = true;
//		});
	}

	public void InsertOrUpdateStageScore (string stage, int score)
	{
		/* 1.Query StageScore by username and stage
		 * 2.if storage score is less than new score, then replace it
		 * 3.else do nothing
		 * 4.if no record, then insert a new object
		 * */
//		var query = ParseObject.GetQuery("StageScore")
//				.WhereEqualTo("username", ParseUser.CurrentUser.Username)
//				.WhereEqualTo("stage", stage);
//		
//		query.FindAsync().ContinueWith(t => {
//			int count = 0;
//			foreach (var s in t.Result) {
//				count++;
//				s.SaveAsync().ContinueWith(tt => {
//					s["score"] = score;
//					s.SaveAsync();
//					PlayerPrefs.SetInt("notUpload" + stage, 0);
//					Debug.Log("updated");
//				});
//			}
//			if (count == 0) {
//				ParseObject gameScore = new ParseObject("StageScore");
//				gameScore["username"] = ParseUser.CurrentUser.Username;
//				gameScore["stage"] = stage;
//				gameScore["score"] = score;
//				Task saveTask = gameScore.SaveAsync();
//				PlayerPrefs.SetInt("notUpload" + stage, 0);
//				Debug.Log("inserted");
//			}
//		});
	}

	/*public void IsAHaveBFriend(string A, string B) {
		isFriendCheck = false;
		var query = ParseObject.GetQuery("FriendList")
			.WhereEqualTo("A", A)
			.WhereEqualTo("B", B);
		query.CountAsync ().ContinueWith (t => {
			if (t.Result > 0) {
				isFriend = true;
			} else {
				isFriend = false;
			}
			isFriendCheck = true;
		});
	}*/

	public void GetFriendList ()
	{
//		isGetFriendList = false;
//		isGetFriendListRef = false;
//		friend.Clear();
//		bell.Clear();
//		nickName.Clear();
//		/*var query = ParseObject.GetQuery("FriendList")
//					.WhereEqualTo("A", ParseUser.CurrentUser.Username)
//					.WhereMatchesKeyInQuery("B", "A", ParseObject.GetQuery("FriendList")
//				                        			  .WhereEqualTo("B", ParseUser.CurrentUser.Username));*/
//		var friendlist = ParseObject.GetQuery("Bell")
//					.WhereMatchesKeyInQuery("username", "B", ParseObject.GetQuery("FriendList")
//			                 								.WhereEqualTo("A", ParseUser.CurrentUser.Username)
//			                        											.WhereMatchesKeyInQuery("B", "A", ParseObject.GetQuery("FriendList")
//			                        											.WhereEqualTo("B", ParseUser.CurrentUser.Username)))
//					.OrderByDescending("bell");
//
//		friendlist.FindAsync().ContinueWith (t => {
//			/*foreach (var tt in t.Result) {
//				friend.Add(tt.Get<string>("B"));
//			}*/
//			foreach (var tt in t.Result) {
//				PlayerPrefs.SetInt("hasFriend", 1);
//				friend.Add(tt.Get<string>("username"));
//				bell.Add(tt.Get<int>("bell"));
//				nickName.Add(tt.Get<string>("nickname"));
//			}
//			friendCount = friend.Count;
//			isGetFriendList = true;
//			isGetFriendListRef = true;
//			//Debug.Log("get Friend List");
//		});

		/*var n = ParseUser.Query
						.WhereMatchesKeyInQuery ("username", "username", friendlist);
		n.FindAsync().ContinueWith (d => {
			if (d.IsCanceled || d.IsFaulted) {
				Debug.Log(d.Exception.HelpLink.ToString());
			} else {
				foreach (var dd in d.Result) {
					nickName.Add(dd.Get<string>("nickname"));
				}
				isGetFriendListNickname = true;
				Debug.Log("get Friend nickname");
			}
		});*/



	}

	public void GetWhoAddMe ()
	{
//		isGetFriendCheck = false;
//		isGetFriendCheckRef = false;
//		friendCheck.Clear();
//		friendCheckNickname.Clear();
//		var query = ParseObject.GetQuery("FriendList")
//			.WhereEqualTo("A", ParseUser.CurrentUser.Username)
//			.Or(ParseObject.GetQuery("FriendList")
//			.WhereEqualTo("B", ParseUser.CurrentUser.Username));
//		ArrayList friendA = new ArrayList();
//		ArrayList friendB = new ArrayList();
//		ArrayList friendC = new ArrayList();
//		query.FindAsync().ContinueWith (t => {
//			foreach (var tt in t.Result) {
//				friendA.Add(tt.Get<string>("A"));
//				friendB.Add(tt.Get<string>("B"));
//				friendC.Add(tt.Get<string>("nickname"));
//			}
//			for (int i = 0; i < friendA.Count; i++) {
//				if (friendA[i].ToString() == ParseUser.CurrentUser.Username) {
//					for (int j = 0; j < friendA.Count; j++) {
//						if (friendB[i].ToString() == friendA[j].ToString()) {
//							friendA.RemoveAt(j);
//							friendB.RemoveAt(j);
//							friendC.RemoveAt(j);
//							i--;
//							break;
//						}
//					}
//				}
//			}
//			for (int i = 0; i < friendA.Count; i++) {
//				if (friendA[i].ToString() == ParseUser.CurrentUser.Username) {
//					friendA.RemoveAt(i);
//					friendB.RemoveAt(i);
//					friendC.RemoveAt(i);
//					i--;
//				}
//			}
//			for (int i = 0; i < friendA.Count; i++) {
//				friendCheck.Add(friendA[i]);
//				friendCheckNickname.Add(friendC[i]);
//			}
//			isGetFriendCheck = true;
//			isGetFriendCheckRef = true;
//			//Debug.Log("get who add me");
//		});
	}

	public void UpdateBellCount (int count)
	{
//		var query = ParseObject.GetQuery("Bell")
//			.WhereEqualTo("username", ParseUser.CurrentUser.Username);
//		
//		query.FindAsync().ContinueWith(t => {
//			foreach (var bell in t.Result) {
//				bell.SaveAsync().ContinueWith(tt => {
//					bell["bell"] = count;
//					bell["nickname"] = WWW.UnEscapeURL(PlayerPrefs.GetString("nickName"));
//					bell.SaveAsync();
//					//Debug.Log("bell updated");
//				});
//			}
//		});
	}

	public void GetStageScore (string stage)
	{
//		isGetScore = false;
//		bool isEnd = false;
//		scoreName.Clear ();
//		stageScore.Clear ();
//		var score = ParseObject.GetQuery ("StageScore")
//			.WhereEqualTo("stage", stage)
//			.OrderByDescending("score");
//
//		score.FindAsync().ContinueWith(t => {
//			int count = 0;
//			foreach (var s in t.Result) {
//				if (!isEnd) {
//					for (int i = 0; i < friend.Count; i++) {
//						count++;
//						if (s.Get<string>("username") == friend[i].ToString()) {
//							scoreName.Add(nickName[i].ToString());
//							stageScore.Add(s.Get<int>("score").ToString());
//							Debug.Log(nickName[i] + " " + s.Get<int>("score"));
//							break;
//						}
//						if (s.Get<string>("username") == WWW.UnEscapeURL(PlayerPrefs.GetString("account")).ToString()) {
//							scoreName.Add(WWW.UnEscapeURL(PlayerPrefs.GetString("nickName")).ToString());
//							stageScore.Add(s.Get<int>("score").ToString());
//							Debug.Log(WWW.UnEscapeURL(PlayerPrefs.GetString("nickName")) + " " + PlayerPrefs.GetInt("score" + stage));
//							isEnd = true;
//							break;
//						}
//					}
//				}
//			}
//			if (count == 0) {
//				scoreName.Add(WWW.UnEscapeURL(PlayerPrefs.GetString("nickName")).ToString());
//				stageScore.Add(PlayerPrefs.GetInt("score" + stage));
//				//Debug.Log(WWW.UnEscapeURL(PlayerPrefs.GetString("nickName")) + " " + PlayerPrefs.GetInt("score" + stage));
//			}
//
//			if (stageScore[0].ToString() == stageScore[scoreName.Count-1].ToString() && scoreName.Count > 1) {
//				scoreName.Clear ();
//				scoreName.Add(WWW.UnEscapeURL(PlayerPrefs.GetString("nickName")).ToString());
//				stageScore.Add(PlayerPrefs.GetInt("score" + stage));
//				Debug.Log(WWW.UnEscapeURL(PlayerPrefs.GetString("nickName")) + " " + PlayerPrefs.GetInt("score" + stage));
//			}
//
//			//Debug.Log("get friend score, count:" + scoreName.Count);
//			isGetScore = true;
//			//if no record, then pretend result is null
//			/*if (stageScore[0].ToString() == "0") {
//				isGetScore = false;
//			}*/
//		});
	}

	public void DeleteFriend (string ID)
	{
//		var friend = ParseObject.GetQuery ("FriendList")
//				.WhereEqualTo ("A", ParseUser.CurrentUser.Username)
//				.WhereEqualTo ("B", ID)
//				.Or (ParseObject.GetQuery ("FriendList")
//				     .WhereEqualTo("B", ParseUser.CurrentUser.Username)
//				     .WhereEqualTo("A", ID));
//		friend.FindAsync().ContinueWith(t => {
//			foreach (ParseObject tt in t.Result) {
//				tt.DeleteAsync();
//			}
//			GetFriendList();
//			//GetWhoAddMe();
//			Debug.Log("Delete friend:" + ID);
//		});
	}

	public static void QueryMallMoney ()
	{
//		if (QueryCount > 0) {
//			MyParse.QueryCount--;
//			isGetMallMoney = false;
//			var query = ParseUser.Query
//				.WhereEqualTo ("username", ParseUser.CurrentUser.Username);
//			query.FindAsync().ContinueWith(t => {
//				foreach (var s in t.Result) {
//					mallMoney = s.Get<int>("mallMoney");
//				}
//				isGetMallMoney = true;
//			});
//		}
	}
//
//	public static void AddMallMoney (string id)
//	{
//		int money = 0;
//
//
//		if (id == "co.tamatem.tinygemhunter.money30_5") {
//			money = 5;
//		} else if (id == "co.tamatem.tinygemhunter.money60_15") {
//			money = 15;
//		} else if (id == "co.tamatem.tinygemhunter.money90_30") {
//			money = 30;
//		} else if (id == "co.tamatem.tinygemhunter.money150_60") {
//			money = 60;
//		} else if (id == "co.tamatem.tinygemhunter.money300_200") {
//			money = 200;
//		} else if (id == "co.tamatem.tinygemhunter.money600_500") {
//			money = 500;
//		} else if (id == "co.tamatem.tinygemhunter.money30_10") {
//			money = 10;
//		} else if (id == "co.tamatem.tinygemhunter.money60_20") {
//			money = 20;
//		} else if (id == "co.tamatem.tinygemhunter.money90_40") {
//			money = 40;
//		} else if (id == "co.tamatem.tinygemhunter.money150_90") {
//			money = 90;
//		} else if (id == "co.tamatem.tinygemhunter.money300_250") {
//			money = 250;
//		} else if (id == "co.tamatem.tinygemhunter.money600_600") {
//			money = 600;
//		}
//
//
//	}

	public static void AddMallMoney (string id, int money)
	{
//		ParseObject purchaseInfo = new ParseObject ("PurchaseInfo");
//
//		purchaseInfo ["username"] = ParseUser.CurrentUser.Username;
//		purchaseInfo ["ProductID"] = id;
//		Task saveTask = purchaseInfo.SaveAsync ();
//		Debug.Log ("purchase log");
//		
//		
//		var query = ParseUser.Query
//			.WhereEqualTo ("username", ParseUser.CurrentUser.Username);
//		
//		query.FindAsync ().ContinueWith (t => {
//			foreach (var s in t.Result) {
//				s.SaveAsync ().ContinueWith (tt => {
//					s ["mallMoney"] = s.Get<int> ("mallMoney") + money;
//					s.SaveAsync ();
//					Debug.Log ("Add " + money + " money");
//				});
//			}
//			
//			QueryCount = 2;
//			QueryMallMoney ();
//		});
	}

	public static void MinusMallMoney (int money)
	{
//		var query = ParseUser.Query
//			.WhereEqualTo ("username", ParseUser.CurrentUser.Username);
//		
//		query.FindAsync().ContinueWith(t => {
//			foreach (var s in t.Result) {
//				s.SaveAsync().ContinueWith(tt => {
//					s["mallMoney"] = s.Get<int>("mallMoney") - money;
//					s.SaveAsync();
//					//Debug.Log("Minus " + money + " money");
//				});
//			}
//			QueryCount = 2;
//			QueryMallMoney();
//		});
	}

	public void GetMallList ()
	{
		isGetMallList = false;
		mallList.Clear ();
		mallMoneyList.Clear ();
		mallIndexList.Clear ();
		mallTagList.Clear ();
		ParseQuery<ParseObject> malllist = ParseObject.GetQuery ("ArabicMallList")
					.WhereEqualTo ("Enable", true)
					.OrderBy ("Index");
		
		malllist.FindAsync ().ContinueWith (t => {
			foreach (var tt in t.Result) {
				mallList.Add (tt.Get<string> ("ProductID"));
				mallMoneyList.Add (tt.Get<int> ("MallMoney"));
				mallIndexList.Add (tt.Get<int> ("Index"));
				mallTagList.Add (tt.Get<int> ("Tag"));
			}
			isGetMallList = true;
			//Debug.Log("get Mall List");
		});
	}
}