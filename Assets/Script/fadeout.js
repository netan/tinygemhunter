

var fadeSpeed = 0.5;
var alpha = 1.0; 
var fadeDir = -1;


function OnGUI(){


alpha += fadeDir * fadeSpeed * Time.deltaTime;  
alpha = Mathf.Clamp01(alpha);  
this.renderer.material.color.a = alpha;
  



}