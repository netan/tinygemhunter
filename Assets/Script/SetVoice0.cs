﻿using UnityEngine;
using System.Collections;

public class SetVoice0 : MonoBehaviour {

	public UISlider mSlider;
	public UIButton sprite;

	void Start() {
		sprite = GetComponent<UIButton> ();
	}

	public void OnClick() {
		if (sprite.normalSprite == "25-1") {
			PlayerPrefs.SetFloat ("preMusicVolume", PlayerPrefs.GetFloat("musicVolume"));
			mSlider.value = 0.0f;
		} else {
			mSlider.value = PlayerPrefs.GetFloat("preMusicVolume");
		}

		if (mSlider.value == 0.0f) {
			sprite.normalSprite = "25-2";
		} else {
			sprite.normalSprite = "25-1";
		}
	}
}
