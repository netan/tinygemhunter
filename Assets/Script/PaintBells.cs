﻿using UnityEngine;
using System.Collections;

public class PaintBells : MonoBehaviour {
	
	public GameObject UnlockSound;

	public int[]  arr_Treasure;
	public int[]  arr_Stage1_Clear;
	public int[]  arr_Stage2_Clear;
	public int[]  arr_Stage1_Star;
	public int[]  arr_Stage2_Star;
	//SP Lock Condition=========
	public int[]  arr_Stage1_Lock;
	public int[]  arr_Stage2_Lock;

	public SaveMyData sc;
	public GameObject obLock ;

	public bool setLock=false;

	void Start () {
		setLock=false;
		sc = this.gameObject.GetComponent<SaveMyData> ();

	}
	

	void Update () {
		setLock = LevelSelcet.forRepaintLevel;


		obLock= GameObject.FindGameObjectWithTag ("LockBG");

		if(Application.loadedLevelName=="Stage2" && !setLock)
		{

			if(obLock){
				ShowLockBG(obLock);
				LevelSelcet.forRepaintLevel=false;
			}
		}

		this.arr_Treasure = sc.arr_Treasure;
		this.arr_Stage1_Clear = sc.arr_Stage1_Clear;
		this.arr_Stage2_Clear = sc.arr_Stage2_Clear;
		this.arr_Stage1_Star = sc.arr_Stage1_Star;
		this.arr_Stage2_Star = sc.arr_Stage2_Star;
		this.arr_Stage1_Lock = sc.arr_Stage1_Lock;
		this.arr_Stage2_Lock = sc.arr_Stage2_Lock;


		RepaintStar ();
	}



	public void Unlock(GameObject obLock){

		SaveMyData saveBoxSC = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent <SaveMyData> ();
		LoadCSV	csvSC = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent <LoadCSV>();
		System.Collections.Generic.List<int> array_Parameter = csvSC.GetLockItemsNeed (LevelSelcet.pressNow_LevelName);

		int okNumber=0;
		for(int y=0;y<array_Parameter.Count;y++){
			//		print ("所需道具: 道具 "+array_Parameter[y]);
			if(saveBoxSC.arr_Treasure [array_Parameter[y]] >0){
				okNumber++;
			}
		}
		if (array_Parameter.Count > 0) {
//									print ("需要道具");
			
						//集滿道具
						if (okNumber == array_Parameter.Count) {

								
								//取得解鎖資訊
								int[] tempS = saveBoxSC.GetStage_Lock (LevelSelcet.stageInt);
								//未解鎖
								if (tempS [LevelSelcet.levelInt - 1] == 1) {
										Instantiate (UnlockSound, this.gameObject.transform.position, Quaternion.identity);
										saveBoxSC.SetStage_Lock (LevelSelcet.stageInt, LevelSelcet.levelInt - 1);//解鎖
								}
								//一般UI
								obLock.GetComponent<UISprite> ().enabled = false;//解鎖按鈕 消失
		
								Transform[] allChildren0 = obLock.GetComponentsInChildren<Transform> ();
								foreach (Transform child in allChildren0) {
										if (child.gameObject.name == "Describe1" || child.gameObject.name == "Describe2" || child.gameObject.name == "Describe3") {
												child.GetComponent<UILabel> ().enabled = false;
										}
										if (child.gameObject.name == "Lock Button" || child.gameObject.name == "Play_Button_Fake") {
												child.GetComponent<UISprite> ().enabled = false;
										}
								}


								//道具UI
								for (int q=0; q<2; q++) {

										GameObject abc = GameObject.Find ("Need Icon" + (q + 1).ToString ());

										if (q < array_Parameter.Count) {//道具範圍內
												abc.GetComponent<UISprite> ().enabled = false;
						
												Transform[] allChildren1 = abc.GetComponentsInChildren<Transform> ();
												foreach (Transform child in allChildren1) {
														if (child.name == "Sprite") {
//														child.gameObject.GetComponent<UISprite> ().spriteName =s "";
																child.gameObject.GetComponent<UISprite> ().enabled = false;
														}
														if (child.name == "cover") {//沒有黑色覆蓋
																child.gameObject.GetComponent<UISprite> ().enabled = false;
														}
												}
										}

								}
						}
				}
	}


	//顯示 鎖住關卡
	public void ShowLockBG(GameObject obLock){

		SaveMyData	saveBoxSC = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent <SaveMyData>();
		LoadCSV	csvSC = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent <LoadCSV>();
		//取得所有關卡是否鎖
		int[] a=saveBoxSC.GetStage_Lock (LevelSelcet.stageInt);

//		print ("是否鎖住:"+a[0]);
		System.Collections.Generic.List<int> array_Parameter = csvSC.GetLockItemsNeed (LevelSelcet.pressNow_LevelName);

		int okNumber=0;
		for(int y=0;y<array_Parameter.Count;y++){
//		print ("所需道具: 道具 "+array_Parameter[y]);
			if(saveBoxSC.arr_Treasure [array_Parameter[y]] >0){
				okNumber++;
			}
		}
		
//		print ("取得道具"+okNumber);

		if (array_Parameter.Count > 0 && a[0]==1) {
//			print ("需要道具");

						//集滿道具
						if (okNumber == array_Parameter.Count) {
//						print ("集滿道具");

//								//取得解鎖資訊
//								int[] tempS = saveBoxSC.GetStage_Lock (LevelSelcet.stageInt);
//								//未解鎖
//								if (tempS [LevelSelcet.levelInt - 1] == 1) {
//										Instantiate (UnlockSound, this.gameObject.transform.position, Quaternion.identity);
//										saveBoxSC.SetStage_Lock (LevelSelcet.stageInt, LevelSelcet.levelInt - 1);//解鎖
//								}
//
								//一般UI
//								obLock.GetComponent<UISprite> ().enabled = false;
//
//								Transform[] allChildren0 = obLock.GetComponentsInChildren<Transform> ();
//								foreach (Transform child in allChildren0) {
//										if (child.gameObject.name == "Describe1" || child.gameObject.name == "Describe2" || child.gameObject.name == "Describe3") {
//												child.GetComponent<UILabel> ().enabled = false;
//										}
//										if (child.gameObject.name == "Lock Button" || child.gameObject.name == "Play_Button_Fake") {
//												child.GetComponent<UISprite> ().enabled = false;
//										}
//								}
								//道具UI
//						for (int q=0; q<2; q++) {
//
//						GameObject abc = GameObject.Find ("Need Icon" + (q + 1).ToString ());
//
//						if(q<array_Parameter.Count)//道具範圍內
//						{
//							abc.GetComponent<UISprite> ().enabled = true;
//						
//							Transform[] allChildren1 = abc.GetComponentsInChildren<Transform> ();
//							foreach (Transform child in allChildren1) 
//							{
//								if (child.name == "Sprite") {
//								child.gameObject.GetComponent<UISprite> ().enabled = true;
//
//								}
//								if (child.name == "cover") {//沒有黑色覆蓋
//								child.gameObject.GetComponent<UISprite> ().enabled = false;
//
//								}
//							}
//						}
//					else{//範圍外
//						abc.GetComponent<UISprite> ().enabled = false;
//
//						Transform[] allChildren1 = abc.GetComponentsInChildren<Transform> ();
//						foreach (Transform child in allChildren1) 
//						{
//							if (child.name == "Sprite") {
////								child.gameObject.GetComponent<UISprite> ().spriteName = "";
//								child.gameObject.GetComponent<UISprite> ().enabled = false;
//							}
//							if (child.name == "cover") {//沒有黑色覆蓋
//								child.gameObject.GetComponent<UISprite> ().enabled = false;
//							}
//						}


//								}
//							}



				//LockBG=====
				obLock.GetComponent<UISprite> ().enabled = true;
				Transform[] allChildren0 = obLock.GetComponentsInChildren<Transform> ();
				foreach (Transform child in allChildren0) {
					if (child.gameObject.name == "Describe1" || child.gameObject.name == "Describe2" || child.gameObject.name == "Describe3") {
						child.GetComponent<UILabel> ().enabled = true;
					}
					
					if (child.gameObject.name == "Lock Button" || child.gameObject.name == "Play_Button_Fake") {
						child.GetComponent<UISprite> ().enabled = true;
						
					}
				}

				//道具 ICON=====
				for (int q=0; q<array_Parameter.Count; q++) {

					GameObject abc = GameObject.Find ("Need Icon" + (q + 1).ToString ());
					abc.gameObject.GetComponent<UISprite> ().enabled = true;
					abc.gameObject.GetComponent<UISprite> ().spriteName="28";
				
					Transform[] allChildren1 = abc.GetComponentsInChildren<Transform> ();
					foreach (Transform child in allChildren1) {
						string fileName = "";
				
						if (array_Parameter [q] + 1 > 0 && array_Parameter [q] + 1 < 10)
							fileName = "item00" + (array_Parameter [q] + 1);
						else
							fileName = "item0" + (array_Parameter [q] + 1);
						
						if (saveBoxSC.arr_Treasure [array_Parameter [q]] > 0) {//有得到道具
							
							if (child.name == "Sprite") {
								child.gameObject.GetComponent<UISprite> ().spriteName = fileName;
							}
							if (child.name == "cover") {
								child.gameObject.GetComponent<UISprite> ().enabled = false;
							}
						} else {//沒得到道具
							
							if (child.name == "Sprite") {
								child.gameObject.GetComponent<UISprite> ().spriteName = fileName;
							}
							if (child.name == "cover") {
								child.gameObject.GetComponent<UISprite> ().enabled = true;
								child.gameObject.GetComponent<UISprite> ().spriteName = fileName;
							}
						}
					}
				}
				
				
				
			} 
			else {//沒集滿道具
				
				print ("沒集滿");
				//一般UI
				obLock.GetComponent<UISprite> ().enabled = true;
				
				
				Transform[] allChildren0 = obLock.GetComponentsInChildren<Transform> ();
								foreach (Transform child in allChildren0) {
										if (child.gameObject.name == "Describe1" || child.gameObject.name == "Describe2" || child.gameObject.name == "Describe3") {
												child.GetComponent<UILabel> ().enabled = true;
										}
				
										if (child.gameObject.name == "Lock Button" || child.gameObject.name == "Play_Button_Fake") {
												child.GetComponent<UISprite> ().enabled = true;

										}
								}

								//道具小於3
								for (int q=0; q<array_Parameter.Count; q++) {

										if (array_Parameter.Count < 3 && GameObject.Find ("Need Icon" + (3).ToString ())) {
					
												GameObject abc = GameObject.Find ("Need Icon" + (3).ToString ());
												abc.gameObject.GetComponent<UISprite> ().spriteName="28";

												Transform[] allChildren1 = abc.GetComponentsInChildren<Transform> ();
												foreach (Transform child in allChildren1) {
														if (child.name == "Sprite") {
																child.gameObject.GetComponent<UISprite> ().spriteName = "";
														}
														if (child.name == "cover") {
																child.gameObject.GetComponent<UISprite> ().enabled = false;
														}
												}
										}
								}


								for (int q=0; q<array_Parameter.Count; q++) {

//										
										GameObject abc = GameObject.Find ("Need Icon" + (q + 1).ToString ());
										abc.gameObject.GetComponent<UISprite> ().enabled = true;
										abc.gameObject.GetComponent<UISprite> ().spriteName="28";

										Transform[] allChildren1 = abc.GetComponentsInChildren<Transform> ();
										foreach (Transform child in allChildren1) {
					
												string fileName = "";
					
												if (array_Parameter [q] + 1 > 0 && array_Parameter [q] + 1 < 10)
														fileName = "item00" + (array_Parameter [q] + 1);
												else
														fileName = "item0" + (array_Parameter [q] + 1);

												if (saveBoxSC.arr_Treasure [array_Parameter [q]] > 0) {//有得到道具
	
														if (child.name == "Sprite") {
																child.gameObject.GetComponent<UISprite> ().spriteName = fileName;
														}
														if (child.name == "cover") {
																child.gameObject.GetComponent<UISprite> ().enabled = false;
														}
												} else {//沒得到道具
						
														if (child.name == "Sprite") {
																child.gameObject.GetComponent<UISprite> ().spriteName = fileName;
														}
														if (child.name == "cover") {
																child.gameObject.GetComponent<UISprite> ().enabled = true;
																child.gameObject.GetComponent<UISprite> ().spriteName = fileName;
														}
												}
										}
								}

						}//Else End


				} 
		else //不需集道具
		{
				
//			print("不用道具");
			//一般UI
			obLock.GetComponent<UISprite> ().enabled = false;
			
			Transform[] allChildren0 = obLock.GetComponentsInChildren<Transform> ();
			foreach (Transform child in allChildren0) {
				if (child.gameObject.name == "Describe1" || child.gameObject.name == "Describe2" || child.gameObject.name == "Describe3") {
					child.GetComponent<UILabel> ().enabled = false;
				}
				
				if (child.gameObject.name == "Lock Button" || child.gameObject.name == "Play_Button_Fake") {
					child.GetComponent<UISprite> ().enabled = false;
					
				}
			}
			
			//道具小於3
			for (int q=0; q<2; q++) {
				
//				if (array_Parameter.Count < 3 && GameObject.Find ("Need Icon" + (3).ToString ())) {
					
					GameObject abc = GameObject.Find ("Need Icon" + (q+1).ToString ());
				abc.GetComponent<UISprite> ().spriteName = "";
					
					Transform[] allChildren1 = abc.GetComponentsInChildren<Transform> ();
					foreach (Transform child in allChildren1) {
						if (child.name == "Sprite") {
							child.gameObject.GetComponent<UISprite> ().spriteName = "";
						}
						if (child.name == "cover") {
							child.gameObject.GetComponent<UISprite> ().enabled = false;
						}
					}
//				}
			}
		}









		setLock=true;


		
	}






	void RepaintStar(){
//		print ("RepaintStar");

		for(int a=0;a<arr_Stage1_Star.Length;a++){
			
			string levelName="";
			switch(a){
			case 0:
				levelName="1-1";
				break;
			case 1:
				levelName="1-2";
				break;
			case 2:
				levelName="1-3";
				break;
			case 3:
				levelName="1-4";
				break;
			case 4:
				levelName="1-5";
				break;
			case 5 :
				levelName="1-6";
				break;
			case 6:
				levelName="1-7";
				break;
			case 7:
				levelName="1-8";
				break;
			case 8:
				levelName="1-9";
				break;
			case 9:
				levelName="1-10";
				break;
				
			}
			
			
//						print ("NOW:"+levelName);
			//			GameObject box=GameObject.FindGameObjectWithTag(levelName);
			GameObject box=GameObject.Find(levelName);
			
			if(	box!=null)
			{
				//Clear Status=================================
				if(arr_Stage1_Clear[a]==1)//過關
				{
					Transform[] allChildren = box.GetComponentsInChildren<Transform>();
					foreach (Transform child in allChildren) {
						if(child.name=="Lock_Background")
						{
							child.gameObject.GetComponent<UISprite>().enabled = false;
							child.gameObject.GetComponent<BoxCollider>().enabled = false;
						}
					}
				}
				else if(arr_Stage1_Clear[a]==0)//鎖住
				{
					Transform[] allChildren = box.GetComponentsInChildren<Transform>();
					foreach (Transform child in allChildren) {
						if(child.name=="Lock_Background")
						{
							child.gameObject.GetComponent<UISprite>().enabled = true;
							child.gameObject.GetComponent<BoxCollider>().enabled = true;
						}
					}
				}


				
				//Star Number==================================
				if(arr_Stage1_Star[a]==0)
				{
					
					Transform[] allChildren = box.GetComponentsInChildren<Transform>();
					foreach (Transform child in allChildren) {
						if(child.name=="bell")
							child.gameObject.GetComponent<UISprite>().enabled = false;
						if(child.name=="bell2")
							child.gameObject.GetComponent<UISprite>().enabled = false;
						if(child.name=="bell3")
							child.gameObject.GetComponent<UISprite>().enabled = false;
					}
				}
				else if(arr_Stage1_Star[a]==1)
				{
					
					Transform[] allChildren = box.GetComponentsInChildren<Transform>();
					foreach (Transform child in allChildren) {
						if(child.name=="bell")
							child.gameObject.GetComponent<UISprite>().enabled = true;
						if(child.name=="bell2")
							child.gameObject.GetComponent<UISprite>().enabled = false;
						if(child.name=="bell3")
							child.gameObject.GetComponent<UISprite>().enabled = false;
					}
				}
				else if(arr_Stage1_Star[a]==2)
				{
					Transform[] allChildren = box.GetComponentsInChildren<Transform>();
					foreach (Transform child in allChildren) {
						if(child.name=="bell")
							child.gameObject.GetComponent<UISprite>().enabled = true;
						if(child.name=="bell2")
							child.gameObject.GetComponent<UISprite>().enabled = true;
						if(child.name=="bell3")
							child.gameObject.GetComponent<UISprite>().enabled = false;
					}
					
				}
				else if(arr_Stage1_Star[a]==3)
				{
					Transform[] allChildren = box.GetComponentsInChildren<Transform>();
					foreach (Transform child in allChildren) {
						if(child.name=="bell")
							child.gameObject.GetComponent<UISprite>().enabled = true;
						if(child.name=="bell2")
							child.gameObject.GetComponent<UISprite>().enabled = true;
						if(child.name=="bell3")
							child.gameObject.GetComponent<UISprite>().enabled = true;
					}
				}
			}
			
		}//stage1
		
		
		
		
		for(int b=0;b<arr_Stage2_Star.Length;b++){
			
			string levelName="";
			switch(b){
			case 0:
				levelName="2-1";
				break;
			case 1:
				levelName="2-2";
				break;
			case 2:
				levelName="2-3";
				break;
			case 3:
				levelName="2-4";
				break;
			case 4:
				levelName="2-5";
				break;
			case 5 :
				levelName="2-6";
				break;
				
				
			}
			

			GameObject box=GameObject.Find(levelName);
//			GameObject lockbg=GameObject.FindGameObjectWithTag("LockBG");
			
			if(	box!=null)
			{
				//Clear Status=================================
				if(arr_Stage2_Clear[b]==1)//過關
				{
					Transform[] allChildren = box.GetComponentsInChildren<Transform>();
					foreach (Transform child in allChildren) {
						if(child.name=="Lock_Background")
						{
							child.gameObject.GetComponent<UISprite>().enabled = false;
							child.gameObject.GetComponent<BoxCollider>().enabled = false;
						}
					}
				}
				else if(arr_Stage1_Clear[b]==0)//鎖住
				{
					Transform[] allChildren = box.GetComponentsInChildren<Transform>();
					foreach (Transform child in allChildren) {
						if(child.name=="Lock_Background")
						{
							child.gameObject.GetComponent<UISprite>().enabled = true;
							child.gameObject.GetComponent<BoxCollider>().enabled = true;
						}
					}
				}
				
				//Star Number==================================
				if(arr_Stage2_Star[b]==0)
				{
					
					Transform[] allChildren = box.GetComponentsInChildren<Transform>();
					foreach (Transform child in allChildren) {
						if(child.name=="bell")
							child.gameObject.GetComponent<UISprite>().enabled = false;
						if(child.name=="bell2")
							child.gameObject.GetComponent<UISprite>().enabled = false;
						if(child.name=="bell3")
							child.gameObject.GetComponent<UISprite>().enabled = false;
					}
				}
				else if(arr_Stage2_Star[b]==1)
				{
					
					Transform[] allChildren = box.GetComponentsInChildren<Transform>();
					foreach (Transform child in allChildren) {
						if(child.name=="bell")
							child.gameObject.GetComponent<UISprite>().enabled = false;
						if(child.name=="bell2")
							child.gameObject.GetComponent<UISprite>().enabled = false;
						if(child.name=="bell3")
							child.gameObject.GetComponent<UISprite>().enabled = false;
					}
				}
				else if(arr_Stage2_Star[b]==2)
				{
					Transform[] allChildren = box.GetComponentsInChildren<Transform>();
					foreach (Transform child in allChildren) {
						if(child.name=="bell")
							child.gameObject.GetComponent<UISprite>().enabled = true;
						if(child.name=="bell2")
							child.gameObject.GetComponent<UISprite>().enabled = true;
						if(child.name=="bell3")
							child.gameObject.GetComponent<UISprite>().enabled = false;
					}
					
				}
				else if(arr_Stage2_Star[b]==3)
				{
					Transform[] allChildren = box.GetComponentsInChildren<Transform>();
					foreach (Transform child in allChildren) {
						if(child.name=="bell")
							child.gameObject.GetComponent<UISprite>().enabled = true;
						if(child.name=="bell2")
							child.gameObject.GetComponent<UISprite>().enabled = true;
						if(child.name=="bell3")
							child.gameObject.GetComponent<UISprite>().enabled = true;
					}
				}
				
			}
			
		}//stage1
		
	}


}
