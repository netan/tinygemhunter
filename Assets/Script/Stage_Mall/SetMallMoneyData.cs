﻿using UnityEngine;
using System.Collections;

public class SetMallMoneyData : MonoBehaviour {

	public UILabel label;
	public UILabel moneyLabel;
	public string productID;
	public int money;
	public UISprite tagUI;
	public UISprite background;
	public UISprite logo;
	
	public void SetData(string pID, int m, int tag, int index) {
		productID = pID;
		money = m;
		label.text = "× " + m;
		switch ((((index - 1) % 6) + 1)) {
		case 1:
			moneyLabel.text = "USD $0.99";
			break;
		case 2:
			moneyLabel.text = "USD $1.99";
			break;
		case 3:
			moneyLabel.text = "USD $2.99";
			break;
		case 4:
			moneyLabel.text = "USD $4.99";
			break;
		case 5:
			moneyLabel.text = "USD $9.99";
			break;
		case 6:
			moneyLabel.text = "USD $19.99";
			break;
		}

		tagUI.spriteName = "tag" + tag;

		if (index <= 6) {
			background.spriteName = "Sale";
		} else {
			background.spriteName = "Normal";
		}

		logo.spriteName = (((index - 1) % 6) + 1).ToString();
	}
}
