﻿using UnityEngine;
using System.Collections;

public class MallItemClick : MonoBehaviour {

	public GameObject Stuff_Number;
	public GameObject Cross;
	public GameObject Question;
	public int MyName;

	public GameObject Cross1;
	public GameObject Cross2;
	public GameObject Cross3;
	public GameObject Cross4;

	public GameObject Stuff_Number1;
	public GameObject Stuff_Number2;
	public GameObject Stuff_Number3;
	public GameObject Stuff_Number4;

	public GameObject Hook1;
	public GameObject Hook2;
	public GameObject Hook3;
	public GameObject Hook4;

	public GameObject BG1;
	public GameObject BG2;
	public GameObject BG3;
	public GameObject BG4;
	

	//問MallController是否有該道具的數量以及有錢
	public bool BoolHook = false;
	public bool BoolHook_Star = false;
	public bool BoolHook_Bomb = false;
	public bool BoolHook_RandomItem = false;
	
	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void MallItemMouseDown(){

		LevelSelcet LS;
		LS = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent<LevelSelcet>();

		MallController MC;
		MC = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent<MallController>();



		//前三道具和隨機道具的互動
		if (this.gameObject.name == "Hook1") {
			MyName = 1;
			Debug.Log (this.gameObject.name+"    "+MyName);
		}
		if (this.gameObject.name == "Hook2") {
			MyName = 2;
			Debug.Log (this.gameObject.name+"    "+MyName);
		}
		if (this.gameObject.name == "Hook3") {
			MyName = 3;
			Debug.Log (this.gameObject.name+"    "+MyName);
		}
		if (this.gameObject.name == "Hook4") {
			MyName = 4;
			Debug.Log (this.gameObject.name+"    "+MyName);
		}

		MC.isNeedWarning = MC.Used_LoveOrTime || MC.Used_Star || MC.Used_Bomb || MC.Used_RandomItem;

		switch (MyName) 
		{
		case 0 :break;

		case 1 :
			if(PlayerPrefs.GetInt("openLoveOrTime")== 1)
			{
				if (this.gameObject.activeSelf)//關勾勾
				{
					MC.Used_LoveOrTime = false;
					MC.isPayLoveOrTime = false;
					MC.isPayMoney = MC.Used_LoveOrTime || MC.Used_Star || MC.Used_Bomb || MC.Used_RandomItem;
					MC.CloseUsedItem((int)1);
					BG1.GetComponent<UISprite>().spriteName = "Stage_Play_Frame2"; 
					if(MC.LoveOrTime_Number > 0)
					{
						Stuff_Number.gameObject.SetActive(true);
					}
					this.gameObject.SetActive (false);

				}
				else
				{

					BoolHook = MC.CheckUsedItem((int)1);

					if(BoolHook)//打勾勾
					{
						Stuff_Number.gameObject.SetActive(false);
						this.gameObject.SetActive (true);
						BG1.GetComponent<UISprite>().spriteName = "Stage_Play_Frame1"; 
						MC.Used_LoveOrTime = true;
					}
				}

				if(Question.gameObject.activeSelf)
				{

					if(BoolHook)
					{
						BG4.GetComponent<UISprite>().spriteName = "Stage_Play_Frame2"; 
					}
					else
					{
						this.gameObject.SetActive (false);
						Stuff_Number.gameObject.SetActive(true);
					}
					Cross1.gameObject.SetActive(false);

					Cross2.gameObject.SetActive(false);
					Stuff_Number2.gameObject.SetActive(true);

					Cross3.gameObject.SetActive(false);
					Stuff_Number3.gameObject.SetActive(true);

					Hook4.gameObject.SetActive(false);
					Stuff_Number4.gameObject.SetActive(true);
					
				}
			}
		break;

		case 2 :
			if(PlayerPrefs.GetInt("openStar")== 1)
			{
				if (this.gameObject.activeSelf)//關勾勾
				{
					MC.Used_Star = false;
					MC.isPayStar = false;
					MC.isPayMoney = MC.Used_LoveOrTime || MC.Used_Star || MC.Used_Bomb || MC.Used_RandomItem;
					MC.CloseUsedItem((int)2);
					BG2.GetComponent<UISprite>().spriteName = "Stage_Play_Frame2"; 

					if(MC.Star_Number > 0)
					{
						Stuff_Number.gameObject.SetActive(true);
					}
					this.gameObject.SetActive (false);
				}
				else
				{

					BoolHook_Star = MC.CheckUsedItem((int)2);

					if(BoolHook_Star)//打勾勾
					{
						Stuff_Number.gameObject.SetActive(false);
						this.gameObject.SetActive (true);
						BG2.GetComponent<UISprite>().spriteName = "Stage_Play_Frame1"; 
						MC.Used_Star = true;
					}
				}

				if(Question.gameObject.activeSelf)
				{
					if(BoolHook_Star)
					{
						BG4.GetComponent<UISprite>().spriteName = "Stage_Play_Frame2"; 
					}
					else
					{
						this.gameObject.SetActive (false);
						Stuff_Number.gameObject.SetActive(true);
					}
					Cross2.gameObject.SetActive(false);

					Cross1.gameObject.SetActive(false);
					Stuff_Number1.gameObject.SetActive(true);
					
					Cross3.gameObject.SetActive(false);
					Stuff_Number3.gameObject.SetActive(true);
					
					Hook4.gameObject.SetActive(false);
					Stuff_Number4.gameObject.SetActive(true);
				}
			}
			break;

		case 3 :
			if(PlayerPrefs.GetInt("openBomb")== 1)
			{
				if (this.gameObject.activeSelf)
				{
					MC.Used_Bomb = false;
					MC.isPayBomb = false;
					MC.isPayMoney = MC.Used_LoveOrTime || MC.Used_Star || MC.Used_Bomb || MC.Used_RandomItem;
					MC.CloseUsedItem((int)3);
					BG3.GetComponent<UISprite>().spriteName = "Stage_Play_Frame2"; 

					if(MC.Bomb_Number > 0)
					{
						Stuff_Number.gameObject.SetActive(true);
					}
					this.gameObject.SetActive (false);
				}
				else
				{

					BoolHook_Bomb = MC.CheckUsedItem((int)3);

					if(BoolHook_Bomb)//打勾勾
					{
						Stuff_Number.gameObject.SetActive(false);
						this.gameObject.SetActive (true);
						BG3.GetComponent<UISprite>().spriteName = "Stage_Play_Frame1"; 
						MC.Used_Bomb = true;
					}
				}

				if(Question.gameObject.activeSelf)
				{
					if(BoolHook_Bomb)
					{
						BG4.GetComponent<UISprite>().spriteName = "Stage_Play_Frame2"; 
					}
					else
					{
						this.gameObject.SetActive (false);
						Stuff_Number.gameObject.SetActive(true);
					}

					Cross3.gameObject.SetActive(false);
					
					Cross1.gameObject.SetActive(false);
					Stuff_Number1.gameObject.SetActive(true);
					
					Cross2.gameObject.SetActive(false);
					Stuff_Number2.gameObject.SetActive(true);
					
					Hook4.gameObject.SetActive(false);
					Stuff_Number4.gameObject.SetActive(true);
				}
			}
			break;

		case 4 :
			if(PlayerPrefs.GetInt("openRandomItem")== 1)
			{
				if (this.gameObject.activeSelf)
				{
					MC.Used_RandomItem = false;
					MC.isPayRandomItem = false;
					MC.isPayMoney = MC.Used_LoveOrTime || MC.Used_Star || MC.Used_Bomb || MC.Used_RandomItem;
					MC.CloseUsedItem((int)4);
					BG4.GetComponent<UISprite>().spriteName = "Stage_Play_Frame2"; 

					Stuff_Number.gameObject.SetActive(true);
					this.gameObject.SetActive (false);
				}
				else
				{

					BoolHook_RandomItem = MC.CheckUsedItem((int)4);

					if(BoolHook_RandomItem)//打勾勾
					{
						Stuff_Number.gameObject.SetActive(false);
						this.gameObject.SetActive (true);
						BG4.GetComponent<UISprite>().spriteName = "Stage_Play_Frame1"; 

						BG1.GetComponent<UISprite>().spriteName = "Stage_Play_Frame2"; 
						BG2.GetComponent<UISprite>().spriteName = "Stage_Play_Frame2"; 
						BG3.GetComponent<UISprite>().spriteName = "Stage_Play_Frame2"; 
						MC.Used_RandomItem = true;
					}
					
				}
			}
			break;
		

		}

	}

	public void ItemDisabled(){

		if (PlayerPrefs.GetInt("openRandomItem")== 1) 
		{

			if (Question.gameObject.activeSelf) {
					this.gameObject.SetActive (false);
					Stuff_Number.gameObject.SetActive (false);
					Cross.gameObject.SetActive (true);
	
			} else {
					this.gameObject.SetActive (false);
					Stuff_Number.gameObject.SetActive (true);
					Cross.gameObject.SetActive (false);
			}
		}
	}
}
