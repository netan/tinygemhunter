﻿using UnityEngine;
using System.Collections;

public class Continue : MonoBehaviour {

	public MallController MC;
	public MyStage MS;
	public TestNetwork TN;
	public GameObject Continue_UI;
	public GameObject NoMoney_UI;
	// Use this for initialization
	void Start () {
		MC = GameObject.FindGameObjectWithTag("EventControoler").GetComponent<MallController>();
		TN = GameObject.FindGameObjectWithTag("EventControoler").GetComponent<TestNetwork>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void IfContinue () {
		Continue_UI.SetActive (true);
		MS.gameStatus = 15;
	}
	public void DontContinue () {
		Continue_UI.SetActive (false);
		MS.gameStatus = 8;
	}
	public void DoContinue () {
		if(MC.NowMallMoney >= 1)
		{
			Continue_UI.SetActive (false);
			PlayerPrefs.SetInt("docontinue", 1);
			if(TN.isConnect && !TN.isReConnect) 
			{
				MyParse.MinusMallMoney(1);
			}
			else
			{
				//MC.NowMallMoney -= 1;
				PlayerPrefs.SetInt("costed", PlayerPrefs.GetInt("costed") + 1);
			}
		
		
			MS.gameStatus = 5;
		}
		else
		{
			NoMoney_UI.SetActive(true);
		}
	}
}
