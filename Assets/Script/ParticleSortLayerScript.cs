﻿using UnityEngine;
using System.Collections;




//[RequireComponent(typeof(SpriteRenderer))]

public class ParticleSortLayerScript : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
		particleSystem.renderer.sortingLayerID = spriteRenderer.sortingLayerID;
		particleSystem.renderer.sortingOrder = spriteRenderer.sortingOrder;
	}
	
}