﻿using UnityEngine;
using System.Collections;

public class ChangeMultiLanguage : MonoBehaviour {

	public string[] id;
	public int[] index;
	public LoadCSV csv;

	public int idx;

	private Font[] font = new Font[4];

	
	// Use this for initialization

	public void preSet() {
		index = new int[id.Length];
		csv = GameObject.Find ("EventControoler").GetComponent<LoadCSV> ();
		for (int i = 0; i < id.Length; i++) {
			for (int j = 0; j < csv.array_TextTable_Number.Count - 1; j++) {
				if (csv.array_TextTable_Number[j] == id[i]) {
					Debug.Log(id[i]);
					index[i] = j;
				}
			}
		}
		font[0] = Resources.Load("Font/JF Flat regular", typeof(Font)) as Font;
		font[1] = Resources.Load("Font/ZN", typeof(Font)) as Font;
		font[2] = Resources.Load("Font/JP", typeof(Font)) as Font;
		font[3] = Resources.Load("Font/CN", typeof(Font)) as Font;
		gameObject.GetComponent<UILabel> ().trueTypeFont = font [PlayerPrefs.GetInt ("Language")];
	}
	
	public string getLanguage(string getID) {
		for (int i = 0; i < id.Length; i++) {
			if (id[i] == getID) {
				idx = index[i];
			}
		}
		return csv.array_TextTable [idx] [PlayerPrefs.GetInt ("Language")];
	}
}
