﻿using UnityEngine;
using System.Collections;

public class FadeOut : MonoBehaviour {


	float fadeSpeed = 0.5f;
	float alpha = 1.0f; 
	float fadeDir = -1;



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI(){
		alpha += fadeDir * fadeSpeed * Time.deltaTime;  
		alpha = Mathf.Clamp01(alpha);  
		this.renderer.material.color = new Color(1,1,1,alpha);

	}

}
