﻿using UnityEngine;
using System.Collections;
using ArabicSupport;

public class ArabicText : MonoBehaviour {

	private UILabel label;
	private bool tashkeel = true;
	private bool hinduNumbers = true;

	void Awake () {
		label = this.gameObject.GetComponent<UILabel> ();
	}

	void Update () {
		label.text = ArabicFixer.Fix(label.text, tashkeel, hinduNumbers);
	}
}
