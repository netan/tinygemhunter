﻿using UnityEngine;
using System.Collections;

public class ItemTeachNext : MonoBehaviour {
	
	public LoadItemTeach LT;
	public ChangeLanguage label;

	public GameObject UI;

	public void OnClick() {
		if (LT.index < LT.length-2) {
			LT.index++;
			label.ReLoadID("10091");
		} else if (LT.index < LT.length-1) {
			LT.index++;
			label.ReLoadID("10092");
		} else {
			UI.SetActive(false);
		}

	}
}
