﻿using UnityEngine;
using System.Collections;
using ArabicSupport;

public class ChangeLanguage : MonoBehaviour {

	public string id;
	public UILabel label;

	private Font[] font;

	public LoadCSV csv;
	public int index;

	public bool isChangeLanguage;

	private bool tashkeel = true;
	private bool hinduNumbers = true;

	// Use this for initialization
	void Start () {
		csv = GameObject.Find ("EventControoler").GetComponent<LoadCSV> ();
		index = csv.getIndexByID(id);
		//font = new Font[2];
		font = new Font[4];
		font[0] = Resources.Load("Font/JF Flat regular", typeof(Font)) as Font;
		font[1] = Resources.Load("Font/ZN", typeof(Font)) as Font;
		font[2] = Resources.Load("Font/JP", typeof(Font)) as Font;
		font[3] = Resources.Load("Font/CN", typeof(Font)) as Font;
	}
	
	// Update is called once per frame
	void Update () {
		if (csv.array_TextTable [index] [PlayerPrefs.GetInt ("Language")] != label.text) {
			label.text = csv.getTextByIndex(index);
			label.text = ArabicFixer.Fix(label.text, tashkeel, hinduNumbers);
		}
		label.trueTypeFont = font[PlayerPrefs.GetInt ("Language")];
	}

	public void ReLoadID(string newID) {
		id = newID;
		csv = GameObject.Find ("EventControoler").GetComponent<LoadCSV> ();
		index = csv.getIndexByID(id);
	}

	void OnClick() {
		if (isChangeLanguage) {
			PlayerPrefs.SetInt ("Language", (PlayerPrefs.GetInt ("Language") + 1) % 4);
		}
	}
	/*
	public void OnValueChange() {  //change language
		PlayerPrefs.SetInt ("Language", UIPopupList.current.items.IndexOf(UIPopupList.current.value));
	}*/
}
