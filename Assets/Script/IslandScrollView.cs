﻿using UnityEngine;
using System.Collections;

public class IslandScrollView : MonoBehaviour {

	public Test stagenow;
	public UIScrollView SV;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (stagenow.stageNow == 1 || stagenow.stageNow == 7) {
			SV.momentumAmount = 110;
		} else {
			SV.momentumAmount = 70;
		}
	}
}
