#pragma strict

var health : int = 0;

var ItemGot:GameObject;

//音效======================
var sound_BrickCrack:GameObject;
var sound_BrickBreak:GameObject;
var sound_BobBreak:GameObject;
var sound_SpidersilkBreak:GameObject;
var sound_Treasure:GameObject;
var sound_SUPER:GameObject;
var sound_Bomb:GameObject;
var sound_ToxicBallBomb:GameObject;
var sound_LaserBomb:GameObject;


//Perfab====================
var love:GameObject;
var hourglass:GameObject;

var green_none:GameObject;
var green_BlueGem:GameObject;
var green_GreenGem:GameObject;
var green_RedGem:GameObject;
var green_SilverGem:GameObject;
var green_GoldGem:GameObject;
var green_Bell:GameObject;


//特效===
var shiny:GameObject;//閃光

var smoke1:GameObject;//白煙
var smoke2:GameObject;//黃煙
var smoke3:GameObject;//綠煙
var explose:GameObject;//爆炸
var bigbumb:GameObject;//炸彈中心爆炸特效
var PoisonBomb:GameObject;//毒球爆炸中心特效
var icebreak:GameObject;//冰塊爆炸特效


//範圍=======================
var laser:GameObject;//閃電1ont
var laserCross:GameObject;//閃電2
var bumbRange:GameObject;//炸彈 範圍1
var bumbRangeEx:GameObject;//炸彈 範圍2

var explosiveRange:GameObject;//火藥岩 範圍
var iceRange1:GameObject;//冰1 範圍
var iceRange2:GameObject;//冰2 範圍
var toxicRange:GameObject;//毒 範圍
var powerRange:GameObject;//POWER 範圍

//寶石======================
var blueGem:GameObject;
var greenGem:GameObject;
var redGem:GameObject;
var silverGem:GameObject;
var goldGem:GameObject;
var dragonOb:GameObject;

var dragonPlayer:GameObject;

//SCRIPT====================
var dragonScript:Dragon;
var levelScript:MyStage;
var scoreScript:MyScore;
var playerScript:PlayerSC;
var eventScript:LoadCSV;

var got:boolean=false;//是否 取得資料
var gotItem:boolean=false;//是否 取得道具

var anim:Animator;//動畫
var anim_child_gem:Animator;
var anim_child_break:Animator;
var anim_child_cover:Animator;
var child_gem:GameObject;
var child_break:GameObject;
var child_cover:GameObject;

var child_cover_boxcollider2D : BoxCollider2D;

var soundBox:GameObject;
var prefabsStand:GameObject;

var isUpdateIce:boolean;

var myselfTransform : Vector3;

var PowerCrackIce : boolean = false;

var score : LoadScore;

var frameground : FrameGroundStar;

var RC : RandomCandy;
//商城道具的母物件 +隨機愛心的母物件
var Mall_Love_Father : GameObject;
var Mall_Star1_Father : GameObject; 
var loveFather:GameObject;

//Destroy前報自己的位置給SetBrickPosition.cs
var SBP : SetBrickPosition;


function Awake () {
gameObject.AddComponent ("SetBrickPosition");
}

function Start () {
	
	RC = gameObject.AddComponent ("RandomCandy");
	SBP = this.GetComponent(SetBrickPosition);
	
	//RC = this.gameObject.transform.GetComponent(RandomCandy);
	//myselfTransform = this.gameObject.transform.

	if(GameObject.FindGameObjectWithTag("Player") != null) {
	dragonPlayer=GameObject.FindGameObjectWithTag("Player");
	}
	soundBox=GameObject.FindGameObjectWithTag("SoundBox");
	prefabsStand=GameObject.FindGameObjectWithTag("PrefabsStand");
	
	
	dragonScript=dragonPlayer.gameObject.GetComponent(Dragon);
	playerScript= dragonPlayer.gameObject.GetComponent(PlayerSC);
	
//	scoreScript=GameObject.FindGameObjectWithTag("ScoreBox").GetComponent(MyScore);
	levelScript=GameObject.FindGameObjectWithTag("LevelManager").GetComponent(MyStage);
	eventScript=GameObject.FindGameObjectWithTag("EventControoler").GetComponent(LoadCSV);

	score = GameObject.Find("ScoreData").GetComponent(LoadScore);
	
	frameground = GameObject.Find("FrameGround").GetComponent(FrameGroundStar);
	
		//2014.7.31_18:18
	var cover : GameObject = Instantiate(Resources.Load("cover"));
	cover.transform.parent =this.transform;
	cover.transform.position = this.transform.position;
	
	if(playerScript.GoMall_Star1)
	{
		this.GetComponent(BoxCollider2D).collider2D.isTrigger=true;
		cover.GetComponent(BoxCollider2D).collider2D.isTrigger=true;
	}
	
	
	
	anim = gameObject.GetComponent(Animator);
	isUpdateIce=false;
	var children = GetComponentsInChildren (Transform);
	for (var child : Transform in children) {
	
	if(child.name=="gem")
	{
		child_gem=child.gameObject;
		anim_child_gem = child.GetComponent(Animator);
	}
		
	if(child.name=="break")
	{
		child_break=child.gameObject;
		anim_child_break = child.GetComponent(Animator);
	}
		
	if(child.name=="cover(Clone)")
	{
		child_cover=child.gameObject;
		anim_child_cover = child.GetComponent(Animator);
		child_cover_boxcollider2D = child.GetComponent(BoxCollider2D);
	}
		
	}
}







function Update () {

if(GameObject.FindGameObjectWithTag("Player") != null) {

if(child_cover_boxcollider2D.enabled)
{
PowerCrackIce = true;
this.transform.GetComponent(Animator).enabled = false;
}
else
{
this.transform.GetComponent(Animator).enabled = true;
}

if((dragonScript.status==11  || dragonScript.status==3)  && !isUpdateIce)
{
	
	isUpdateIce = true;
	//Debug.Log("entry" + dragonScript.status.ToString() + "  " + (isUpdateIce));
	IceSpread();
	
} 
else if (dragonScript.status!=11 &&  dragonScript.status!=3 && isUpdateIce) {
	isUpdateIce = false;
}


BreakBobAndFan();
//BreakSpidersilk();
}
}





function BreakBobAndFan(){
//FanTimerIns();

var hit : RaycastHit2D = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

//鎖住風扇
if (Input.GetMouseButtonDown(0) && hit.collider != null  && (hit.collider.name=="Fan_R" || hit.collider.name=="Fan_L"  ))
{

    if(hit.collider != null && hit.transform.parent.gameObject!=null &&hit.transform.parent.gameObject == this.gameObject)  
    {
//    print(hit.collider);    
    var children = hit.transform.parent.gameObject.GetComponentsInChildren (Transform);	
    	
//    	if(hit.transform.name=="21.Bob(Clone)" )
//    	{
//    		if(hit.transform.position==dragonPlayer.transform.position)
//    		{
//    		playerScript.gotBreak=true;
//    		}
//    		this.transform.collider2D.enabled=false;
//    		
//			Instantiate (sound_BobBreak, this.gameObject.transform.position, Quaternion.identity);
//    		scoreScript.Bob21_Nnumber++;
//    		hit.transform.GetComponent(Animator).SetInteger("status", -1);
//    		yield WaitForSeconds(.3);
//    		levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
//    		Destroy(hit.collider.gameObject);	
//    	}
//    	
    	
    	if(hit.transform.name=="Fan_R" && hit.transform.parent.gameObject == this.gameObject )
    	{   
			levelScript.FanR_Number++;

    		hit.transform.parent.GetComponent(Animator).SetInteger("status", -1);
    		
    		for (var child : Transform in children) {
    			if(child.name=="Fan_R")
    				child.transform.GetComponent(BoxCollider2D).isTrigger = true;
    		}
    		yield WaitForSeconds(2);

    		hit.transform.parent.GetComponent(Animator).SetInteger("status", 0);
    		
    		for (var child : Transform in children) {
    			if(child.name=="Fan_R")
    				child.transform.GetComponent(BoxCollider2D).isTrigger = false;
    		}
    	}
     	if(hit.transform.name=="Fan_L" && hit.transform.parent.gameObject == this.gameObject )   	
    	{	
			levelScript.FanL_Number++;

			hit.transform.parent.GetComponent(Animator).SetInteger("status", -1);

    		for (var child : Transform in children) {
    			if(child.name=="Fan_L")
    				child.transform.GetComponent(BoxCollider2D).isTrigger = true;
    		}
    		yield WaitForSeconds(2);

    		hit.transform.parent.GetComponent(Animator).SetInteger("status", 0);
    		
    		for (var child : Transform in children) {
    			if(child.name=="Fan_L")
    				child.transform.GetComponent(BoxCollider2D).isTrigger = false;
    		}	

    	}

    	
    	
    	
    }
}
}


function BreakSpidersilk()
{

if(Input.touchCount>0 && Input.GetTouch(0).phase== TouchPhase.Moved)
{
var  hit:RaycastHit2D;
var touchDeltaPosition:Vector3 = Input.GetTouch(0).deltaPosition;
var ray = Camera.main.ScreenPointToRay(touchDeltaPosition);

	if (touchDeltaPosition.x > 1 )
	{
//	tex.guiText.text="Right Swipe";
    hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
    	if(hit.collider != null)
    	{
//    	print(hit.collider .name);
var tempSilk1:GameObject=hit.transform.gameObject;
    		if(hit.collider.name=="22.SpiderSilk(Clone)")
    		{
    			if(hit.transform.position==dragonPlayer.transform.position)
    			{
    			print("same!");
    			playerScript.gotBreak=true;
    			}
//    			if(this.gameObject.GetComponent(CircleCollider2D).enabled==true;){
//    			hit.collider.transform.collider2D.enabled=false;
//    			}
				hit.collider.transform.GetComponent(BoxCollider2D).enabled=false;

				Instantiate (sound_SpidersilkBreak, this.gameObject.transform.position, Quaternion.identity);
    			scoreScript.SpiderSilk22_Nnumber++;
    			hit.transform.GetComponent(Animator).SetInteger("status", -1);
    			yield WaitForSeconds(.5);
//    			if(hit.collider.gameObject==null)
//						return;
//    		   	else
    		   		Destroy(tempSilk1);
    		}
    	}
	}
	
	else if (touchDeltaPosition.x < 1 )
	{
//	tex.guiText.text="L Swipe";
    hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
    	if(hit.collider != null)
    	{
    	var tempSilk2:GameObject=hit.transform.gameObject;
//    	print(hit.collider .name);
    		if(hit.collider.name=="22.SpiderSilk(Clone)")
    		{
    			if(hit.transform.position==dragonPlayer.transform.position)
    			{
//    			print("same!");
    			playerScript.gotBreak=true;
    			}
    			hit.collider.transform.GetComponent(BoxCollider2D).enabled=false;
				Instantiate (sound_SpidersilkBreak, this.gameObject.transform.position, Quaternion.identity);
    			scoreScript.SpiderSilk22_Nnumber++;
    			hit.transform.GetComponent(Animator).SetInteger("status", -1);
    			yield WaitForSeconds(.5);
//    			if(hit.collider.gameObject!=null)
    		   		Destroy(tempSilk2);
    		
    		}
    	}
	}
}

}


//Brick Effect=========================================================
function Black_Effect() {
	health--;
	if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Black");
		score.AddScore(5);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Black_Number++;
		}
				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
}

function Green_Effect(){
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次	
			//get score
			Debug.Log("get Green");
			score.AddScore(8);	
			this.collider2D.isTrigger = true;//取消碰撞			
			//黃煙
			Instantiate (smoke3, this.gameObject.transform.position, Quaternion.identity);
			//發出空格訊息
			//levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
				levelScript.Green_Number++;//增加 土塊 數					
			//Destroy(this.gameObject);//摧毀物件
			SBP.SendEmptyAndDestroy();
		}
	}
}
function Green_Blue_Effect(){
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
		
			//get score
			Debug.Log("get Green_Blue");
			score.AddScore(8);
			score.AddScore(10);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Blue");				
			//綠煙
			Instantiate (smoke3, this.gameObject.transform.position, Quaternion.identity);
			//發出空格訊息
			//levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Green_Number++;//增加 土塊 數	
				levelScript.BlueGem_Number++;//增加 土塊 數	
			}
							
			//Destroy(this.gameObject);//摧毀物件
			SBP.SendEmptyAndDestroy();

		}
	}
}
function Green_Green_Effect(){
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Green_Green");
			score.AddScore(8);
			score.AddScore(11);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Green");				
			//白煙
			Instantiate (smoke3, this.gameObject.transform.position, Quaternion.identity);
			//發出空格訊息
			//levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Green_Number++;//增加 土塊 數	
				levelScript.GreenGem_Number++;//增加 土塊 數	
			}
							
			//Destroy(this.gameObject);//摧毀物件
			SBP.SendEmptyAndDestroy();

		}
	}
}
function Green_Red_Effect(){
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Green_Red");
			score.AddScore(8);
			score.AddScore(9);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Red");				
			//白煙
			Instantiate (smoke3, this.gameObject.transform.position, Quaternion.identity);
			//發出空格訊息
			//levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Green_Number++;//增加 土塊 數	
				levelScript.RedGem_Number++;//增加 土塊 數	
			}
							
			//Destroy(this.gameObject);//摧毀物件
			SBP.SendEmptyAndDestroy();

		}
	}
}
function Green_Silver_Effect(){
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Green_Red");
			score.AddScore(8);
			score.AddScore(12);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Silver");				
			//白煙
			Instantiate (smoke3, this.gameObject.transform.position, Quaternion.identity);
			//發出空格訊息
			//levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Green_Number++;//增加 土塊 數	
				levelScript.SilverGem_Number++;//增加 土塊 數	
			}
							
			//Destroy(this.gameObject);//摧毀物件
			SBP.SendEmptyAndDestroy();

		}
	}
}
function Green_Gold_Effect(){
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Green_Gold");
			score.AddScore(8);
			score.AddScore(13);
		
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Gold");				
			//白煙
			Instantiate (smoke3, this.gameObject.transform.position, Quaternion.identity);
			//發出空格訊息
			//levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Green_Number++;//增加 土塊 數	
				levelScript.GoldGem_Number++;//增加 土塊 數	
			}
							
			//Destroy(this.gameObject);//摧毀物件
			SBP.SendEmptyAndDestroy();

		}
	}
}
function Green_Bell_Effect(){
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Green_Bell");
			score.AddScore(8);
			score.AddScore(21);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Bell");				
			//白煙
			Instantiate (smoke3, this.gameObject.transform.position, Quaternion.identity);
			//發出空格訊息
			//levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Green_Number++;//增加 土塊 數	
				levelScript.Bell_Number++;//增加 土塊 數	
			}
							
			//Destroy(this.gameObject);//摧毀物件
			SBP.SendEmptyAndDestroy();

		}
	}
}

function Sand_Effect(){
	health--;
	
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			//Debug.Log("get Sand");
			score.AddScore(0);
			
			this.collider2D.isTrigger = true;//取消碰撞
			
			//黃煙
			Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			sound_BrickBreak=Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//發出空格訊息
			//levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
						
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)
			{//Play時
				levelScript.Sand_Number++;//增加 土塊 數	
			}
				
			//Destroy(this.gameObject);//摧毀物件
			SBP.SendEmptyAndDestroy();

		}
	}
}

function Soil_Effect(){
	health--;
	
	
	if (health == 1) 
	{
		//白煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);	
		//裂痕
		anim_child_break.SetInteger("status",4);		
	}
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			//Debug.Log("get Soil");
			score.AddScore(1);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//泡泡來吧
			//var Candy : GameObject = Instantiate((Resources.Load("Candy")), this.gameObject.transform.position, Quaternion.identity);
			//黃煙
			Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//發出空格訊息
			//levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
				levelScript.Soil_Number++;//增加 土塊 數	
				
			//Destroy(this.gameObject);//摧毀物件
			SBP.SendEmptyAndDestroy();
		}
	}
}
function Rock_Effect(){
	health--;
	if (health == 2) 
	{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);		
	}		
	if (health == 1) 
	{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);	
		//裂痕4
		anim_child_break.SetInteger("status",4);		
	}
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
//			Debug.Log("get Rock");
			score.AddScore(2);
			
			this.collider2D.isTrigger = true;//取消碰撞
			
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//發出空格訊息
			//levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
				levelScript.Rock_Number++;//增加 土塊 數	
				
			//Destroy(this.gameObject);//摧毀物件
			SBP.SendEmptyAndDestroy();
		}
	}
}

function Red_Effect(){
		health--;
		if(health==3)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",2);
		}			
		else if(health==2)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);
		}	
		else if(health==1)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕4
		anim_child_break.SetInteger("status",4);
		}
		else if(health==0){
		
			if(!got)
			{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Red");
			score.AddScore(3);
			
			this.collider2D.isTrigger = true;//取消碰撞
			
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
			//發出空格訊息
			//levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Red_Number++;//增加 石塊 數	
			}
			
			//Destroy(this.gameObject);//摧毀物件
			SBP.SendEmptyAndDestroy();
			
			}	
		}
}


function Valcon_Effect(){
		health--;
		if(health==4)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",1);
		}		
		else if(health==3)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",2);
		}			
		else if(health==2)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);
		}	
		else if(health==1)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕4
		anim_child_break.SetInteger("status",4);
		}
		else if(health==0){
		
			if(!got)
			{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Valcon");
			score.AddScore(4);
		
			this.collider2D.isTrigger = true;//取消碰撞
			
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Valcon_Number++;//增加 石塊 數	
			}
			
			SBP.SendEmptyAndDestroy();
			
			}	
		}
}


function Sand_Blue_Effect(){
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
//			Debug.Log("get Sand_Blue");
			score.AddScore(0);
			score.AddScore(10);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Blue");				
			//黃煙
			Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			
			//藍寶石
			Instantiate (blueGem, this.gameObject.transform.position, Quaternion.identity);
						
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Sand_Number++;//增加 沙塊 數	
				levelScript.BlueGem_Number++;//增加 寶石 數	
			}
				
				
			SBP.SendEmptyAndDestroy();
		}
	}
}
function Sand_Green_Effect(){
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Sand_Green");
			score.AddScore(0);
			score.AddScore(11);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Green");				
			//黃煙
			Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			
			//綠寶石
			Instantiate (greenGem, this.gameObject.transform.position, Quaternion.identity);			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Sand_Number++;//增加 沙塊 數	
				levelScript.GreenGem_Number++;//增加 寶石 數	
			}
				
				
			SBP.SendEmptyAndDestroy();
		}
	}
}
function Sand_Red_Effect(){
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			//Debug.Log("get Sand_Red");
			score.AddScore(0);
			score.AddScore(9);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Red");				
			//黃煙
			Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			
			//紅寶石
			Instantiate (redGem, this.gameObject.transform.position, Quaternion.identity);		
				
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Sand_Number++;//增加 沙塊 數	
				levelScript.RedGem_Number++;//增加 寶石 數	
			}
				
			SBP.SendEmptyAndDestroy();
		}
	}
}
function Sand_Silver_Effect(){
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Sand_Silver");
			score.AddScore(0);
			score.AddScore(12);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Silver");				
			//黃煙
			Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//銀寶石
			Instantiate (silverGem, this.gameObject.transform.position, Quaternion.identity);
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Sand_Number++;//增加 沙塊 數	
				levelScript.SilverGem_Number++;//增加 寶石 數	
			}
				
			SBP.SendEmptyAndDestroy();
		}
	}
}
function Sand_Gold_Effect(){
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Sand_Gold");
			score.AddScore(0);
			score.AddScore(13);
		
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Gold");				
			//黃煙
			Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//金寶石
			Instantiate (goldGem, this.gameObject.transform.position, Quaternion.identity);
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Sand_Number++;//增加 沙塊 數	
				levelScript.GoldGem_Number++;//增加 寶石 數	
			}
				
			SBP.SendEmptyAndDestroy();
		}
	}
}

function Soil_Blue_Effect(){
	health--;
	if (health == 1) 
	{
		//白煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);	
		//裂痕
		anim_child_break.SetInteger("status",4);		
	}
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Soil_Blue");
			score.AddScore(1);
			score.AddScore(10);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Blue");				
			//黃煙
			Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//藍寶石
			Instantiate (blueGem, this.gameObject.transform.position, Quaternion.identity);
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Soil_Number++;//增加 土塊 數	
				levelScript.BlueGem_Number++;//增加 寶石 數	
			}
				
			SBP.SendEmptyAndDestroy();
		}
	}
}
function Soil_Green_Effect(){
	health--;
	if (health == 1) 
	{
		//白煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);	
		//裂痕
		anim_child_break.SetInteger("status",4);		
	}
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Soil_Green");
			score.AddScore(1);
			score.AddScore(11);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Green");				
			//黃煙
			Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//綠寶石
			Instantiate (greenGem, this.gameObject.transform.position, Quaternion.identity);
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Soil_Number++;//增加 土塊 數	
				levelScript.GreenGem_Number++;//增加 寶石 數	
			}
			SBP.SendEmptyAndDestroy();
		}
	}
}
function Soil_Red_Effect(){
	health--;
	if (health == 1) 
	{
		//白煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);	
		//裂痕
		anim_child_break.SetInteger("status",4);		
	}
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			//Debug.Log("i'm here!!" + got);
			
			//get score
			Debug.Log("get Soil_Red");
			score.AddScore(1);
			score.AddScore(9);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Red");				
			//黃煙
			Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//紅寶石
			Instantiate (redGem, this.gameObject.transform.position, Quaternion.identity);
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Soil_Number++;//增加 土塊 數	
				levelScript.RedGem_Number++;//增加 寶石 數	
			}
				SBP.SendEmptyAndDestroy();
			
		}
	}
}
function Soil_Silver_Effect(){
	health--;
	if (health == 1) 
	{
		//白煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);	
		//裂痕
		anim_child_break.SetInteger("status",4);		
	}
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Soil_Silver");
			score.AddScore(1);
			score.AddScore(12);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Silver");				
			//黃煙
			Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//銀寶石
			Instantiate (silverGem, this.gameObject.transform.position, Quaternion.identity);
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Soil_Number++;//增加 土塊 數	
				levelScript.SilverGem_Number++;//增加 寶石 數	
			}
				
			SBP.SendEmptyAndDestroy();
		}
	}
}
function Soil_Gold_Effect(){
	health--;
	if (health == 1) 
	{
		//白煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);	
		//裂痕
		anim_child_break.SetInteger("status",4);		
	}
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Soil_Gold");
			score.AddScore(1);
			score.AddScore(13);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Gold");				
			//黃煙
			Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//金寶石
			Instantiate (goldGem, this.gameObject.transform.position, Quaternion.identity);
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Soil_Number++;//增加 土塊 數	
				levelScript.GoldGem_Number++;//增加 寶石 數	
			}
				
			SBP.SendEmptyAndDestroy();
		}
	}
}

function Rock_Blue_Effect(){
	health--;
	if (health == 2) 
	{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);		
	}		
	if (health == 1) 
	{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);	
		//裂痕4
		anim_child_break.SetInteger("status",4);		
	}
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Rock_Blue");
			score.AddScore(2);
			score.AddScore(10);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Blue");				
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//藍寶石
			Instantiate (blueGem, this.gameObject.transform.position, Quaternion.identity);
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Rock_Number++;//增加 石塊 數	
				levelScript.BlueGem_Number++;//增加 寶石 數	
			}
				
			SBP.SendEmptyAndDestroy();
		}
	}
}
function Rock_Green_Effect(){
	health--;
	if (health == 2) 
	{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);		
	}		
	if (health == 1) 
	{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);	
		//裂痕4
		anim_child_break.SetInteger("status",4);		
	}
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Rock_Green");
			score.AddScore(2);
			score.AddScore(11);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Green");				
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//綠寶石
			Instantiate (greenGem, this.gameObject.transform.position, Quaternion.identity);			
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Rock_Number++;//增加 石塊 數	
				levelScript.GreenGem_Number++;//增加 寶石 數	
			}
				
			SBP.SendEmptyAndDestroy();
		}
	}
}
function Rock_Red_Effect(){
	health--;
	if (health == 2) 
	{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);		
	}		
	if (health == 1) 
	{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);	
		//裂痕4
		anim_child_break.SetInteger("status",4);		
	}
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Rock_Red");
			score.AddScore(2);
			score.AddScore(9);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Red");				
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//紅寶石
			Instantiate (redGem, this.gameObject.transform.position, Quaternion.identity);			
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Rock_Number++;//增加 石塊 數	
				levelScript.RedGem_Number++;//增加 寶石 數	
			}
				
			SBP.SendEmptyAndDestroy();
		}
	}	
}
function Rock_Silver_Effect(){
	health--;
	if (health == 2) 
	{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);		
	}		
	if (health == 1) 
	{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);	
		//裂痕4
		anim_child_break.SetInteger("status",4);		
	}
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Rock_Silver");
			score.AddScore(2);
			score.AddScore(12);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Silver");				
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//銀寶石
			Instantiate (silverGem, this.gameObject.transform.position, Quaternion.identity);			
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Rock_Number++;//增加 石塊 數	
				levelScript.SilverGem_Number++;//增加 寶石 數	
			}
			SBP.SendEmptyAndDestroy();
		}
	}
}
function Rock_Gold_Effect(){
	health--;
	if (health == 2) 
	{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);		
	}		
	if (health == 1) 
	{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);	
		//裂痕4
		anim_child_break.SetInteger("status",4);		
	}
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Rock_Gold");
			score.AddScore(2);
			score.AddScore(13);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Gold");				
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//金寶石
			Instantiate (goldGem, this.gameObject.transform.position, Quaternion.identity);			
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Rock_Number++;//增加 石塊 數	
				levelScript.GoldGem_Number++;//增加 寶石 數	
			}
			SBP.SendEmptyAndDestroy();
		}
	}
}

function Red_Blue_Effect(){
		health--;
		if(health==3)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",2);
		}			
		else if(health==2)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);
		}	
		else if(health==1)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕4
		anim_child_break.SetInteger("status",4);
		}
		else if(health==0){
		
			if(!got)
			{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Red_Blue");
			score.AddScore(3);
			score.AddScore(10);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Blue");				
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//藍寶石
			Instantiate (blueGem, this.gameObject.transform.position, Quaternion.identity);			
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Red_Number++;//增加 石塊 數	
				levelScript.BlueGem_Number++;//增加 寶石 數	
			}
			
			SBP.SendEmptyAndDestroy();
			
			}	
		}
}
function Red_Green_Effect(){
		health--;
		if(health == 3) {
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//裂開聲	
			Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
			//裂痕2
			anim_child_break.SetInteger("status",2);
		} else if(health == 2) {
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//裂開聲	
			Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
			//裂痕3
			anim_child_break.SetInteger("status",3);
		} else if(health==1) {
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//裂開聲	
			Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
			//裂痕4
			anim_child_break.SetInteger("status",4);
		} else if(health == 0) {
		
			if(!got) {
				got=true;//只跑一次
				
				//get score
				Debug.Log("get Red_Green");
				score.AddScore(3);
				score.AddScore(11);
				
				this.collider2D.isTrigger = true;//取消碰撞
				//產生物件
				ProduceGemAndLove("Green");				
				//白煙
				Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
				//爆炸聲	
				Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
				//綠寶石
				Instantiate (greenGem, this.gameObject.transform.position, Quaternion.identity);			
				
				
				if(levelScript.gameStatus==5 || levelScript.gameStatus==2) {//Play時
					levelScript.Red_Number++;//增加 石塊 數	
					levelScript.GreenGem_Number++;//增加 寶石 數	
				}
				
				SBP.SendEmptyAndDestroy();
			}	
		}	
}
function Red_Red_Effect(){
		health--;
		if(health==3)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",2);
		}			
		else if(health==2)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);
		}	
		else if(health==1)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕4
		anim_child_break.SetInteger("status",4);
		}
		else if(health==0){
		
			if(!got)
			{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Red_Red");
			score.AddScore(3);
			score.AddScore(9);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Red");				
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//紅寶石
			Instantiate (redGem, this.gameObject.transform.position, Quaternion.identity);			
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Red_Number++;//增加 石塊 數	
				levelScript.RedGem_Number++;//增加 寶石 數	
			}
			
			SBP.SendEmptyAndDestroy();
			
			}	
		}
}
function Red_Silver_Effect(){
		health--;
		if(health==3)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",2);
		}			
		else if(health==2)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);
		}	
		else if(health==1)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕4
		anim_child_break.SetInteger("status",4);
		}
		else if(health==0){
		
			if(!got)
			{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Red_Silver");
			score.AddScore(3);
			score.AddScore(12);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Silver");				
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//銀寶石
			Instantiate (silverGem, this.gameObject.transform.position, Quaternion.identity);			
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Red_Number++;//增加 石塊 數	
				levelScript.SilverGem_Number++;//增加 寶石 數	
			}
			
			SBP.SendEmptyAndDestroy();
			
			}	
		}
}
function Red_Gold_Effect(){
		health--;
		if(health==3)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",2);
		}			
		else if(health==2)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);
		}	
		else if(health==1)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕4
		anim_child_break.SetInteger("status",4);
		}
		else if(health==0){
		
			if(!got)
			{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Red_Gold");
			score.AddScore(3);
			score.AddScore(13);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Gold");				
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//金寶石
			Instantiate (goldGem, this.gameObject.transform.position, Quaternion.identity);			
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Red_Number++;//增加 石塊 數	
				levelScript.GoldGem_Number++;//增加 寶石 數	
			}
			
			SBP.SendEmptyAndDestroy();
			
			}	
		}	
}

function Valcon_Blue_Effect(){
		health--;
		if(health==4)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",1);
		}		
		else if(health==3)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",2);
		}			
		else if(health==2)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);
		}	
		else if(health==1)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕4
		anim_child_break.SetInteger("status",4);
		}
		else if(health==0){
		
			if(!got)
			{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Valcon_Blue");
			score.AddScore(4);
			score.AddScore(10);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Blue");				
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//藍寶石
			Instantiate (blueGem, this.gameObject.transform.position, Quaternion.identity);			
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Valcon_Number++;//增加 石塊 數	
				levelScript.BlueGem_Number++;//增加 寶石 數	
			}
			
			SBP.SendEmptyAndDestroy();
			
			}	
		}
}
function Valcon_Green_Effect(){
		health--;
		if(health==4)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",1);
		}		
		else if(health==3)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",2);
		}			
		else if(health==2)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);
		}	
		else if(health==1)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕4
		anim_child_break.SetInteger("status",4);
		}
		else if(health==0){
		
			if(!got)
			{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Valcon_Green");
			score.AddScore(4);
			score.AddScore(11);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Green");				
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//綠寶石
			Instantiate (greenGem, this.gameObject.transform.position, Quaternion.identity);			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Valcon_Number++;//增加 石塊 數	
				levelScript.GreenGem_Number++;//增加 寶石 數	
			}
			
			SBP.SendEmptyAndDestroy();
			
			}	
		}	
}
function Valcon_Red_Effect(){
		health--;
		if(health==4)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",1);
		}		
		else if(health==3)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",2);
		}			
		else if(health==2)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);
		}	
		else if(health==1)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕4
		anim_child_break.SetInteger("status",4);
		}
		else if(health==0){
		
			if(!got)
			{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Valcon_Red");
			score.AddScore(4);
			score.AddScore(9);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Red");				
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//紅寶石
			Instantiate (redGem, this.gameObject.transform.position, Quaternion.identity);			
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Valcon_Number++;//增加 石塊 數	
				levelScript.RedGem_Number++;//增加 寶石 數	
			}
			
			SBP.SendEmptyAndDestroy();
			
			}	
		}
}
function Valcon_Silver_Effect(){
		health--;
		if(health==4)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",1);
		}		
		else if(health==3)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",2);
		}			
		else if(health==2)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);
		}	
		else if(health==1)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕4
		anim_child_break.SetInteger("status",4);
		}
		else if(health==0){
		
			if(!got)
			{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Valcon_Silver");
			score.AddScore(4);
			score.AddScore(12);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Silver");				
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//銀寶石
			Instantiate (silverGem, this.gameObject.transform.position, Quaternion.identity);			
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Valcon_Number++;//增加 石塊 數	
				levelScript.SilverGem_Number++;//增加 寶石 數	
			}
			
			SBP.SendEmptyAndDestroy();
			
			}	
		}	
}
function Valcon_Gold_Effect(){
		health--;
		if(health==4)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",1);
		}		
		else if(health==3)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",2);
		}			
		else if(health==2)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);
		}	
		else if(health==1)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕4
		anim_child_break.SetInteger("status",4);
		}
		else if(health==0){
		
			if(!got)
			{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Valcon_Gold");
			score.AddScore(4);
			score.AddScore(13);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Gold");				
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			//金寶石
			Instantiate (goldGem, this.gameObject.transform.position, Quaternion.identity);			
			
			
			if(levelScript.gameStatus==5 || levelScript.gameStatus==2)//Play時
			{
				levelScript.Valcon_Number++;//增加 石塊 數	
				levelScript.GoldGem_Number++;//增加 寶石 數	
			}
			
			SBP.SendEmptyAndDestroy();
			
			}	
		}
}




//=========================================================Brick Effect





function OnCollisionEnter2D (coll: Collision2D) {
	//Debug.Log(coll.transform.name);
	if(dragonScript.status == 2 || dragonScript.status == 6 || dragonScript.status == 7 || dragonScript.status == 8 ){
		if(((coll.gameObject.name=="dragon" || coll.gameObject.name=="dragon(Clone)") && !crackEffect) && (this.gameObject.name != "Treasure(Clone)" &&
		this.gameObject.name != "ToxicBall(Clone)" &&this.gameObject.name != "ThunderBall(Clone)" &&this.gameObject.name != "Star3(Clone)" &&
		this.gameObject.name != "Star2(Clone)" &&this.gameObject.name != "Star1(Clone)" &&this.gameObject.name != "Love(Clone)" &&
		this.gameObject.name != "Ice(Clone)" && this.gameObject.name != "Hourglass(Clone)" && this.gameObject.name != "Bumb(Clone)" && 
		this.gameObject.name != "Bell(Clone)" && this.gameObject.name != "Black(Clone)" && this.gameObject.name != "Fan_R(Clone)" && this.gameObject.name != "Fan_L(Clone)"))
		{
			RC.randCandy();
		}
	
	//Debug.Log(this.gameObject.name);
	//產生 Crack效應
	if(coll.gameObject.name=="dragon")
	{
		if(coll.gameObject.GetComponent(PlayerSC).powerExsit)	
		{
			Instantiate (powerRange, this.gameObject.transform.position, Quaternion.identity);
			coll.gameObject.GetComponent(PlayerSC).powerExsit=false;
		}
	}

	if(((coll.gameObject.name=="dragon" || coll.gameObject.name=="dragon(Clone)") && !crackEffect) && (this.gameObject.name != "Black(Clone)" && this.gameObject.name != "Fan_R(Clone)" && this.gameObject.name != "Fan_L(Clone)"))
	playerScript.comboNumber++;
	//crackEffect=true;
	
	
	var name=this.gameObject.name;
	

	switch(name)
	{
	//EXtra=========================================
	case "Green(Clone)":
		Green_Effect();
	break;
	
	case "Green_Blue(Clone)":
		Green_Blue_Effect();
	break;
	
	case "Green_Green(Clone)":
		Green_Green_Effect();
	break;
	
	case "Green_Red(Clone)":
		Green_Red_Effect();
	break;
	
	case "Green_Silver(Clone)":
		Green_Silver_Effect();	
	break;
	
	case "Green_Gold(Clone)":
		Green_Gold_Effect();
	break;
	
	case "Green_Bell(Clone)":
		Green_Bell_Effect();
	break;
	//NEW============================================
	//1.沙塊
	case "Sand(Clone)":
		Sand_Effect();
	break;	
	
	//2.土塊
	case "Soil(Clone)":
		Soil_Effect();
	break;
	
	//3.石塊
	case "Rock(Clone)":
		Rock_Effect();
	break;	
	
	//4.紅塊
	case "Red(Clone)":
		Red_Effect();
	break;
	
	//5.火山岩
	case "Valcon(Clone)":
		Valcon_Effect();
	break;	
	
	//6.黑塊(無法擊破)
	case "Black(Clone)":
	break;
	
	//沙塊===============================================================
	//7.沙塊 藍寶石
	case "Sand_Blue(Clone)":
		Sand_Blue_Effect();
	break;
	
	//8.沙塊 綠寶石
	case "Sand_Green(Clone)":
		Sand_Green_Effect();
	break;	
	
	//9.沙塊 紅寶石
	case "Sand_Red(Clone)":
		Sand_Red_Effect();
	break;	
	
	//10.沙塊 銀寶石
	case "Sand_Silver(Clone)":
		Sand_Silver_Effect();
	break;	
	
	//11.沙塊 金寶石
	case "Sand_Gold(Clone)":
		Sand_Gold_Effect();
	break;	
	
	//土塊===============================================================
	//12.土塊 藍寶石
	case "Soil_Blue(Clone)":
		Soil_Blue_Effect();
	break;		
	
	//13.土塊 綠寶石
	case "Soil_Green(Clone)":
		Soil_Green_Effect();
	break;	
	
	//14.土塊 紅寶石
	case "Soil_Red(Clone)":
		Soil_Red_Effect();
	break;	
	
	//15.土塊 銀寶石
	case "Soil_Silver(Clone)":
		Soil_Silver_Effect();
	break;	
		
	//16.土塊 金寶石
	case "Soil_Gold(Clone)":
		Soil_Gold_Effect();
	break;	
	
	//石塊===============================================================
	//17.石塊 藍寶石
	case "Rock_Blue(Clone)":
		Rock_Blue_Effect();
	break;		
	//18.石塊 綠寶石
	case "Rock_Green(Clone)":
		Rock_Green_Effect();
	break;		
	//19.石塊 紅寶石
	case "Rock_Red(Clone)":
		Rock_Red_Effect();
	break;		
	//20.石塊 銀寶石
	case "Rock_Silver(Clone)":
		Rock_Silver_Effect();
	break;		
	//21.石塊 金寶石
	case "Rock_Gold(Clone)":
		Rock_Gold_Effect();
	break;		
	
	//紅塊===============================================================	
	//22.紅塊 藍寶石	
	case "Red_Blue(Clone)":
		Red_Blue_Effect();
	break;	
	//23.紅塊 綠寶石	
	case "Red_Green(Clone)":
		Red_Green_Effect();
	break;		
	//24.紅塊 紅寶石
	case "Red_Red(Clone)":
		Red_Red_Effect();
	break;
	//25.紅塊 銀寶石
	case "Red_Silver(Clone)":
		Red_Silver_Effect();
	break;	
	//26.紅塊 金寶石
	case "Red_Gold(Clone)":
		Red_Gold_Effect();
	break;	
	//火山岩===============================================================	
	//27.火山岩 藍寶石	
	case "Valcon_Blue(Clone)":
		Valcon_Blue_Effect();
	break;		
	//28.火山岩 綠寶石	
	case "Valcon_Green(Clone)":
		Valcon_Green_Effect();
	break;		
	//29.火山岩 紅寶石	
	case "Valcon_Red(Clone)":
		Valcon_Red_Effect();
	break;		
	//30.火山岩 銀寶石	
	case "Valcon_Silver(Clone)":
		Valcon_Silver_Effect();
	break;		
	//31.火山岩 金寶石	
	case "Valcon_Gold(Clone)":
		Valcon_Gold_Effect();
	break;		
	
//	//32.星星1
//	case "Star1(Clone)":
//	break;	
//	//33.星星2
//	case "Star2(Clone)":
//	break;		
//	//34.星星3
//	case "Star3(Clone)":
//	break;		

	//35.寶箱
	case "Treasure(Clone)"://(health=1)
		health--;
		//Standby
		if(health==1)
		{
		yield WaitForSeconds(2);
		anim.SetInteger("status", 1);
		}
		//Disappear
		else if (health <= 0 ) 
		{
		//get score
//		Debug.Log("get treasure");
		score.AddScore(25);
		Instantiate (sound_Treasure, this.gameObject.transform.position, Quaternion.identity);		
		this.collider2D.isTrigger = true;
		var itemNumber=eventScript.GetReward(levelScript.stage);
		
		//產生寶箱動態
		ProduceItem(itemNumber);
				
				
				
//		print("得到道具:"+itemNumber);
		var itemString = "item0" + String.Format("{0:00}",itemNumber);
		//print(itemString);
		levelScript.gainItem.Add(itemString);
		//PlayerPrefs.SetInt(itemString, PlayerPrefs.GetInt(itemString) + 1);
			if(itemNumber==-1)//沒得到東西
			{
				//加生命
				if(levelScript.lifeLimited)
				{
					if(levelScript.gameStatus==5)//Play時
						levelScript.Love_Number++;
					var lo=Instantiate (love, this.gameObject.transform.position, Quaternion.identity);
					Instantiate (shiny, this.gameObject.transform.position, Quaternion.identity);
					yield WaitForSeconds(.5);
					Destroy(lo);
					playerScript.life+=3;
				}else
				{//加時間
					if(levelScript.gameStatus==5)//Play時
						levelScript.Hourglass_Number+=50;						
					var hg=Instantiate (hourglass, this.gameObject.transform.position, Quaternion.identity);
					Instantiate (shiny, this.gameObject.transform.position, Quaternion.identity);
					yield WaitForSeconds(.5);
					Destroy(hg);
					playerScript.Timer+=10;
				}
			}
			else{//有東西
				if(!gotItem)
				{	
					levelScript.listMyItem.Add(itemNumber);
					gotItem=true;
				}
			}
			
		if(!levelScript.isGameOver)
			levelScript.Treasure_Number++;
			
		anim.SetInteger("status", -1);
		yield WaitForSeconds(.5);
		SBP.SendEmptyAndDestroy();
		}
	break;		
	//36.閃電球
	case "ThunderBall(Clone)":
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get ThunderBall");
			score.AddScore(14);
		
			//雷射
			Instantiate (laser, this.gameObject.transform.position, Quaternion.identity);
			//雷射音效
			Instantiate (sound_LaserBomb, this.gameObject.transform.position, Quaternion.identity);
			
			
			if(levelScript.gameStatus==5)//Play時
			{
				levelScript.ThunderBall_Number++;//增加 BELL								
			}

				
			SBP.SendEmptyAndDestroy();
		}
	}	
	break;	
	//37.毒液球
	case "ToxicBall(Clone)":
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get ToxicBall");
			score.AddScore(15);
			
			//毒
			Instantiate (toxicRange, this.gameObject.transform.position, Quaternion.identity);
			//爆炸
			Instantiate (PoisonBomb, this.gameObject.transform.position, Quaternion.identity);
			//毒球爆炸音效
			Instantiate (sound_ToxicBallBomb, this.gameObject.transform.position, Quaternion.identity);
			
			
			if(levelScript.gameStatus==5)//Play時
			{
				levelScript.ToxicBall_Number++;//增加 BELL								
			}
			SBP.SendEmptyAndDestroy();
		}
	}	
	break;		
	
	
	//38.炸彈
	case "Bumb(Clone)":
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Bumb");
			score.AddScore(16);
			
			//爆炸範圍
			Instantiate (bumbRange, this.gameObject.transform.position, Quaternion.identity);
			//爆炸
			Instantiate (bigbumb, this.gameObject.transform.position, Quaternion.identity);
			//爆炸音效
			Instantiate (sound_Bomb, this.gameObject.transform.position, Quaternion.identity);

			
			
			if(levelScript.gameStatus==5)//Play時
			{
				levelScript.Bumb_Number++;//增加 吃到炸彈的過關條件							
			}

				
			SBP.SendEmptyAndDestroy();
		}
	}		
	break;	

	//40
	case "Sand_Bell(Clone)":
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Sand_Bell");
			score.AddScore(0);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Bell");				
			//黃煙
			Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			sound_BrickBreak=Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			
			
			if(levelScript.gameStatus==5)//Play時
			{
				levelScript.Sand_Number++;//增加 土塊 數	
				levelScript.Bell_Number++;//增加 BELL								
			}

				
			SBP.SendEmptyAndDestroy();

		}
	}	
	break;	
	//41
	case "Soil_Bell(Clone)":
	health--;
	if (health == 1) 
	{
		//白煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);	
		//裂痕
		anim_child_break.SetInteger("status",4);		
	}
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Soil_Bell");
			score.AddScore(1);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Bell");				
			//黃煙
			Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);
			
			
			if(levelScript.gameStatus==5)//Play時
			{
				levelScript.Soil_Number++;//增加 土塊 數	
				levelScript.Bell_Number++;//增加 寶石 數	
			}
				
			SBP.SendEmptyAndDestroy();
		}
	}	
	break;		
	//42
	case "Rock_Bell(Clone)":
	health--;
	if (health == 2) 
	{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);		
	}		
	if (health == 1) 
	{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);	
		//裂痕4
		anim_child_break.SetInteger("status",4);		
	}
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Rock_Bell");
			score.AddScore(2);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Bell");				
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);		
			
			
			if(levelScript.gameStatus==5)//Play時
			{
				levelScript.Rock_Number++;//增加 石塊 數	
				levelScript.Bell_Number++;//增加 寶石 數	
			}
				
			SBP.SendEmptyAndDestroy();
		}
	}	
	break;		
	//43
	case "Red_Bell(Clone)":
		health--;
		if(health==3)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",2);
		}			
		else if(health==2)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);
		}	
		else if(health==1)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕4
		anim_child_break.SetInteger("status",4);
		}
		else if(health==0){
		
			if(!got)
			{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Red_Bell");
			score.AddScore(3);
			
			this.collider2D.isTrigger = true;//取消碰撞
			//產生物件
			ProduceGemAndLove("Bell");				
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);		
			
			
			if(levelScript.gameStatus==5)//Play時
			{
				levelScript.Red_Number++;//增加 石塊 數	
				levelScript.Bell_Number++;//增加 寶石 數	
			}
			
			SBP.SendEmptyAndDestroy();
			
			}	
		}		
	break;		
	//44
	case "Valcon_Bell(Clone)":
		health--;
		if(health==4)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",1);
		}		
		else if(health==3)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕2
		anim_child_break.SetInteger("status",2);
		}			
		else if(health==2)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕3
		anim_child_break.SetInteger("status",3);
		}	
		else if(health==1)
		{
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		//裂開聲	
		Instantiate (sound_BrickCrack, this.gameObject.transform.position, Quaternion.identity);
		//裂痕4
		anim_child_break.SetInteger("status",4);
		}
		else if(health==0){
		
			if(!got)
			{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get Valcon_Bell");
			score.AddScore(4);
			
			this.collider2D.isTrigger = true;//取消碰撞
			
			//產生物件
			ProduceGemAndLove("Bell");			
			//白煙
			Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
			//爆炸聲	
			Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);		
			
			
			if(levelScript.gameStatus==5)//Play時
			{
				levelScript.Valcon_Number++;//增加 石塊 數	
				levelScript.Bell_Number++;//增加 寶石 數	
			}
			
			SBP.SendEmptyAndDestroy();
			
			}	
		}	
	break;		
	
	//45
//	case "Fan_R(Clone)":
//	break;		
//	//46
//	case "Fan_L(Clone)":
//	break;	
	
	//47.冰
	case "Ice(Clone)":
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
		
			//get score
			//Debug.Log("get Ice");
			score.AddScore(6);

			
			
			if(levelScript.gameStatus==5)//Play時
			{
				levelScript.Ice_Number++;//增加 冰塊的過關條件							
			}

			//爆炸
			Instantiate (icebreak, this.gameObject.transform.position, Quaternion.identity);
			SBP.SendEmptyAndDestroy();
		}
	}	
	
	break;	
	
	//48.火藥岩
	case "GunPowder(Clone)":
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
			
			//get score
			Debug.Log("get GunPowder");
			score.AddScore(7);
		
			//爆炸範圍
			Instantiate (explosiveRange, this.gameObject.transform.position, Quaternion.identity);
			//爆炸
			Instantiate (bigbumb, this.gameObject.transform.position, Quaternion.identity);
			//爆炸音效
			Instantiate (sound_Bomb, this.gameObject.transform.position, Quaternion.identity);


			
			
			if(levelScript.gameStatus==5)//Play時
			{
				levelScript.GunPowder_Number++;//增加 GunPodwer								
			}

				
			SBP.SendEmptyAndDestroy();
		}
	}	
	break;	
		
	}
}
}//Collision END






function ProduceItem(ItemNumber:int){

//產生 道具包包物		
var ItemGot:GameObject=Instantiate (ItemGot, this.gameObject.transform.position, Quaternion.identity);	
var itemSc:AutoInPack=ItemGot.GetComponent(AutoInPack);	
				
itemSc.path="Altas/Item/";	

if(ItemNumber>0 && ItemNumber<10)
	itemSc.SpriteName="item00"+ItemNumber;
else
	itemSc.SpriteName="item0"+ItemNumber;	

}



function ProduceGemAndLove(ItemName:String){

//產生 道具包包物		
var ItemGot:GameObject=Instantiate (ItemGot, this.gameObject.transform.position, Quaternion.identity);	
var itemSc:AutoInPack=ItemGot.GetComponent(AutoInPack);	

itemSc.path="Picture/";
										
switch(ItemName)
{
case "Blue":
itemSc.SpriteName="gem_b";
break;
case "Green":
itemSc.SpriteName="gem_g";
break;
case "Red":
itemSc.SpriteName="gem_r";
break;
case "Silver":
itemSc.SpriteName="gem_w";
break;
case "Gold":
itemSc.SpriteName="gem_y";
break;

case "Bell":
itemSc.SpriteName="bell";
break;
case "Love":
itemSc.SpriteName="heart";
break;
case "Hourglass":
itemSc.SpriteName="hourglass";
break;


}						


}








//var iceTimer:float=0;
var iceRangle1:boolean=false;
var iceRangle2:boolean=false;


/*function insIceTime(){
iceTimer+=Time.deltaTime*1;
}*/


function IceSpread(){

if(this.gameObject.name=="Ice(Clone)"){
//insIceTime();
	/*if(iceTimer>5)
	{
		if(!iceRangle1)
		{	
			Instantiate (iceRange1, this.gameObject.transform.position, Quaternion.identity);
			iceRangle1=true;
			iceRangle2=false;
			iceTimer=0;
		}
		else if(iceRangle1 && iceRangle2==false) 
		{
			Instantiate (iceRange2, this.gameObject.transform.position, Quaternion.identity);
			iceRangle2=true;
			iceRangle1=false;
			iceTimer=0;
		}
	}*/
	
	
	Instantiate (iceRange1, this.gameObject.transform.position, Quaternion.identity);
	
}

}


function ThunderBallEffect_siimple(){
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
		
			//雷射
			Instantiate (laser, this.gameObject.transform.position, Quaternion.identity);
			
			
			if(levelScript.gameStatus==5)//Play時
			{
				levelScript.ThunderBall_Number++;//增加 閃電球								
			}
				
			SBP.SendEmptyAndDestroy();
		}
	}
}

function ThunderBallEffect(){
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
		
			//雷射
			Instantiate (laserCross, this.gameObject.transform.position, Quaternion.identity);
			
			
			if(levelScript.gameStatus==5)//Play時
			{
				levelScript.ThunderBall_Number++;//增加 閃電球								
			}
				
			SBP.SendEmptyAndDestroy();
		}
	}

}

function BumbEffect(){
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
		
			//爆炸範圍
			Instantiate (bumbRange, this.gameObject.transform.position, Quaternion.identity);
			//爆炸
			Instantiate (bigbumb, this.gameObject.transform.position, Quaternion.identity);
			//爆炸音效
			Instantiate (sound_Bomb, this.gameObject.transform.position, Quaternion.identity);
			
			
			
			if(levelScript.gameStatus==5)//Play時
			{
				levelScript.Bumb_Number++;//增加 Bumb								
			}
	
			SBP.SendEmptyAndDestroy();
		}
	}
}

function ToxicEffect(){
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
		
			//毒
			Instantiate (toxicRange, this.gameObject.transform.position, Quaternion.identity);
			
			
			
			if(levelScript.gameStatus==5)//Play時
			{
				levelScript.ToxicBall_Number++;//增加 BELL								
			}
			SBP.SendEmptyAndDestroy();
		}
	}
}

function GunpowderEffect(){
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
		
			//爆炸範圍
			Instantiate (explosiveRange, this.gameObject.transform.position, Quaternion.identity);
			//爆炸
			Instantiate (bigbumb, this.gameObject.transform.position, Quaternion.identity);

			
			
			if(levelScript.gameStatus==5)//Play時
			{
//				print(this.gameObject.name);
				NumberAdd(this.gameObject.name);
				levelScript.GunPowder_Number++;//增加 GunPodwer								
			}

				
			SBP.SendEmptyAndDestroy();
		}
	}
}

function NumberAdd(ObName:String){

switch(ObName){
case"Bell(Clone)":
levelScript.Bell_Number++;
break;
case"Black(Clone)":
levelScript.Black_Number++;
break;
case"Bumb(Clone)":
levelScript.Bumb_Number++;
break;
case"Fan_L(Clone)":
levelScript.FanL_Number++;
break;
case"Fan_R(Clone)":
levelScript.FanR_Number++;
break;
case"Green(Clone)":
levelScript.Green_Number++;
break;
case"Green_Bell(Clone)":
levelScript.Green_Number++;
levelScript.Bell_Number++;
break;
case"Green_Gold(Clone)":
levelScript.Green_Number++;
levelScript.GoldGem_Number++;
break;
case"Green_Green(Clone)":
levelScript.Green_Number++;
levelScript.GreenGem_Number++;
break;
case"Green_Red(Clone)":
levelScript.Green_Number++;
levelScript.RedGem_Number++;
break;
case"Green_Silver(Clone)":
levelScript.Green_Number++;
levelScript.SilverGem_Number++;
break;
case"GunPowder(Clone)":
levelScript.GunPowder_Number++;
break;
case"Hourglass(Clone)":
levelScript.Hourglass_Number++;
break;
case"Ice(Clone)":
levelScript.Ice_Number++;
break;
case"Love(Clone)":
levelScript.Love_Number++;
break;
case"Red(Clone)":
levelScript.Red_Number++;
break;
case"Red_Bell(Clone)":
levelScript.Red_Number++;
levelScript.Bell_Number++;
break;
case"Red_Blue(Clone)":
levelScript.Red_Number++;
levelScript.BlueGem_Number++;
break;
case"Red_Gold(Clone)":
levelScript.Red_Number++;
levelScript.GoldGem_Number++;
break;
case"Red_Green(Clone)":
levelScript.Red_Number++;
levelScript.GreenGem_Number++;
break;
case"Red_Red(Clone)":
levelScript.Red_Number++;
levelScript.RedGem_Number++;
break;
case"Red_Silver(Clone)":
levelScript.Red_Number++;
levelScript.SilverGem_Number++;
break;
case"Rock(Clone)":
levelScript.Rock_Number++;
break;
case"Rock_Bell(Clone)":
levelScript.Rock_Number++;
levelScript.Bell_Number++;
break;
case"Rock_Blue(Clone)":
levelScript.Rock_Number++;
levelScript.BlueGem_Number++;
break;
case"Rock_Gold(Clone)":
levelScript.Rock_Number++;
levelScript.GoldGem_Number++;
break;
case"Rock_Green(Clone)":
levelScript.Rock_Number++;
levelScript.GreenGem_Number++;
break;
case"Rock_Red(Clone)":
levelScript.Rock_Number++;
levelScript.RedGem_Number++;
break;
case"Rock_Silver(Clone)":
levelScript.Rock_Number++;
levelScript.SilverGem_Number++;
break;
case"Sand(Clone)":
levelScript.Sand_Number++;
break;
case"Sand_Bell(Clone)":
levelScript.Sand_Number++;
levelScript.Bell_Number++;
break;
case"Sand_Blue(Clone)":
levelScript.Sand_Number++;
levelScript.BlueGem_Number++;
break;
case"Sand_Gold(Clone)":
levelScript.Sand_Number++;
levelScript.GoldGem_Number++;
break;
case"Sand_Green(Clone)":
levelScript.Sand_Number++;
levelScript.GreenGem_Number++;
break;
case"Sand_Red(Clone)":
levelScript.Sand_Number++;
levelScript.RedGem_Number++;
break;
case"Sand_Silver(Clone)":
levelScript.Sand_Number++;
levelScript.SilverGem_Number++;
break;
//2014.08.20_14:42 炸彈和雷射把砂塊和砂塊內的寶石炸掉後，會(Game內)任務回報
case"Soil(Clone)":
levelScript.Soil_Number++;
break;
case"Soil_Bell(Clone)":
levelScript.Soil_Number++;
levelScript.Bell_Number++;
break;
case"Soil_Blue(Clone)":
levelScript.Soil_Number++;
levelScript.BlueGem_Number++;
break;
case"Soil_Gold(Clone)":
levelScript.Soil_Number++;
levelScript.GoldGem_Number++;
break;
case"Soil_Green(Clone)":
levelScript.Soil_Number++;
levelScript.GreenGem_Number++;
break;
case"Soil_Red(Clone)":
levelScript.Soil_Number++;
levelScript.RedGem_Number++;
break;
case"Soil_Silver(Clone)":
levelScript.Soil_Number++;
levelScript.SilverGem_Number++;
break;
///////
case"Star1(Clone)":
levelScript.Star1_Number++;
break;
case"Star2(Clone)":
levelScript.Star2_Number++;
break;
case"Star3(Clone)":
levelScript.Star3_Number++;
break;
case"ThunderBall(Clone)":
levelScript.ThunderBall_Number++;
break;
case"ToxicBall(Clone)":
levelScript.ToxicBall_Number++;
break;
case"Treasure(Clone)":
levelScript.Treasure_Number++;
break;
case"Valcon(Clone)":
levelScript.Valcon_Number++;
break;
case"Valcon_Bell(Clone)":
levelScript.Valcon_Number++;
levelScript.Bell_Number++;
break;
//27.火山岩 藍寶石 2014.09.11_18:49補上藍寶石被火藥岩炸掉後有紀錄	
case "Valcon_Blue(Clone)":
levelScript.Valcon_Number++;
levelScript.BlueGem_Number++;
break;	
case"Valcon_Gold(Clone)":
levelScript.Valcon_Number++;
levelScript.GoldGem_Number++;
break;
case"Valcon_Green(Clone)":
levelScript.Valcon_Number++;
levelScript.GreenGem_Number++;
break;
case"Valcon_Red(Clone)":
levelScript.Valcon_Number++;
levelScript.RedGem_Number++;
break;
case"Valcon_Silver(Clone)":
levelScript.Valcon_Number++;
levelScript.SilverGem_Number++;
break;


}



}





var crackEffect:boolean=false;

var addExplose:boolean=false;
var chageGreen:boolean=false;

//模擬吃到 星星1 的狀況
function OnTriggerEnter2D(other: Collider2D) {

if(GameObject.FindGameObjectWithTag("Player") != null) {
var myTag:String=this.gameObject.tag;
var name:String=this.gameObject.name;//本物件名
var otherNmae:String=other.gameObject.name;
var otherTag:String=other.gameObject.tag;

//觸碰 火藥岩 範圍
if(otherTag=="GunpodwerRange")
{
	//閃電球 十字爆炸 SP
	if(myTag=="ThunderBall")//閃電球
	{
	 	ThunderBallEffect();
	}
	else if(myTag=="ToxicBall")//毒液球
	{
		ToxicEffect();
	}
	else if(myTag=="Bumb")
	{
		BumbEffect();
	}
	else if(myTag=="Gunpodwer")
	{
		GunpowderEffect();
	}
	else{
		if(!got)
		{
		got=true;
		health=0;
		if(!addExplose)
		{
		Instantiate (explose, this.gameObject.transform.position, Quaternion.identity);

		addExplose=true;
		}
		//yield WaitForSeconds(.5);
		NumberAdd(name);//增加數量
		
		SBP.SendEmptyAndDestroy();
		}
	}



}

	//商城愛心
	if(name == "Mall_Love" && otherNmae == "dragon" && !got /*&& levelScript.gameStatus == 3*/)
	{
		got=true;
		score.AddScore(17);
				
		levelScript.dragonLife = levelScript.dragonLife + 3;
		playerScript.life = playerScript.life + 3;
		
		health--;
		
		if (health <= 0) 
		{
		
			levelScript.Love_Number+=3;
			
		//閃亮
		Instantiate (shiny, this.gameObject.transform.position, Quaternion.identity);
		Instantiate(Resources.Load("Game_Mall_Items/Mall_Number"), this.gameObject.transform.position, Quaternion.identity);
		
		
		//anim.SetInteger("status", -1);
		
		
		//發出空格訊息
		//levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
		
		Destroy(Mall_Love_Father.gameObject);
		
		}
	}
	
	//商城沙漏
	if(name == "Mall_Hourglass" && otherNmae == "dragon" && !got)
	{
		got=true;
		
		//get score
		Debug.Log("get Hourglass");
		score.AddScore(17);
		
		levelScript.isMall_Hourglass = true;
		health--;
		
		if (health <= 0) 
		{
		
			levelScript.Hourglass_Number++;
		
		//閃亮		
		Instantiate (shiny, this.gameObject.transform.position, Quaternion.identity);
		Instantiate(Resources.Load("Game_Mall_Items/Mall_Number2"), this.gameObject.transform.position, Quaternion.identity);
		
		
		Destroy(this.gameObject);
		}
	}	
	
	//商城星星1
	
	if(name == "Mall_Star1" && otherNmae == "dragon" && !got)
	{
		got=true;
		
		//get score
//		Debug.Log("get Star1");
		score.AddScore(18);
		
		//frameground play
		frameground.isPlay = true;
		playerScript.GoMall_Star1 = true;
		
		//刪除所有分身
		DeleteAllCopyDragons();
		
		health--;
		
		if (health <= 0) 
		{
		
		//playerScript.star1Timer+=10;
		
		if(levelScript.gameStatus==5)
			levelScript.Star1_Number++;

		//聲音 SUPER
		Instantiate (sound_SUPER, this.gameObject.transform.position, Quaternion.identity);
		//閃亮
		Instantiate (shiny, this.gameObject.transform.position, Quaternion.identity);
		
		
		Destroy(Mall_Star1_Father.gameObject);
		}
	}	
	
	//商城炸彈
if(otherTag=="Mall_BigBomb")
{
	
	/*if(myTag=="Gunpodwer")
	{
		GunpowderEffect();
	}	*/
	if(myTag=="Green")//綠塊 產生二次爆炸範圍
	{
		if(!got)
		{
		got=true;
		
		health=0;
		if(!addExplose)
		{
		//炸彈 範圍2
		Instantiate (bumbRangeEx, this.gameObject.transform.position, Quaternion.identity);
		//爆炸
		Instantiate (explose, this.gameObject.transform.position, Quaternion.identity);

		addExplose=true;
		}
			
		if(levelScript.gameStatus==5)//Play時
		{
			levelScript.Green_Number++;//增加 綠塊								
		}
		
		yield WaitForSeconds(.5);
		NumberAdd(name);//增加數量
		//發出空格訊息
		levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
		
		Destroy(this.gameObject);
		
		}
	}
	else //普通方塊
	{	
		if(myTag=="Bell" || myTag=="Fan"||
		myTag=="Hourglass" ||  myTag=="Love" || 
		 myTag=="Treasure")
		{
			//Do Nothing
		}
		else{
			/*if(!got)
			{
				got=true;
				health=0;
				if(!addExplose)
				{
				//爆炸
					Instantiate (explose, this.gameObject.transform.position, Quaternion.identity);
				

				addExplose=true;
				}
			
				yield WaitForSeconds(.5);
				NumberAdd(name);//增加數量
				//發出空格訊息
				levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
				Destroy(this.gameObject);*/
				
		switch(name)
	{
	
	case "Green(Clone)":
		Green_Effect();
	break;
	
	case "Green_Blue(Clone)":
		Green_Blue_Effect();
	break;
	
	case "Green_Green(Clone)":
		Green_Green_Effect();
	break;
	
	case "Green_Red(Clone)":
		Green_Red_Effect();
	break;
	
	case "Green_Silver(Clone)":
		Green_Silver_Effect();	
	break;
	
	case "Green_Gold(Clone)":
		Green_Gold_Effect();
	break;
	
	case "Green_Bell(Clone)":
		Green_Bell_Effect();
	break;
	//NEW============================================
	//1.沙塊
	case "Sand(Clone)":
		Sand_Effect();
	break;	
	//2.土塊
	case "Soil(Clone)":
		Soil_Effect();
	break;
	
	//3.石塊
	case "Rock(Clone)":
		Rock_Effect();
	break;	
	
	//4.紅塊
	case "Red(Clone)":
		Red_Effect();
	break;
	
	//5.火山岩
	case "Valcon(Clone)":
		Valcon_Effect();
	break;	
	
	//6.黑塊(無法擊破)
	case "Black(Clone)":
	break;
	
	//沙塊===============================================================
	//7.沙塊 藍寶石
	case "Sand_Blue(Clone)":
		Sand_Blue_Effect();
	break;
	
	//8.沙塊 綠寶石
	case "Sand_Green(Clone)":
		Sand_Green_Effect();
	break;	
	
	//9.沙塊 紅寶石
	case "Sand_Red(Clone)":
		Sand_Red_Effect();
	break;	
	
	//10.沙塊 銀寶石
	case "Sand_Silver(Clone)":
		Sand_Silver_Effect();
	break;	
	
	//11.沙塊 金寶石
	case "Sand_Gold(Clone)":
		Sand_Gold_Effect();
	break;	
	
	//土塊===============================================================
	//12.土塊 藍寶石
	case "Soil_Blue(Clone)":
		Soil_Blue_Effect();
	break;		
	
	//13.土塊 綠寶石
	case "Soil_Green(Clone)":
		Soil_Green_Effect();
	break;	
	
	//14.土塊 紅寶石
	case "Soil_Red(Clone)":
		Soil_Red_Effect();
	break;	
	
	//15.土塊 銀寶石
	case "Soil_Silver(Clone)":
		Soil_Silver_Effect();
	break;	
		
	//16.土塊 金寶石
	case "Soil_Gold(Clone)":
		Soil_Gold_Effect();
	break;	
	
	//石塊===============================================================
	//17.石塊 藍寶石
	case "Rock_Blue(Clone)":
		Rock_Blue_Effect();
	break;		
	//18.石塊 綠寶石
	case "Rock_Green(Clone)":
		Rock_Green_Effect();
	break;		
	//19.石塊 紅寶石
	case "Rock_Red(Clone)":
		Rock_Red_Effect();
	break;		
	//20.石塊 銀寶石
	case "Rock_Silver(Clone)":
		Rock_Silver_Effect();
	break;		
	//21.石塊 金寶石
	case "Rock_Gold(Clone)":
		Rock_Gold_Effect();
	break;		
	
	//紅塊===============================================================	
	//22.紅塊 藍寶石	
	case "Red_Blue(Clone)":
		Red_Blue_Effect();
	break;	
	//23.紅塊 綠寶石	
	case "Red_Green(Clone)":
		Red_Green_Effect();
	break;		
	//24.紅塊 紅寶石
	case "Red_Red(Clone)":
		Red_Red_Effect();
	break;
	//25.紅塊 銀寶石
	case "Red_Silver(Clone)":
		Red_Silver_Effect();
	break;	
	//26.紅塊 金寶石
	case "Red_Gold(Clone)":
		Red_Gold_Effect();
	break;	
	//火山岩===============================================================	
	//27.火山岩 藍寶石	
	case "Valcon_Blue(Clone)":
		Valcon_Blue_Effect();
	break;		
	//28.火山岩 綠寶石	
	case "Valcon_Green(Clone)":
		Valcon_Green_Effect();
	break;		
	//29.火山岩 紅寶石	
	case "Valcon_Red(Clone)":
		Valcon_Red_Effect();
	break;		
	//30.火山岩 銀寶石	
	case "Valcon_Silver(Clone)":
		Valcon_Silver_Effect();
	break;		
	//31.火山岩 金寶石	
	case "Valcon_Gold(Clone)":
		Valcon_Gold_Effect();
	break;	
	

			}

		}
	}
}
	
		
				

//觸碰 冰 範圍
if(otherTag=="IceRange" && name!="Ice(Clone)")//只有冰以外的東西才會結冰
{
	if(child_cover && anim_child_cover)
	{
	child_cover.GetComponent(BoxCollider2D).enabled=true;
	anim_child_cover.SetInteger("status", 1);
	}
}
//觸碰 冰的冰範圍 2014.07.31_16:18
if(otherTag=="IceRange2" && name!="Ice(Clone)")//只有冰以外的東西才會結冰
{
	if(child_cover && anim_child_cover)
	{
	child_cover.GetComponent(BoxCollider2D).enabled=true;
	anim_child_cover.SetInteger("status", 1);
	}
}

if(dragonScript.status == 2 || dragonScript.status == 6 || dragonScript.status == 7 || dragonScript.status == 8 ){

if(((other.gameObject.name=="dragon" || other.gameObject.name=="dragon(Clone)") && !crackEffect) && (this.gameObject.name != "Mall_Love(Clone)" &&
 this.gameObject.name != "Treasure(Clone)" &&this.gameObject.name != "ToxicBall(Clone)" &&this.gameObject.name != "ThunderBall(Clone)" &&
 this.gameObject.name != "Star3(Clone)" &&this.gameObject.name != "Star2(Clone)" &&this.gameObject.name != "Star1(Clone)" &&
 this.gameObject.name != "Love(Clone)" && this.gameObject.name != "Ice(Clone)" && this.gameObject.name != "Hourglass(Clone)" && 
 this.gameObject.name != "Bumb(Clone)" && this.gameObject.name != "Bell(Clone)" && this.gameObject.name != "Black(Clone)" && 
 this.gameObject.name != "Fan_R(Clone)" && this.gameObject.name != "Fan_L(Clone)")){
RC.randCandy();
}



//觸碰 集氣攻擊 範圍
if(otherTag=="PowerCrackRange" && !crackEffect) 
{

//crackEffect=true;

playerScript.comboNumber++;

if(PowerCrackIce)
{
yield WaitForSeconds (1.0);
}
else
{switch(name)
	{
	case "Green(Clone)":
		Green_Effect();
	break;
	
	case "Green_Blue(Clone)":
		Green_Blue_Effect();
	break;
	
	case "Green_Green(Clone)":
		Green_Green_Effect();
	break;
	
	case "Green_Red(Clone)":
		Green_Red_Effect();
	break;
	
	case "Green_Silver(Clone)":
		Green_Silver_Effect();	
	break;
	
	case "Green_Gold(Clone)":
		Green_Gold_Effect();
	break;
	
	case "Green_Bell(Clone)":
		Green_Bell_Effect();
	break;
	//NEW============================================
	//1.沙塊
	case "Sand(Clone)":
		Sand_Effect();
	break;	
	//2.土塊
	case "Soil(Clone)":
		Soil_Effect();
	break;
	
	//3.石塊
	case "Rock(Clone)":
		Rock_Effect();
	break;	
	
	//4.紅塊
	case "Red(Clone)":
		Red_Effect();
	break;
	
	//5.火山岩
	case "Valcon(Clone)":
		Valcon_Effect();
	break;	
	
	//6.黑塊(無法擊破)
	case "Black(Clone)":
	break;
	
	//沙塊===============================================================
	//7.沙塊 藍寶石
	case "Sand_Blue(Clone)":
		Sand_Blue_Effect();
	break;
	
	//8.沙塊 綠寶石
	case "Sand_Green(Clone)":
		Sand_Green_Effect();
	break;	
	
	//9.沙塊 紅寶石
	case "Sand_Red(Clone)":
		Sand_Red_Effect();
	break;	
	
	//10.沙塊 銀寶石
	case "Sand_Silver(Clone)":
		Sand_Silver_Effect();
	break;	
	
	//11.沙塊 金寶石
	case "Sand_Gold(Clone)":
		Sand_Gold_Effect();
	break;	
	
	//土塊===============================================================
	//12.土塊 藍寶石
	case "Soil_Blue(Clone)":
		Soil_Blue_Effect();
	break;		
	
	//13.土塊 綠寶石
	case "Soil_Green(Clone)":
		Soil_Green_Effect();
	break;	
	
	//14.土塊 紅寶石
	case "Soil_Red(Clone)":
		Soil_Red_Effect();
	break;	
	
	//15.土塊 銀寶石
	case "Soil_Silver(Clone)":
		Soil_Silver_Effect();
	break;	
		
	//16.土塊 金寶石
	case "Soil_Gold(Clone)":
		Soil_Gold_Effect();
	break;	
	
	//石塊===============================================================
	//17.石塊 藍寶石
	case "Rock_Blue(Clone)":
		Rock_Blue_Effect();
	break;		
	//18.石塊 綠寶石
	case "Rock_Green(Clone)":
		Rock_Green_Effect();
	break;		
	//19.石塊 紅寶石
	case "Rock_Red(Clone)":
		Rock_Red_Effect();
	break;		
	//20.石塊 銀寶石
	case "Rock_Silver(Clone)":
		Rock_Silver_Effect();
	break;		
	//21.石塊 金寶石
	case "Rock_Gold(Clone)":
		Rock_Gold_Effect();
	break;		
	
	//紅塊===============================================================	
	//22.紅塊 藍寶石	
	case "Red_Blue(Clone)":
		Red_Blue_Effect();
	break;	
	//23.紅塊 綠寶石	
	case "Red_Green(Clone)":
		Red_Green_Effect();
	break;		
	//24.紅塊 紅寶石
	case "Red_Red(Clone)":
		Red_Red_Effect();
	break;
	//25.紅塊 銀寶石
	case "Red_Silver(Clone)":
		Red_Silver_Effect();
	break;	
	//26.紅塊 金寶石
	case "Red_Gold(Clone)":
		Red_Gold_Effect();
	break;	
	//火山岩===============================================================	
	//27.火山岩 藍寶石	
	case "Valcon_Blue(Clone)":
		Valcon_Blue_Effect();
	break;		
	//28.火山岩 綠寶石	
	case "Valcon_Green(Clone)":
		Valcon_Green_Effect();
	break;		
	//29.火山岩 紅寶石	
	case "Valcon_Red(Clone)":
		Valcon_Red_Effect();
	break;		
	//30.火山岩 銀寶石	
	case "Valcon_Silver(Clone)":
		Valcon_Silver_Effect();
	break;		
	//31.火山岩 金寶石	
	case "Valcon_Gold(Clone)":
		Valcon_Gold_Effect();
	break;	
	
}}

}


//觸碰 雷射 範圍
if(otherTag=="Laser") 
{
	if(myTag=="Bell" || myTag=="Fan"||
	myTag=="Hourglass" ||  myTag=="Love" || 
	 myTag=="Treasure")
	{
		//Do Nothing
	}
	else if(myTag=="ThunderBall")
	{
		ThunderBallEffect_siimple();
	}		
	else if(myTag=="Gunpodwer")
	{
		GunpowderEffect();
	}	
	else if(myTag=="ToxicBall")
	{
		ToxicEffect();
	}
	else if(myTag=="Bumb")
	{
		BumbEffect();
	}
	else//Normal Brick
	{
		/*if(!got)
		{
		got=true;*/
	
	health = 1;	
		switch(name)
	{
	
	case "Green(Clone)":
		Green_Effect();
	break;
	
	case "Green_Blue(Clone)":
		Green_Blue_Effect();
	break;
	
	case "Green_Green(Clone)":
		Green_Green_Effect();
	break;
	
	case "Green_Red(Clone)":
		Green_Red_Effect();
	break;
	
	case "Green_Silver(Clone)":
		Green_Silver_Effect();	
	break;
	
	case "Green_Gold(Clone)":
		Green_Gold_Effect();
	break;
	
	case "Green_Bell(Clone)":
		Green_Bell_Effect();
	break;
	//NEW============================================
	//1.沙塊
	case "Sand(Clone)":
		Sand_Effect();
	break;	
	//2.土塊
	case "Soil(Clone)":
		Soil_Effect();
	break;
	
	//3.石塊
	case "Rock(Clone)":
		Rock_Effect();
	break;	
	
	//4.紅塊
	case "Red(Clone)":
		Red_Effect();
	break;
	
	//5.火山岩
	case "Valcon(Clone)":
		Valcon_Effect();
	break;	
	
	//6.黑塊(無法擊破)
	case "Black(Clone)":
		Black_Effect();
	break;
	
	//沙塊===============================================================
	//7.沙塊 藍寶石
	case "Sand_Blue(Clone)":
		Sand_Blue_Effect();
	break;
	
	//8.沙塊 綠寶石
	case "Sand_Green(Clone)":
		Sand_Green_Effect();
	break;	
	
	//9.沙塊 紅寶石
	case "Sand_Red(Clone)":
		Sand_Red_Effect();
	break;	
	
	//10.沙塊 銀寶石
	case "Sand_Silver(Clone)":
		Sand_Silver_Effect();
	break;	
	
	//11.沙塊 金寶石
	case "Sand_Gold(Clone)":
		Sand_Gold_Effect();
	break;	
	
	//土塊===============================================================
	//12.土塊 藍寶石
	case "Soil_Blue(Clone)":
		Soil_Blue_Effect();
	break;		
	
	//13.土塊 綠寶石
	case "Soil_Green(Clone)":
		Soil_Green_Effect();
	break;	
	
	//14.土塊 紅寶石
	case "Soil_Red(Clone)":
		Soil_Red_Effect();
	break;	
	
	//15.土塊 銀寶石
	case "Soil_Silver(Clone)":
		Soil_Silver_Effect();
	break;	
		
	//16.土塊 金寶石
	case "Soil_Gold(Clone)":
		Soil_Gold_Effect();
	break;	
	
	//石塊===============================================================
	//17.石塊 藍寶石
	case "Rock_Blue(Clone)":
		Rock_Blue_Effect();
	break;		
	//18.石塊 綠寶石
	case "Rock_Green(Clone)":
		Rock_Green_Effect();
	break;		
	//19.石塊 紅寶石
	case "Rock_Red(Clone)":
		Rock_Red_Effect();
	break;		
	//20.石塊 銀寶石
	case "Rock_Silver(Clone)":
		Rock_Silver_Effect();
	break;		
	//21.石塊 金寶石
	case "Rock_Gold(Clone)":
		Rock_Gold_Effect();
	break;		
	
	//紅塊===============================================================	
	//22.紅塊 藍寶石	
	case "Red_Blue(Clone)":
		Red_Blue_Effect();
	break;	
	//23.紅塊 綠寶石	
	case "Red_Green(Clone)":
		Red_Green_Effect();
	break;		
	//24.紅塊 紅寶石
	case "Red_Red(Clone)":
		Red_Red_Effect();
	break;
	//25.紅塊 銀寶石
	case "Red_Silver(Clone)":
		Red_Silver_Effect();
	break;	
	//26.紅塊 金寶石
	case "Red_Gold(Clone)":
		Red_Gold_Effect();
	break;	
	//火山岩===============================================================	
	//27.火山岩 藍寶石	
	case "Valcon_Blue(Clone)":
		Valcon_Blue_Effect();
	break;		
	//28.火山岩 綠寶石	
	case "Valcon_Green(Clone)":
		Valcon_Green_Effect();
	break;		
	//29.火山岩 紅寶石	
	case "Valcon_Red(Clone)":
		Valcon_Red_Effect();
	break;		
	//30.火山岩 銀寶石	
	case "Valcon_Silver(Clone)":
		Valcon_Silver_Effect();
	break;		
	//31.火山岩 金寶石	
	case "Valcon_Gold(Clone)":
		Valcon_Gold_Effect();
	break;	
	
}
		//health=0;
		//anim.SetInteger("status", -1);//白煙	
		/*NumberAdd(name);//增加數量
		yield WaitForSeconds(.2);
		
		//發出空格訊息
		levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
		Destroy(this.gameObject);*/
		}
	}
	




//觸碰 炸藥1 範圍
if(otherTag=="BumbRange")
{
	//閃電球 十字爆炸 SP
	if(myTag=="ThunderBall")//閃電球
	{
	 	ThunderBallEffect();
	}
	else if(myTag=="ToxicBall")//毒液球
	{
		ToxicEffect();
	}
	else if(myTag=="Bumb")
	{
		BumbEffect();
	}
	else if(myTag=="Gunpodwer")
	{
		GunpowderEffect();
	}	
	else if(myTag=="Green")//綠塊 產生二次爆炸範圍
	{
		if(!got)
		{
		got=true;
		
		health=0;
		if(!addExplose)
		{
		//炸彈 範圍2
		Instantiate (bumbRangeEx, this.gameObject.transform.position, Quaternion.identity);
		//爆炸
		Instantiate (explose, this.gameObject.transform.position, Quaternion.identity);

		addExplose=true;
		}
			
		if(levelScript.gameStatus==5)//Play時
		{
			levelScript.Green_Number++;//增加 綠塊								
		}
		
		yield WaitForSeconds(.5);
		NumberAdd(name);//增加數量
		//發出空格訊息
		levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
		
		Destroy(this.gameObject);
		
		}
	}
	else //普通方塊
	{	
		if(myTag=="Bell" || myTag=="Fan"||
		myTag=="Hourglass" ||  myTag=="Love" || 
		 myTag=="Treasure")
		{
			//Do Nothing
		}
		else{
			/*if(!got)
			{
				got=true;
				health=0;
				if(!addExplose)
				{
				//爆炸
					Instantiate (explose, this.gameObject.transform.position, Quaternion.identity);
				

				addExplose=true;
				}
			
				yield WaitForSeconds(.5);
				NumberAdd(name);//增加數量
				//發出空格訊息
				levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
				Destroy(this.gameObject);*/
				
		switch(name)
	{
	
	case "Green(Clone)":
		Green_Effect();
	break;
	
	case "Green_Blue(Clone)":
		Green_Blue_Effect();
	break;
	
	case "Green_Green(Clone)":
		Green_Green_Effect();
	break;
	
	case "Green_Red(Clone)":
		Green_Red_Effect();
	break;
	
	case "Green_Silver(Clone)":
		Green_Silver_Effect();	
	break;
	
	case "Green_Gold(Clone)":
		Green_Gold_Effect();
	break;
	
	case "Green_Bell(Clone)":
		Green_Bell_Effect();
	break;
	//NEW============================================
	//1.沙塊
	case "Sand(Clone)":
		Sand_Effect();
	break;	
	//2.土塊
	case "Soil(Clone)":
		Soil_Effect();
	break;
	
	//3.石塊
	case "Rock(Clone)":
		Rock_Effect();
	break;	
	
	//4.紅塊
	case "Red(Clone)":
		Red_Effect();
	break;
	
	//5.火山岩
	case "Valcon(Clone)":
		Valcon_Effect();
	break;	
	
	//6.黑塊(無法擊破)
	case "Black(Clone)":
	break;
	
	//沙塊===============================================================
	//7.沙塊 藍寶石
	case "Sand_Blue(Clone)":
		Sand_Blue_Effect();
	break;
	
	//8.沙塊 綠寶石
	case "Sand_Green(Clone)":
		Sand_Green_Effect();
	break;	
	
	//9.沙塊 紅寶石
	case "Sand_Red(Clone)":
		Sand_Red_Effect();
	break;	
	
	//10.沙塊 銀寶石
	case "Sand_Silver(Clone)":
		Sand_Silver_Effect();
	break;	
	
	//11.沙塊 金寶石
	case "Sand_Gold(Clone)":
		Sand_Gold_Effect();
	break;	
	
	//土塊===============================================================
	//12.土塊 藍寶石
	case "Soil_Blue(Clone)":
		Soil_Blue_Effect();
	break;		
	
	//13.土塊 綠寶石
	case "Soil_Green(Clone)":
		Soil_Green_Effect();
	break;	
	
	//14.土塊 紅寶石
	case "Soil_Red(Clone)":
		Soil_Red_Effect();
	break;	
	
	//15.土塊 銀寶石
	case "Soil_Silver(Clone)":
		Soil_Silver_Effect();
	break;	
		
	//16.土塊 金寶石
	case "Soil_Gold(Clone)":
		Soil_Gold_Effect();
	break;	
	
	//石塊===============================================================
	//17.石塊 藍寶石
	case "Rock_Blue(Clone)":
		Rock_Blue_Effect();
	break;		
	//18.石塊 綠寶石
	case "Rock_Green(Clone)":
		Rock_Green_Effect();
	break;		
	//19.石塊 紅寶石
	case "Rock_Red(Clone)":
		Rock_Red_Effect();
	break;		
	//20.石塊 銀寶石
	case "Rock_Silver(Clone)":
		Rock_Silver_Effect();
	break;		
	//21.石塊 金寶石
	case "Rock_Gold(Clone)":
		Rock_Gold_Effect();
	break;		
	
	//紅塊===============================================================	
	//22.紅塊 藍寶石	
	case "Red_Blue(Clone)":
		Red_Blue_Effect();
	break;	
	//23.紅塊 綠寶石	
	case "Red_Green(Clone)":
		Red_Green_Effect();
	break;		
	//24.紅塊 紅寶石
	case "Red_Red(Clone)":
		Red_Red_Effect();
	break;
	//25.紅塊 銀寶石
	case "Red_Silver(Clone)":
		Red_Silver_Effect();
	break;	
	//26.紅塊 金寶石
	case "Red_Gold(Clone)":
		Red_Gold_Effect();
	break;	
	//火山岩===============================================================	
	//27.火山岩 藍寶石	
	case "Valcon_Blue(Clone)":
		Valcon_Blue_Effect();
	break;		
	//28.火山岩 綠寶石	
	case "Valcon_Green(Clone)":
		Valcon_Green_Effect();
	break;		
	//29.火山岩 紅寶石	
	case "Valcon_Red(Clone)":
		Valcon_Red_Effect();
	break;		
	//30.火山岩 銀寶石	
	case "Valcon_Silver(Clone)":
		Valcon_Silver_Effect();
	break;		
	//31.火山岩 金寶石	
	case "Valcon_Gold(Clone)":
		Valcon_Gold_Effect();
	break;	
	

			}

		}
	}
}
//觸碰 炸藥2 範圍
if(otherTag=="BumbRangeEx")
{
	if(myTag=="Green")//綠塊
	{
		if(!got)
		{
			got=true;
			health=0;
			if(!addExplose)
			{
			//炸彈 範圍2
			Instantiate (bumbRangeEx, this.gameObject.transform.position, Quaternion.identity);
			//爆炸
			Instantiate (explose, this.gameObject.transform.position, Quaternion.identity);
			addExplose=true;
			}
			yield WaitForSeconds(.5);
		
			NumberAdd(name);//增加數量
			//發出空格訊息
			levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
			Destroy(this.gameObject);
		}
	}
}




//觸碰 火藥岩 範圍
if(otherTag=="GunpodwerRange")
{
	//閃電球 十字爆炸 SP
	if(myTag=="ThunderBall")//閃電球
	{
	 	ThunderBallEffect();
	}
	else if(myTag=="ToxicBall")//毒液球
	{
		ToxicEffect();
	}
	else if(myTag=="Bumb")
	{
		BumbEffect();
	}
	else if(myTag=="Gunpodwer")
	{
		GunpowderEffect();
	}
	else{
		if(!got)
		{
		got=true;
		health=0;
		if(!addExplose)
		{
		Instantiate (explose, this.gameObject.transform.position, Quaternion.identity);

		addExplose=true;
		}
		yield WaitForSeconds(.5);
		NumberAdd(name);//增加數量
		
		SBP.SendEmptyAndDestroy();
		}
	}



}




//觸碰 腐蝕 範圍
if(otherTag=="ToxicRange" && !chageGreen  )
{

	chageGreen=true;	
		//綠塊
		if(
		name=="Sand(Clone)" ||
		name=="Soil(Clone)" ||
		name=="Rock(Clone)" ||
		name=="Red(Clone)" ||
		name=="Valcon(Clone)" ||
		name=="Black(Clone)" 
		){
			var green = Instantiate (green_none, this.gameObject.transform.position, Quaternion.identity);
			green.GetComponent(SetBrickPosition).XBrick = SBP.XBrick;
			green.GetComponent(SetBrickPosition).YBrick = SBP.YBrick;
			Destroy(this.gameObject);
		}
		else if(//綠塊 籃寶石
		name=="Sand_Blue(Clone)" ||
		name=="Soil_Blue(Clone)" ||
		name=="Rock_Blue(Clone)" ||
		name=="Red_Blue(Clone)" ||
		name=="Valcon_Blue(Clone)" )
		{
			Debug.Log("im here");
			var green_BlueGem = Instantiate (green_BlueGem, this.gameObject.transform.position, Quaternion.identity);
			green_BlueGem.GetComponent(SetBrickPosition).XBrick = SBP.XBrick;
			green_BlueGem.GetComponent(SetBrickPosition).YBrick = SBP.YBrick;
			Destroy(this.gameObject);
		}
		else if(//綠塊 綠寶石
			name=="Sand_Green(Clone)" ||
			name=="Soil_Green(Clone)" ||
			name=="Rock_Green(Clone)" ||
			name=="Red_Green(Clone)" ||
			name=="Valcon_Green(Clone)" )
		{
			var green_GreenGem = Instantiate (green_GreenGem, this.gameObject.transform.position, Quaternion.identity);	
			green_GreenGem.GetComponent(SetBrickPosition).XBrick = SBP.XBrick;
			green_GreenGem.GetComponent(SetBrickPosition).YBrick = SBP.YBrick;
			Destroy(this.gameObject);
		}
		else if(//綠塊 紅寶石
		name=="Sand_Red(Clone)" ||
		name=="Soil_Red(Clone)" ||
		name=="Rock_Red(Clone)" ||
		name=="Red_Red(Clone)" ||
		name=="Valcon_Red(Clone)" )
		{
			var green_RedGem = Instantiate (green_RedGem, this.gameObject.transform.position, Quaternion.identity);
			green_RedGem.GetComponent(SetBrickPosition).XBrick = SBP.XBrick;
			green_RedGem.GetComponent(SetBrickPosition).YBrick = SBP.YBrick;	
			Destroy(this.gameObject);
		}
		else if(//綠塊 銀寶石
		name=="Sand_Silver(Clone)" ||
		name=="Soil_Silver(Clone)" ||
		name=="Rock_Silver(Clone)" ||
		name=="Red_Silver(Clone)" ||
		name=="Valcon_Silver(Clone)" )
		{
			var green_SilverGem = Instantiate (green_SilverGem, this.gameObject.transform.position, Quaternion.identity);	
			green_SilverGem.GetComponent(SetBrickPosition).XBrick = SBP.XBrick;
			green_SilverGem.GetComponent(SetBrickPosition).YBrick = SBP.YBrick;
			Destroy(this.gameObject);
		}
		else if(//綠塊 金寶石
		name=="Sand_Gold(Clone)" ||
		name=="Soil_Gold(Clone)" ||
		name=="Rock_Gold(Clone)" ||
		name=="Red_Gold(Clone)" ||
		name=="Valcon_Gold(Clone)" )
		{
			var green_GoldGem = Instantiate (green_GoldGem, this.gameObject.transform.position, Quaternion.identity);
			green_GoldGem.GetComponent(SetBrickPosition).XBrick = SBP.XBrick;
			green_GoldGem.GetComponent(SetBrickPosition).YBrick = SBP.YBrick;
			Destroy(this.gameObject);	
		}
		else if(//綠塊 Bell
		name=="Sand_Bell(Clone)" ||
		name=="Soil_Bell(Clone)" ||
		name=="Rock_Bell(Clone)" ||
		name=="Red_Bell(Clone)" ||
		name=="Valcon_Bell(Clone)" )
		{
			var green_Bell = Instantiate (green_Bell, this.gameObject.transform.position, Quaternion.identity);
			green_Bell.GetComponent(SetBrickPosition).XBrick = SBP.XBrick;
			green_Bell.GetComponent(SetBrickPosition).YBrick = SBP.YBrick;	
			Destroy(this.gameObject);
		}
}




if(otherNmae=="dragon" || otherNmae=="dragon(Clone)" ){

switch(name)
{
	case "Ice(Clone)":
	health--;
	if (health <= 0) 
	{
		if(!got)
		{
			got=true;//只跑一次
		
			//get score
			//Debug.Log("get Ice");
			score.AddScore(6);

			
			
			if(levelScript.gameStatus==5)//Play時
			{
				levelScript.Ice_Number++;//增加 冰塊的過關條件							
			}

			//爆炸
			Instantiate (icebreak, this.gameObject.transform.position, Quaternion.identity);
			SBP.SendEmptyAndDestroy();
		}
	}	

//星星====================
	//32.星星1
	case "Star1(Clone)":
	if(!got && dragonScript.status != 6 && dragonScript.status != 7 && dragonScript.status != 8 && !dragonScript.isStar3)
	{
		got=true;
		
		//get score
//		Debug.Log("get Star1");
		score.AddScore(18);
		
		//frameground play
		frameground.isPlay = true;
		
		dragonScript.OnStar1();
		
		//刪除所有分身
		DeleteAllCopyDragons();
		
		health--;
		
		if (health <= 0) 
		{
		
		playerScript.star1Timer+=10;
		playerScript.isChangedMusic = false;
		
		if(levelScript.gameStatus==5)
			levelScript.Star1_Number++;

		//聲音 SUPER
		Instantiate (sound_SUPER, this.gameObject.transform.position, Quaternion.identity);
		//閃亮
		Instantiate (shiny, this.gameObject.transform.position, Quaternion.identity);
		
		anim.SetInteger("status", -1);
		
		yield WaitForSeconds(.2);
		
		SBP.SendEmptyAndDestroy();
		}
	}	
	break;	
	//33.星星2
	case "Star2(Clone)":
	if(!got && dragonScript.status != 6 && dragonScript.status != 7 && dragonScript.status != 8 && !dragonScript.isStar3)
	{
		got=true;
		
		//get score
//		Debug.Log("get Star2");
		score.AddScore(19);
		
		//frameground play
		frameground.isPlay = true;
		
		dragonScript.OnStar2();
		
		
		//刪除所有分身
		DeleteAllCopyDragons();

		health--;
		
		if (health <= 0) 
		{
		playerScript.isChangedMusic = false;
		playerScript.star2Timer+=10;//增加星星時間
		
		if(levelScript.gameStatus==5)
			levelScript.Star2_Number++;
			
		//聲音 SUPER
		Instantiate (sound_SUPER, this.gameObject.transform.position, Quaternion.identity);
		//閃亮
		Instantiate (shiny, this.gameObject.transform.position, Quaternion.identity);
		
		anim.SetInteger("status", -1);
		
		yield WaitForSeconds(.2);
		
		SBP.SendEmptyAndDestroy();
		}
	}	
	break;		
	
	//34.星星3
	case "Star3(Clone)":
	if(!got && dragonScript.status != 6 && dragonScript.status != 7 && dragonScript.status != 8 && !dragonScript.isStar3)
	{
		got=true;
		playerScript.GoMall_Star1 = false;
		//get score
		Debug.Log("get Star3");
		score.AddScore(20);
		
		//frameground play
		frameground.isPlay = true;
		
		health--;
		playerScript.avatarbool = true;
		playerScript.isChangedMusic = false;
		playerScript.star3Timer+=10;
		dragonScript.status = 2;
		dragonScript.isStar3 = true;
		playerScript.CloseStar1And2();
		
		if (health <= 0) 
		{

		if(levelScript.gameStatus==5)
			levelScript.Star3_Number++;

		//刪除所有分身
		DeleteAllCopyDragons();
				
		//SUPER聲音
		Instantiate (sound_SUPER, this.gameObject.transform.position, Quaternion.identity);
		
		
		
		
		//Copy 1
		var ob1:GameObject=Instantiate (dragonOb, GameObject.FindGameObjectWithTag("Player").gameObject.transform.position, Quaternion.identity);
		ob1.transform.GetComponent(Dragon).OnStar3();
		ob1.GetComponent(PlayerSC).copy=true;
		ob1.transform.rigidbody2D.AddForce(Vector2(-450,-450));
		ob1.transform.GetComponent(PlayerSC).star3Timer+=10;
		//Copy 2
		var ob2:GameObject=Instantiate (dragonOb, GameObject.FindGameObjectWithTag("Player").gameObject.transform.position, Quaternion.identity);
		ob2.transform.GetComponent(Dragon).OnStar3();		
		ob2.GetComponent(PlayerSC).copy=true;
		ob2.transform.rigidbody2D.AddForce(Vector2(450,450));
		ob2.transform.GetComponent(PlayerSC).star3Timer+=10;
		
		anim.SetInteger("status", -1);
		yield WaitForSeconds(.2);
		SBP.SendEmptyAndDestroy();
		}
	}	
	break;	

//Sand====================
case "Sand(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
//		Debug.Log("get Sand");
		score.AddScore(0);
		
		if(levelScript.gameStatus==5)
			levelScript.Sand_Number++;
				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//黃煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Sand_Bell(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Sand_Bell");
		score.AddScore(0);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Sand_Number++;
			levelScript.Bell_Number++;
		}
		//產生物件
		ProduceGemAndLove("Bell");			
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//黃煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Sand_Blue(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
//		Debug.Log("get Sand_Blue");
		score.AddScore(0);
		score.AddScore(10);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Sand_Number++;
			levelScript.BlueGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Blue");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//黃煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Sand_Green(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Sand_Green");
		score.AddScore(0);
		score.AddScore(11);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Sand_Number++;
			levelScript.GreenGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Green");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//黃煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Sand_Red(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Sand_Red");
		score.AddScore(0);
		score.AddScore(9);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Sand_Number++;
			levelScript.RedGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Red");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//黃煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Sand_Silver(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Sand_Silver");
		score.AddScore(0);
		score.AddScore(12);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Sand_Number++;
			levelScript.SilverGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Silver");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//黃煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Sand_Gold(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Sand_Gold");
		score.AddScore(0);
		score.AddScore(13);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Sand_Number++;
			levelScript.GoldGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Gold");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//黃煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
//Soil====================
case "Soil(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Soil");
		score.AddScore(1);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Soil_Number++;
		}
				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//黃煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Soil_Bell(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Soil_Bell");
		score.AddScore(1);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Soil_Number++;
			levelScript.Bell_Number++;
		}
		//產生物件
		ProduceGemAndLove("Bell");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//黃煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Soil_Blue(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Soil_Blue");
		score.AddScore(1);
		score.AddScore(10);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Soil_Number++;
			levelScript.BlueGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Blue");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//黃煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Soil_Green(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Soil_Green");
		score.AddScore(1);
		score.AddScore(11);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Soil_Number++;
			levelScript.GreenGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Green");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//黃煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Soil_Red(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Soil_Red");
		score.AddScore(1);
		score.AddScore(9);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Soil_Number++;
			levelScript.RedGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Red");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//黃煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Soil_Silver(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Soil_Silver");
		score.AddScore(1);
		score.AddScore(12);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Soil_Number++;
			levelScript.SilverGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Silver");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//黃煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Soil_Gold(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Soil_Gold");
		score.AddScore(1);
		score.AddScore(13);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Soil_Number++;
			levelScript.GoldGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Gold");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//黃煙
		Instantiate (smoke2, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
//Rock====================
case "Rock(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
//		Debug.Log("get Rock");
		score.AddScore(2);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Rock_Number++;
		}
				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Rock_Bell(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Rock_Bell");
		score.AddScore(2);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Rock_Number++;
			levelScript.Bell_Number++;
		}
		//產生物件
		ProduceGemAndLove("Bell");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Rock_Blue(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Rock_Blue");
		score.AddScore(2);
		score.AddScore(10);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Rock_Number++;
			levelScript.BlueGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Blue");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Rock_Green(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Rock_Green");
		score.AddScore(2);
		score.AddScore(11);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Rock_Number++;
			levelScript.GreenGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Green");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Rock_Red(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Rock_Red");
		score.AddScore(2);
		score.AddScore(9);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Rock_Number++;
			levelScript.RedGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Red");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Rock_Silver(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Rock_Silver");
		score.AddScore(2);
		score.AddScore(12);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Rock_Number++;
			levelScript.SilverGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Silver");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Rock_Gold(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Rock_Gold");
		score.AddScore(2);
		score.AddScore(13);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Rock_Number++;
			levelScript.GoldGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Gold");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
//Red====================
case "Red(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Red");
		score.AddScore(3);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Red_Number++;
//			levelScript.GoldGem_Number++;
		}
		//產生物件
		//ProduceGemAndLove("Red");		2014.08.20_13:30 小恐龍吃到超級星星撞到Red(Clone)紅塊會出現紅寶石圖到背包		
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Red_Bell(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Red_Bell");
		score.AddScore(3);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Red_Number++;
			levelScript.Bell_Number++;
		}
		//產生物件
		ProduceGemAndLove("Bell");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Red_Blue(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Red_Blue");
		score.AddScore(3);
		score.AddScore(10);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Red_Number++;
			levelScript.BlueGem_Number++; //9/23_12:45 紅磚藍寶石掉紅寶石
		}
		//產生物件
		ProduceGemAndLove("Blue");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Red_Green(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Red_Green");
		score.AddScore(3);
		score.AddScore(11);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Red_Number++;
			levelScript.GreenGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Green");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Red_Red(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Red_Red");
		score.AddScore(3);
		score.AddScore(9);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Red_Number++;
			levelScript.RedGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Red");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;

case "Red_Silver(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Red_Silver");
		score.AddScore(3);
		score.AddScore(12);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Red_Number++;
			levelScript.SilverGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Silver");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Red_Gold(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Red_Gold");
		score.AddScore(3);
		score.AddScore(13);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Red_Number++;
			levelScript.GoldGem_Number++;
		}
		
		//產生物件
		ProduceGemAndLove("Gold");			
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
//Valcon====================
case "Valcon(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Valcon");
		score.AddScore(4);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Valcon_Number++;
		}
				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
	break;
	
case "Valcon_Bell(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Valcon_Bell");
		score.AddScore(4);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Valcon_Number++;
			levelScript.Bell_Number++;
		}
		//產生物件
		ProduceGemAndLove("Bell");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Valcon_Blue(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Valcon_Blue");
		score.AddScore(4);
		score.AddScore(10);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Valcon_Number++;
			levelScript.BlueGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Blue");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Valcon_Green(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Valcon_Green");
		score.AddScore(4);
		score.AddScore(11);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Valcon_Number++;
			levelScript.GreenGem_Number++;
		}
		//產生物件
		ProduceGemAndLove("Green");				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Valcon_Red(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Valcon_Red");
		score.AddScore(4);
		score.AddScore(9);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Valcon_Number++;
			levelScript.RedGem_Number++;
		}
				
		//產生物件
		ProduceGemAndLove("Red");		
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Valcon_Silver(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Valcon_Silver");
		score.AddScore(4);
		score.AddScore(12);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Valcon_Number++;
			levelScript.SilverGem_Number++;
		}
		
		//產生物件
		ProduceGemAndLove("Silver");		
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
case "Valcon_Gold(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Valcon_Gold");
		score.AddScore(4);
		score.AddScore(13);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Valcon_Number++;
			levelScript.GoldGem_Number++;
		}
		
		//產生物件
		ProduceGemAndLove("Gold");		
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;
//Black====================
case "Black(Clone)":
	if(dragonScript.status==6)//星星1
	{
		if(!got)
		{
		got=true;
		
		//get score
		Debug.Log("get Black");
		score.AddScore(5);
		
		if(levelScript.gameStatus==5)
		{
			levelScript.Black_Number++;
		}
				
		//爆炸聲		
		Instantiate (sound_BrickBreak, this.gameObject.transform.position, Quaternion.identity);	
		//白煙
		Instantiate (smoke1, this.gameObject.transform.position, Quaternion.identity);
		SBP.SendEmptyAndDestroy();
		}
	}
break;

	//39.鈴鐺
	case "Bell(Clone)":
	
	if(!got)
	{
		got=true;	
		
		//get score
		//Debug.Log("get Bell");
		score.AddScore(21);
	
		health--;
			
		if (health <= 0) 
		{
			anim.SetInteger("status", -1);
			
			
		
		if(levelScript.gameStatus==5)
			levelScript.Bell_Number++;

			
			//閃亮
			Instantiate (shiny, this.gameObject.transform.position, Quaternion.identity);
			
			yield WaitForSeconds(.5);
			SBP.SendEmptyAndDestroy();
		}
		
	}
	break;		
	
	//49.愛心
	case "Love(Clone)":
	if(!got)
	{
		
		got=true;
		
		//get score
		Debug.Log("get Love");
		score.AddScore(17);
				
		levelScript.dragonLife = levelScript.dragonLife + 1;
		playerScript.life = playerScript.life + 1;
		health--;
		
		if (health <= 0) 
		{
		if(levelScript.gameStatus==5)
			levelScript.Love_Number++;
			
		//閃亮
		Instantiate (shiny, this.gameObject.transform.position, Quaternion.identity);
		
		//anim.SetInteger("status", -1);
		Debug.Log("im here");
		yield WaitForSeconds(.5);
		SBP.SendEmptyAndDestroy();
		}
	}
	break;		
	
	//50.沙漏
	case "Hourglass(Clone)":
	if(!got)
	{
		got=true;
		
		//get score
		Debug.Log("get Hourglass");
		score.AddScore(17);
		
		levelScript.isHourglass = true;
		health--;
		
		if (health <= 0) 
		{
		if(levelScript.gameStatus==5)
			levelScript.Hourglass_Number++;
		
		//閃亮		
		Instantiate (shiny, this.gameObject.transform.position, Quaternion.identity);
		
		
		anim.SetInteger("status", -1);

		yield WaitForSeconds(.5);
		SBP.SendEmptyAndDestroy();
		}
		
		
			
	}	
	break;	
	
	//60.綠塊們
	case "Green(Clone)":
		Green_Effect();
	break;
	
	case "Green_Blue(Clone)":
		Green_Blue_Effect();
	break;
	
	case "Green_Green(Clone)":
		Green_Green_Effect();
	break;
	
	case "Green_Red(Clone)":
		Green_Red_Effect();
	break;
	
	case "Green_Silver(Clone)":
		Green_Silver_Effect();	
	break;
	
	case "Green_Gold(Clone)":
		Green_Gold_Effect();
	break;
	
	case "Green_Bell(Clone)":
		Green_Bell_Effect();
	break;
	
	//51.商城愛心
	/*case "Mall_Love(Clone)":
	if(!got)
	{
		
		got=true;
		
		//get score
		Debug.Log("get Mall_Lovee");
		score.AddScore(17);
				
		levelScript.dragonLife = levelScript.dragonLife + 1;
		playerScript.life = playerScript.life + 1;
		health--;
		
		if (health <= 0) 
		{
		if(levelScript.gameStatus==5)
			levelScript.Love_Number++;
			
		//閃亮
		Instantiate (shiny, this.gameObject.transform.position, Quaternion.identity);
		
		//anim.SetInteger("status", -1);
		Debug.Log("im here");
		
		//發出空格訊息
		//levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
		
		Destroy(this.gameObject);
		}
	}
	break;	*/		
	

}

			
				
					
							
//	case "21.Bob(Clone)":
//	if(dragonScript.status==6){//星星狀態
//	
//		Instantiate (sound_BobBreak, this.gameObject.transform.position, Quaternion.identity);
//		if(!levelScript.isGameOver)
//			scoreScript.Bob21_Nnumber++;
//		anim.SetInteger("status", -1);
//		levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
//		yield WaitForSeconds(.5);
//		Destroy(this.gameObject);	
//	}
//	else{//其他狀態
//	playerScript.DragonTrap(this.gameObject);
//	}
//	
//	break;
//	
//	case "22.SpiderSilk(Clone)":
//	if(dragonScript.status==6){
//	
//	
//	Instantiate (sound_SpidersilkBreak, this.gameObject.transform.position, Quaternion.identity);
//	
//	if(!levelScript.isGameOver)
//		scoreScript.SpiderSilk22_Nnumber++;
//		
//		anim.SetInteger("status", -1);
//		levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
//		yield WaitForSeconds(.5);
//		Destroy(this.gameObject);	
//	}
//	else{
//	playerScript.DragonTrap(this.gameObject);
//	
//	
//	}
//	
//	case "23.FanR(Clone)":
//			
//	break;
//			
//	case "24.FanL(Clone)":
//			
//	break;
//	
//		
//		case "25.Bell(Clone)":
//		
//		health--;
//		if (health <= 0) 
//		{
//		anim.SetInteger("status", -1);
////		print("DESTORY BELL!");
//		if(!levelScript.isGameOver)
//			scoreScript.Bell25_Nnumber++;
////		this.collider2D.isTrigger = true;
//		yield WaitForSeconds(.5);
//		Instantiate (shiny, this.gameObject.transform.position, Quaternion.identity);
//		levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
//		Destroy(this.gameObject);
//		}
//	break;
//			
//			
//			
//	case "Star1(Clone)":
//	
//	if(!got)
//	{
//		got=true;
//	
////		this.gameObject.layer=11;
//		
////		print("S1");
//		dragonScript.OnStar1();
//		
//		DeleteAllCopyDragons();
//		
//		health--;
//		
//		if (health <= 0) 
//		{
//		
//		playerScript.star1Timer+=10;
//		if(!levelScript.isGameOver)
//			scoreScript.star1_Number++;
//			
////		this.collider2D.isTrigger = true;
//		anim.SetInteger("status", -1);
//		
//		Instantiate (sound_SUPER, this.gameObject.transform.position, Quaternion.identity);
//		Instantiate (shiny, this.gameObject.transform.position, Quaternion.identity);
//		yield WaitForSeconds(.2);
//		levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
//		Destroy(this.gameObject);
//		}
//	
//	
//	}
//	
//
//	break;	
//	
//	
//	case "Star2(Clone)":
//	
//	if(!got)
//	{
//		got=true;
//		dragonScript.OnStar2();
//		DeleteAllCopyDragons();
////		this.gameObject.layer=11;
//		health--;
//		
//		if (health <= 0) 
//		{
//		
//		playerScript.star2Timer+=10;//增加星星時間
//		if(!levelScript.isGameOver)
//			scoreScript.star2_Number++;
//		Instantiate (sound_SUPER, this.gameObject.transform.position, Quaternion.identity);
//		Instantiate (shiny, this.gameObject.transform.position, Quaternion.identity);
//		anim.SetInteger("status", -1);
//		yield WaitForSeconds(.2);
//		levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
//		Destroy(this.gameObject);
//		}
//	}	
//		
//	break;			
//	case "Star3(Clone)":
//	if(!got)
//	{
//		got=true;
////	this.gameObject.layer=11;
//
//		health--;
//		if (health <= 0) 
//		{
//
//		if(!levelScript.isGameOver)
//			scoreScript.star3_Number++;
//
//
//		DeleteAllCopyDragons();
//		
//				
//		//SUPER聲音
//		Instantiate (sound_SUPER, this.gameObject.transform.position, Quaternion.identity);
//		
//		//Copy 1
////		var ob1:GameObject=Instantiate (dragonOb, this.gameObject.transform.position, Quaternion.identity);
//		var ob1:GameObject=Instantiate (dragonOb, GameObject.FindGameObjectWithTag("Player").gameObject.transform.position, Quaternion.identity);
//		ob1.transform.GetComponent(Dragon).OnStar3();
//		ob1.GetComponent(PlayerSC).copy=true;
//		ob1.transform.rigidbody2D.AddForce(Vector2(-300,-300));
//		ob1.transform.GetComponent(PlayerSC).star3Timer+=10;
//		//Copy 2
//		var ob2:GameObject=Instantiate (dragonOb, GameObject.FindGameObjectWithTag("Player").gameObject.transform.position, Quaternion.identity);
////		var ob2:GameObject=Instantiate (dragonOb, this.gameObject.transform.position, Quaternion.identity);
//		ob2.transform.GetComponent(Dragon).OnStar3();		
//		ob2.GetComponent(PlayerSC).copy=true;
//		ob2.transform.rigidbody2D.AddForce(Vector2(300,300));
//		ob2.transform.GetComponent(PlayerSC).star3Timer+=10;
//	
//		anim.SetInteger("status", -1);
//		yield WaitForSeconds(.2);
//		levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
//		Destroy(this.gameObject);
//		}
//	}
//	break;			
//	case "Hourglass(Clone)":
//					health--;
//		if (health <= 0) 
//		{
//		if(!levelScript.isGameOver)
//			scoreScript.hourGlass_Number++;
//		Instantiate (shiny, this.gameObject.transform.position, Quaternion.identity);
//		anim.SetInteger("status", -1);
////		this.collider2D.isTrigger = true;
//		yield WaitForSeconds(.5);
//		levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
//		Destroy(this.gameObject);
//		}
//	break;	
//		case "Love(Clone)":
//		health--;
//		if (health <= 0) 
//		{
//		if(!levelScript.isGameOver)
//			scoreScript.love_Number++;
//		Instantiate (shiny, this.gameObject.transform.position, Quaternion.identity);
//		anim.SetInteger("status", -1);
////		this.collider2D.isTrigger = true;
//		yield WaitForSeconds(.5);
//		levelScript.getEmptyMessage(this.gameObject.transform.position.x  ,this.gameObject.transform.position.y);
//		Destroy(this.gameObject);
//		}
//	break;	
}//Switch END
	
	}

	}
}



function DeleteAllCopyDragons(){
	var respawns = GameObject.FindGameObjectsWithTag ("Copy");
	for (var respawn in respawns)
	Destroy(respawn);
}

















