﻿using UnityEngine;
using System.Collections;

public class Game_RandomItem : MonoBehaviour {

	private string[][] scoreData;
	
	public int ran;
	public int[] itemGet;
	
	public int Game_Mall_LoveTime;
	public int Game_Mall_Star1;
	public int Game_Mall_Bomb;
	CharacterScript CS;
	MyStage MS;
	MallController MC;


	// Use this for initialization
	void Start () {
		CS = this.GetComponent<CharacterScript>();
		MS = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<MyStage>();
		MC = GameObject.FindGameObjectWithTag("EventControoler").GetComponent<MallController> ();
		TextAsset binAsset = Resources.Load ("random", typeof(TextAsset)) as TextAsset;         
		
		//读取每一行的内容  
		string [] lineArray = binAsset.text.Split ("\n"[0]);  
		
		//创建二维数组  
		scoreData = new string [lineArray.Length][];  
		
		//把csv中的数据储存在二位数组中  
		for(int i = 0; i < lineArray.Length; i++)  
		{  
			scoreData[i] = lineArray[i].Split (',');  
		}
		
		itemGet = new int[scoreData.Length];
		
		Game_Mall_LoveTime = int.Parse (scoreData [4] [1]);
		Game_Mall_Star1 = int.Parse (scoreData [5] [1]);
		Game_Mall_Bomb = int.Parse (scoreData [6] [1]);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void RandomMallItem () {//隨機掉落物件
		ran = UnityEngine.Random.Range (0, 100);
		if (ran <= Game_Mall_LoveTime) {//愛心沙漏
			MC.ranLoveOrTime = true;
			if(MC.isLove)
			{
				GameObject Mall_Love = Instantiate(Resources.Load("Game_Mall_Items/Mall_Love", typeof(GameObject)), new Vector3(0, -9.44f, 0), Quaternion.identity) as GameObject;
			}
			else
			{
				GameObject Mall_Hourglass = Instantiate(Resources.Load("Game_Mall_Items/Mall_Hourglass", typeof(GameObject)), new Vector3(0, -9.44f, 0), Quaternion.identity) as GameObject;
			}
		}

		else if (Game_Mall_LoveTime < ran && ran <= Game_Mall_LoveTime + Game_Mall_Star1) {//超級星星
			MC.ranStar1 = true;
			GameObject Mall_Star1 = Instantiate(Resources.Load("Game_Mall_Items/Mall_Star1", typeof(GameObject)), new Vector3(0, -9.44f, 0), Quaternion.identity) as GameObject;
		}

		else if (Game_Mall_LoveTime + Game_Mall_Star1 < ran && ran <= Game_Mall_LoveTime + Game_Mall_Star1 + Game_Mall_Bomb) {//鞭炮
			MC.ranBomb = true;
			GameObject Mall_Bomb = Instantiate(Resources.Load("Game_Mall_Items/Mall_Bomb" , typeof(GameObject) ), new Vector3(0, 2.18f, 0), Quaternion.identity) as GameObject;	
		}
	}
}
