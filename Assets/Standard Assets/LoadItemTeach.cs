﻿using UnityEngine;
using System.Collections;

public class LoadItemTeach : MonoBehaviour {

	private string[] tmp;
	public string[][] teachData;    //0: sprite ,  1: text
	public int length;
	public int index;
	public LevelSelcet LS;
	public LoadCSV LC;

	// Use this for initialization
	void Start () {
		LS = GameObject.Find ("EventControoler").GetComponent<LevelSelcet> ();
		LC = GameObject.Find ("EventControoler").GetComponent<LoadCSV> ();
		index = 0;
		
		TextAsset binAsset = Resources.Load ("itemTeach", typeof(TextAsset)) as TextAsset;         
		string [] lineArray = binAsset.text.Split ("\n"[0]);  
		
		for (int i = 0; i < lineArray.Length; i++) {  
			tmp = lineArray[i].Split (',');
			if (tmp[0] == LS.itemTeachIndex.ToString()) {
				
				for (int j = tmp.Length - 1; j >= 0; j--) {
					if (tmp[j].Length > 1) {
						length = j/2;
						break;
					}
				}
				
				teachData = new string[length][];
				for (int k = 0; k < length; k++) {
					teachData[k] = new string[2];
					teachData[k][0] = tmp[k*2+1];
					teachData[k][1] = tmp[k*2+2];
				}
				break;
			}
		}
		
		for (int i = 0; i < length; i++) {
			string t = "";
			for (int j = 0; j < 2; j++) {
				t = t + teachData[i][j] + " ";
			}
			//Debug.Log(t);
		}
	}

	public Sprite getSprite() {
		return Resources.Load("Teach/" + teachData[index][0], typeof(Sprite)) as Sprite;
	}
	
	public string getText() {
				Debug.Log (LC.getTextByID(teachData[index][1].Trim()));
		return LC.getTextByID(teachData[index][1].Trim());
	}
	
}
