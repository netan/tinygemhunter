﻿using UnityEngine;
using System.Collections;

public class LockInfo : MonoBehaviour {

	private string[] lockData;

	public StageLock stage;
	public LoadCSV LC;
	
	public UILabel label;
	public int index;

	public int itemCount;
	public GameObject Odd;
	public GameObject[] OddItem;
	public GameObject Even;
	public GameObject[] EvenItem;

	public string[] name = new string[6];

	// Use this for initialization
	void Start () {
		LC = GameObject.Find ("EventControoler").GetComponent<LoadCSV> ();
		itemCount = LC.countSeriesNumber ((stage.index-1).ToString());
		TextAsset binAsset = Resources.Load ("lock", typeof(TextAsset)) as TextAsset;
		string [] lineArray = binAsset.text.Split ("\n"[0]);  

		lockData = new string [lineArray.Length];  
		for (int i = 0; i < lineArray.Length; i++) {  
			lockData = lineArray[i].Split (',');
			if (lockData[0] == stage.index.ToString()) {
				index = LC.getIndexByID(lockData[1]);
			}
		}



		/*int j = 0;
		for (int i = 0; i < LC.array_ItemTable_SeriesNumber.Count; i++) {
			if (LC.array_ItemTable_SeriesNumber[i] == (stage.index-1).ToString()) {
				name[j] = "item0" + LC.array_ItemTable_Name[i].Substring(3);
				j++;
			}
		}

		SetItem (name);*/
	}
	
	// Update is called once per frame
	void Update () {
		if (LC.array_TextTable [index] [PlayerPrefs.GetInt ("Language")] != label.text) {
			label.text = LC.array_TextTable [index] [PlayerPrefs.GetInt ("Language")];
		}
	}

	/*public void SetItem(string[] name) {
		switch (itemCount) {
		case 1:
			Odd.SetActive(true);
			Even.SetActive(false);
			OddItem[0].SetActive(false);
			OddItem[1].SetActive(false);
			OddItem[2].SetActive(true);
			OddItem[2].GetComponent<UISprite>().spriteName = name[0];
			IsShouldGray(OddItem[2].GetComponent<UISprite>(), name[0]);
			OddItem[3].SetActive(false);
			OddItem[4].SetActive(false);
			break;
		case 2:
			Odd.SetActive(false);
			Even.SetActive(true);
			EvenItem[0].SetActive(false);
			EvenItem[1].SetActive(false);
			
			EvenItem[2].SetActive(true);
			EvenItem[2].GetComponent<UISprite>().spriteName = name[0];
			IsShouldGray(EvenItem[2].GetComponent<UISprite>(), name[0]);
			
			EvenItem[3].SetActive(true);
			EvenItem[3].GetComponent<UISprite>().spriteName = name[1];
			IsShouldGray(EvenItem[3].GetComponent<UISprite>(), name[1]);
			
			EvenItem[4].SetActive(false);
			EvenItem[5].SetActive(false);
			break;
		case 3:
			Odd.SetActive(true);
			Even.SetActive(false);
			OddItem[0].SetActive(false);
			
			OddItem[1].SetActive(true);
			OddItem[1].GetComponent<UISprite>().spriteName = name[0];
			IsShouldGray(OddItem[1].GetComponent<UISprite>(), name[0]);
			
			OddItem[2].SetActive(true);
			OddItem[2].GetComponent<UISprite>().spriteName = name[1];
			IsShouldGray(OddItem[2].GetComponent<UISprite>(), name[1]);
			
			OddItem[3].SetActive(true);
			OddItem[3].GetComponent<UISprite>().spriteName = name[2];
			IsShouldGray(OddItem[3].GetComponent<UISprite>(), name[2]);
			
			OddItem[4].SetActive(false);
			break;
		case 4:
			Odd.SetActive(false);
			Even.SetActive(true);
			EvenItem[0].SetActive(false);
			
			EvenItem[1].SetActive(true);
			EvenItem[1].GetComponent<UISprite>().spriteName = name[0];
			IsShouldGray(EvenItem[1].GetComponent<UISprite>(), name[0]);
			
			EvenItem[2].SetActive(true);
			EvenItem[2].GetComponent<UISprite>().spriteName = name[1];
			IsShouldGray(EvenItem[2].GetComponent<UISprite>(), name[1]);
			
			EvenItem[3].SetActive(true);
			EvenItem[3].GetComponent<UISprite>().spriteName = name[2];
			IsShouldGray(EvenItem[3].GetComponent<UISprite>(), name[2]);
			
			EvenItem[4].SetActive(true);
			EvenItem[4].GetComponent<UISprite>().spriteName = name[3];
			IsShouldGray(EvenItem[4].GetComponent<UISprite>(), name[3]);
			
			EvenItem[5].SetActive(false);
			break;
		case 5:
			Odd.SetActive(true);
			Even.SetActive(false);
			OddItem[0].SetActive(true);
			OddItem[0].GetComponent<UISprite>().spriteName = name[0];
			IsShouldGray(OddItem[0].GetComponent<UISprite>(), name[0]);
			
			OddItem[1].SetActive(true);
			OddItem[1].GetComponent<UISprite>().spriteName = name[1];
			IsShouldGray(OddItem[1].GetComponent<UISprite>(), name[1]);
			
			OddItem[2].SetActive(true);
			OddItem[2].GetComponent<UISprite>().spriteName = name[2];
			IsShouldGray(OddItem[2].GetComponent<UISprite>(), name[2]);
			
			OddItem[3].SetActive(true);
			OddItem[3].GetComponent<UISprite>().spriteName = name[3];
			IsShouldGray(OddItem[3].GetComponent<UISprite>(), name[3]);
			
			OddItem[4].SetActive(true);
			OddItem[4].GetComponent<UISprite>().spriteName = name[4];
			IsShouldGray(OddItem[4].GetComponent<UISprite>(), name[4]);
			break;
		case 6:
			Odd.SetActive(false);
			Even.SetActive(true);
			EvenItem[0].SetActive(true);
			EvenItem[0].GetComponent<UISprite>().spriteName = name[0];
			IsShouldGray(EvenItem[0].GetComponent<UISprite>(), name[0]);
			
			EvenItem[1].SetActive(true);
			EvenItem[1].GetComponent<UISprite>().spriteName = name[1];
			IsShouldGray(EvenItem[1].GetComponent<UISprite>(), name[1]);
			
			EvenItem[2].SetActive(true);
			EvenItem[2].GetComponent<UISprite>().spriteName = name[2];
			IsShouldGray(EvenItem[2].GetComponent<UISprite>(), name[2]);
			
			EvenItem[3].SetActive(true);
			EvenItem[3].GetComponent<UISprite>().spriteName = name[3];
			IsShouldGray(EvenItem[3].GetComponent<UISprite>(), name[3]);
			
			EvenItem[4].SetActive(true);
			EvenItem[4].GetComponent<UISprite>().spriteName = name[4];
			IsShouldGray(EvenItem[4].GetComponent<UISprite>(), name[4]);
			
			EvenItem[5].SetActive(true);
			EvenItem[5].GetComponent<UISprite>().spriteName = name[5];
			IsShouldGray(EvenItem[5].GetComponent<UISprite>(), name[5]);
			
			break;
		}
	}

	public void IsShouldGray(UISprite color, string name) {
		if (PlayerPrefs.GetInt(name) == 0) {
			color.color = new Color(50f / 255f, 50f / 255f, 50f / 255f, 1);
		}
	}*/
}
