﻿using UnityEngine;
using System.Collections;
using Parse;
using System.Threading.Tasks;

public class FriendCheckButton : MonoBehaviour {

	public MyParse myParse;

	public bool isOK;

	public UISprite result;
	public UISprite friendCheckUI;
	public UISprite friendCheckSprite;
	//public UILabel friendCheckLabel1;
	public UILabel IDLabel;

	public UIButton okButton;
	public UIButton notokButton;

	public SetFriendCheckData ID;

	public GameObject UI;
	public GameObject alert;

	private int friendLimit = 20;

	//public ChangeLanguage friendCheckLabel;

	public void OnClick() {
		myParse = GameObject.Find ("EventControoler").GetComponent<MyParse> ();
		UI = GameObject.Find ("FriendCheck");
		GameObject clone = NGUITools.AddChild (UI, alert);
		clone.GetComponentInChildren<ChangeLanguage>().ReLoadID("10116");
		if (myParse.friend.Count < friendLimit) {
			if (myParse.isGetFriendCheckRef) {
				if (isOK) {
					var friendlist = ParseObject.GetQuery("FriendList")
									.WhereEqualTo("A", ID.realID)
									.WhereMatchesKeyInQuery("B", "A", ParseObject.GetQuery("FriendList")
						                        					.WhereEqualTo("B", ID.realID));
					friendlist.CountAsync().ContinueWith (f_limit => {
					if (f_limit.Result < friendLimit) {
							var query = ParseObject.GetQuery("FriendList")
								.WhereEqualTo("A", ParseUser.CurrentUser.Username)
									.WhereEqualTo("B", ID.realID);
							query.CountAsync().ContinueWith (t => {
								if (t.Result == 0) {
									ParseObject friendList = new ParseObject("FriendList");
									friendList["A"] = ParseUser.CurrentUser.Username;
									friendList["B"] = ID.realID;
									friendList["nickname"] = WWW.UnEscapeURL(PlayerPrefs.GetString("nickName"));
									Task saveTask = friendList.SaveAsync();
									myParse.friend.Add(ID.realID);
									clone.GetComponentInChildren<ChangeLanguage>().ReLoadID("10124");
									Debug.Log("Friend checked");
								} else {
									clone.GetComponentInChildren<ChangeLanguage>().ReLoadID("10123");
									Debug.Log("Checked already");
								}
							});
							result.enabled = true;
							result.spriteName = "ok";
							friendCheckUI.color = Color.gray;
							friendCheckSprite.color = Color.gray;
							//friendCheckLabel1.color = Color.gray;
							IDLabel.color = Color.gray;
							
							//friendCheckLabel.ReLoadID ("10124");
							okButton.isEnabled = false;
							notokButton.isEnabled = false;
						} else {
							clone.GetComponentInChildren<ChangeLanguage>().ReLoadID("10131");
							Debug.Log("Your friend have too much friend");
						}
					});
					
				} else {
					myParse.DeleteFriend(ID.realID);
					result.enabled = true;
					result.spriteName = "notok";
					friendCheckUI.color = Color.gray;
					friendCheckSprite.color = Color.gray;
					//friendCheckLabel1.color = Color.gray;
					IDLabel.color = Color.gray;
					
					//friendCheckLabel.ReLoadID ("10124");
					okButton.isEnabled = false;
					notokButton.isEnabled = false;
				}
			}
		} else {
			clone.GetComponentInChildren<ChangeLanguage>().ReLoadID("10130");
			Debug.Log("Friend too much");
		}



	}
}
