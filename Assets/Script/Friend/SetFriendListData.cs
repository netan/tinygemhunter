﻿using UnityEngine;
using System.Collections;

public class SetFriendListData : MonoBehaviour {

	public string realID;
	public UILabel label;
	public UILabel belllabel;
	
	public void SetData(string ID, string nickname, string bell) {
		realID = ID;
		label.text = nickname;
		belllabel.text = bell;
	}
}
