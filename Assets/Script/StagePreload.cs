﻿using UnityEngine;
using System.Collections;
using ChartboostSDK;

public class StagePreload : MonoBehaviour
{

	public MyParse myParse;
	public TestNetwork testNetwork;
	public ChangeLanguage moneyLabel;

	// Use this for initialization
	void Start ()
	{
		myParse = GameObject.Find ("EventControoler").GetComponent<MyParse> ();
		testNetwork = GameObject.Find ("EventControoler").GetComponent<TestNetwork> ();
		if (testNetwork.isConnect && !testNetwork.isReConnect) {
			myParse.GetMallList ();
		} else {
			moneyLabel.ReLoadID ("10117");
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}
