﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SaveMyData : MonoBehaviour
{

	public int treasureNumber = 0;
		
	//Saved Data Parameters=====
	public int[]  arr_Treasure;
	public int[]  arr_Stage1_Clear;
	public int[]  arr_Stage2_Clear;
	public int[]  arr_Stage3_Clear;
	public int[]  arr_Stage4_Clear;
	public int[]  arr_Stage5_Clear;
	public int[]  arr_Stage6_Clear;
	public int[]  arr_Stage7_Clear;
	public int[]  arr_Stage8_Clear;
	public int[]  arr_Stage9_Clear;
	public int[]  arr_Stage10_Clear;
	//Star====
	public int[]  arr_Stage1_Star;
	public int[]  arr_Stage2_Star;
	public int[]  arr_Stage3_Star;
	public int[]  arr_Stage4_Star;
	public int[]  arr_Stage5_Star;
	public int[]  arr_Stage6_Star;
	public int[]  arr_Stage7_Star;
	public int[]  arr_Stage8_Star;
	public int[]  arr_Stage9_Star;
	public int[]  arr_Stage10_Star;

	//SP Lock Condition=========
	public int[]  arr_Stage1_Lock;
	public int[]  arr_Stage2_Lock;
	public int[]  arr_Stage3_Lock;
	public int[]  arr_Stage4_Lock;
	public int[]  arr_Stage5_Lock;
	public int[]  arr_Stage6_Lock;
	public int[]  arr_Stage7_Lock;
	public int[]  arr_Stage8_Lock;
	public int[]  arr_Stage9_Lock;
	public int[]  arr_Stage10_Lock;
	public LoadCSV CSVscript;
	public bool Initialed = false;





	//Treasure============
	public void SetTreasure (int itemID, int itemPlusNumber)
	{
		string name = "Treasure";
//		print (arr_Treasure [itemID]+" + "+itemPlusNumber );
		arr_Treasure [itemID] += (itemPlusNumber - 1);//??????
//		print ("得到:"+arr_Treasure [itemID] );
		PlayerPrefsX.SetIntArray (name, arr_Treasure);
//		print ("AC Set!!");
	}
	
	public int[] GetTreasure ()
	{
		int[] temp;
		string name = "Treasure";
		temp = PlayerPrefsX.GetIntArray (name);
		
		if (temp.Length == 0) { //Empty
			temp = new int[treasureNumber]; 
		}
		return temp;
	}
	//============Treasure
	
	//STARs Of Stage=======
	public void SetStage_Star (int stage, int level, int starNumber)
	{
		string name = "Stage_" + stage.ToString () + "_Star";
		switch (stage) {
			case 1:
				arr_Stage1_Star [level - 1] = starNumber;
				PlayerPrefsX.SetIntArray (name, arr_Stage1_Star);
				break;
			case 2:
				arr_Stage2_Star [level - 1] = starNumber;
				PlayerPrefsX.SetIntArray (name, arr_Stage2_Star);
				break;
			case 3:
				arr_Stage3_Star [level - 1] = starNumber;
				PlayerPrefsX.SetIntArray (name, arr_Stage3_Star);
				break;
			case 4:
				arr_Stage4_Star [level - 1] = starNumber;
				PlayerPrefsX.SetIntArray (name, arr_Stage4_Star);
				break;
			case 5:
				arr_Stage5_Star [level - 1] = starNumber;
				PlayerPrefsX.SetIntArray (name, arr_Stage5_Star);
				break;
			case 6:
				arr_Stage6_Star [level - 1] = starNumber;
				PlayerPrefsX.SetIntArray (name, arr_Stage6_Star);
				break;
			case 7:
				arr_Stage7_Star [level - 1] = starNumber;
				PlayerPrefsX.SetIntArray (name, arr_Stage7_Star);
				break;
			case 8:
				arr_Stage8_Star [level - 1] = starNumber;
				PlayerPrefsX.SetIntArray (name, arr_Stage8_Star);
				break;		
			case 9:
				arr_Stage9_Star [level - 1] = starNumber;
				PlayerPrefsX.SetIntArray (name, arr_Stage9_Star);
				break;
			case 10:
				arr_Stage10_Star [level - 1] = starNumber;
				PlayerPrefsX.SetIntArray (name, arr_Stage10_Star);
				break;
		}
	}

	public int[] GetStage_Star (int stage)
	{

		int[] temp;
		string name = "Stage_" + stage.ToString () + "_Star";

		temp = PlayerPrefsX.GetIntArray (name);

		if (temp.Length == 0) { //Empty
			temp = new int[CSVscript.GetLevelNumberOfThisStage (stage)];	
		}
		else {
			switch (stage) {
				case 1:
					arr_Stage1_Star = temp;
					break;
				case 2:
					;
					arr_Stage2_Star = temp;
					break;
				case 3:
					arr_Stage3_Star = temp;
					break;
				case 4:
					arr_Stage4_Star = temp;
					break;
				case 5:
					arr_Stage5_Star = temp;
					break;
				case 6:
					arr_Stage6_Star = temp;
					break;
				case 7:
					arr_Stage7_Star = temp;
					break;
				case 8:
					arr_Stage8_Star = temp;
					break;
				case 9:
					arr_Stage9_Star = temp;
					break;
				case 10:
					arr_Stage10_Star = temp;
					;
					break;
			}
		}

		return temp;
	}
	//=======STARs Of Stage



	//LOCK===========================
	public void SetStage_Lock (int stage, int level)
	{

		string name = "Stage_" + stage.ToString () + "_Lock";
//		print ("SET_LOCK:"+name+" stage:"+stage+" level-1:"+(level-1).ToString());
		switch (stage) {
			case 1:
				arr_Stage1_Lock [level] = 0;
				PlayerPrefsX.SetIntArray (name, arr_Stage1_Lock);
				break;
			case 2:
				arr_Stage2_Lock [level] = 0;
				PlayerPrefsX.SetIntArray (name, arr_Stage2_Lock);
				break;
			case 3:
				arr_Stage3_Lock [level] = 0;
				PlayerPrefsX.SetIntArray (name, arr_Stage3_Lock);
				break;
			case 4:
				arr_Stage4_Lock [level] = 0;
				PlayerPrefsX.SetIntArray (name, arr_Stage4_Lock);
				break;
			case 5:
				arr_Stage5_Lock [level] = 0;
				PlayerPrefsX.SetIntArray (name, arr_Stage5_Lock);
				break;
			case 6:
				arr_Stage6_Lock [level] = 0;
				PlayerPrefsX.SetIntArray (name, arr_Stage6_Lock);
				break;
			case 7:
				arr_Stage7_Lock [level] = 0;
				PlayerPrefsX.SetIntArray (name, arr_Stage7_Lock);
				break;
			case 8:
				arr_Stage8_Lock [level] = 0;
				PlayerPrefsX.SetIntArray (name, arr_Stage8_Lock);
				break;
			case 9:
				arr_Stage9_Lock [level] = 0;
				PlayerPrefsX.SetIntArray (name, arr_Stage9_Lock);
				break;
			case 10:
				arr_Stage10_Lock [level] = 0;
				PlayerPrefsX.SetIntArray (name, arr_Stage10_Lock);
				break;
		}

//		if(stage==1)
//		{
//			arr_Stage1_Lock [level] = 0;
//			PlayerPrefsX.SetIntArray (name, arr_Stage1_Lock);
//		}
//		if(stage==2)
//		{
//			arr_Stage2_Lock [level] = 0;
//			PlayerPrefsX.SetIntArray (name, arr_Stage2_Lock);
//		}
		
	}

	public int[] GetStage_Lock (int stage)
	{
		
		int[] temp;
		string name = "Stage_" + stage.ToString () + "_Lock";
		temp = PlayerPrefsX.GetIntArray (name);

		if (temp.Length == 0) { //Empty
			temp = new int[CSVscript.GetLevelNumberOfThisStage (stage)]; 
		}
		else {
			switch (stage) {
				case 1:
					arr_Stage1_Lock = temp;
					break;
				case 2:
					arr_Stage2_Lock = temp;
					break;
				case 3:
					arr_Stage3_Lock = temp;
					break;
				case 4:
					arr_Stage4_Lock = temp;
					break;
				case 5:
					arr_Stage5_Lock = temp;
					break;
				case 6:
					arr_Stage6_Lock = temp;
					break;
				case 7:
					arr_Stage7_Lock = temp;
					break;
				case 8:
					arr_Stage8_Lock = temp;
					break;
				case 9:
					arr_Stage9_Lock = temp;
					break;
				case 10:
					arr_Stage10_Lock = temp;
					break;
			}

//
//		if (stage == 1)
//			arr_Stage1_Lock = temp;
//		if(stage==2)
//			arr_Stage2_Lock= temp;
		}
		
		return temp;
	}

	//===========================LOCK

	//Stage=================
	public void SetStage_Clear (int stage, int level)
	{
		string name = "Stage_" + stage.ToString () + "_Clear";

		// 1:過關 0:未過關
		switch (stage) {
			case 1:
				arr_Stage1_Clear [level - 1] = 1;
				PlayerPrefsX.SetIntArray (name, arr_Stage1_Clear);
				break;
			case 2:
				arr_Stage2_Clear [level - 1] = 1;
				PlayerPrefsX.SetIntArray (name, arr_Stage2_Clear);
				break;
			case 3:
				arr_Stage3_Clear [level - 1] = 1;
				PlayerPrefsX.SetIntArray (name, arr_Stage3_Clear);
				break;
			case 4:
				arr_Stage4_Clear [level - 1] = 1;
				PlayerPrefsX.SetIntArray (name, arr_Stage4_Clear);
				break;
			case 5:
				arr_Stage5_Clear [level - 1] = 1;
				PlayerPrefsX.SetIntArray (name, arr_Stage5_Clear);
				break;
			case 6:
				arr_Stage6_Clear [level - 1] = 1;
				PlayerPrefsX.SetIntArray (name, arr_Stage6_Clear);
				break;
			case 7:
				arr_Stage7_Clear [level - 1] = 1;
				PlayerPrefsX.SetIntArray (name, arr_Stage7_Clear);
				break;
			case 8:
				arr_Stage8_Clear [level - 1] = 1;
				PlayerPrefsX.SetIntArray (name, arr_Stage8_Clear);
				break;
			case 9:
				arr_Stage9_Clear [level - 1] = 1;
				PlayerPrefsX.SetIntArray (name, arr_Stage9_Clear);
				break;
			case 10:
				arr_Stage10_Clear [level - 1] = 1;
				PlayerPrefsX.SetIntArray (name, arr_Stage10_Clear);
				break;
		}




//		if(stage==1)
//		{
//			arr_Stage1_Clear [level-1] = 1;
//			PlayerPrefsX.SetIntArray (name, arr_Stage1_Clear);
//		}
//		if(stage==2)
//		{
//			arr_Stage2_Clear [level-1] = 1;
//			PlayerPrefsX.SetIntArray (name, arr_Stage2_Clear);
//		}

	}

	public int[] GetStage_Clear (int stage)
	{
		int[] temp;
		string name = "Stage_" + stage.ToString () + "_Clear";
		temp = PlayerPrefsX.GetIntArray (name);

		if (temp.Length == 0) { //Empty
			temp = new int[CSVscript.GetLevelNumberOfThisStage (stage)]; 
//			if(stage==1)
//				temp=new int[CSVscript.GetLevelNumberOfThisStage(stage)]; 
//			if(stage==2)
//				temp=new int[CSVscript.GetLevelNumberOfThisStage(stage)]; 	
		}
		return temp;
	}
	//=================Stage

	//Clear ALL==========================================
	public void ClearAllStageRecord ()
	{

		string name_stage1 = "Stage_1_Clear";
		string name_star1 = "Stage_1_Star";
		string name_stage2 = "Stage_2_Clear";
		string name_star2 = "Stage_2_Star";
		string name_treasure = "Treasure";


		PlayerPrefs.DeleteAll ();
		Debug.Log (PlayerPrefs.GetInt ("openBomb"));
		//刪除 紀錄
//		PlayerPrefs. DeleteKey (name_treasure);  
//		PlayerPrefs. DeleteKey (name_stage1);  
//		PlayerPrefs. DeleteKey (name_star1);  
//		PlayerPrefs. DeleteKey (name_stage2);  
//		PlayerPrefs. DeleteKey (name_star2);  


		for (int a=0; a<arr_Stage1_Clear.Length; a++) {
			arr_Stage1_Clear [a] = 0;
			arr_Stage1_Star [a] = 0;
		}
		for (int b=0; b<arr_Stage2_Clear.Length; b++) {
			arr_Stage2_Clear [b] = 0;
			arr_Stage2_Star [b] = 0;
		}
		for (int c=0; c<arr_Treasure.Length; c++) {
			arr_Treasure [c] = 0;
		}
		//Lock
		for (int d=0; d<arr_Stage1_Lock.Length; d++) {
			arr_Stage1_Lock [d] = 0;
		}
		for (int e=0; e<arr_Stage2_Lock.Length; e++) {
			if (e == 0) {
				arr_Stage2_Lock [e] = 1;
			}
			else {
				arr_Stage2_Lock [e] = 0;
			}
		}		
		
		print ("clear");
//		Init ();
	}

	public void ClearTempTreasure ()
	{

		for (int a=0; a<arr_Treasure.Length; a++) {
				
			arr_Treasure [a] = 0;

		}

	}

	void Init ()
	{
	

		//STAR=============================================================
		//Stage1 Satr
		int[] tempStar1 = GetStage_Star (1);
		if (tempStar1.Length > 0) {
			arr_Stage1_Star = tempStar1;
		}
		else {
			arr_Stage1_Star = new int[CSVscript.GetLevelNumberOfThisStage (1)];
		}

		//Stage2 Satr
		int[] tempStar2 = GetStage_Star (2);
		if (tempStar2.Length > 0) {
			arr_Stage2_Star = tempStar2;
		}
		else {
			arr_Stage2_Star = new int[CSVscript.GetLevelNumberOfThisStage (2)];
		}

		//Stage3 Satr
		int[] tempStar3 = GetStage_Star (3);
		if (tempStar3.Length > 0) {
			arr_Stage3_Star = tempStar3;
		}
		else {
			arr_Stage3_Star = new int[CSVscript.GetLevelNumberOfThisStage (3)];
		}
		
		//Stage4 Satr
		int[] tempStar4 = GetStage_Star (4);
		if (tempStar4.Length > 0) {
			arr_Stage4_Star = tempStar4;
		}
		else {
			arr_Stage4_Star = new int[CSVscript.GetLevelNumberOfThisStage (4)];
		}

		//Stage5 Satr
		int[] tempStar5 = GetStage_Star (5);
		if (tempStar5.Length > 0) {
			arr_Stage5_Star = tempStar5;
		}
		else {
			arr_Stage5_Star = new int[CSVscript.GetLevelNumberOfThisStage (5)];
		}
		
		//Stage6 Satr
		int[] tempStar6 = GetStage_Star (6);
		if (tempStar6.Length > 0) {
			arr_Stage6_Star = tempStar6;
		}
		else {
			arr_Stage6_Star = new int[CSVscript.GetLevelNumberOfThisStage (6)];
		}

		//Stage7 Satr
		int[] tempStar7 = GetStage_Star (7);
		if (tempStar7.Length > 0) {
			arr_Stage7_Star = tempStar7;
		}
		else {
			arr_Stage7_Star = new int[CSVscript.GetLevelNumberOfThisStage (7)];
		}
		
		//Stage8 Satr
		int[] tempStar8 = GetStage_Star (8);
		if (tempStar8.Length > 0) {
			arr_Stage8_Star = tempStar8;
		}
		else {
			arr_Stage8_Star = new int[CSVscript.GetLevelNumberOfThisStage (8)];
		}

		//Stage9 Satr
		int[] tempStar9 = GetStage_Star (9);
		if (tempStar9.Length > 0) {
			arr_Stage9_Star = tempStar9;
		}
		else {
			arr_Stage9_Star = new int[CSVscript.GetLevelNumberOfThisStage (9)];
		}
		
		//Stage10 Satr
		int[] tempStar10 = GetStage_Star (10);
		if (tempStar10.Length > 0) {
			arr_Stage10_Star = tempStar10;
		}
		else {
			arr_Stage10_Star = new int[CSVscript.GetLevelNumberOfThisStage (10)];
		}






		//STAGE=============================================================
		//Stage1 Clear
		int[] tempClear1 = GetStage_Clear (1);
		if (tempClear1.Length > 0) {
			arr_Stage1_Clear = tempClear1;
		}
		else {
			arr_Stage1_Clear = new int[CSVscript.GetLevelNumberOfThisStage (1)];
		}

		//Stage2 Clear
		int[] tempClear2 = GetStage_Clear (2);
		if (tempClear2.Length > 0) {
			arr_Stage2_Clear = tempClear2;
		}
		else {
			arr_Stage2_Clear = new int[CSVscript.GetLevelNumberOfThisStage (2)];
		}

		//Stage3 Clear
		int[] tempClear3 = GetStage_Clear (3);
		if (tempClear3.Length > 0) {
			arr_Stage3_Clear = tempClear3;
		}
		else {
			arr_Stage3_Clear = new int[CSVscript.GetLevelNumberOfThisStage (3)];
		}

		//Stage4 Clear
		int[] tempClear4 = GetStage_Clear (4);
		if (tempClear4.Length > 0) {
			arr_Stage4_Clear = tempClear4;
		}
		else {
			arr_Stage4_Clear = new int[CSVscript.GetLevelNumberOfThisStage (4)];
		}

		//Stage5 Clear
		int[] tempClear5 = GetStage_Clear (5);
		if (tempClear5.Length > 0) {
			arr_Stage5_Clear = tempClear5;
		}
		else {
			arr_Stage5_Clear = new int[CSVscript.GetLevelNumberOfThisStage (5)];
		}

		//Stage6 Clear
		int[] tempClear6 = GetStage_Clear (6);
		if (tempClear6.Length > 0) {
			arr_Stage6_Clear = tempClear6;
		}
		else {
			arr_Stage6_Clear = new int[CSVscript.GetLevelNumberOfThisStage (6)];
		}

		//Stage7 Clear
		int[] tempClear7 = GetStage_Clear (7);
		if (tempClear7.Length > 0) {
			arr_Stage7_Clear = tempClear7;
		}
		else {
			arr_Stage7_Clear = new int[CSVscript.GetLevelNumberOfThisStage (7)];
		}

		//Stage8 Clear
		int[] tempClear8 = GetStage_Clear (8);
		if (tempClear8.Length > 0) {
			arr_Stage8_Clear = tempClear8;
		}
		else {
			arr_Stage8_Clear = new int[CSVscript.GetLevelNumberOfThisStage (8)];
		}

		//Stage9 Clear
		int[] tempClear9 = GetStage_Clear (9);
		if (tempClear9.Length > 0) {
			arr_Stage9_Clear = tempClear9;
		}
		else {
			arr_Stage9_Clear = new int[CSVscript.GetLevelNumberOfThisStage (9)];
		}

		//Stage10 Clear
		int[] tempClear10 = GetStage_Clear (10);
		if (tempClear10.Length > 0) {
			arr_Stage10_Clear = tempClear10;
		}
		else {
			arr_Stage10_Clear = new int[CSVscript.GetLevelNumberOfThisStage (10)];
		}

		//LOCK=============================================================
		//Stage1 Lock
		int[] tempLock1 = GetStage_Lock (1);
		if (tempLock1.Length > 0) {
			arr_Stage1_Lock = tempLock1;
		}
		else {
			arr_Stage1_Lock = new int[CSVscript.GetLevelNumberOfThisStage (1)];
		}

		//Stage2 Lock
		int[] tempLock2 = GetStage_Lock (2);
		if (tempLock2.Length > 0) {
			arr_Stage2_Lock = tempLock2;
		}
		else {
			arr_Stage2_Lock = new int[CSVscript.GetLevelNumberOfThisStage (2)];
		}
	
		//Stage3 Lock
		int[] tempLock3 = GetStage_Lock (3);
		if (tempLock3.Length > 0) {
			arr_Stage3_Lock = tempLock3;
		}
		else {
			arr_Stage3_Lock = new int[CSVscript.GetLevelNumberOfThisStage (3)];
		}
		
		//Stage4 Lock
		int[] tempLock4 = GetStage_Lock (4);
		if (tempLock4.Length > 0) {
			arr_Stage4_Lock = tempLock4;
		}
		else {
			arr_Stage4_Lock = new int[CSVscript.GetLevelNumberOfThisStage (4)];
		}

		//Stage5 Lock
		int[] tempLock5 = GetStage_Lock (5);
		if (tempLock5.Length > 0) {
			arr_Stage5_Lock = tempLock5;
		}
		else {
			arr_Stage5_Lock = new int[CSVscript.GetLevelNumberOfThisStage (5)];
		}
		
		//Stage6 Lock
		int[] tempLock6 = GetStage_Lock (6);
		if (tempLock6.Length > 0) {
			arr_Stage6_Lock = tempLock6;
		}
		else {
			arr_Stage6_Lock = new int[CSVscript.GetLevelNumberOfThisStage (6)];
		}

		//Stage7 Lock
		int[] tempLock7 = GetStage_Lock (7);
		if (tempLock7.Length > 0) {
			arr_Stage7_Lock = tempLock7;
		}
		else {
			arr_Stage7_Lock = new int[CSVscript.GetLevelNumberOfThisStage (7)];
		}
		
		//Stage8 Lock
		int[] tempLock8 = GetStage_Lock (8);
		if (tempLock8.Length > 0) {
			arr_Stage8_Lock = tempLock8;
		}
		else {
			arr_Stage8_Lock = new int[CSVscript.GetLevelNumberOfThisStage (8)];
		}
	
		//Stage9 Lock
		int[] tempLock9 = GetStage_Lock (9);
		if (tempLock9.Length > 0) {
			arr_Stage9_Lock = tempLock9;
		}
		else {
			arr_Stage9_Lock = new int[CSVscript.GetLevelNumberOfThisStage (9)];
		}
		
		//Stage10 Lock
		int[] tempLock10 = GetStage_Lock (10);
		if (tempLock10.Length > 0) {
			arr_Stage10_Lock = tempLock10;
		}
		else {
			arr_Stage10_Lock = new int[CSVscript.GetLevelNumberOfThisStage (10)];
		}







		//TREASURE=============================================================
		//Treasure
		int[] tempTreasure = GetTreasure ();
//		print (tempTreasure.Length);
		if (tempTreasure.Length > 0) {
			arr_Treasure = tempTreasure;
		}
		else {
			arr_Treasure = new int[treasureNumber];
		}
	}

	void Start ()
	{
		CSVscript = this.gameObject.GetComponent<LoadCSV> ();
	}

	void OnGUI ()
	{
//		RepaintStar ();
		//删除 PlayerPrefs 中某一个key的值  
//		PlayerPrefs. DeleteKey (“key”);  
		//判断 PlayerPrefs中是否存在这个key  
//		bool b = PlayerPrefs.HasKey(“key”);  
	}
	
	void Update ()
	{
		if (LoadCSV.isReadFinish && !Initialed) {
			treasureNumber = CSVscript.GetItemNumber ();
			Init ();
			Initialed = true;
		}
	}
}
