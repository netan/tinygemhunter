﻿#pragma strict

public var anim : Animator;
public var status : int = 0;
public var isStar3 : boolean = false;


function Start () {
	anim = gameObject.GetComponent(Animator);
}

function Update () {
}

//StandBy 1(原點)
function OnIdle () {
	status = 0;
	this.anim.SetInteger("Status", status);
}
//準備發射
function OnPrapare () {
	status = 1;
	this.anim.SetInteger("Status", status);
}
//飛行
function OnFly () {
	status = 2;
	this.anim.SetInteger("Status", status);
}
//死亡
function OnDead () {
	status = 3;
	this.anim.SetInteger("Status", status);
}
//向左走
function OnWalkToLeft () {
	status = 4;
	this.anim.SetInteger("Status", status);
}
//向右走
function OnWalkToRight () {
	status =5;
	this.anim.SetInteger("Status", status);
}
//吃到星星1
function OnStar1() {
	status =6;
	this.anim.SetInteger("Status", status);
}
//吃到星星2
function OnStar2() {
	status =7;
	this.anim.SetInteger("Status", status);
}
//吃到星星3
function OnStar3() {
	status =8;
	this.anim.SetInteger("Status", status);
	
}
//困住
function OnTrap(){
	status =9;
	this.anim.SetInteger("Status", status);
}
//StandBy 2(Sleep)
function OnSleep(){
	status =10;
	this.anim.SetInteger("Status", status);
}
//死亡 2
function OnDead2(){
	status =11;
	this.anim.SetInteger("Status", status);
}











