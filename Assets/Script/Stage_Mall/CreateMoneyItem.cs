﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Prime31;
using System.Linq;
using System;

public class CreateMoneyItem : MonoBehaviour
{

	public MyParse myParse;
	public TestNetwork testNetwork;
	public GameObject scrollView;
	public GameObject MoneyItem;
	public float localPosition;
	public GameObject loadLabel;
	private SaveMyData saveBoxSC;
	// Use this for initialization
	void Start ()
	{
		saveBoxSC = GameObject.FindGameObjectWithTag ("EventControoler").GetComponent <SaveMyData> ();
		myParse = GameObject.Find ("EventControoler").GetComponent<MyParse> ();
		testNetwork = GameObject.Find ("EventControoler").GetComponent<TestNetwork> ();

		StoreKitManager.productListReceivedEvent += allProducts =>
		{
			Debug.Log ("received total products: " + allProducts.Count);
			_products = allProducts;
		};

		StoreKitManager.purchaseSuccessfulEvent += purchaseSuccessfulEvent;
	}

	void purchaseSuccessfulEvent (StoreKitTransaction transaction)
	{
		Debug.Log ("purchaseSuccessfulEvent: " + transaction);

		for (int i = 0 , max = myParse.mallList.Count; i < max; i++)
		{
			if (myParse.mallList [i].ToString () == transaction.productIdentifier)
			{
				int money = int.Parse (myParse.mallMoneyList [i].ToString ());
				myParse.AddMallMoney (Mathf.Abs (money));
				return;
			}
		}
	}

#if UNITY_IOS
	private List<StoreKitProduct> _products;
	void OnEnable ()
	{
		StoreKitBinding.canMakePayments();
	}	
#endif

	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.A))
		{

			MyParse.AddMallMoney ("co.tamatem.tinygemhunter.money30_5", 5);
			Debug.Log (myParse.mallList [0].ToString ());
		}

		if (testNetwork.isConnect && !testNetwork.isReConnect)
		{
			if (myParse.isGetMallList)
			{
				myParse.isGetMallList = false;

				string[] productIdentifiers = (string[])myParse.mallList.ToArray (typeof(string));
				StoreKitBinding.requestProductData (productIdentifiers);

				loadLabel.SetActive (false);
				loadLabel.GetComponent<ChangeLanguage> ().ReLoadID ("10116");
				GameObject[] destoryFriendCheck = GameObject.FindGameObjectsWithTag ("MoneyItem");
				foreach (GameObject d in destoryFriendCheck)
				{
					Destroy (d);
				}
				localPosition = 255.0f;
				for (int i = 0; i < myParse.mallList.Count; i++)
				{
					Debug.Log ("myParse.mallList [i].ToString () ?? " + myParse.mallList [i].ToString ()) ;

					GameObject clone = NGUITools.AddChild (scrollView, MoneyItem);
					clone.transform.localPosition = new Vector3 (0, localPosition, 0);
					clone.GetComponent<SetMallMoneyData> ().SetData (myParse.mallList [i].ToString (), int.Parse (myParse.mallMoneyList [i].ToString ()), int.Parse (myParse.mallTagList [i].ToString ()), int.Parse (myParse.mallIndexList [i].ToString ()));
					clone.GetComponentInChildren<AddMallMoney> ().purchaseProduct = myParse.mallList [i].ToString ();
#if UNITY_IOS
					if (_products != null && i < _products.Count) {
						var temp = _products.Single ( x  => x.productIdentifier == myParse.mallList [i].ToString () );
						clone.GetComponentInChildren<AddMallMoney> ().ApplyStoreKitProduct (temp);
					}
#endif
					localPosition -= 100.0f;
				}
				loadLabel.SetActive (false);
			}
		}
		else
		{
			loadLabel.GetComponent<ChangeLanguage> ().ReLoadID ("10117");
		}
	}

}

